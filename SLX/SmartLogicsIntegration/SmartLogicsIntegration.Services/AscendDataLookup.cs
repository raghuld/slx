﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Services
{

    public class AscendDataLookup : IAscendDataLookup
    {

        /// <summary>
        /// Lookups the ship to identifier and standard acct identifier.
        /// </summary>
        /// <param name="currentShipToId">The current ship to identifier.</param>
        /// <param name="currentStandardAcctId">The current standard acct identifier.</param>
        /// <param name="standardAcctNo">The standard acct no.</param>
        /// <param name="shipToCode">The ship to code.</param>
        public void LookupShipToIdAndStandardAcctId(ref string currentShipToId, ref string currentStandardAcctId,
            string standardAcctNo, string shipToCode)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            var shipToIdLookupQueryString =
                "SELECT ARShipTo.ShipToID AS ShipToID, ARStandardAcct.StandardAcctID AS StandardAcctID FROM ARShipTo " +
                " JOIN ARStandardAcct ON ARStandardAcct.StandardAcctID = ARShipTo.StandardAcctID " +
                " WHERE ARShipTo.Code = @Code and ARStandardAcct.StandardAcctNo = @StandardAcctNo";

            // Lookup ShipTo MasterDataID
            using (var mySqlDataAdapter = new SqlDataAdapter())
            {
                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(shipToIdLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = shipToCode;
                        cmd.Parameters.Add("@StandardAcctNo", SqlDbType.VarChar, 50).Value = standardAcctNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                        var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                        if (myMasterDataIdDataTable.Rows.Count > 0)
                        {
                            currentShipToId = myMasterDataIdDataTable.Rows[0]["ShipToID"].ToString();
                            currentStandardAcctId = myMasterDataIdDataTable.Rows[0]["StandardAcctID"].ToString();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Lookups the inventory adjustment site.
        /// </summary>
        /// <param name="vehicleCode">The vehicle code.</param>
        /// <returns></returns>
        public string LookupInventoryAdjustmentSite(string vehicleCode)
        {
            string currentShipToId = null;

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            var shipToIdLookupQueryString =
                "SELECT Vehicle.HomeSiteID AS ShipToID FROM Vehicle " +
                " WHERE Vehicle.Code = @Code";

            using (var mySqlDataAdapter = new SqlDataAdapter())
            {
                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(shipToIdLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = vehicleCode;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                        var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                        if (myMasterDataIdDataTable.Rows.Count > 0)
                        {
                            currentShipToId = myMasterDataIdDataTable.Rows[0]["ShipToID"].ToString();
                        }
                    }
                }
            }

            return currentShipToId;
        }

        /// <summary>
        /// Lookups the inventory site identifier.
        /// </summary>
        /// <param name="inventorySiteCode">The inventory site code.</param>
        /// <returns></returns>
        public string LookupInventorySiteId(string inventorySiteCode)
        {
            string currentInventorySiteId = null;
            if (!string.IsNullOrEmpty(inventorySiteCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                // Lookup Terminal ID in inventory sites
                var terminalQueryString = "SELECT SiteID from dbo." +
                                          AscendTable.InSiteDataTableName +
                                          " WHERE Code = @Code";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    using (var terminalSqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(terminalQueryString, cn))
                        {
                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 24).Value = inventorySiteCode;
                            terminalSqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            terminalSqlDataAdapter.Fill(myDataSet, AscendTable.InSiteDataTableName);
                            var terminalDataSet = myDataSet.Tables[AscendTable.InSiteDataTableName];
                            if (terminalDataSet.Rows.Count > 0)
                            {
                                currentInventorySiteId = terminalDataSet.Rows[0]["SiteID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentInventorySiteId;
        }


        /// <summary>
        /// Lookups the supplier supply pt identifier.
        /// </summary>
        /// <param name="supplierCode">The supplier code.</param>
        /// <param name="supplyPtCode">The supply pt code.</param>
        /// <returns></returns>
        public string LookupSupplierSupplyPtId(string supplierCode, string supplyPtCode)
        {
            string currentTerminalId = null;

            if (!string.IsNullOrEmpty(supplierCode) && !string.IsNullOrEmpty(supplyPtCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var terminalQueryString =
                    "SELECT SupplierSupplyPt.SupplierSupplyPtID AS SupplierSupplyPtID FROM dbo." +
                    AscendTable.SupplierSupplyPtDataTableName +
                    " JOIN Supplier ON SupplierSupplyPt.SupplierID = Supplier.SupplierID" +
                    " JOIN SupplyPt ON SupplierSupplyPt.SupplyPtID = SupplyPt.SupplyPtID" +
                    " WHERE Supplier.Code = @SupplierID and SupplyPt.Code = @SupplyPtID";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    using (var terminalSqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(terminalQueryString, cn))
                        {
                            cmd.Parameters.Add("@SupplierID", SqlDbType.VarChar, 24).Value = supplierCode;
                            cmd.Parameters.Add("@SupplyPtID", SqlDbType.VarChar, 24).Value = supplyPtCode;
                            terminalSqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            terminalSqlDataAdapter.Fill(myDataSet, AscendTable.SupplierSupplyPtDataTableName);
                            var terminalDataSet =
                                myDataSet.Tables[AscendTable.SupplierSupplyPtDataTableName];
                            if (terminalDataSet.Rows.Count > 0)
                            {
                                currentTerminalId = terminalDataSet.Rows[0]["SupplierSupplyPtID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentTerminalId;
        }

        /// <summary>
        /// Lookups the vehicle identifier.
        /// </summary>
        /// <param name="vehicleCode">The vehicle code.</param>
        /// <returns></returns>
        public string LookupVehicleId(string vehicleCode)
        {
            string currentVehicleId = null;

            if (vehicleCode != null)
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var vehicleIdLookupQueryString =
                    "SELECT VehicleID from Vehicle where Code = @Code";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    // Lookup Vehicle MasterDataID
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(vehicleIdLookupQueryString, cn))
                        {
                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = vehicleCode;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                            var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                            if (myMasterDataIdDataTable.Rows.Count > 0)
                            {
                                currentVehicleId = myMasterDataIdDataTable.Rows[0]["VehicleID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentVehicleId;
        }

        /// <summary>
        /// Lookups the sales alias identifier.
        /// </summary>
        /// <param name="salesAliasCode">The sales alias code.</param>
        /// <returns></returns>
        public string LookupSalesAliasId(string salesAliasCode)
        {
            string currentSalesAliasId = null;

            if (!string.IsNullOrEmpty(salesAliasCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var productIdLookupQueryString =
                    "SELECT SalesAlias.SalesAliasID AS ProductID FROM dbo." +
                    AscendTable.SalesAliasTableName +
                    " WHERE Code = @Code";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    // Look up Product MasterDataID
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(productIdLookupQueryString, cn))
                        {
                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = salesAliasCode;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                            var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                            if (myMasterDataIdDataTable.Rows.Count > 0)
                            {
                                currentSalesAliasId = myMasterDataIdDataTable.Rows[0]["ProductID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentSalesAliasId;
        }

        /// <summary>
        /// Lookups the purch alias identifier.
        /// </summary>
        /// <param name="purchAliasCode">The purch alias code.</param>
        /// <returns></returns>
        public string LookupPurchAliasId(string purchAliasCode)
        {
            string currentPurchAliasId = null;

            if (!string.IsNullOrEmpty(purchAliasCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var productIdLookupQueryString =
                    "SELECT PurchAlias.PurchAliasID AS ProductID FROM dbo." +
                    AscendTable.PurchAliasTableName +
                    " WHERE Code = @Code";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    // Look up Product MasterDataID
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(productIdLookupQueryString, cn))
                        {
                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = purchAliasCode;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                            var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                            if (myMasterDataIdDataTable.Rows.Count > 0)
                            {
                                currentPurchAliasId = myMasterDataIdDataTable.Rows[0]["ProductID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentPurchAliasId;
        }

        /// <summary>
        /// Lookups the billing item identifier.
        /// </summary>
        /// <param name="billingItemCode">The billing item code.</param>
        /// <returns></returns>
        public string LookupBillingItemId(string billingItemCode)
        {
            string currentBillingItemId = null;

            if (!string.IsNullOrEmpty(billingItemCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var productIdLookupQueryString =
                    "SELECT BillingItem.BillingItemID AS ProductID FROM dbo." +
                    AscendTable.BillingItemDataTableName +
                    " WHERE Code = @Code";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    // Look up Product MasterDataID
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(productIdLookupQueryString, cn))
                        {
                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = billingItemCode;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, "MasterDataID");
                            var myMasterDataIdDataTable = myDataSet.Tables["MasterDataID"];
                            if (myMasterDataIdDataTable.Rows.Count > 0)
                            {
                                currentBillingItemId = myMasterDataIdDataTable.Rows[0]["ProductID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentBillingItemId;
        }

        /// <summary>
        /// Lookups the tank identifier.
        /// </summary>
        /// <param name="tankCode">The tank code.</param>
        /// <returns></returns>
        public string LookupTankId(string tankCode)
        {
            string currentTankId = null;

            if (!string.IsNullOrEmpty(tankCode))
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                string connectionString;
                if (connection != null)
                {
                    connectionString = connection.ConnectionString;
                }
                else
                {
                    connectionString = "context connection=true";
                }

                var tankQueryString =
                    "SELECT TankID FROM dbo." +
                    AscendTable.ARShipToTankDataTableName +
                    " WHERE Code = @TankCode";

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    using (var tankSqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(tankQueryString, cn))
                        {
                            cmd.Parameters.Add("@TankCode", SqlDbType.VarChar, 24).Value = tankCode;
                            tankSqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            tankSqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                            var tankDataSet = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                            if (tankDataSet.Rows.Count > 0)
                            {
                                currentTankId = tankDataSet.Rows[0]["TankID"].ToString();
                            }
                        }
                    }
                }
            }

            return currentTankId;
        }

        /// <summary>
        /// Lookups the state of the ship to.
        /// </summary>
        /// <param name="shipToId">The ship to identifier.</param>
        /// <returns></returns>
        public string LookupShipToState(string shipToId)
        {
            string stateCode = null;

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var queryString =
                    "SELECT StateProv.Code AS StateCode FROM ARShipTo " +
                    " JOIN ARShipToAddress ON ARShipToAddress.ShipToId = ARShipTo.ShipToID " +
                    " JOIN StateProv ON StateProv.ID = ARShipToAddress.StateProvID " +
                    " WHERE ARShipTo.ShipToID = @ShipToId AND ARShipToAddress.Type = 'P'";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@ShipToId", SqlDbType.BigInt).Value = shipToId;
                        sqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        sqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        var dataSet = myDataSet.Tables[AscendTable.ARShipToDataTableName];
                        if (dataSet.Rows.Count > 0)
                        {
                            stateCode = dataSet.Rows[0]["StateCode"].ToString();
                        }
                    }
                }
            }

            return stateCode;
        }

        /// <summary>
        /// Lookups the driver Id in ascend to confirm it is real
        /// </summary>
        /// <param name="driverId">The driver identifier.</param>
        /// <returns>True if the driver ID is valid otherwise false</returns>
        public bool IsValidDriverId(string driverId)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var queryString =
                    "SELECT DriverId FROM Drivers " +
                    " WHERE DriverId = @DriverId";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@DriverId", SqlDbType.BigInt).Value = driverId;
                        sqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        sqlDataAdapter.Fill(myDataSet, AscendTable.DriversDataTableName);
                        var dataSet = myDataSet.Tables[AscendTable.DriversDataTableName];
                        if (dataSet.Rows.Count > 0)
                        {
                            return true;
                        }
                    }
                }
            }            

            return false;
        }

        public bool IsValidProductAtSupplierSupplyPt(string productId, string supplierSupplyPtId)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var queryString =
                    "SELECT SupplierSupplyPtID FROM PurchRack " +
                    " WHERE SupplierSupplyPtID = @SupplierSupplyPtID and PurchAliasID = @PurchAliasID";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@SupplierSupplyPtID", SqlDbType.BigInt).Value = supplierSupplyPtId;
                        cmd.Parameters.Add("@PurchAliasID", SqlDbType.BigInt).Value = productId;
                        sqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        sqlDataAdapter.Fill(myDataSet, AscendTable.PurchRackDataTableName);
                        var dataSet = myDataSet.Tables[AscendTable.PurchRackDataTableName];
                        if (dataSet.Rows.Count > 0)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
