﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel;
using Newtonsoft.Json;
using SmartLogicsIntegration.Domain;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services.Interfaces;
using SmartLogixIntegration.Services.SalesOrderService;
using SalesOrderType = SmartLogixIntegration.Services.SalesOrderService.SalesOrderType;
//TWFF Test
namespace SmartLogixIntegration.Services
{
    public class SLXOrderManager : ISLXOrderManager
    {
        #region Fields

        /// <summary>
        /// The entities
        /// </summary>
        private readonly SmartLogicsIntegrationEntities _entities = new SmartLogicsIntegrationEntities();

        private ISLXConfigurationManager SLXConfigurationManager { get; set; }
        private IAscendDataLookup AscendDataLookup { get; set; }
        private ISLXMasterDataPusher SLXMasterDataPusher { get; set; }

        #endregion

        #region Constructor

        public SLXOrderManager(ISLXConfigurationManager slxConfigurationManager, IAscendDataLookup ascendDataLookup, ISLXMasterDataPusher slxMasterDataPusher)
        {
            SLXConfigurationManager = slxConfigurationManager;
            AscendDataLookup = ascendDataLookup;
            SLXMasterDataPusher = slxMasterDataPusher;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Updates the order status.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse UpdateOrderStatus(UpdateOrderStatusModel model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            var connectionString = connection.ConnectionString;

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                try
                {
                    // Update the status where the order number is orderno
                    string updateStatusQuery = "UPDATE OrderHdr Set OrderStatusID = (SELECT TOP 1 OrderStatusID FROM OrderOEStatus where Code = @Status) WHERE OrderNo = @OrderNo";

                    using (var cmd = new SqlCommand(updateStatusQuery, cn))
                    {
                        cmd.Parameters.Add("@Status", SqlDbType.VarChar, 24).Value = model.Status;
                        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 24).Value = model.OrderNo;

                        cmd.ExecuteNonQuery();
                    }

                    Logger.WriteToSLXLog(LookupSysTrxNo(model.OrderNo), model.OrderNo, "UpdateOrderStatus", 1, null,
                    JsonConvert.SerializeObject(model.OrderNo) + JsonConvert.SerializeObject(model.Status), null, false);

                    return new SmartLogixResponse { ResponseMessage = "Success", ResponseStatus = 1, SLXException = null };
                }
                catch(Exception ex)
                {
                    Logger.WriteToSLXLog(LookupSysTrxNo(model.OrderNo), model.OrderNo, "UpdateOrderStatus", 2, null,
                    JsonConvert.SerializeObject(model.OrderNo) + JsonConvert.SerializeObject(model.Status), ex, true);

                    throw;
                }
            }
        }

        /// <summary>
        /// News the sales order.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Result received from SLX is null.</exception>
        public SmartLogixResponse NewSalesOrder(SalesOrderModel model)
        {
            long sysTrxNo;
            // Send response to the SLX New Sales Order API
            string currentShipToId = null;
            string currentStandardAcctId = null;

            // Validate the sales order before we do anything else.  If it is invalid in any way we return the order to SLX
            // with the reasons why it is invalid
            // TODO: Add this code back after the order validator has been fully tested
            //var validationResponse = ValidateSalesOrder(model);

            //if (validationResponse.ResponseStatus != 1)
            //{
            //    return validationResponse;
            //}

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            var connectionString = connection.ConnectionString;

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get the next sales order unique identifier number to use for this sales order
                var getNextSalesOrderNumberQuery = "dbo.GetNextInventoryCtr";

                using (var cmd = new SqlCommand(getNextSalesOrderNumberQuery, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    var outputIdParam = new SqlParameter("@IDNumber", SqlDbType.BigInt)
                    {
                        Direction = ParameterDirection.Output
                    };

                    cmd.Parameters.AddWithValue("@CounterID", "SysTrxNo");
                    cmd.Parameters.Add(outputIdParam);

                    cmd.ExecuteNonQuery();

                    // Set return values
                    sysTrxNo = Convert.ToInt64(cmd.Parameters["@IDNumber"].Value);
                }
            }

            try
            {
                SmartLogixResponse result;
                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    if (model.IsInventoryAdjustment)
                    {
                        currentShipToId = AscendDataLookup.LookupInventoryAdjustmentSite(model.VehicleId);
                    }
                    else if (model.ShipToId != null)
                    {
                        // Lookup real shiptoID
                        AscendDataLookup.LookupShipToIdAndStandardAcctId(ref currentShipToId, ref currentStandardAcctId, model.StandardAcctId, model.ShipToId);
                    }

                    var currentVehicleId = AscendDataLookup.LookupVehicleId(model.VehicleId);

                    var counter = 1;
                    // Loop through all the products passed in through the new sales order model
                    foreach (var product in model.ProductList)
                    {
                        string currentTerminalId = null;

                        // SLX passes null for net quantity if they do not have it
                        if (product.NetQuantity == null)
                        {
                            product.NetQuantity = product.Quantity;
                        }

                        // If the product is in the PurchAlias table it is invalid
                        var currentProductId = AscendDataLookup.LookupPurchAliasId(product.ProductId);

                        if (currentProductId != null)
                        {
                            // Need to lookup the sales alias to ensure that this same code is not used there.
                            currentProductId = AscendDataLookup.LookupSalesAliasId(product.ProductId);
                            if (currentProductId == null)
                            {
                                result = new SmartLogixResponse
                                {
                                    ResponseMessage = "Failure - Invalid Product - Purchase products cannot be used as sales products.",
                                    ResponseStatus = 0
                                };

                                return result;
                            }
                        }

                        if (product.ProductId != null)
                        {
                            currentProductId = AscendDataLookup.LookupSalesAliasId(product.ProductId);

                            if (string.IsNullOrEmpty(currentProductId))
                            {
                                currentProductId = AscendDataLookup.LookupBillingItemId(product.ProductId);
                            }

                            if (currentProductId == null)
                            {
                                result = new SmartLogixResponse
                                {
                                    ResponseMessage = "Failure - Invalid Product",
                                    ResponseStatus = 0
                                };

                                return result;
                            }
                        }

                        if (!string.IsNullOrEmpty(product.SupplyPtId) && !string.IsNullOrEmpty(product.SupplierId))
                        {
                            currentTerminalId = AscendDataLookup.LookupSupplierSupplyPtId(product.SupplierId, product.SupplyPtId);
                        }

                        // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both
                        if (string.IsNullOrEmpty(currentTerminalId))
                        {
                            if (!string.IsNullOrEmpty(product.SupplyPtId))
                            {
                                currentTerminalId = AscendDataLookup.LookupInventorySiteId(product.SupplyPtId);
                            }

                            if (string.IsNullOrEmpty(currentTerminalId) &&
                                !string.IsNullOrEmpty(product.InventorySiteId))
                            {
                                currentTerminalId = AscendDataLookup.LookupInventorySiteId(product.InventorySiteId);
                            }
                        }

                        var currentTankId = AscendDataLookup.LookupTankId(product.AssetId);             

                        // Insert the products passed to NewSalesorder into the SmartLogixProduct table so that they can be used in the CreateSalesOrder stored proc
                        InsertSmartLogixProduct(sysTrxNo, currentTerminalId, currentProductId, product.ProductId,
                            product.Quantity, Convert.ToDouble(product.NetQuantity), product.Date, product.Price,
                            counter, currentTankId, 0, null, null);

                        counter++;
                    }

                    // Execute a stored proc to create all the things we need for a new sales order
                    var storedProcQueryString =
                        "SmartLogix_CreateSalesOrder";

                    // create command
                    using (var cmd = new SqlCommand(storedProcQueryString, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.Parameters.Add("@CompanyId", SqlDbType.VarChar, 8).Value = "01";
                        if (string.IsNullOrEmpty(currentStandardAcctId))
                        {
                            cmd.Parameters.Add("@StandardAcctId", SqlDbType.Int).Value =
                                Convert.ToInt32(currentStandardAcctId);
                        }
                        else
                        {
                            cmd.Parameters.Add("@StandardAcctId", SqlDbType.Int).Value = DBNull.Value;
                        }
                        if (string.IsNullOrEmpty(model.DriverId))
                        {
                            cmd.Parameters.Add("@DriverId", SqlDbType.Int).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@DriverId", SqlDbType.Int).Value = Convert.ToInt32(model.DriverId);
                        }
                        if (string.IsNullOrEmpty(currentShipToId))
                        {
                            cmd.Parameters.Add("@ToSiteId", SqlDbType.Int).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@ToSiteId", SqlDbType.Int).Value = Convert.ToInt32(currentShipToId);
                        }
                        if (string.IsNullOrEmpty(currentVehicleId))
                        {
                            cmd.Parameters.Add("@VehicleId", SqlDbType.Int).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@VehicleId", SqlDbType.Int).Value = Convert.ToInt32(currentVehicleId);
                        }
                        if (model.VehicleType == null)
                        {
                            cmd.Parameters.Add("@VehicleType", SqlDbType.Int).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@VehicleType", SqlDbType.Int).Value = (int?) model.VehicleType;
                        }
                        cmd.Parameters.Add("@OrderDtTm", SqlDbType.DateTime).Value = DateTime.Now;
                        if (model.UserId == null)
                        {
                            cmd.Parameters.Add("@InUserId", SqlDbType.VarChar, 32).Value =
                                SmartLogixConstants.SmartLogixWebServiceName;
                        }
                        else
                        {
                            cmd.Parameters.Add("@InUserId", SqlDbType.VarChar, 32).Value = model.UserId;
                        }
                        if (model.OrderDeliveryDate == null)
                        {
                            cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.DateTime).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.DateTime).Value = model.OrderDeliveryDate;
                        }

                        if (model.PONumber != null)
                        {
                            cmd.Parameters.AddWithValue("@PONumber", model.PONumber);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@PONumber", DBNull.Value);
                        }

                        if (model.Status == null)
                        {
                            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 24).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 24).Value = model.Status;
                        }

                        var outputIdParam = new SqlParameter("@OutMessage", SqlDbType.VarChar, 1000)
                        {
                            Direction = ParameterDirection.Output
                        };
                        cmd.Parameters.Add(outputIdParam);

                        cmd.ExecuteNonQuery();
                    }

                    // Lookup the PONumber and Hold status from OrderHdr table
                    GetOrderHdrInfo(sysTrxNo, model);

                    // Lookup the CODNotes and CODFlag for PaymentTerms
                    LookupPaymentTerms(Convert.ToInt64(currentShipToId), model);

                    // The Order was successfully created in ascend so we store it so we know the order came from SLX
                    var smartLogixOrderQueryString =
                        "INSERT INTO SmartLogixOrders VALUES (@SysTrxNo)";

                    // create command
                    using (var cmd = new SqlCommand(smartLogixOrderQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.ExecuteNonQuery();
                    }

                    // Set Ascend number for backoffice number
                    model.AscendSalesOrderNumber = LookupSalesOrderNo(sysTrxNo);

                    // Get each of the values from the SmartLogixProduct table that we care about for this sales order
                    var smartLogixProductQueryString =
                        "SELECT SysTrxNo, ItemNumber, TerminalID, ProductID, ProductCode, Quantity, OrderDate, Price from SmartLogixProduct WHERE SysTrxNo = @SysTrxNo";

                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        DataSet smartLogixProductDataSet;
                        // create command
                        using (var cmd = new SqlCommand(smartLogixProductQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            mySqlDataAdapter.SelectCommand = cmd;
                            smartLogixProductDataSet = new DataSet();
                            mySqlDataAdapter.Fill(smartLogixProductDataSet, "SmartLogixProduct");
                        }

                        // Foreach of the items we pulled in the above query of the SmartLogixProduct table run logic to collect tax info and pricing info
                        foreach (DataRow product in smartLogixProductDataSet.Tables["SmartLogixProduct"].Rows)
                        {
                            CalculateTaxAndFreight(sysTrxNo, (int)product["ItemNumber"], product["ProductCode"].ToString(), model);
                        }
                    }
                }

                // Call the SLX Integration web service and push data to it from Ascend for NewSalesOrder

                var configuration = SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixWebServiceName);

                using (var client = new WebClient())
                {                    
                    string uri = configuration.Url;
                  
                    if (!String.IsNullOrEmpty(uri))
                    {
                        uri += "SmartLogixApi/PushNewSalesOrder";

                        client.Headers.Clear();
                        client.Headers.Add("content-type", "application/json");
                        client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                        var serialisedData = JsonConvert.SerializeObject(model);

                        // HTTP POST
                        var response = client.UploadString(uri, serialisedData);

                        result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                        if (result == null || result.ResponseStatus != 1)
                        {
                            // Something went wrong
                            if (result == null)
                            {
                                throw new ApplicationException("Result received from SLX is null.");
                            }
                            throw result.SLXException;
                        }
                    }
                    // SmartLogix is not in place so the order was successfully added to ascend and NSO is technically successful
                    // TODO: Pass back order information to places other than Ascend? Maybe demandstream?
                    else
                    {
                        result = new SmartLogixResponse();
                        result.ResponseMessage = "Success";
                        result.ResponseStatus = 1;
                    }
                }                

                // Ensure that the SLX new sales order API returns success
                return result;
            }
            catch (Exception ex)
            {
                var salesOrderNo = LookupSalesOrderNo(sysTrxNo) ?? sysTrxNo.ToString();

                Logger.WriteToSLXLog(sysTrxNo.ToString(), salesOrderNo, "NewSalesOrder", 2, null,
                    JsonConvert.SerializeObject(model), ex, true);

                throw;
            }
            finally
            {
                // Remove products from the SmartLogixProducts temporary storage table
                _entities.SmartLogixProducts.RemoveRange(_entities.SmartLogixProducts.Where(p => p.SysTrxNo == sysTrxNo));
                _entities.SaveChanges();
            }
        }

        /// <summary>
        /// Modifieds the sales order. The point of this method is to allow SLX to pass me a sales order model with potential
        /// updates and this method will go in and make the updates.
        /// It will then call the PushModifiedSales order to push the new pricing and freight info back to SLX
        /// </summary>
        /// <param name="model">The sales order model.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Result received from SLX is null.</exception>
        /// <exception cref="System.ApplicationException">Result received from SLX is null.</exception>
        public SmartLogixResponse ModifiedSalesOrder(SalesOrderModel model)
        {
            // I have to do this sometimes because SLX decides not to pass me the ascend sales order number in the correct field so its
            // easier to do this then to argue with them
            if (model.AscendSalesOrderNumber == null)
            {
                model.AscendSalesOrderNumber = model.SLXSalesOrderNumber;
            }

            // Validate the sales order before we do anything else.  If it is invalid in any way we return the order to SLX
            // with the reasons why it is invalid
            // TODO: Add this code back after the order validator has been fully tested
            //var validationResponse = ValidateSalesOrder(model);

            //if (validationResponse.ResponseStatus != 1)
            //{
            //    return validationResponse;
            //}

            // Lookup the systrxno
            long sysTrxNo = Convert.ToInt64(LookupSysTrxNo(model.AscendSalesOrderNumber));

            try
            {
                SmartLogixResponse result;
                string currentShipToId = null;
                string currentStandardAcctId = null;

                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                var connectionString = connection.ConnectionString;

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    // Make sure the order has been added to ascend from SLX
                    var smartLogixOrdersQueryString =
                        "SELECT SysTrxNo from SmartLogixOrders where SysTrxNo = @SysTrxNo";

                    var isValidSmartLogixOrder = false;
                    // Lookup Vehicle MasterDataID
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(smartLogixOrdersQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var smartLogixOrdersDataSet = new DataSet();
                            mySqlDataAdapter.Fill(smartLogixOrdersDataSet, "SmartLogixOrders");
                            if (smartLogixOrdersDataSet.Tables["SmartLogixOrders"].Rows.Count > 0)
                            {
                                isValidSmartLogixOrder = true;
                            }
                        }
                    }

                    if (isValidSmartLogixOrder)
                    {
                        // Get all orderitems
                        var fullOrderItemQueryString =
                            "SELECT * from OrderItem where SysTrxNo = @SysTrxNo";

                        // Lookup Vehicle MasterDataID
                        using (var mySqlDataAdapter = new SqlDataAdapter())
                        {
                            using (var cmd = new SqlCommand(fullOrderItemQueryString, cn))
                            {
                                cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                mySqlDataAdapter.SelectCommand = cmd;
                                var fullOrderItemDataSet = new DataSet();
                                mySqlDataAdapter.Fill(fullOrderItemDataSet, "OrderItem");
                            }
                        }

                        if (model.IsInventoryAdjustment)
                        {
                            currentShipToId = AscendDataLookup.LookupInventoryAdjustmentSite(model.VehicleId);
                        }
                        else if (model.ShipToId != null)
                        {
                            // Lookup real shiptoID
                            AscendDataLookup.LookupShipToIdAndStandardAcctId(ref currentShipToId, ref currentStandardAcctId, model.StandardAcctId, model.ShipToId);
                        }

                        var currentVehicleId = AscendDataLookup.LookupVehicleId(model.VehicleId);

                        // Lookup the PONumber and Hold status from OrderHdr table
                        GetOrderHdrInfo(sysTrxNo, model);

                        // Lookup the CODNotes and CODFlag for PaymentTerms
                        LookupPaymentTerms(Convert.ToInt64(currentShipToId), model);

                        var counter = 1;
                        // Loop through all the products passed in through the new sales order model
                        foreach (ProductModel product in model.ProductList)
                        {
                            string currentTerminalId = null;

                            // SLX passes null for net quantity if they do not have it
                            if (product.NetQuantity == null)
                            {
                                product.NetQuantity = product.Quantity;
                            }

                            // If the product is in the PurchAlias table it is invalid
                            var currentProductId = AscendDataLookup.LookupPurchAliasId(product.ProductId);

                            if (currentProductId != null)
                            {
                                // Need to lookup the sales alias to ensure that this same code is not used there.
                                currentProductId = AscendDataLookup.LookupSalesAliasId(product.ProductId);
                                if (currentProductId == null)
                                {
                                    result = new SmartLogixResponse
                                    {
                                        ResponseMessage = "Failure - Invalid Product - Purchase products cannot be used as sales products.",
                                        ResponseStatus = 0
                                    };

                                    return result;
                                }
                            }

                            currentProductId = AscendDataLookup.LookupSalesAliasId(product.ProductId);

                            if (string.IsNullOrEmpty(currentProductId))
                            {
                                currentProductId = AscendDataLookup.LookupBillingItemId(product.ProductId);
                            }

                            if (currentProductId == null)
                            {
                                result = new SmartLogixResponse
                                {
                                    ResponseMessage = "Failure - Invalid Product",
                                    ResponseStatus = 0
                                };

                                return result;
                            }

                            if (!string.IsNullOrEmpty(product.SupplierId) && !string.IsNullOrEmpty(product.SupplyPtId))
                            {
                                currentTerminalId = AscendDataLookup.LookupSupplierSupplyPtId(product.SupplierId, product.SupplyPtId);
                            }

                            // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both
                            if (string.IsNullOrEmpty(currentTerminalId))
                            {
                                if (!string.IsNullOrEmpty(product.SupplyPtId))
                                {
                                    currentTerminalId = AscendDataLookup.LookupInventorySiteId(product.SupplyPtId);
                                }

                                if (string.IsNullOrEmpty(currentTerminalId) &&
                                    !string.IsNullOrEmpty(product.InventorySiteId))
                                {
                                    currentTerminalId = AscendDataLookup.LookupInventorySiteId(product.InventorySiteId);
                                }
                            }

                            // if the terminal ID is still null then it means SLX didnt pass it to me, most likely they will not so this code will be executed
                            if (currentTerminalId == null && product.BoughtAsProducts != null)
                            {
                                foreach (var boughtAsProduct in product.BoughtAsProducts)
                                {
                                    if (currentTerminalId == null)
                                    {
                                        // Lookup boughtasproduct terminal
                                        if (!string.IsNullOrEmpty(boughtAsProduct.SupplierId) &&
                                            !string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                        {
                                            currentTerminalId = AscendDataLookup.LookupSupplierSupplyPtId(boughtAsProduct.SupplierId,
                                                boughtAsProduct.SupplyPtId);
                                        }

                                        // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both
                                        if (string.IsNullOrEmpty(currentTerminalId))
                                        {
                                            if (!string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                            {
                                                currentTerminalId = AscendDataLookup.LookupInventorySiteId(boughtAsProduct.SupplyPtId);
                                            }

                                            if (string.IsNullOrEmpty(currentTerminalId) &&
                                                !string.IsNullOrEmpty(boughtAsProduct.InventorySiteId))
                                            {
                                                currentTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.InventorySiteId);
                                            }
                                            else
                                            {
                                                product.InventorySiteId = boughtAsProduct.InventorySiteId;
                                            }
                                        }
                                        else
                                        {
                                            product.SupplyPtId = boughtAsProduct.SupplyPtId;
                                            product.SupplierId = boughtAsProduct.SupplierId;
                                        }
                                    }
                                }
                            }

                            var currentTankId = AscendDataLookup.LookupTankId(product.AssetId);

                            // Update the products passed to ModifiedSalesorder into the SmartLogixProduct table so that they can be used in the ModifySalesOrder stored proc
                            InsertSmartLogixProduct(sysTrxNo, currentTerminalId, currentProductId, product.ProductId,
                                product.Quantity, Convert.ToDouble(product.NetQuantity), product.Date, product.Price,
                                counter, currentTankId, 0, null, null);

                            var componentCounter = 1;
                            if (product.BoughtAsProducts != null)
                            {
                                foreach (var boughtAsProduct in product.BoughtAsProducts)
                                {
                                    if (!string.IsNullOrEmpty(boughtAsProduct.ProductId))
                                    {
                                        // SLX passes null for net quantity if they do not have it
                                        if (boughtAsProduct.NetQuantity == null)
                                        {
                                            boughtAsProduct.NetQuantity = boughtAsProduct.Quantity;
                                        }

                                        // Lookup boughtasproduct terminal

                                        string currentBoughtAsTerminalId = null;

                                        if (!string.IsNullOrEmpty(boughtAsProduct.SupplierId) &&
                                            !string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                        {
                                            currentBoughtAsTerminalId =
                                                AscendDataLookup.LookupSupplierSupplyPtId(boughtAsProduct.SupplierId,
                                                    boughtAsProduct.SupplyPtId);
                                        }

                                        // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both
                                        if (string.IsNullOrEmpty(currentBoughtAsTerminalId))
                                        {
                                            if (!string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                            {
                                                currentBoughtAsTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.SupplyPtId);
                                            }

                                            if (string.IsNullOrEmpty(currentBoughtAsTerminalId) &&
                                                !string.IsNullOrEmpty(boughtAsProduct.InventorySiteId))
                                            {
                                                currentBoughtAsTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.InventorySiteId);
                                            }
                                        }

                                        var currentBoughtAsProductId = AscendDataLookup.LookupPurchAliasId(boughtAsProduct.ProductId);

                                        InsertSmartLogixProduct(sysTrxNo, currentBoughtAsTerminalId,
                                            currentBoughtAsProductId,
                                            boughtAsProduct.ProductId,
                                            boughtAsProduct.Quantity, Convert.ToDouble(boughtAsProduct.NetQuantity),
                                            product.Date, null, counter, currentTankId, componentCounter,
                                            null, null);

                                        componentCounter++;
                                    }
                                }
                            }
                            counter++;
                        }


                        // Execute a stored proc to create all the things we need for a new sales order
                        var storedProcQueryString =
                            "SmartLogix_ModifySalesOrder";

                        // create command
                        using (var cmd = new SqlCommand(storedProcQueryString, cn))
                        {
                            cmd.CommandTimeout = 0;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            cmd.Parameters.Add("@CompanyId", SqlDbType.VarChar, 8).Value = "01";
                            if (string.IsNullOrEmpty(currentStandardAcctId))
                            {
                                cmd.Parameters.Add("@StandardAcctId", SqlDbType.Int).Value =
                                    Convert.ToInt32(currentStandardAcctId);
                            }
                            else
                            {
                                cmd.Parameters.Add("@StandardAcctId", SqlDbType.Int).Value = DBNull.Value;
                            }
                            if (string.IsNullOrEmpty(model.DriverId))
                            {
                                cmd.Parameters.Add("@DriverId", SqlDbType.Int).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@DriverId", SqlDbType.Int).Value = Convert.ToInt32(model.DriverId);
                            }
                            if (string.IsNullOrEmpty(currentShipToId))
                            {
                                cmd.Parameters.Add("@ToSiteId", SqlDbType.Int).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@ToSiteId", SqlDbType.Int).Value = Convert.ToInt32(currentShipToId);
                            }
                            if (string.IsNullOrEmpty(currentVehicleId))
                            {
                                cmd.Parameters.Add("@VehicleId", SqlDbType.Int).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@VehicleId", SqlDbType.Int).Value = Convert.ToInt32(currentVehicleId);
                            }
                            if (model.VehicleType == null)
                            {
                                cmd.Parameters.Add("@VehicleType", SqlDbType.Int).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@VehicleType", SqlDbType.Int).Value = (int?) model.VehicleType;
                            }
                            cmd.Parameters.Add("@OrderDtTm", SqlDbType.DateTime).Value = DateTime.Now;
                            if (model.UserId == null)
                            {
                                cmd.Parameters.Add("@InUserId", SqlDbType.VarChar, 32).Value =
                                    SmartLogixConstants.SmartLogixWebServiceName;
                            }
                            else
                            {
                                cmd.Parameters.Add("@InUserId", SqlDbType.VarChar, 32).Value = model.UserId;
                            }
                            if (model.OrderDeliveryDate == null)
                            {
                                cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.DateTime).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.DateTime).Value =
                                    model.OrderDeliveryDate;
                            }

                            if (model.PONumber != null)
                            {
                                cmd.Parameters.AddWithValue("@PONumber", model.PONumber);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@PONumber", DBNull.Value);
                            }

                            if (model.Status == null)
                            {
                                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 24).Value = DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 24).Value = model.Status;
                            }

                            var outputIdParam = new SqlParameter("@OutMessage", SqlDbType.VarChar, 1000)
                            {
                                Direction = ParameterDirection.Output
                            };
                            cmd.Parameters.Add(outputIdParam);

                            cmd.ExecuteNonQuery();
                        }

                        // Get each of the values from the SmartLogixProduct table that we care about for this sales order
                        var smartLogixProductQueryString =
                            "SELECT SysTrxNo, ItemNumber, TerminalID, ProductID, ProductCode, Quantity, OrderDate, Price from SmartLogixProduct WHERE SysTrxNo = @SysTrxNo";

                        using (var mySqlDataAdapter = new SqlDataAdapter())
                        {
                            DataSet smartLogixProductDataSet;
                            // create command
                            using (var cmd = new SqlCommand(smartLogixProductQueryString, cn))
                            {
                                cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                mySqlDataAdapter.SelectCommand = cmd;
                                smartLogixProductDataSet = new DataSet();
                                mySqlDataAdapter.Fill(smartLogixProductDataSet, "SmartLogixProduct");
                            }

                            // Foreach of the items we pulled in the above query of the SmartLogixProduct table run logic to collect tax info and pricing info
                            foreach (DataRow product in smartLogixProductDataSet.Tables["SmartLogixProduct"].Rows)
                            {
                                CalculateTaxAndFreight(sysTrxNo, (int)product["ItemNumber"], product["ProductCode"].ToString(), model);
                            }
                        }
                    }
                }

                // Call the SLX Integration web service and push data to it from Ascend for ModifiedSalesOrder

                var configuration = SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixWebServiceName);

                using (var client = new WebClient())
                {
                    string uri = configuration.Url;

                    if (!String.IsNullOrEmpty(uri))
                    {
                        uri += "SmartLogixApi/PushModifiedSalesOrder";

                        client.Headers.Clear();
                        client.Headers.Add("content-type", "application/json");
                        client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                        var serialisedData = JsonConvert.SerializeObject(model);

                        // HTTP POST
                        var response = client.UploadString(uri, serialisedData);

                        result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                        if (result == null || result.ResponseStatus != 1)
                        {
                            // Something went wrong
                            if (result == null)
                            {
                                throw new ApplicationException("Result received from SLX is null.");
                            }
                            throw result.SLXException;
                        }
                    }
                    // SmartLogix is not in place so the order was successfully added to ascend and MSO is technically successful
                    // TODO: Pass back order information to places other than Ascend? Maybe demandstream?
                    else
                    {
                        result = new SmartLogixResponse();
                        result.ResponseMessage = "Success";
                        result.ResponseStatus = 1;
                    }
                }

                // Ensure that the SLX new sales order API returns success
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteToSLXLog(sysTrxNo.ToString(), LookupSalesOrderNo(sysTrxNo), "ModifiedSalesOrder", 2, null,
                    JsonConvert.SerializeObject(model), ex, true);

                throw;
            }
            finally
            {
                // Remove products from the SmartLogixProducts temporary storage table
                _entities.SmartLogixProducts.RemoveRange(_entities.SmartLogixProducts.Where(p => p.SysTrxNo == sysTrxNo));
                _entities.SaveChanges();
            }
        }

        /// <summary>
        /// Pushes the modified sales order.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse PushModifiedSalesOrder(SalesOrderModel model)
        {
            // I have to do this sometimes because SLX decides not to pass me the ascend sales order number in the correct field so its
            // easier to do this then to argue with them
            if (model.AscendSalesOrderNumber == null)
            {
                model.AscendSalesOrderNumber = model.SLXSalesOrderNumber;
            }

            // Lookup the systrxno
            long sysTrxNo = Convert.ToInt64(LookupSysTrxNo(model.AscendSalesOrderNumber));

            try
            {
                var response = new SmartLogixResponse();

                var configuration = SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                var currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "SalesOrderService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                var binding = SLXConfigurationManager.SetBinding();

                var salesOrder = new NewSalesOrder();

                using (var client = new SalesOrderServiceClient(binding, endpointAddress))
                {
                    salesOrder.NewSalesOrderDetailList = new NewSalesOrderDetailList();

                    // Build up the SalesOrder to return to SLX
                    salesOrder.BackOfficeOrderNumber = model.AscendSalesOrderNumber;
                    salesOrder.CustomerId = model.StandardAcctId;
                    salesOrder.ShipToId = model.ShipToId;
                    salesOrder.Status = SalesOrderStatus.New;
                    salesOrder.IsCreditHold = model.IsOnHold;
                    salesOrder.NoteCOD = model.NoteCOD;
                    salesOrder.CODFlag = model.CODFlag;

                    // Add all the products to the new sales order             
                    foreach (ProductModel product in model.ProductList)
                    {
                        var billingItemId = AscendDataLookup.LookupBillingItemId(product.ProductId);

                        if (string.IsNullOrEmpty(billingItemId))
                        {
                            var salesOrderDetail = new NewSalesOrderDetail();

                            salesOrderDetail.Price = (decimal?) product.Price;
                            salesOrderDetail.RequiredQuantity = (float) product.Quantity;
                            salesOrderDetail.ProductId = product.ProductId;

                            if (!string.IsNullOrEmpty(product.SupplyPtId) ||
                                !string.IsNullOrEmpty(product.InventorySiteId))
                            {
                                salesOrderDetail.FreightList = new FreightList();

                                var currentFreight = new Freight
                                {
                                    Amount = product.Freight,
                                    ProductId = product.ProductId
                                };

                                if (!string.IsNullOrEmpty(product.SupplyPtId))
                                {
                                    currentFreight.TerminalNumber = product.SupplyPtId;
                                }
                                else if (!string.IsNullOrEmpty(product.InventorySiteId))
                                {
                                    currentFreight.TerminalNumber = product.InventorySiteId;
                                }

                                salesOrderDetail.FreightList.Add(currentFreight);
                            }

                            var taxFees = new TaxFeeItemList();

                            if (product.Tax != null)
                            {
                                // Add all the taxes to the sales order detail
                                foreach (var tax in product.Tax)
                                {
                                    if (tax.Amount > 0)
                                    {
                                        var taxFee = new TaxFeeItem
                                        {
                                            Amount = (decimal) tax.Amount,
                                            Description = tax.Description,
                                            TaxFeeItemType = (TaxFeeItemType) tax.TaxFeeTypeId,
                                            CompoundTaxFeeItemsList = new CompoundTaxFeeItemsList()
                                        };

                                        foreach (var compoundTax in tax.CompoundTaxFeeItemsList)
                                        {
                                            var compoundTaxFee = new CompoundTaxFeeItem
                                            {
                                                Amount = Convert.ToDecimal(compoundTax.Amount),
                                                Description = compoundTax.Description,
                                                TaxFeeItemType = (TaxFeeItemType) compoundTax.STaxFeeTypeId
                                            };
                                            taxFee.CompoundTaxFeeItemsList.Add(compoundTaxFee);
                                        }
                                        taxFees.Add(taxFee);
                                    }
                                }
                            }

                            salesOrderDetail.TaxFeeItemsList = taxFees;

                            salesOrder.NewSalesOrderDetailList.Add(salesOrderDetail);
                        }
                    }

                    salesOrder.SalesOrderType = SalesOrderType.Tank;

                    response.ResponseMessage = "In Progress";
                    response.ResponseStatus = 0;

                    Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "ModifiedSalesOrder",
                        response.ResponseStatus, JsonConvert.SerializeObject(salesOrder),
                        JsonConvert.SerializeObject(model), null, false);

                    string[] responseMessages;
                    string salesOrderNumber;
                    var responseDetailList = client.UpdateSalesOrder(ref currentDateTime, ref messageType, false,
                        salesOrder, null, "slx", 0, "slx",
                        out responseMessages, out salesOrderNumber);

                    // Check for web service success
                    foreach (SalesOrderService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(SalesOrderService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = "Failure";

                            Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "ModifiedSalesOrder", null,
                                null, JsonConvert.SerializeObject(model), response.SLXException, true);

                            // SLX is broken and doesnt really return me the exception to log so this is the workaround to say its their issue.
                            if (string.IsNullOrEmpty(detail.Message))
                            {
                                detail.SLXException =
                                    new ApplicationException("Error occurred after passing data to SLX");
                            }
                            else
                            {
                                detail.SLXException =
                                    new ApplicationException("Error occurred after passing data to SLX - " +
                                                             detail.Message);
                            }

                            throw detail.SLXException;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "ModifiedSalesOrder",
                    response.ResponseStatus,
                    JsonConvert.SerializeObject(salesOrder), JsonConvert.SerializeObject(model), null, false);

                return response;
            }
            catch (Exception ex)
            {
                Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "ModifiedSalesOrder", 2,
                    null, JsonConvert.SerializeObject(model), ex, true);
                throw;
            }
        }

        /// <summary>
        /// Pushes the new sales order.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse PushNewSalesOrder(SalesOrderModel model)
        {
            // I have to do this sometimes because SLX decides not to pass me the ascend sales order number in the correct field so its
            // easier to do this then to argue with them
            if (model.AscendSalesOrderNumber == null)
            {
                model.AscendSalesOrderNumber = model.SLXSalesOrderNumber;
            }

            // Lookup the systrxno
            long sysTrxNo = Convert.ToInt64(LookupSysTrxNo(model.AscendSalesOrderNumber));

            try
            {
                var response = new SmartLogixResponse();

                var configuration = SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                var currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "SalesOrderService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                var binding = SLXConfigurationManager.SetBinding();

                using (var client = new SalesOrderServiceClient(binding, endpointAddress))
                {
                    var salesOrder = new NewSalesOrder();

                    salesOrder.NewSalesOrderDetailList = new NewSalesOrderDetailList();

                    // Build up the NewSalesOrder to return to SLX
                    salesOrder.BackOfficeOrderNumber = model.AscendSalesOrderNumber;
                    salesOrder.CustomerId = model.StandardAcctId;
                    salesOrder.SalesOrderNumber = model.SLXSalesOrderNumber;
                    salesOrder.ShipToId = model.ShipToId;
                    salesOrder.CustomerPo = model.PONumber;
                    salesOrder.Status = SalesOrderStatus.New;
                    salesOrder.IsCreditHold = model.IsOnHold;
                    salesOrder.NoteCOD = model.NoteCOD;
                    salesOrder.CODFlag = model.CODFlag;

                    // Add all the products to the new sales order
                    foreach (var product in model.ProductList)
                    {
                        var salesOrderDetail = new NewSalesOrderDetail
                        {
                            Price = (decimal?) product.Price,
                            RequiredQuantity = (float) product.Quantity,
                            ProductId = product.ProductId,
                            FreightList = new FreightList()
                        };


                        var currentFreight = new Freight
                        {
                            Amount = product.Freight,
                            ProductId = product.ProductId
                        };
                        if (!string.IsNullOrEmpty(product.SupplyPtId))
                        {
                            currentFreight.TerminalNumber = product.SupplyPtId;
                        }
                        else if (!string.IsNullOrEmpty(product.InventorySiteId))
                        {
                            currentFreight.TerminalNumber = product.InventorySiteId;
                        }

                        salesOrderDetail.FreightList.Add(currentFreight);

                        var taxFees = new TaxFeeItemList();

                        if (product.Tax != null)
                        { 
                            // Add all the taxes to the sales order detail
                            foreach (var tax in product.Tax)
                            {
                                if (tax.Amount > 0)
                                {
                                    var taxFee = new TaxFeeItem
                                    {
                                        Amount = (decimal) tax.Amount,
                                        Description = tax.Description,
                                        TaxFeeItemType = (TaxFeeItemType) tax.TaxFeeTypeId,
                                        CompoundTaxFeeItemsList = new CompoundTaxFeeItemsList()
                                    };

                                    foreach (var compoundTax in tax.CompoundTaxFeeItemsList)
                                    {
                                        var compoundTaxFee = new CompoundTaxFeeItem
                                        {
                                            Amount = Convert.ToDecimal(compoundTax.Amount),
                                            Description = compoundTax.Description,
                                            TaxFeeItemType = (TaxFeeItemType) compoundTax.STaxFeeTypeId
                                        };
                                        taxFee.CompoundTaxFeeItemsList.Add(compoundTaxFee);
                                    }
                                    taxFees.Add(taxFee);
                                }
                            }
                        }

                        salesOrderDetail.TaxFeeItemsList = taxFees;

                        salesOrder.NewSalesOrderDetailList.Add(salesOrderDetail);
                    }

                    salesOrder.SalesOrderType = SalesOrderType.Tank;

                    response.ResponseMessage = "In Progress";
                    response.ResponseStatus = 0;

                    Logger.WriteToSLXLog(sysTrxNo.ToString(), salesOrder.SalesOrderNumber, "NewSalesOrder", response.ResponseStatus,
                        JsonConvert.SerializeObject(salesOrder), JsonConvert.SerializeObject(model), null, false);

                    SalesOrderService.ResponseDetail[] responseDetailList;
                    string[] responseMessages;
                    client.NewSalesOrder(ref currentDateTime, ref messageType, false, salesOrder, null, "slx", 0, "slx",
                        out responseDetailList, out responseMessages);

                    // Check for web service success
                    foreach (var detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(SalesOrderService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = "Failure";

                            Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "NewSalesOrder", 2,
                                null, JsonConvert.SerializeObject(model), response.SLXException, true);

                            // SLX is broken and doesnt really return me the exception to log so this is the workaround to say its their issue.
                            if (detail.SLXException == null || string.IsNullOrEmpty(detail.SLXException.Message))
                            {
                                detail.SLXException =
                                    new ApplicationException("Error occurred after passing data to SLX");
                            }
                            else
                            {
                                detail.SLXException =
                                    new ApplicationException("Error occurred after passing data to SLX - " +
                                                             detail.SLXException.Message);
                            }

                            throw detail.SLXException;
                        }
                    }

                    response.ResponseStatus = 1;
                    response.ResponseMessage = "Success";

                    Logger.WriteToSLXLog(sysTrxNo.ToString(), salesOrder.SalesOrderNumber, "NewSalesOrder", response.ResponseStatus,
                        JsonConvert.SerializeObject(salesOrder), JsonConvert.SerializeObject(model), null, false);
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "NewSalesOrder", 2,
                    null, JsonConvert.SerializeObject(model), ex, true);
                throw;
            }
        }

        /// <summary>
        /// Completes the sales order.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse CompletedSalesOrder(CompletedSalesOrderModel model)
        {            
            // Log the success
            var response = new SmartLogixResponse
            {
                ResponseStatus = 0,
                ResponseMessage = "In Progress"
            };

            // Lookup the SysTrxNo from the Sales Order Number
            long sysTrxNo = Convert.ToInt64(LookupSysTrxNo(model.AscendSalesOrderNumber));
            
            Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "CompletedSalesOrder",
                response.ResponseStatus,
                null, JsonConvert.SerializeObject(model), null, false);

            try
            {
                // Validate the sales order before we do anything else.  If it is invalid in any way we return the order to SLX
                // with the reasons why it is invalid
                // TODO: Add this code back after the order validator has been fully tested
                //var validationResponse = ValidateCompletedSalesOrder(model);

                //if (validationResponse.ResponseStatus != 1)
                //{
                //    return validationResponse;
                //}

                // We need to do this because SLX wont change the model since its so late in the game, this is really stupid code
                model = ReorganizeModel(model);

                string currentVehicleId = null;

                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                var connectionString = connection.ConnectionString;

                // Open connection
                using (var cn = new SqlConnection(connectionString))
                {
                    // Open the connection if its not already open
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    // Get all orderitems
                    var fullOrderItemQueryString =
                        "SELECT * from OrderItem where SysTrxNo = @SysTrxNo";

                    DataSet fullOrderItemDataSet;
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(fullOrderItemQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            mySqlDataAdapter.SelectCommand = cmd;
                            fullOrderItemDataSet = new DataSet();
                            mySqlDataAdapter.Fill(fullOrderItemDataSet, "OrderItem");
                        }
                    }

                    
                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        var counter = 1;
                        // Loop through all of the BOLs in the model
                        foreach (var bol in model.BOLS)
                        {
                            if (bol.VehicleId != null)
                            {
                                currentVehicleId = AscendDataLookup.LookupVehicleId(bol.VehicleId);
                            }

                            // Execute for each item in each BOL
                            foreach (var bolItem in bol.Products)
                            {
                                string currentTerminalId = null;
                                string currentBoughtAsProductId = null;
                                string currentTankId = null;

                                // SLX passes null for net quantity if they do not have it
                                if (bolItem.NetQuantity == null)
                                {
                                    bolItem.NetQuantity = bolItem.Quantity;
                                }

                                // Need to lookup the sales alias to ensure that the code is available there.
                                var currentProductId = AscendDataLookup.LookupSalesAliasId(bolItem.ProductId);

                                // If it was not a salesalias then it might be a billing item
                                if (string.IsNullOrEmpty(currentProductId))
                                {
                                    currentProductId = AscendDataLookup.LookupBillingItemId(bolItem.ProductId);
                                    // If we found that the product is a billing item we clear out all the bought as products for it
                                    // BillingItems do not have boughtasproducts (purchalias).  This is necessary to fix a bug on SLX side
                                    // where they pass a bought as product for billing items.
                                    if (currentProductId != null)
                                    {
                                        bolItem.BoughtAsProducts = new List<BoughtAsProduct>();
                                    }
                                }

                                // If its neither a salesalias nor a billingitem then the product is invalid
                                if (currentProductId == null)
                                {
                                    var result = new SmartLogixResponse
                                    {
                                        ResponseMessage = "Failure - Invalid Product - Purchase products cannot be used as sales products or the product does not exist.",
                                        ResponseStatus = 2
                                    };

                                    return result;
                                }

                                // Now that we know the currentProductId in the BOL was a valid product and we know its masterprodid 
                                // we check to see if it is already in the order       

                                // Lookup if the product has a row already
                                // Get the current set of order items for this sales order so we can look them up
                                var orderLookupQueryString =
                                    "SELECT SysTrxNo, SysTrxLine, MasterProdID FROM OrderItem " +
                                    " WHERE SysTrxNo = @SysTrxNo AND MasterProdID = @MasterProdID ORDER BY SysTrxLine";

                                DataSet orderItemDataSet;
                                using (var cmd = new SqlCommand(orderLookupQueryString, cn))
                                {
                                    cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                    cmd.Parameters.Add("@MasterProdID", SqlDbType.BigInt).Value = currentProductId;
                                    mySqlDataAdapter.SelectCommand = cmd;
                                    orderItemDataSet = new DataSet();
                                    mySqlDataAdapter.Fill(orderItemDataSet, "OrderItem");
                                }

                                int sysTrxLine;

                                // If true there is a line that matches the product ID
                                if (orderItemDataSet.Tables["OrderItem"].Rows.Count > 0)
                                {
                                    sysTrxLine = Convert.ToInt32(orderItemDataSet.Tables["OrderItem"].Rows[0]["SysTrxLine"]);
                                }
                                // Otherwise there is no row matching this product so the sales order is invalid, in order to add a new product to the order
                                // a new MSO would need to be run first
                                else
                                {
                                    var result = new SmartLogixResponse
                                    {
                                        ResponseMessage = "Failure - Invalid Product - The product sent does not match a product on the order.  Please use " +
                                                            "MSO to update the order with the correct product list before trying to close the order.",
                                        ResponseStatus = 2
                                    };

                                    return result;
                                }

                                // Look to see if a supplier supplypt was provided by SLX
                                if (!string.IsNullOrEmpty(bolItem.SupplierId) &&
                                    !string.IsNullOrEmpty(bolItem.SupplyPtId))
                                {
                                    currentTerminalId = AscendDataLookup.LookupSupplierSupplyPtId(bolItem.SupplierId,
                                        bolItem.SupplyPtId);
                                }

                                // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both meaning we should
                                // check to see if they put an inventory site in the supplypt field
                                if (string.IsNullOrEmpty(currentTerminalId))
                                {
                                    if (!string.IsNullOrEmpty(bolItem.SupplyPtId))
                                    {
                                        currentTerminalId = AscendDataLookup.LookupInventorySiteId(bolItem.SupplyPtId);
                                    }

                                    if (string.IsNullOrEmpty(currentTerminalId) &&
                                        !string.IsNullOrEmpty(bolItem.InventorySiteId))
                                    {
                                        currentTerminalId = AscendDataLookup.LookupInventorySiteId(bolItem.InventorySiteId);
                                    }
                                }

                                // if the terminal ID is still null then it means SLX didnt pass it to me, most likely they will not so this code will be executed
                                // and we will lookup the suppliersupplypt already provided
                                if (currentTerminalId == null && bolItem.BoughtAsProducts != null)
                                {
                                    if (currentTerminalId == null)
                                    {
                                        // Lookup the first product that has a supplier supply pt combo
                                        var boughtAsProduct =
                                            bolItem.BoughtAsProducts.FirstOrDefault(p => p.SupplierId != null && p.SupplyPtId != null);

                                        if (boughtAsProduct != null)
                                        {
                                            // Lookup boughtasproduct terminal
                                            currentTerminalId =
                                                AscendDataLookup.LookupSupplierSupplyPtId(boughtAsProduct.SupplierId,
                                                    boughtAsProduct.SupplyPtId);
                                        }
                                        else
                                        {
                                            boughtAsProduct = bolItem.BoughtAsProducts.FirstOrDefault(p => p.InventorySiteId != null);

                                            if (boughtAsProduct != null)
                                            {
                                                currentTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.InventorySiteId);
                                            }                                                                                             
                                        }                                                
                                    }                                        
                                }

                                if (!string.IsNullOrEmpty(bolItem.AssetId))
                                {
                                    var tankQueryString =
                                        "SELECT TankID FROM dbo." +
                                        AscendTable.ARShipToTankDataTableName +
                                        " WHERE Code = @TankCode";

                                    using (var tankSqlDataAdapter = new SqlDataAdapter())
                                    {
                                        using (var cmd = new SqlCommand(tankQueryString, cn))
                                        {
                                            cmd.Parameters.Add("@TankCode", SqlDbType.VarChar, 24).Value =
                                                bolItem.AssetId;
                                            tankSqlDataAdapter.SelectCommand = cmd;
                                            var myDataSet = new DataSet();
                                            tankSqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                                            DataTable tankDataSet =
                                                myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                                            if (tankDataSet.Rows.Count > 0)
                                            {
                                                currentTankId = tankDataSet.Rows[0]["TankID"].ToString();
                                            }
                                        }
                                    }
                                }

                                // Get the price for the current orderitem
                                if (fullOrderItemDataSet?.Tables["OrderItem"] != null)
                                {
                                    // See if this order item was previously in the list
                                    foreach (DataRow orderItem in fullOrderItemDataSet.Tables["OrderItem"].Rows)
                                    {
                                        if (currentProductId != null && orderItem["MasterProdID"] != DBNull.Value)
                                        {
                                            if (orderItem["MasterProdID"].ToString() == currentProductId)
                                            {
                                                if (orderItem["UnitPriceKeyed"] != DBNull.Value)
                                                {
                                                    bolItem.Price = Convert.ToDouble(orderItem["UnitPriceKeyed"].ToString());
                                                }
                                            }
                                        }
                                    }
                                }

                                if (bolItem.BoughtAsProducts != null)
                                {
                                    var componentCounter = 1;

                                    // Loop through all the bought as products passed in through the new sales order model
                                    foreach (var boughtAsProduct in bolItem.BoughtAsProducts)
                                    {
                                        string currentBoughtAsTerminalId = null;

                                        // SLX passes null for net quantity if they do not have it
                                        if (boughtAsProduct.NetQuantity == null)
                                        {
                                            boughtAsProduct.NetQuantity = boughtAsProduct.Quantity;
                                        }

                                        if (!string.IsNullOrEmpty(boughtAsProduct.SupplierId) &&
                                            !string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                        {
                                            currentBoughtAsTerminalId =
                                                AscendDataLookup.LookupSupplierSupplyPtId(boughtAsProduct.SupplierId,
                                                    boughtAsProduct.SupplyPtId);
                                        }

                                        // SLX does not differentiate between suppliersupplypt and inventory site so they are using the same fields for both
                                        if (string.IsNullOrEmpty(currentBoughtAsTerminalId))
                                        {
                                            if (!string.IsNullOrEmpty(boughtAsProduct.SupplyPtId))
                                            {
                                                currentBoughtAsTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.SupplyPtId);
                                            }

                                            if (string.IsNullOrEmpty(currentBoughtAsTerminalId))
                                            {
                                                currentBoughtAsTerminalId =
                                                    AscendDataLookup.LookupInventorySiteId(boughtAsProduct.InventorySiteId);
                                            }
                                        }

                                        var productIdLookupQueryString =
                                            "SELECT PurchAlias.PurchAliasID AS ProductID FROM dbo." +
                                            AscendTable.PurchAliasTableName +
                                            " WHERE Code = @Code";

                                        // Look up Product MasterDataID
                                        using (var cmd = new SqlCommand(productIdLookupQueryString, cn))
                                        {
                                            cmd.Parameters.Add("@Code", SqlDbType.VarChar, 24).Value =
                                                boughtAsProduct.ProductId;
                                            mySqlDataAdapter.SelectCommand = cmd;
                                            var productDataSet = new DataSet();
                                            mySqlDataAdapter.Fill(productDataSet, "MasterDataID");
                                            var myMasterDataIdDataTable = productDataSet.Tables["MasterDataID"];
                                            if (myMasterDataIdDataTable.Rows.Count > 0)
                                            {
                                                currentBoughtAsProductId =
                                                    myMasterDataIdDataTable.Rows[0]["ProductID"].ToString();
                                            }
                                        }

                                        // Insert the products passed to NewSalesorder into the SmartLogixProduct table so that they can be used in the 
                                        // CreateSalesOrder stored proc
                                        InsertSmartLogixProduct(sysTrxNo, currentBoughtAsTerminalId,
                                            currentBoughtAsProductId, boughtAsProduct.ProductId,
                                            QuantityMultiplier(Convert.ToInt32(currentBoughtAsProductId), boughtAsProduct.Quantity), 
                                            QuantityMultiplier(Convert.ToInt32(currentBoughtAsProductId), (double)boughtAsProduct.NetQuantity),
                                            boughtAsProduct.BOLDate, null, sysTrxLine, currentTankId,
                                            componentCounter, boughtAsProduct.BOLNumber, boughtAsProduct.DiversionDestState);

                                        componentCounter++;
                                    }
                                }

                                var storedProcQueryString = "SmartLogix_CompletedSalesOrder";

                                // Execute a stored proc to create all the things we need for a completed sales order
                                using (var cmd = new SqlCommand(storedProcQueryString, cn))
                                {
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@SysTrxNo", sysTrxNo);
                                    cmd.Parameters.AddWithValue("@SysTrxLine", sysTrxLine);
                                    cmd.Parameters.AddWithValue("@CompanyId", "01");
                                    if (!string.IsNullOrEmpty(bol.BOLNumber))
                                    {
                                        cmd.Parameters.AddWithValue("@BolNo", bol.BOLNumber);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@BolNo", DBNull.Value);
                                    }

                                    cmd.Parameters.AddWithValue("@GrossQty",
                                            QuantityMultiplier(Convert.ToInt32(currentProductId),
                                                bolItem.Quantity));

                                    if (bolItem.NetQuantity != null)
                                    {
                                        cmd.Parameters.AddWithValue("@NetQty",
                                            QuantityMultiplier(Convert.ToInt32(currentProductId),
                                                Convert.ToDouble(bolItem.NetQuantity)));
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@NetQty", QuantityMultiplier(Convert.ToInt32(currentProductId),
                                                Convert.ToDouble(bolItem.Quantity)));
                                    }  

                                    if (!string.IsNullOrEmpty(currentTerminalId))
                                    {
                                        cmd.Parameters.AddWithValue("@TerminalId", currentTerminalId);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@TerminalId", DBNull.Value);
                                    }

                                    if (!string.IsNullOrEmpty(currentVehicleId))
                                    {
                                        cmd.Parameters.AddWithValue("@VehicleId", currentVehicleId);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@VehicleId", DBNull.Value);
                                    }

                                    if (!string.IsNullOrEmpty(bol.DriverId) && AscendDataLookup.IsValidDriverId(bol.DriverId))
                                    {
                                        cmd.Parameters.AddWithValue("@DriverId", bol.DriverId);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@DriverId", DBNull.Value);
                                    }

                                    if (bolItem.Price != null)
                                    {
                                        cmd.Parameters.AddWithValue("@Price", bolItem.Price);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@Price", DBNull.Value);
                                    }
                                    if (model.UserId != null)
                                    {
                                        cmd.Parameters.AddWithValue("@UserID", model.UserId);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@UserID",
                                            SmartLogixConstants.SmartLogixWebServiceName);
                                    }
                                    if (model.OrderDeliveryDate == null)
                                    {
                                        cmd.Parameters.Add("@OrderDeliveryDate", SqlDbType.DateTime).Value =
                                            DBNull.Value;
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add("@OrderDeliveryDate", SqlDbType.DateTime).Value =
                                            model.OrderDeliveryDate;
                                    }
                                    if (currentTankId != null)
                                    {
                                        cmd.Parameters.Add("@TankID", SqlDbType.Int).Value =
                                            Convert.ToInt32(currentTankId);
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add("@TankID", SqlDbType.Int).Value = DBNull.Value;
                                    }

                                    if (!string.IsNullOrEmpty(bol.DiversionDestState))
                                    {
                                        cmd.Parameters.Add("@DiversionDestState", SqlDbType.VarChar, 2).Value =
                                            bol.DiversionDestState;
                                    }
                                    else
                                    {
                                        cmd.Parameters.Add("@DiversionDestState", SqlDbType.VarChar, 2).Value =
                                            DBNull.Value;
                                    }

                                    if (model.IsNetBilling != null)
                                    {
                                        cmd.Parameters.AddWithValue("@IsNetBilling", model.IsNetBilling);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@IsNetBilling", DBNull.Value);
                                    }

                                    if (model.PONumber != null)
                                    {
                                        cmd.Parameters.AddWithValue("@PONumber", model.PONumber);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@PONumber", DBNull.Value);
                                    }

                                    cmd.Parameters.AddWithValue("@OrderNo", model.AscendSalesOrderNumber);

                                    cmd.Parameters.AddWithValue("@SaleProdId", currentProductId);

                                    // Get the return status since Completed Sales Order is prone to returning an error.
                                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, -1).Direction =
                                        ParameterDirection.Output;

                                    cmd.ExecuteNonQuery();

                                    Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber,
                                        "CompletedSalesOrderAfterSP execute", response.ResponseStatus,
                                        null, JsonConvert.SerializeObject(model), null, false);
                                }

                                counter++;
                            }
                        }
                    }
                }                

                // Log the success
                response = new SmartLogixResponse
                {
                    ResponseStatus = 1,
                    ResponseMessage = "Success"
                };

                Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "CompletedSalesOrderEnd",
                    response.ResponseStatus,
                    null, JsonConvert.SerializeObject(model), null, false);

                return response;
            }
            catch (Exception ex)
            {
                Logger.WriteToSLXLog(sysTrxNo.ToString(), model.AscendSalesOrderNumber, "CompletedSalesOrderInsideCatch",
                    response.ResponseStatus,
                    null, JsonConvert.SerializeObject(model), ex, true);

                throw;
            }
            finally
            {
                // Remove products from the SmartLogixProducts temporary storage table
                _entities.SmartLogixProducts.RemoveRange(
                    _entities.SmartLogixProducts.Where(p => p.SysTrxNo == sysTrxNo));
                _entities.SaveChanges();
            }
        }

        /// <summary>
        /// Cancels the sales order.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse CancelSalesOrder(CancelSalesOrder model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            var connectionString = connection.ConnectionString;

            var sysTrxNo = LookupSysTrxNo(model.SalesOrderNumber);

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                Logger.WriteToSLXLog(sysTrxNo, model.SalesOrderNumber, "CancelSalesOrder", 0,
                    JsonConvert.SerializeObject(model), JsonConvert.SerializeObject(model), null, false);

                var query =
                    "Update OrderHdr SET Status = 'A' WHERE OrderNo = @OrderNo";

                // create command
                using (var cmd = new SqlCommand(query, cn))
                {
                    cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 24).Value = model.SalesOrderNumber;

                    cmd.ExecuteNonQuery();
                }

                // Log the success
                var response = new SmartLogixResponse
                {
                    ResponseStatus = 1,
                    ResponseMessage = "Success"
                };

                Logger.WriteToSLXLog(sysTrxNo, model.SalesOrderNumber, "CancelSalesOrder", response.ResponseStatus,
                    JsonConvert.SerializeObject(model), JsonConvert.SerializeObject(model), null, false);

                return response;
            }
        }

        public SmartLogixResponse ValidateSalesOrder(SalesOrderModel model)
        {
            SmartLogixResponse response = new SmartLogixResponse
            {
                ResponseStatus = 2,
                ResponseMessage = "The following parts of the order are invalid:\n"
            };

            bool isValid = true;
            string standardAcctId = null;
            string shipToId = null;
                        
            // Ensure all data is correct

            // Validate the vehicle ID is real
            if (!String.IsNullOrEmpty(model.VehicleId))
            {
                string vehicleId = AscendDataLookup.LookupVehicleId(model.VehicleId);
                if (String.IsNullOrEmpty(vehicleId))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Vehicle: " + model.VehicleId + " \n";
                }
            }

            // Validate the shiptoid and standardacctid are real
            if (model.ShipToId != null && model.StandardAcctId != null)
            {
                AscendDataLookup.LookupShipToIdAndStandardAcctId(ref shipToId, ref standardAcctId, model.StandardAcctId, model.ShipToId);
                if (String.IsNullOrEmpty(shipToId) || String.IsNullOrEmpty(standardAcctId))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Standard Account or Ship To: " + model.StandardAcctId + "/" + model.ShipToId + " \n";
                }
            }
            else if (model.IsInventoryAdjustment)
            {
                shipToId = AscendDataLookup.LookupInventoryAdjustmentSite(model.VehicleId);
                if (String.IsNullOrEmpty(shipToId))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Vehicle Adjustment Site for vehicle: " + model.VehicleId + " \n";
                }
            }
            else
            {
                isValid = false;
                response.ResponseMessage += "-Invalid Standard Account or Ship To \n";
            }

            // Validate the driver ID is real
            if (!String.IsNullOrEmpty(model.DriverId))
            {
                if (!AscendDataLookup.IsValidDriverId(model.DriverId))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Driver: " + model.DriverId + " \n";
                }
            }

            // Look through th products and validate each of them including their boughtasproducts
            foreach (ProductModel product in model.ProductList)
            {
                if (!ValidateProductData(product, response))
                {
                    isValid = false;                    
                }
            }

            if (isValid)
            {
                response.ResponseMessage = "Success";
                response.ResponseStatus = 1;
            }
            
            return response;
        }

        /// <summary>
        /// Determines whether [is valid cso] [the specified model].
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public SmartLogixResponse ValidateCompletedSalesOrder(CompletedSalesOrderModel model)
        {
            SmartLogixResponse response = new SmartLogixResponse
            {
                ResponseStatus = 2,
                ResponseMessage = "The following parts of the order are invalid:\n"
            };

            bool isValid = true;
            string standardAcctId = null;
            string shipToId = null;

            // Validate the sales order number is populated
            if (model.AscendSalesOrderNumber == null)
            {
                isValid = false;
                response.ResponseMessage += "-Sales order number cannot be null \n";
            }

            // We need to do this because SLX wont change the model since its so late in the game, this is really stupid code
            ReorganizeModel(model);

            if (model.BOLS.Any())
            {
                foreach (var bol in model.BOLS)
                {
                    var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                    var connectionString = connection.ConnectionString;

                    // Open connection
                    using (var cn = new SqlConnection(connectionString))
                    {
                        // Open the connection if its not already open
                        if (!cn.State.Equals(ConnectionState.Open))
                        {
                            cn.Open();
                        }

                        var sysTrxNo = LookupSysTrxNo(model.AscendSalesOrderNumber);

                        // Ensure the order came from SLX through NSO
                        // Make sure the order has been added to ascend from SLX
                        var smartLogixOrdersQueryString =
                            "SELECT SysTrxNo from SmartLogixOrders where SysTrxNo = @SysTrxNo";

                        var isValidSmartLogixOrder = false;

                        using (var mySqlDataAdapter = new SqlDataAdapter())
                        {
                            using (var cmd = new SqlCommand(smartLogixOrdersQueryString, cn))
                            {
                                cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                mySqlDataAdapter.SelectCommand = cmd;
                                var smartLogixOrdersDataSet = new DataSet();
                                mySqlDataAdapter.Fill(smartLogixOrdersDataSet, "SmartLogixOrders");
                                if (smartLogixOrdersDataSet.Tables["SmartLogixOrders"].Rows.Count > 0)
                                {
                                    isValidSmartLogixOrder = true;
                                }
                            }
                        }

                        if (isValidSmartLogixOrder)
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid sales order sales order must be created in SLX through New Sales Order Process \n";
                        }

                        if (isValidSmartLogixOrder)
                        {
                            // Make sure the order has open status
                            var orderStatusQueryString =
                                "SELECT OrderStatusID from OrderHdr where SysTrxNo = @SysTrxNo";

                            using (var mySqlDataAdapter = new SqlDataAdapter())
                            {
                                using (var cmd = new SqlCommand(orderStatusQueryString, cn))
                                {
                                    cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                    mySqlDataAdapter.SelectCommand = cmd;
                                    var smartLogixOrdersDataSet = new DataSet();
                                    mySqlDataAdapter.Fill(smartLogixOrdersDataSet, "SmartLogixOrderStatus");
                                    var status = smartLogixOrdersDataSet.Tables["SmartLogixOrderStatus"].Rows[0]["OrderStatusID"].ToString();
                                    if (!status.Equals("1001") && !status.Equals("1003") && !status.Equals("1005"))
                                    {
                                        isValidSmartLogixOrder = false;
                                    }
                                }
                            }
                        }

                        if (isValidSmartLogixOrder)
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid sales order sales order must be in the open state to be completed \n";
                        }
                    }

                    // Ensure order items match previous NSO / MSO request

                    // Ensure all data is correct

                    // Validate the vehicle ID is real
                    if (!String.IsNullOrEmpty(bol.VehicleId))
                    {
                        string vehicleId = AscendDataLookup.LookupVehicleId(bol.VehicleId);
                        if (String.IsNullOrEmpty(vehicleId))
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid Vehicle: " + bol.VehicleId + " \n";
                        }
                    }
                    else
                    {
                        isValid = false;
                        response.ResponseMessage += "-Completed Sales Orders require a vehicle \n";
                    }

                    // Validate the shiptoid and standardacctid are real
                    if (bol.ShipToId != null && bol.StandardAcctId != null)
                    {
                        AscendDataLookup.LookupShipToIdAndStandardAcctId(ref shipToId, ref standardAcctId, bol.StandardAcctId, bol.ShipToId);
                        if (String.IsNullOrEmpty(shipToId) || String.IsNullOrEmpty(standardAcctId))
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid Standard Account or Ship To: " + bol.StandardAcctId + "/" + bol.ShipToId + " \n";
                        }
                    }
                    else if (bol.IsInventoryAdjustment)
                    {
                        shipToId = AscendDataLookup.LookupInventoryAdjustmentSite(bol.VehicleId);
                        if (String.IsNullOrEmpty(shipToId))
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid Vehicle Adjustment Site for vehicle: " + bol.VehicleId + " \n";
                        }
                    }
                    else
                    {
                        isValid = false;
                        response.ResponseMessage += "-Invalid Standard Account or Ship To \n";
                    }

                    // Validate the driver ID is real
                    if (!String.IsNullOrEmpty(bol.DriverId))
                    {
                        if (!AscendDataLookup.IsValidDriverId(bol.DriverId))
                        {
                            isValid = false;
                            response.ResponseMessage += "-Invalid Driver: " + bol.DriverId + " \n";
                        }
                    }

                    // Look through th products and validate each of them including their boughtasproducts
                    foreach (ProductModel product in bol.Products)
                    {
                        if (!ValidateProductData(product, response))
                        {
                            isValid = false;
                        }
                    }
                }
            }
            else
            {
                isValid = false;
                response.ResponseMessage += "-Order must contain at least one BOL \n";
            }

            if (isValid)
            {
                response.ResponseMessage = "Success";
                response.ResponseStatus = 1;
            }

            return response;
        }

        #endregion

        #region Private Methods

        private bool ValidateProductData(ProductModel model, SmartLogixResponse response)
        {
            bool isValid = true;

            if (String.IsNullOrEmpty(AscendDataLookup.LookupBillingItemId(model.ProductId)) &&
                String.IsNullOrEmpty(AscendDataLookup.LookupSalesAliasId(model.ProductId)))
            {
                isValid = false;
                response.ResponseMessage += "-Invalid sold as product: " + model.ProductId + " \n";
            }

            // Ensure inventory site or supplier supply point is valid
            if (!String.IsNullOrEmpty(model.InventorySiteId))
            {
                if (String.IsNullOrEmpty(AscendDataLookup.LookupInventorySiteId(model.InventorySiteId)))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Inventory site: " + model.InventorySiteId + " for sold as product: " + model.ProductId + " \n";
                }
            }

            // Ensure the supplier and supply point are valid
            if (!String.IsNullOrEmpty(model.SupplyPtId) || !String.IsNullOrEmpty(model.SupplierId))
            {
                if (String.IsNullOrEmpty(AscendDataLookup.LookupSupplierSupplyPtId(model.SupplierId, model.SupplyPtId)))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid supplier / supplypt: " + model.SupplierId + "/" + model.SupplyPtId +
                                                " for sold as product: " + model.ProductId + " \n";
                }
            }
            // If a supply point is supplied it must have a supplier and vice versa
            else if ((String.IsNullOrEmpty(model.SupplyPtId) && !String.IsNullOrEmpty(model.SupplierId)) ||
                     (String.IsNullOrEmpty(model.SupplierId) && !String.IsNullOrEmpty(model.SupplyPtId)))
            {
                isValid = false;
                response.ResponseMessage += "-Missing Supplier or Supply Point for sold as product: " + model.ProductId + " \n";
            }

            // Ensure Quantity does not equal 0
            if (model.Quantity == 0)
            {
                response.ResponseMessage += "-Invalid quantity of 0 for sold as product: " + model.ProductId + " \n";
                isValid = false;
            }

            foreach (var boughtAsProduct in model.BoughtAsProducts)
            {
                if (!ValidateBoughtAsProductData(boughtAsProduct, response))
                {
                    isValid = false;
                }
            }
            
            return isValid;
        }

        private bool ValidateBoughtAsProductData(BoughtAsProduct model, SmartLogixResponse response)
        {
            bool isValid = true;
            string supplierSupplyPtId = null;

            // Ensure inventory site or supplier supply point is valid
            if (!String.IsNullOrEmpty(model.InventorySiteId))
            {
                if (String.IsNullOrEmpty(AscendDataLookup.LookupInventorySiteId(model.InventorySiteId)))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid Inventory site: " + model.InventorySiteId + " for bought as product: " + model.ProductId + " \n";
                }
            }

            // Ensure the supplier and supply point are valid
            if (!String.IsNullOrEmpty(model.SupplyPtId) || !String.IsNullOrEmpty(model.SupplierId))
            {
                supplierSupplyPtId = AscendDataLookup.LookupSupplierSupplyPtId(model.SupplierId, model.SupplyPtId);
                if (String.IsNullOrEmpty(supplierSupplyPtId))
                {
                    isValid = false;
                    response.ResponseMessage += "-Invalid supplier / supplypt: " + model.SupplierId + "/" + model.SupplyPtId + 
                                                " for bought as product: " + model.ProductId + " \n";
                }
            }
            // If a supply point is supplied it must have a supplier and vice versa
            else if ((String.IsNullOrEmpty(model.SupplyPtId) && !String.IsNullOrEmpty(model.SupplierId)) ||
                     (String.IsNullOrEmpty(model.SupplierId) && !String.IsNullOrEmpty(model.SupplyPtId)))
            {
                isValid = false;
                response.ResponseMessage += "-Missing Supplier or Supply Point for bought as product: " + model.ProductId + " \n";
            }            

            // Ensure that the product is a purchalias or a billing item.  If it is a billing item technically it is invalid
            // however we allow it since the CSO handles it.  This is basically a workaround to a bug in SLX.          
            if (String.IsNullOrEmpty(AscendDataLookup.LookupPurchAliasId(model.ProductId)) ||
                String.IsNullOrEmpty(AscendDataLookup.LookupBillingItemId(model.ProductId)))
            {
                isValid = false;
                response.ResponseMessage += "-Invalid bought as product: " + model.ProductId + " \n";
            }

            // Ensure Quantity does not equal 0
            if (model.Quantity == 0)
            {
                isValid = false;
                response.ResponseMessage += "-Invalid quantity of 0 for bought as product: " + model.ProductId + " \n";                
            }

            // Ensure the product is setup at the supplier supply point
            if (!AscendDataLookup.IsValidProductAtSupplierSupplyPt(model.ProductId, supplierSupplyPtId))
            {
                isValid = false;
                response.ResponseMessage += "-Bought as product: " + model.ProductId + " is not setup at supplier/supplypt: " + model.SupplierId + "/" + model.SupplyPtId + " \n";
            }

            return isValid;
        }

        /// <summary>
        /// Quantities the multiplier.
        /// </summary>
        /// <param name="masterProdId">The master product identifier.</param>
        /// <param name="currentQuantity">The current quantity.</param>
        /// <returns></returns>
        private double QuantityMultiplier(int masterProdId, double currentQuantity)
        {
            var quantity = currentQuantity;

            if (currentQuantity != 0)
            {
                var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

                var connectionString = connection.ConnectionString;

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    var configurationQueryString =
                        "SELECT DXCode, MasterProdID, Conversion from dbo.DXXrefExternalOrderUOM " +
                        " JOIN DXXrefFormat ON DXXrefFormat.DXXrefFormatID = DXXrefExternalOrderUOM.DXXrefFormatID " +
                        " WHERE MasterProdID = @MasterProdID and DXXrefFormat.Code = 'SLX'";

                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(configurationQueryString, cn))
                        {
                            cmd.Parameters.Add("@MasterProdID", SqlDbType.Int).Value = masterProdId;
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, "DXXrefExternalOrderUOM");
                            if (myDataSet.Tables["DXXrefExternalOrderUOM"].Rows.Count > 0)
                            {
                                quantity = currentQuantity *
                                           Convert.ToDouble(myDataSet.Tables["DXXrefExternalOrderUOM"].Rows[0]["Conversion"].ToString());
                            }
                        }
                    }
                }
            }

            return quantity;
        }        

        /// <summary>
        /// Lookups the sales order no.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <returns></returns>
        private string LookupSalesOrderNo(long sysTrxNo)
        {
            string salesOrderNo = null;

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            var orderNoLookupQueryString =
                "SELECT SysTrxNo, OrderNo FROM dbo." +
                AscendTable.OrderHdrDataTableName +
                " WHERE SysTrxNo = @SysTrxNo";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(orderNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        var myOrderNoDataTable = myDataSet.Tables["OrderHdr"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            salesOrderNo = myOrderNoDataTable.Rows[0]["OrderNo"].ToString();
                        }
                    }
                }
            }

            return salesOrderNo;
        }

        /// <summary>
        /// Lookups the system TRX no.
        /// </summary>
        /// <param name="orderNo">The order no.</param>
        /// <returns></returns>
        private string LookupSysTrxNo(string orderNo)
        {
            string sysTrxNo = null;

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            var sysTrxNoLookupQueryString =
                "SELECT SysTrxNo, OrderNo FROM dbo." +
                AscendTable.OrderHdrDataTableName +
                " WHERE OrderNo = @OrderNo";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(sysTrxNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 24).Value = orderNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        var myOrderNoDataTable = myDataSet.Tables["OrderHdr"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            sysTrxNo = myOrderNoDataTable.Rows[0]["SysTrxNo"].ToString();
                        }
                    }
                }
            }

            return sysTrxNo;
        }


        /// <summary>
        /// Inserts the smart logix product.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <param name="currentTerminalId">The current terminal identifier.</param>
        /// <param name="currentProductId">The current product identifier.</param>
        /// <param name="currentProductCode">The current product code.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="netQuantity">The net quantity.</param>
        /// <param name="orderDate">The order date.</param>
        /// <param name="price">The price.</param>
        /// <param name="itemNumber">The item number.</param>
        /// <param name="currentTankId">The current tank identifier.</param>
        /// <param name="componentNumber">The component number.</param>
        /// <param name="bolNumber">The bol number.</param>
        private void InsertSmartLogixProduct(long sysTrxNo, string currentTerminalId, string currentProductId,
            string currentProductCode, double quantity, double netQuantity, DateTime? orderDate,
            double? price, int itemNumber, string currentTankId, int componentNumber, string bolNumber,
            string diversionDestState)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var insertSmartLogixProducts =
                    "INSERT INTO SmartLogixProduct (SysTrxNo, TerminalID, ProductID, ProductCode, Quantity, NetQuantity, OrderDate, Price, ItemNumber, TankID, ComponentNumber, BOLNumber, DiversionDestState) " +
                    "VALUES (@SysTrxNo, @TerminalId, @ProductId, @ProductCode, @Quantity, @NetQuantity, @OrderDate, @Price, @ItemNumber, @TankID, @ComponentNumber, @BOLNumber, @DiversionDestState)";

                // create command
                using (var cmd = new SqlCommand(insertSmartLogixProducts, cn))
                {
                    cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                    if (currentTerminalId == null)
                    {
                        cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = Convert.ToInt32(currentTerminalId);
                    }

                    cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 50).Value = currentProductCode;
                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = Convert.ToInt32(currentProductId);
                    cmd.Parameters.Add("@Quantity", SqlDbType.Decimal).Value =
                        QuantityMultiplier(Convert.ToInt32(currentProductId), quantity);
                    cmd.Parameters.Add("@NetQuantity", SqlDbType.Decimal).Value =
                        QuantityMultiplier(Convert.ToInt32(currentProductId), netQuantity);
                    if (orderDate == null)
                    {
                        cmd.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = orderDate;
                    }
                    if (price == null)
                    {
                        cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = price;
                    }

                    if (currentTankId != null)
                    {
                        cmd.Parameters.Add("@TankID", SqlDbType.Int).Value = Convert.ToInt32(currentTankId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@TankID", SqlDbType.Int).Value = DBNull.Value;
                    }

                    if (bolNumber != null)
                    {
                        cmd.Parameters.Add("@BOLNumber", SqlDbType.VarChar, 50).Value = bolNumber;
                    }
                    else
                    {
                        cmd.Parameters.Add("@BOLNumber", SqlDbType.VarChar, 50).Value = "";
                    }

                    if (!String.IsNullOrEmpty(diversionDestState))
                    {
                        cmd.Parameters.Add("@DiversionDestState", SqlDbType.VarChar, 2).Value = diversionDestState;
                    }
                    else
                    {
                        cmd.Parameters.Add("@DiversionDestState", SqlDbType.VarChar, 2).Value = DBNull.Value;
                    }

                    cmd.Parameters.Add("@ComponentNumber", SqlDbType.Int).Value = componentNumber;

                    cmd.Parameters.Add("@ItemNumber", SqlDbType.Int).Value = itemNumber;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Reorganizes the model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private CompletedSalesOrderModel ReorganizeModel(CompletedSalesOrderModel model)
        {
            var newSalesOrderModel = new CompletedSalesOrderModel
            {
                AscendSalesOrderNumber = model.AscendSalesOrderNumber,
                UserId = model.UserId,
                OrderDeliveryDate = model.OrderDeliveryDate,
                PONumber = model.PONumber,
                BOLS = new List<BOLModel>()
            };

            var newBOLModel = new BOLModel
            {
                Products = new List<ProductModel>()
            };

            // SLX Passes each BOL separately in this list, however we need the BOLs at the product level so we need to go through
            // each BOL and merge into a better model
            foreach (BOLModel bolItem in model.BOLS)
            {
                newBOLModel.BOLNumber = null;
                newBOLModel.BOLDate = null;
                newBOLModel.ShipToId = bolItem.ShipToId;
                newBOLModel.StandardAcctId = bolItem.StandardAcctId;
                newBOLModel.PONumber = bolItem.PONumber;
                if (!string.IsNullOrEmpty(AscendDataLookup.LookupVehicleId(bolItem.VehicleId)))
                {
                    newBOLModel.VehicleId = bolItem.VehicleId;
                }

                newBOLModel.DriverId = bolItem.DriverId;

                // SLX passes the DiversionDestState ALWAYS and they threw a fit about changing it to its up to us to check
                // and see if the state they pass us is the same as the ship to state.  If it is we must clear out the 
                // DiversionDestState they pass us since it is invalid
                if (!String.IsNullOrEmpty(bolItem.DiversionDestState))
                {
                    string shipToId = null;
                    string standardAcctId = null;

                    if (newBOLModel.IsInventoryAdjustment)
                    {
                        shipToId = AscendDataLookup.LookupInventoryAdjustmentSite(newBOLModel.VehicleId);
                    }
                    else
                    {
                        // Lookup the ship to state
                        AscendDataLookup.LookupShipToIdAndStandardAcctId(ref shipToId, ref standardAcctId, newBOLModel.StandardAcctId,
                                                                         newBOLModel.ShipToId);
                    }

                    string shipToState = AscendDataLookup.LookupShipToState(shipToId);

                    if (shipToState == bolItem.DiversionDestState)
                    {
                        newBOLModel.DiversionDestState = null;
                    }
                    else
                    {
                        newBOLModel.DiversionDestState = bolItem.DiversionDestState;
                    }
                }

                foreach (ProductModel product in bolItem.Products)
                {
                    if (product.Quantity != 0)
                    {
                        ProductModel lookupProduct = newBOLModel.Products.FirstOrDefault(p => p.ProductId == product.ProductId);

                        // If the sales alias was already found in the new model then we need to add to its values
                        if (lookupProduct != null)
                        {
                            if (product.NetQuantity == null)
                            {
                                product.NetQuantity = product.Quantity;
                            }

                            lookupProduct.Quantity += product.Quantity;
                            lookupProduct.NetQuantity += product.NetQuantity;
                            foreach (BoughtAsProduct boughtAsProduct in product.BoughtAsProducts)
                            {
                                if (boughtAsProduct.NetQuantity == null)
                                {
                                    boughtAsProduct.NetQuantity = boughtAsProduct.Quantity;
                                }

                                // A bought as product is only considered an exact match if the product id, supplier, supplypt, inventory site, bolnumber, and diversiondeststate are the same.
                                // Diversion destination state will almost never be different however there is a rare edge case where it could be.
                                BoughtAsProduct lookupBoughtAsProduct =
                                    lookupProduct.BoughtAsProducts.FirstOrDefault(
                                        p =>
                                            p.ProductId == boughtAsProduct.ProductId &&
                                            p.SupplyPtId == boughtAsProduct.SupplyPtId &&
                                            p.SupplierId == boughtAsProduct.SupplierId &&
                                            p.InventorySiteId == boughtAsProduct.InventorySiteId &&
                                            p.BOLNumber == boughtAsProduct.BOLNumber &&
                                            p.DiversionDestState == boughtAsProduct.DiversionDestState);
                                if (lookupBoughtAsProduct == null)
                                {
                                    var newBoughtAsProduct = new BoughtAsProduct
                                    {
                                        SupplyPtId = boughtAsProduct.SupplyPtId,
                                        SupplierId = boughtAsProduct.SupplierId,
                                        InventorySiteId = boughtAsProduct.InventorySiteId,
                                        ProductId = boughtAsProduct.ProductId,
                                        Quantity = boughtAsProduct.Quantity,
                                        NetQuantity = boughtAsProduct.NetQuantity,
                                        BOLNumber = bolItem.BOLNumber,
                                        BOLDate = bolItem.BOLDate
                                    };

                                    if (String.IsNullOrEmpty(boughtAsProduct.DiversionDestState))
                                    {
                                        newBoughtAsProduct.DiversionDestState = bolItem.DiversionDestState;
                                    }

                                    lookupProduct.BoughtAsProducts.Add(newBoughtAsProduct);
                                }
                                else
                                {
                                    lookupBoughtAsProduct.Quantity += boughtAsProduct.Quantity;
                                    lookupBoughtAsProduct.NetQuantity += boughtAsProduct.NetQuantity;
                                }

                                if (boughtAsProduct.InventorySiteId == null && boughtAsProduct.SupplyPtId != null &&
                                    boughtAsProduct.SupplierId != null)
                                {
                                    lookupProduct.SupplyPtId = boughtAsProduct.SupplyPtId;
                                    lookupProduct.SupplierId = boughtAsProduct.SupplierId;
                                }
                            }
                        }
                        else
                        {
                            var boughtAsProductsList = new List<BoughtAsProduct>();
                            foreach (BoughtAsProduct boughtAsProduct in product.BoughtAsProducts)
                            {
                                if (boughtAsProduct.NetQuantity == null)
                                {
                                    boughtAsProduct.NetQuantity = boughtAsProduct.Quantity;
                                }

                                var lookupBoughtAsProduct =
                                    boughtAsProductsList.FirstOrDefault(
                                        p =>
                                            p.ProductId == boughtAsProduct.ProductId &&
                                            p.SupplyPtId == boughtAsProduct.SupplyPtId &&
                                            p.SupplierId == boughtAsProduct.SupplierId &&
                                            p.InventorySiteId == boughtAsProduct.InventorySiteId &&
                                            p.BOLNumber == boughtAsProduct.BOLNumber &&
                                            p.DiversionDestState == boughtAsProduct.DiversionDestState);
                                if (lookupBoughtAsProduct == null)
                                {
                                    var newBoughtAsProduct = new BoughtAsProduct
                                    {
                                        SupplyPtId = boughtAsProduct.SupplyPtId,
                                        SupplierId = boughtAsProduct.SupplierId,
                                        InventorySiteId = boughtAsProduct.InventorySiteId,
                                        ProductId = boughtAsProduct.ProductId,
                                        Quantity = boughtAsProduct.Quantity,
                                        NetQuantity = boughtAsProduct.NetQuantity,
                                        BOLNumber = bolItem.BOLNumber,
                                        BOLDate = bolItem.BOLDate
                                    };

                                    if (String.IsNullOrEmpty(boughtAsProduct.DiversionDestState))
                                    {
                                        newBoughtAsProduct.DiversionDestState = bolItem.DiversionDestState;
                                    }

                                    boughtAsProductsList.Add(newBoughtAsProduct);
                                }
                                else
                                {

                                    lookupBoughtAsProduct.Quantity += boughtAsProduct.Quantity;
                                    lookupBoughtAsProduct.NetQuantity += boughtAsProduct.NetQuantity;

                                    if (String.IsNullOrEmpty(lookupBoughtAsProduct.DiversionDestState))
                                    {
                                        lookupBoughtAsProduct.DiversionDestState = boughtAsProduct.DiversionDestState;
                                    }
                                }

                                product.BoughtAsProducts = boughtAsProductsList;
                            }

                            newBOLModel.Products.Add(product);
                        }
                    }
                }                           
            }

            // Determine what the supplier supply pt and inventory site should be for each product
            foreach (var product in newBOLModel.Products)
            {                
                if (product.NetQuantity == null)
                {
                    foreach (var boughtAsProduct in product.BoughtAsProducts.Where(p => p.NetQuantity == 0))
                    {
                        boughtAsProduct.NetQuantity = boughtAsProduct.Quantity;
                    }

                    // Net quantity is sum of bought as products net quantity if not passed
                    product.NetQuantity = product.BoughtAsProducts.Sum(p => p.NetQuantity);

                    // If no net quantity was supplied set the net = gross
                    if (product.NetQuantity == null)
                    {
                        product.NetQuantity = product.Quantity;
                    }
                }

                // Determine what the supplier supply pt for the order item should be
                var componentWithTerminal = product.BoughtAsProducts.FirstOrDefault(
                    p => p.SupplierId != null && p.SupplyPtId != null && AscendDataLookup.LookupSupplierSupplyPtId(p.SupplierId, p.SupplyPtId) != null);

                if (componentWithTerminal != null)
                {
                    product.SupplyPtId = componentWithTerminal.SupplyPtId;
                    product.SupplierId = componentWithTerminal.SupplierId;
                }
                else
                {
                    // Part of this check is here because SLX tends to utilize the SupplyPt field for inventory sites instead of the inventory site field
                    componentWithTerminal = product.BoughtAsProducts.FirstOrDefault(
                        p => p.InventorySiteId != null || AscendDataLookup.LookupInventorySiteId(p.SupplyPtId) != null);

                    if (componentWithTerminal != null)
                    {
                        if (componentWithTerminal.InventorySiteId != null)
                        {
                            product.InventorySiteId = componentWithTerminal.InventorySiteId;
                        }
                        else
                        {
                            product.InventorySiteId = componentWithTerminal.SupplyPtId;
                        }
                    }
                }
            }

            newSalesOrderModel.BOLS.Add(newBOLModel);

            return newSalesOrderModel;
        }

        /// <summary>
        /// Calculates the freight.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <param name="sysTrxLine">The system TRX line.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="model">The model.</param>
        private void CalculateFreight(long sysTrxNo, int sysTrxLine, string productCode, SalesOrderModel model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var freightQueryString = "SELECT FrtAmt FROM OrderItemComponent" +
                                         " WHERE OrderItemComponent.SysTrxNo = @SysTrxNo and OrderItemComponent.SysTrxLine = @SysTrxLine";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    DataSet orderItemFreightDataSet;
                    using (var cmd = new SqlCommand(freightQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                            Convert.ToInt32(sysTrxLine);
                        sqlDataAdapter.SelectCommand = cmd;
                        orderItemFreightDataSet = new DataSet();
                        sqlDataAdapter.Fill(orderItemFreightDataSet, "OrderItemComponent");
                    }

                    decimal freightTotal = 0;

                    foreach (DataRow component in orderItemFreightDataSet.Tables["OrderItemComponent"].Rows)
                    {
                        if (component["FrtAmt"] != DBNull.Value)
                        {
                            freightTotal += (decimal) component["FrtAmt"];
                        }
                    }

                    freightQueryString =
                        "SELECT AmtDue FROM OrderItemTax" +
                        " WHERE OrderItemTax.SysTrxNo = @SysTrxNo and OrderItemTax.SysTrxLine = @SysTrxLine and OrderItemTax.TaxType = 'F' and Exempt = 'N'";

                    using (var cmd = new SqlCommand(freightQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value = Convert.ToInt32(sysTrxLine);
                        sqlDataAdapter.SelectCommand = cmd;
                        orderItemFreightDataSet = new DataSet();
                        sqlDataAdapter.Fill(orderItemFreightDataSet, "OrderItemTax");
                    }

                    foreach (DataRow surcharge in orderItemFreightDataSet.Tables["OrderItemTax"].Rows)
                    {
                        if (surcharge["AmtDue"] != DBNull.Value)
                        {
                            freightTotal += (decimal) surcharge["AmtDue"];
                        }
                    }

                    model.ProductList.First(
                        p => p.ProductId == productCode).Freight = freightTotal;
                }
            }
        }

        /// <summary>
        /// Calculates the tax and freight.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <param name="sysTrxLine">The system TRX line.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="model">The model.</param>
        private void CalculateTaxAndFreight(long sysTrxNo, int sysTrxLine, string productCode, SalesOrderModel model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                var taxQueryString =
                    "SELECT OrderItemTax.TaxID AS TaxID, OrderItemTax.SysTrxNo as SysTrxNo, Tax.Descr as Descr, OrderItemTax.AmtDue as AmtDue, " +
                    " OrderItemTax.TaxPerUnit as TaxPerUnit, OrderItemTax.TaxAmt as TaxAmt, Tax.CalcMethod as CalcMethod, TaxRate.Rate AS Rate, " +
                    " OrderItemTax.OrgTaxRate AS OrgTaxRate " +
                    " FROM OrderItemTax " +
                    " JOIN Tax ON Tax.ID = OrderItemTax.TaxID " +
                    " CROSS APPLY (SELECT top 1 * FROM TaxRate WHERE TaxID = OrderItemTax.TaxID ORDER BY EffDt Desc) TaxRate " +
                    " WHERE OrderItemTax.SysTrxNo = @SysTrxNo and OrderItemTax.SysTrxLine = @SysTrxLine and Tax.TaxFeeAdj = 'T' and OrderItemTax.GLRuleNo = 2 and OrderItemTax.TaxType != 'F' and OrderItemTax.Exempt = 'N'";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    DataSet taxDataSet;
                    using (var cmd = new SqlCommand(taxQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                            Convert.ToInt32(sysTrxLine);
                        sqlDataAdapter.SelectCommand = cmd;
                        taxDataSet = new DataSet();
                        sqlDataAdapter.Fill(taxDataSet, "OrderItemTax");
                    }

                    var priceQueryString =
                        "SELECT SalesAlias.Code AS SalesAliasCode, PurchAlias.Code AS PurchAliasCode, SysTrxNo, SysTrxLine, MasterProdID, UnitPriceKeyed, UnitPrice, FrtBaseTermsAmt, FrtSurchTermsAmt FROM OrderItem " +
                        " LEFT OUTER JOIN SalesAlias ON MasterProdID = SalesAlias.SalesAliasID " +
                        " LEFT OUTER JOIN PurchAlias ON MasterProdID = PurchAlias.PurchAliasID " +
                        " WHERE SysTrxNo = @SysTrxNo";

                    DataSet orderItemDataSet;
                    using (var cmd = new SqlCommand(priceQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        sqlDataAdapter.SelectCommand = cmd;
                        orderItemDataSet = new DataSet();
                        sqlDataAdapter.Fill(orderItemDataSet, "OrderItem");
                    }

                    // Clear out the tax in case SLX passes it
                    if (model.ProductList.Any(p => p.ProductId == productCode))
                    {
                        foreach(var m in model.ProductList.Where(p => p.ProductId == productCode))
                        {
                            m.Tax = new List<TaxModel>();
                        }
                    }

                    if (taxDataSet.Tables["OrderItemTax"] != null)
                    {
                        // Compile tax and pricing info as appropriate to get it ready to return to SLX
                        // Foreach tax line we need to build a tax item that can be sent back to SLX
                        foreach (DataRow taxLineItem in taxDataSet.Tables["OrderItemTax"].Rows)
                        {
                            var tax = new TaxModel();

                            if (taxLineItem["TaxPerUnit"] != DBNull.Value)
                            {
                                tax.Amount = Convert.ToDouble(taxLineItem["TaxPerUnit"].ToString());
                            }
                            if (taxLineItem["Descr"] != DBNull.Value)
                            {
                                tax.Description = taxLineItem["Descr"].ToString();
                            }
                            // We have to multiply percentage based taxes by 100 because thats required by SLX
                            if (taxLineItem["CalcMethod"].ToString().Equals("P"))
                            {
                                tax.TaxFeeTypeId = 1;
                                if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                {
                                    tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString()) * 100;
                                }
                                else
                                {
                                    tax.Amount = Convert.ToDouble(taxLineItem["Rate"].ToString()) * 100;
                                }
                            }
                            else if (taxLineItem["CalcMethod"].ToString().Equals("U"))
                            {
                                tax.TaxFeeTypeId = 2;
                                if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                {
                                    tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString());
                                }
                            }
                            else if (taxLineItem["CalcMethod"].Equals("F"))
                            {
                                tax.TaxFeeTypeId = 3;
                                if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                {
                                    tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString());
                                }
                            }

                            tax.SLSODTI_Number = model.AscendSalesOrderNumber;

                            // Compound Tax Information
                            string compoundTaxQueryString =
                                "select TaxOnTax.TaxID AS TaxOnTaxTaxID, TaxOnTax.SubjectToTaxID AS SubjectToTaxID, Tax.Code As SubjectToTaxCode, " +
                                " Tax.Descr AS SubjectToTaxDescr, Tax.CalcMethod AS SubjectToTaxCalcMethod, " +
                                " TaxRate.Rate AS Rate, " +
                                " OrderItemTax.TaxPerUnit AS TaxPerUnit, " +
                                " OrderItemTax.OrgTaxRate AS OrgTaxRate " +
                                " from TaxOnTax " +
                                " JOIN OrderItemTax on OrderItemTax.TaxID = TaxOnTax.TaxID " +
                                " join Tax ON Tax.ID = TaxOnTax.TaxID " +
                                " CROSS APPLY (SELECT top 1 * FROM TaxRate WHERE TaxRate.TaxID = OrderItemTax.TaxID ORDER BY EffDt Desc) TaxRate" +
                                " where SysTrxNo = @SysTrxNo and SysTrxLine = @SysTrxLine and TaxOnTax.SubjectToTaxID = @TaxID and TaxOnTax.TaxID in (select TaxID from OrderItemTax where SysTrxNo = @SysTrxNo2)" +
                                "  and Tax.TaxFeeAdj = 'T' and OrderItemTax.GLRuleNo = 2 and OrderItemTax.TaxType != 'F' and OrderItemTax.Exempt = 'N'";

                            DataSet compoundTaxDataSet;
                            using (var cmd = new SqlCommand(compoundTaxQueryString, cn))
                            {
                                cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                cmd.Parameters.Add("@SysTrxNo2", SqlDbType.BigInt).Value = sysTrxNo;
                                cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value = sysTrxLine;                                    
                                cmd.Parameters.Add("@TaxID", SqlDbType.Int).Value = Convert.ToInt32(taxLineItem["TaxID"]);
                                sqlDataAdapter.SelectCommand = cmd;
                               
                                compoundTaxDataSet = new DataSet();
                                sqlDataAdapter.Fill(compoundTaxDataSet, "CompoundTax");
                            }

                            tax.CompoundTaxFeeItemsList = new List<CompoundTaxModel>();
                            foreach (DataRow compoundTaxLine in compoundTaxDataSet.Tables["CompoundTax"].Rows)
                            {
                                CompoundTaxModel compoundTax = new CompoundTaxModel();

                                if (compoundTaxLine["TaxPerUnit"] != DBNull.Value)
                                {
                                    compoundTax.Amount = Convert.ToDouble(compoundTaxLine["TaxPerUnit"].ToString());
                                }
                                if (compoundTaxLine["SubjectToTaxDescr"] != DBNull.Value)
                                {
                                    compoundTax.Description = compoundTaxLine["SubjectToTaxDescr"].ToString();
                                }
                                // We have to multiply percentage based taxes by 100 because thats required by SLX
                                if (compoundTaxLine["SubjectToTaxCalcMethod"].ToString().Equals("P"))
                                {
                                    compoundTax.STaxFeeTypeId = 1;
                                    if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                    {
                                        compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString()) * 100;
                                    }
                                    else
                                    {
                                        compoundTax.Amount = Convert.ToDouble(compoundTaxLine["Rate"].ToString()) * 100;
                                    }
                                }
                                else if (compoundTaxLine["SubjectToTaxCalcMethod"].ToString().Equals("U"))
                                {
                                    compoundTax.STaxFeeTypeId = 2;
                                    if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                    {
                                        compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString());
                                    }
                                }
                                else if (compoundTaxLine["SubjectToTaxCalcMethod"].Equals("F"))
                                {
                                    compoundTax.STaxFeeTypeId = 3;
                                    if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                    {
                                        compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString());
                                    }
                                }

                                tax.CompoundTaxFeeItemsList.Add(compoundTax);
                            }                           

                            if (model.ProductList.Any(p => p.ProductId == productCode))
                            {
                                model.ProductList.First(
                                    p => p.ProductId == productCode).Tax.Add(tax);
                            }
                        }
                    }

                    if (orderItemDataSet.Tables["OrderItem"] != null)
                    {
                        // Foreach orderitem / product line we need to match up the price with the current product so it is ready to be sent back to SLX
                        foreach (DataRow lineItem in orderItemDataSet.Tables["OrderItem"].Rows)
                        {
                            // We are looking for a match between the SmartLogixProduct table and the OrderItem table here so the correct properties can be set 
                            // for the particular product in the product list
                            if ((productCode == lineItem["SalesAliasCode"].ToString() ||
                                 productCode == lineItem["PurchAliasCode"].ToString()) &&
                                model.ProductList.Any(p => p.ProductId == productCode))
                            {
                                if (lineItem["UnitPriceKeyed"] != DBNull.Value)
                                {
                                    model.ProductList.First(
                                        p => p.ProductId == productCode).Price
                                        =
                                        Convert.ToDouble(lineItem["UnitPriceKeyed"].ToString());
                                }
                                else
                                {
                                    model.ProductList.First(
                                        p => p.ProductId == productCode).Price
                                        =
                                        Convert.ToDouble(lineItem["UnitPrice"].ToString());
                                }

                                CalculateFreight(sysTrxNo, Convert.ToInt32(lineItem["SysTrxLine"]), productCode, model);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the order HDR information.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <param name="model">The model.</param>
        private void GetOrderHdrInfo(long sysTrxNo, SalesOrderModel model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var orderHdrQueryString = "SELECT PONo, Hold FROM OrderHdr WHERE SysTrxNo = @SysTrxNo";

                using (var orderHdrSqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(orderHdrQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.VarChar, 24).Value = sysTrxNo;
                        orderHdrSqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        orderHdrSqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        var orderHdrDataSet = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                        if (orderHdrDataSet.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(model.PONumber))
                            {
                                model.PONumber = orderHdrDataSet.Rows[0]["PONo"].ToString();
                            }
                            if (orderHdrDataSet.Rows[0]["Hold"].ToString().Equals("Y"))
                            {
                                model.IsOnHold = true;
                            }
                            else
                            {
                                model.IsOnHold = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Lookups the payment terms.
        /// </summary>
        /// <param name="shipToId">The ship to identifier.</param>
        /// <param name="model">The model.</param>
        private void LookupPaymentTerms(long shipToId, SalesOrderModel model)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                var queryString =
                    "SELECT ARShipTo.PrepayCODCustomerFlag AS CODFlag, ARTerms.Descr AS CODNotes FROM ARShipTo " +
                    "JOIN ARTerms ON ARTerms.TermsID = ARShipTo.TermsID WHERE ARShipTo.ShipToID = @ShipToId";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@ShipToId", SqlDbType.BigInt).Value = shipToId;
                        sqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        sqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        var dataSet = myDataSet.Tables[AscendTable.ARShipToDataTableName];
                        if (dataSet.Rows.Count > 0)
                        {
                            if (dataSet.Rows[0]["CODFlag"].ToString().Equals("N"))
                            {
                                model.CODFlag = false;
                            }
                            else
                            {
                                model.CODFlag = true;
                            }

                            if (dataSet.Rows[0]["CODNotes"] != DBNull.Value)
                            {
                                model.NoteCOD = dataSet.Rows[0]["CODNotes"].ToString();
                                if (model.NoteCOD.ToLower().Equals("cash on delivery") || model.NoteCOD.ToLower().Equals("cod"))
                                {
                                    model.CODFlag = true;
                                }
                            }
                        }
                    }
                }
            }
        }        

        #endregion
    }
}