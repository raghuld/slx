﻿using System.Linq;
using SmartLogicsIntegration.Domain;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Services
{
    /// <summary>
    /// This class will be in charge of managing methods required to deal with the SLX logs
    /// </summary>
    /// <seealso cref="SmartLogixIntegration.Services.Interfaces.ISLXLogManager" />
    // TODO: this class still needs to be implemented for better log displays
    public class SLXLogManager : ISLXLogManager
    {
        /// <summary>
        /// The entities
        /// </summary>
        private SmartLogicsIntegrationEntities _entities;

        /// <summary>
        /// Gets the logs.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <param name="skip">The skip.</param>
        /// <returns></returns>
        public IQueryable<SmartLogixPushLog> GetLogs(int n, int skip)
        {            
            return _entities.SmartLogixPushLogs.Skip(skip).Take(n);
        }
    }
}
