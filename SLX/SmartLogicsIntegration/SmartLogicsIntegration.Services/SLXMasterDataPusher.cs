﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using Newtonsoft.Json;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services.BOLService;
using SmartLogixIntegration.Services.CustomerShipToService;
using SmartLogixIntegration.Services.DriverDOTService;
using SmartLogixIntegration.Services.Interfaces;
using SmartLogixIntegration.Services.ProductService;
using Customer = SmartLogixIntegration.Services.CustomerShipToService.Customer;
using CustomerList = SmartLogixIntegration.Services.CustomerShipToService.CustomerList;
using CustomerTank = SmartLogixIntegration.Services.CustomerShipToService.CustomerTank;
using CustomerTankList = SmartLogixIntegration.Services.CustomerShipToService.CustomerTankList;
using Driver = SmartLogixIntegration.Services.DriverDOTService.Driver;
using DriverList = SmartLogixIntegration.Services.DriverDOTService.DriverList;
using FleetVehicle = SmartLogixIntegration.Services.CustomerShipToService.FleetVehicle;
using FleetVehicleList = SmartLogixIntegration.Services.CustomerShipToService.FleetVehicleList;
using Product = SmartLogixIntegration.Services.ProductService.Product;
using ProductList = SmartLogixIntegration.Services.ProductService.ProductList;
using ProductSoldAs = SmartLogixIntegration.Services.ProductService.ProductSoldAs;
using ProductSoldAsList = SmartLogixIntegration.Services.ProductService.ProductSoldAsList;
using PurchaseOrder = SmartLogixIntegration.Services.CustomerShipToService.PurchaseOrder;
using PurchaseOrderList = SmartLogixIntegration.Services.CustomerShipToService.PurchaseOrderList;
using ResponseDetail = SmartLogixIntegration.Services.BOLService.ResponseDetail;
using ResponseStatus = SmartLogixIntegration.Services.BOLService.ResponseStatus;
using SalesOrderType = SmartLogixIntegration.Services.CustomerShipToService.SalesOrderType;
using ShipTo = SmartLogixIntegration.Services.CustomerShipToService.ShipTo;
using ShipToList = SmartLogixIntegration.Services.CustomerShipToService.ShipToList;
using SupplierPeering = SmartLogixIntegration.Services.BOLService.SupplierPeering;
using SupplierPeeringList = SmartLogixIntegration.Services.BOLService.SupplierPeeringList;
using TerminalPeering = SmartLogixIntegration.Services.BOLService.TerminalPeering;
using TerminalPeeringList = SmartLogixIntegration.Services.BOLService.TerminalPeeringList;
using TerminalSupplier = SmartLogixIntegration.Services.BOLService.TerminalSupplier;
using TerminalSupplierList = SmartLogixIntegration.Services.BOLService.TerminalSupplierList;
using Truck = SmartLogixIntegration.Services.DriverDOTService.Truck;
using TruckList = SmartLogixIntegration.Services.DriverDOTService.TruckList;
using Warehouse = SmartLogixIntegration.Services.BOLService.Warehouse;
using WarehouseList = SmartLogixIntegration.Services.BOLService.WarehouseList;
using Carrier = SmartLogixIntegration.Services.BOLService.CarrierPeering;
using CarrierList = SmartLogixIntegration.Services.BOLService.CarrierPeeringList;

namespace SmartLogixIntegration.Services
{
    public class SLXMasterDataPusher : ISLXMasterDataPusher
    {

        private ISLXConfigurationManager SLXConfigurationManager { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SLXMasterDataPusher" /> class.
        /// </summary>
        /// <param name="slxConfigurationManager">The SLX configuration manager.</param>
        public SLXMasterDataPusher(ISLXConfigurationManager slxConfigurationManager)
        {
            SLXConfigurationManager = slxConfigurationManager;
        }

        /// <summary>
        ///     Pushes the insite.
        /// </summary>
        /// <param name="insiteList">The insite list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">InsiteList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushInsite(WarehouseList insiteList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (insiteList != null)
            { 
                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "BOLService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                // Need to truncate description to 30 characters due to SLX field lengths before doing the push
                foreach (Warehouse insite in insiteList)
                {
                    if (insite.Description == null)
                    {
                        insite.Description = "";
                    }
                    insite.Description = insite.Description.Substring(0, Math.Min(insite.Description.Length, 30));
                }

                using (var client = new BOLServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "InsiteList", MasterDataType.InsiteType,
                        response.ResponseStatus,
                        JsonConvert.SerializeObject(insiteList), null, null, false);

                    string[] responseMessages;
                    ResponseDetail[] responseDetailList = client.UpdateWarehouses(ref currentDateTime, ref messageType, false, null,
                        "slx", 0, "slx", insiteList, out responseMessages);

                    // Check for web service success
                    foreach (ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("InsiteList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the supplier.
        /// </summary>
        /// <param name="supplierList">The supplier list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">SupplierList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushSupplier(SupplierPeeringList supplierList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (supplierList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var supplier in supplierList)
                {
                    supplier.PeerId = 0;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "BOLService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                // Need to truncate description to 30 characters due to SLX field lengths before doing the push
                foreach (SupplierPeering supplier in supplierList)
                {
                    if (supplier.Name == null)
                    {
                        supplier.Name = "";
                    }
                    supplier.Name = supplier.Name.Substring(0, Math.Min(supplier.Name.Length, 30));
                }

                using (var client = new BOLServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "SupplierList", MasterDataType.SupplierType, response.ResponseStatus,
                        JsonConvert.SerializeObject(supplierList), null, null, false);

                    string[] responseMessages;
                    ResponseDetail[] responseDetailList = client.UpdateSupplierPeerings(ref currentDateTime, ref messageType, false,
                        null, "slx", 0, supplierList, "slx", out responseMessages);

                    // Check for web service success
                    foreach (ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("SupplierList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the supply pt.
        /// </summary>
        /// <param name="supplyPtList">The supply pt list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">SupplyPtList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushSupplyPt(TerminalPeeringList supplyPtList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (supplyPtList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var product in supplyPtList)
                {
                    product.PeerId = 0;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "BOLService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                // Need to truncate description to 30 characters due to SLX field lengths before doing the push
                foreach (TerminalPeering supplyPt in supplyPtList)
                {
                    if (supplyPt.Name == null)
                    {
                        supplyPt.Name = "";
                    }
                    supplyPt.Name = supplyPt.Name.Substring(0, Math.Min(supplyPt.Name.Length, 30));
                }

                using (var client = new BOLServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "SupplyPtList", MasterDataType.SupplyPtType, response.ResponseStatus,
                        JsonConvert.SerializeObject(supplyPtList), null, null, false);

                    string[] responseMessages;
                    ResponseDetail[] responseDetailList = client.UpdateTerminalPeerings(ref currentDateTime, ref messageType, false,
                        null, "slx", 0, supplyPtList, "slx", out responseMessages);

                    // Check for web service success
                    foreach (ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("SupplyPtList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the supplier supply pt.
        /// </summary>
        /// <param name="supplierSupplyPtList">The supplier supply pt list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">SupplierSupplyPtList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushSupplierSupplyPt(TerminalSupplierList supplierSupplyPtList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (supplierSupplyPtList == null)
            {
                throw new ApplicationException("SupplierSupplyPtList cannot be null.  Contact Firestream to report this issue.");
            }

            var response = new SmartLogixResponse();

            SmartLogixConfiguration configuration = SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

            DateTime currentDateTime = DateTime.Now;
            string messageType = null;
            var serviceUri = new Uri(configuration.Url + "BOLService.svc");
            var endpointAddress = new EndpointAddress(serviceUri);

            //Create the binding here
            WSHttpBinding binding = SLXConfigurationManager.SetBinding();

            using (var client = new BOLServiceClient(binding, endpointAddress))
            {
                response.ResponseStatus = 0;
                response.ResponseMessage = "In Progress";

                Logger.WriteToSLXLog(null, "SupplierSupplyPtList", MasterDataType.SupplierSupplyPtType,
                    response.ResponseStatus, JsonConvert.SerializeObject(supplierSupplyPtList), null, null, false);

                string[] responseMessages;
                ResponseDetail[] responseDetailList = client.UpdateTerminalSupplierPeerings(ref currentDateTime, ref messageType,
                    false, null, "slx", 0, supplierSupplyPtList, "slx", out responseMessages);

                // Check for web service success
                foreach (ResponseDetail detail in responseDetailList)
                {
                    if (!detail.ResponseStatus.Equals(ResponseStatus.Success))
                    {
                        response.ResponseStatus = 2;
                        response.ResponseMessage = detail.Message;
                        response.SLXException = detail.SLXException;
                        return response;
                    }
                }
            }

            response.ResponseStatus = 1;
            response.ResponseMessage = "Success";

            return response;
        }

        /// <summary>
        ///     Pushes the driver.
        /// </summary>
        /// <param name="driverList">The driver list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">DriverList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushDriver(DriverList driverList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (driverList != null)
            {
                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "DriverDOTService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                // Need to truncate description to 30 characters due to SLX field lengths before doing the push
                foreach (Driver driver in driverList)
                {
                    driver.Name = driver.Name.Substring(0, Math.Min(driver.Name.Length, 30));
                }

                using (var client = new DriverDOTServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "DriverList", MasterDataType.DriverType, response.ResponseStatus,
                        JsonConvert.SerializeObject(driverList), null, null, false);

                    string[] responseMessages;
                    DriverDOTService.ResponseDetail[] responseDetailList;
                    client.UpdateDriver(ref currentDateTime, ref messageType, driverList, false, null, "slx", 0, "slx",
                        out responseDetailList, out responseMessages);

                    // Check for web service success
                    foreach (DriverDOTService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(DriverDOTService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("DriverList cannot be null.  Contact Firestream to report this issue.");
        }


        /// <summary>
        ///     Pushes the customer ship to.
        /// </summary>
        /// <param name="shipToList">The ship to list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">CustomerShipToList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushCustomerShipTo(ShipToList shipToList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (shipToList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var shipTo in shipToList)
                {
                    shipTo.PeerId = 0;
                }

                // Call the SLX Integration web service and push data to it from Ascend

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "CustomerShipToService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);
                var response = new SmartLogixResponse();

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new CustomerShipToServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "ShipToList", MasterDataType.ShipToType, response.ResponseStatus,
                        JsonConvert.SerializeObject(shipToList), null, null, false);

                    string[] responseMessages;
                    CustomerShipToService.ResponseDetail[] responseDetailList = client.UpdateShipTo(ref currentDateTime, ref messageType, false, null,
                        "slx", 0,
                        shipToList, "slx", out responseMessages);

                    // Check for web service success
                    foreach (CustomerShipToService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(CustomerShipToService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("CustomerShipToList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the customer.
        /// </summary>
        /// <param name="customerList">The customer list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">CustomerList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushCustomer(CustomerList customerList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (customerList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var customer in customerList)
                {
                    customer.PeerId = 0;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "CustomerShipToService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new CustomerShipToServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "CustomerList", MasterDataType.CustomerType, response.ResponseStatus,
                        JsonConvert.SerializeObject(customerList), null, null, false);

                    string[] responseMessages;
                    CustomerShipToService.ResponseDetail[] responseDetailList = client.UpdateCustomer(ref currentDateTime, ref messageType,
                        customerList, false,
                        null, "slx", 0, "slx", out responseMessages);

                    // Check for web service success
                    foreach (CustomerShipToService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(CustomerShipToService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("CustomerList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the tank.
        /// </summary>
        /// <param name="tankList">The tank list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">TankList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushTank(CustomerTankList tankList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (tankList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var product in tankList)
                {
                    product.CrossReferenceId = null;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "CustomerShipToService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new CustomerShipToServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "TankList", MasterDataType.TankType, response.ResponseStatus,
                        JsonConvert.SerializeObject(tankList), null, null, false);

                    string[] responseMessages;
                    CustomerShipToService.ResponseDetail[] responseDetailList = client.UpdateTanks(ref currentDateTime, ref messageType, tankList,
                        false, null,
                        "slx", 0, "slx", out responseMessages);

                    // Check for web service success
                    foreach (CustomerShipToService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(CustomerShipToService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("TankList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the truck.
        /// </summary>
        /// <param name="truckList">The truck list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">TruckList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushTruck(TruckList truckList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (truckList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var product in truckList)
                {
                    product.AlternativeTruckID = null;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "DriverDOTService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new DriverDOTServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(truckList.First().TruckId, "TruckList", MasterDataType.TruckType,
                        response.ResponseStatus, JsonConvert.SerializeObject(truckList), null, null, false);

                    string[] responseMessages;
                    string truckId;
                    DriverDOTService.ResponseDetail[] responseDetailList = client.UpdateTruck(ref currentDateTime, ref messageType, false, null, "slx",
                        0,
                        truckList, "slx",
                        out responseMessages, out truckId);

                    // Check for web service success
                    foreach (DriverDOTService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(DriverDOTService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("TruckList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the fleet vehicle.
        /// </summary>
        /// <param name="fleetVehicleList">The fleet vehicle list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">FleetVehicleList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushFleetVehicle(FleetVehicleList fleetVehicleList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (fleetVehicleList != null)
            {
                // Clear out the CrossReference IDs used for logging
                foreach (var fleetVehicle in fleetVehicleList)
                {
                    fleetVehicle.CrossReferenceId = null;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "CustomerShipToService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new CustomerShipToServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "FleetVehicleList", MasterDataType.VehicleType, response.ResponseStatus,
                        JsonConvert.SerializeObject(fleetVehicleList), null, null, false);

                    string[] responseMessages;
                    CustomerShipToService.ResponseDetail[] responseDetailList = client.UpdateFleetVehicles(ref currentDateTime, ref messageType,
                        fleetVehicleList,
                        false, null, "slx", 0, "slx", out responseMessages);

                    //Check for web service success
                    foreach (CustomerShipToService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(CustomerShipToService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("FleetVehicleList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes the product.
        /// </summary>
        /// <param name="productList">The product list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">ProductList cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushProduct(ProductList productList)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (productList != null)
            {
                // Clear out the Peer IDs used for logging
                foreach (var product in productList)
                {
                    product.PeerId = 0;
                }

                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "ProductService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                using (var client = new ProductServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "ProductList", MasterDataType.ProductType, response.ResponseStatus,
                        JsonConvert.SerializeObject(productList), null, null, false);

                    string[] responseMessages;
                    ProductService.ResponseDetail[] responseDetailList = client.UpdateProducts(ref currentDateTime, ref messageType, false, null,
                        "slx",
                        0,
                        productList, "slx",
                        out responseMessages);

                    // Check for web service success
                    foreach (ProductService.ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(ProductService.ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("ProductList cannot be null.  Contact Firestream to report this issue.");
        }

        /// <summary>
        ///     Pushes all customers.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllCustomers(int? pushTopN)
        {
            var customerList = new CustomerList();

            // Get data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific customer info
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " ARStandardAcct.StandardAcctID, ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARStandardAcct.CreditHold AS CreditHold, ARStandardAcct.CustomerName AS CustomerName, ARShipToAddress.Addr1 AS Address1," +
                    " City.Code AS CityCode, StateProv.Code AS StateCode, ARShipToAddress.Zip AS Zip FROM dbo.ARStandardAcct " +
                    " LEFT OUTER JOIN(SELECT StandardAcctID, MIN(ShipToID) AS ShipToID FROM ARShipTo " +
                    " WHERE Active = 'Y' GROUP BY StandardAcctID) AS ARShipTo ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " LEFT OUTER JOIN ARShipToAddress" +
                    " ON ARShipto.ShiptoID = ARShipToAddress.ShipToID and ARShipToAddress.Type = 'P'" +
                    " LEFT OUTER JOIN StateProv" +
                    " ON ARShipToAddress.StateProvID = StateProv.ID" +
                    " LEFT OUTER JOIN City ON ARShipToAddress.CityID = City.ID" +
                    " WHERE(ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C') and CustomerName is not null";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARStandardAcctDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARStandardAcctDataTableName];
                        // Build customer list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var customer = new Customer();
                            int customerId = Convert.ToInt32(row["StandardAcctID"]);
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                customer.CustomerId = row["StandardAcctNo"].ToString();
                            }
                            if (row["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = row["CustomerName"].ToString();
                            }
                            if (row["Address1"] != DBNull.Value)
                            {
                                customer.Address = row["Address1"].ToString();
                            }
                            if (row["CityCode"] != DBNull.Value)
                            {
                                customer.City = row["CityCode"].ToString();
                            }
                            if (row["StateCode"] != DBNull.Value)
                            {
                                customer.State = row["StateCode"].ToString();
                            }
                            if (row["Zip"] != DBNull.Value && !row["Zip"].ToString().Trim().Equals(""))
                            {
                                customer.Zip = row["Zip"].ToString();
                            }

                            if (row["CreditHold"] != DBNull.Value && row["CreditHold"].Equals("Y"))
                            {
                                customer.IsCreditHold = true;
                            }
                            else
                            {
                                customer.IsCreditHold = false;
                            }

                            customerList.Add(customer);

                            if (customerList.Any())
                            {
                                string serialisedData = JsonConvert.SerializeObject(customerList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushCustomer(customerList);

                                HandleSLXResponse(customerId.ToString(), customerList.First().CustomerId, MasterDataType.CustomerType,
                                    serialisedData, null, result);

                                foreach (Customer customerItem in customerList)
                                {
                                    Logger.WriteToSLXLog(customerId.ToString(), customerItem.CustomerId, MasterDataType.CustomerType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }
                            }

                            customerList = new CustomerList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all drivers.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllDrivers(int? pushTopN)
        {
            var driverList = new DriverList();

            // Get driver data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " Drivers.DriverId AS DriverID, Drivers.FirstName + ' ' + Drivers.LastName AS Name, Drivers.Email, CellPhone.PhoneNo AS CellPhone from dbo." +
                    AscendTable.DriversDataTableName +
                    " AS Drivers " +
                    " LEFT OUTER JOIN " + AscendTable.DriversPhoneTableName +
                    " AS CellPhone ON CellPhone.DriverID = Drivers.DriverID and CellPhone.Type = (SELECT top 1 PhoneNoTypeID FROM PhoneNoType WHERE Code = 'Mobile' or Code = 'Cell') " +
                    " where Drivers.DriverType='FS'";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.DriversDataTableName);
                        DataTable driversDataTable = myDataSet.Tables[AscendTable.DriversDataTableName];
                        // Build driver list
                        foreach (DataRow row in driversDataTable.Rows)
                        {
                            var driver = new Driver();
                            int driverId = Convert.ToInt32(row["DriverID"]);
                            if (row["DriverId"] != DBNull.Value)
                            {
                                driver.DriverId = row["DriverId"].ToString();
                            }
                            if (row["Name"] != DBNull.Value)
                            {
                                driver.Name = row["Name"].ToString();
                            }
                            if (row["CellPhone"] != DBNull.Value)
                            {
                                driver.CellPhone = row["CellPhone"].ToString();
                            }
                            driverList.Add(driver);

                            if (driverList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(driverList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushDriver(driverList);

                                HandleSLXResponse(driverId.ToString(), driverList.First().DriverId, MasterDataType.DriverType, serialisedData,
                                    null, result);

                                foreach (Driver driverItem in driverList)
                                {
                                    Logger.WriteToSLXLog(driverId.ToString(), driverItem.DriverId, MasterDataType.DriverType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            driverList = new DriverList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all fleet vehicles.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllFleetVehicles(int? pushTopN)
        {
            var tankId = 0;
            var vehicleList = new FleetVehicleList();

            // Get vehicle data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " ARShipTo_Tank.TankID, ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.Code AS ShipToCode, ARShipTo_Tank.Code AS Code, ARShipTo_Tank.Descr AS Descr," +
                    " SalesAlias.Code AS ProductID, ARShipTo_Tank.FillCapacity AS FillCapacity FROM dbo." +
                    AscendTable.ARShipToTankDataTableName +
                    " JOIN " + AscendTable.ARShipToDataTableName + " ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID" +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " JOIN " + AscendTable.ARShipToTankProductDataTableName +
                    " ON ARShipTo_Tank.TankID = ARShipTo_TankProduct.TankID" +
                    " JOIN SalesAlias ON SalesAlias.SalesAliasID = ARShipTo_TankProduct.SalesAliasID" +
                    " WHERE (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];

                        foreach (DataRow row in dataTable.Rows)
                        {
                            var vehicle = new FleetVehicle();
                            tankId = Convert.ToInt32(row["TankID"]);
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                vehicle.CustomerId = row["StandardAcctNo"].ToString();
                            }
                            if (row["ShipToCode"] != DBNull.Value)
                            {
                                vehicle.ShipToId = row["ShipToCode"].ToString();
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                vehicle.VehicleNumber = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                vehicle.Description = row["Descr"].ToString();
                            }
                            if (row["ProductID"] != DBNull.Value)
                            {
                                vehicle.ProductId = row["ProductID"].ToString();
                            }
                            if (row["FillCapacity"] != DBNull.Value)
                            {
                                vehicle.TankSize = (float) Convert.ToDecimal(row["FillCapacity"]);
                            }

                            vehicleList.Add(vehicle);

                            if (vehicleList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(vehicleList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushFleetVehicle(vehicleList);

                                HandleSLXResponse(tankId.ToString(), vehicleList.First().VehicleNumber, MasterDataType.VehicleType, serialisedData,
                                    null, result);

                                foreach (FleetVehicle vehicleItem in vehicleList)
                                {
                                    Logger.WriteToSLXLog(tankId.ToString(), vehicleItem.VehicleNumber, MasterDataType.VehicleType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            vehicleList = new FleetVehicleList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all insites.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllInsites(int? pushTopN)
        {
            var insiteId = 0;
            var insiteList = new WarehouseList();

            // Get insite data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific configuration info from the master database
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " Code, LongDescr FROM dbo.Insite";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.InSiteDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.InSiteDataTableName];

                        // Build insite list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var insite = new Warehouse();
                            if (row["Code"] != DBNull.Value)
                            {
                                insite.WarehouseID = row["Code"].ToString();
                            }
                            if (row["LongDescr"] != DBNull.Value)
                            {
                                insite.Description = row["LongDescr"].ToString();
                            }

                            insiteList.Add(insite);

                            if (insiteList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(insiteList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushInsite(insiteList);

                                HandleSLXResponse(insiteId.ToString(), insiteList.First().WarehouseID, MasterDataType.InventorySiteType,
                                    serialisedData, null, result);

                                foreach (Warehouse insiteItem in insiteList)
                                {
                                    Logger.WriteToSLXLog(insiteId.ToString(), insiteItem.WarehouseID,
                                        MasterDataType.InventorySiteType, result.ResponseStatus, serialisedData, null, null,
                                        false);
                                }                                
                            }

                            insiteList = new WarehouseList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all products.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllProducts(int? pushTopN)
        {
            var productId = 0;
            var productList = new ProductList();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " SalesAlias.Code AS ProductId, SalesAlias.SellAsDescr AS SellAsDescr, HzrdMaterialsInstruction.LabelsReq AS LabelsReq, UOM.UOMID AS UOMID, " +
                    " UOM.Code AS UOM, UOM.IsPackaged AS IsPackaged,ProdType.Code AS ProductCategory" +
                    " FROM dbo." + AscendTable.SalesAliasTableName +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdContTableName +
                    " ON SalesAlias.ProdContID = ProdCont.ProdContID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.HzrdMaterialsInstructionTableName +
                    " ON ProdCont.HzrdMaterialID = HzrdMaterialsInstruction.HzrdMaterialID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.UOMTableName +
                    " ON UOM.UOMID = SalesAlias.SellByUOMID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdTypeTableName +
                    " ON ProdType.ProdTypeID = ProdCont.ProdTypeID";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SalesAliasTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SalesAliasTableName];
                        // Build product list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["SellAsDescr"] != DBNull.Value)
                            {
                                product.Description = row["SellAsDescr"].ToString();
                            }
                            if (row["LabelsReq"] != DBNull.Value)
                            {
                                product.EPADisclaimer = row["LabelsReq"].ToString();
                            }
                            if (row["UOM"] != DBNull.Value)
                            {
                                product.UOM = row["UOM"].ToString();
                            }
                            if (row["ProductCategory"] != DBNull.Value)
                            {
                                product.ProductCategory = row["ProductCategory"].ToString();
                            }

                            product.ProductPurchasedOnly = false;
                            product.NonFuel = row["IsPackaged"].Equals("Y");

                            productList.Add(product);

                            if (productList.Any())
                            {                               
                                string serialisedData = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });


                                var result = PushProduct(productList);

                                HandleSLXResponse(productId.ToString(), productList.First().ProductID, MasterDataType.ProductType, serialisedData,
                                    null, result);

                                foreach (Product productItem in productList)
                                {
                                    Logger.WriteToSLXLog(productId.ToString(), productItem.ProductID, MasterDataType.ProductType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            productList = new ProductList();
                        }
                    }
                }

                queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " PurchAlias.Code AS ProductId, PurchAlias.Descr AS Descr, HzrdMaterialsInstruction.LabelsReq AS LabelsReq, UOM.UOMID AS UOMID, UOM.Code as UOM," +
                    " UOM.IsPackaged, ProdType.Code AS ProductCategory FROM dbo." + AscendTable.PurchAliasTableName +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdContTableName +
                    " ON PurchAlias.ProdContID = ProdCont.ProdContID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.HzrdMaterialsInstructionTableName +
                    " ON ProdCont.HzrdMaterialID = HzrdMaterialsInstruction.HzrdMaterialID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.UOMTableName +
                    " ON UOM.UOMID = PurchAlias.UOMID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdTypeTableName +
                    " ON ProdType.ProdTypeID = ProdCont.ProdTypeID";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.PurchAliasTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.PurchAliasTableName];

                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                product.Description = row["Descr"].ToString();
                            }
                            if (row["LabelsReq"] != DBNull.Value)
                            {
                                product.EPADisclaimer = row["LabelsReq"].ToString();
                            }
                            if (row["UOM"] != DBNull.Value)
                            {
                                product.UOM = row["UOM"].ToString();
                            }
                            if (row["ProductCategory"] != DBNull.Value)
                            {
                                product.ProductCategory = row["ProductCategory"].ToString();
                            }

                            product.ProductPurchasedOnly = CheckIsPurchaseOnly(product.ProductID);
                            product.NonFuel = row["IsPackaged"].Equals("Y");

                            // Need to look up the product sold as values
                            string queryString2 =
                                "select SalesAlias.Code As Code, SalesAlias.SellAsDescr As Description from PurchAlias " +
                                " join SalesAlias ON SalesAlias.ProdContID = PurchAlias.ProdContID WHERE PurchAlias.Code = @PurchAliasId";

                            using (var mySqlDataAdapter2 = new SqlDataAdapter())
                            {
                                using (var cmd2 = new SqlCommand(queryString2, cn))
                                {
                                    cmd2.Parameters.Add("@PurchAliasId", SqlDbType.VarChar, 24).Value = product.ProductID;

                                    mySqlDataAdapter2.SelectCommand = cmd2;
                                    var myDataSet2 = new DataSet();
                                    mySqlDataAdapter2.Fill(myDataSet2, "SoldAsRelationship");
                                    DataTable dataTable2 = myDataSet2.Tables["SoldAsRelationship"];

                                    var soldAsList = new ProductSoldAsList();
                                    foreach (DataRow rowSoldAs in dataTable2.Rows)
                                    {
                                        if (!rowSoldAs["Code"].ToString().Equals(product.ProductID))
                                        {
                                            var soldAsProduct = new ProductSoldAs();

                                            if (rowSoldAs["Description"] != DBNull.Value)
                                            {
                                                soldAsProduct.SoldAsProductDescription = rowSoldAs["Description"].ToString();
                                            }

                                            soldAsProduct.SoldAsProductId = rowSoldAs["Code"].ToString();

                                            soldAsList.Add(soldAsProduct);
                                        }
                                    }
                                    product.ProductSoldAsList = soldAsList;
                                }
                            }

                            productList.Add(product);

                            if (productList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushProduct(productList);

                                HandleSLXResponse(productId.ToString(), productList.First().ProductID, MasterDataType.ProductType, serialisedData,
                                    null, result);

                                foreach (Product productItem in productList)
                                {
                                    Logger.WriteToSLXLog(productId.ToString(), productItem.ProductID, MasterDataType.ProductType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            productList = new ProductList();
                        }
                    }
                }

                queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " BillingItemID, BillingItem.Code AS ProductId, BillingItem.Descr AS Descr FROM dbo." +
                    AscendTable.BillingItemDataTableName;

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.BillingItemDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.BillingItemDataTableName];
                        // Build driver list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            productId = Convert.ToInt32(row["BillingItemID"]);
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                product.Description = row["Descr"].ToString();
                            }

                            product.ProductCategory = "BillingItem";
                            product.ProductPurchasedOnly = false;
                            product.UOM = "Each";
                            product.NonFuel = true;

                            productList.Add(product);

                            if (productList.Any())
                            {                               
                                string serialisedData = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushProduct(productList);

                                HandleSLXResponse(productId.ToString(), productList.First().ProductID, MasterDataType.ProductType, serialisedData,
                                    null, result);

                                foreach (Product productItem in productList)
                                {
                                    Logger.WriteToSLXLog(productId.ToString(), productItem.ProductID, MasterDataType.ProductType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            productList = new ProductList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all ship tos.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllShipTos(int? pushTopN)
        {
            var shipToList = new ShipToList();

            // Get driver data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific configuration info from the master database
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " ARShipTo.ShiptoID AS ShipToID, ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.CLCreditHold AS CreditHold, ARShipTo.Code AS ShipToCode, ARShipTo.LongDescr as ShipToName, ARShipToAddress.Addr1 AS Address1," +
                    " ARShipToAddress.Addr2 AS Address2, City.Code AS CityCode, StateProv.Code AS StateCode, ARShipToAddress.Zip AS Zip, ARShipTo.PrimaryPhone AS Phone, " +
                    " ARShipTo.PrimaryFax as Fax, ARShipTo.PORequired AS PORequired, Dest.PONumber AS PONumber, ARShipTo.Active AS Active FROM dbo." +
                    AscendTable.ARShipToDataTableName +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipto.StandardAcctID = ARStandardAcct.StandardAcctID " +
                    " LEFT OUTER JOIN " + AscendTable.ARShipToAddressDataTableName +
                    " ON ARShipto.ShiptoID = ARShipToAddress.ShipToID and ARShipToAddress.Type = 'P' " +
                    " LEFT OUTER JOIN " + AscendTable.StateDataTableName +
                    " ON ARShipToAddress.StateProvID = StateProv.ID" +
                    " LEFT OUTER JOIN " + AscendTable.CityDataTableName + " ON ARShipToAddress.CityID = City.ID" +
                    " LEFT OUTER JOIN MasterSite" +
                    " ON MasterSite.ShipToID = ARShipTo.ShiptoID" +
                    " LEFT OUTER JOIN Dest" +
                    " ON Dest.DestID = MasterSite.DestID" +
                    " WHERE (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToDataTableName];

                        foreach (DataRow row in dataTable.Rows)
                        {
                            var shipTo = new ShipTo();
                            int shipToId = Convert.ToInt32(row["ShipToID"]);
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                shipTo.CustomerId = row["StandardAcctNo"].ToString();
                            }
                            if (row["ShipToCode"] != DBNull.Value)
                            {
                                shipTo.ShipToId = row["ShipToCode"].ToString();
                            }
                            if (row["ShipToName"] != DBNull.Value)
                            {
                                shipTo.Name = row["ShipToName"].ToString();
                            }
                            if (row["Address1"] != DBNull.Value)
                            {
                                shipTo.Address = row["Address1"].ToString();
                            }
                            if (row["Address2"] != DBNull.Value)
                            {
                                shipTo.Address2 = row["Address2"].ToString();
                            }
                            if (row["CityCode"] != DBNull.Value)
                            {
                                shipTo.City = row["CityCode"].ToString();
                            }
                            if (row["StateCode"] != DBNull.Value)
                            {
                                shipTo.State = row["StateCode"].ToString();
                            }
                            if (row["Zip"] != DBNull.Value && !row["Zip"].ToString().Trim().Equals(""))
                            {
                                shipTo.Zip = row["Zip"].ToString();
                            }
                            if (row["Phone"] != DBNull.Value)
                            {
                                shipTo.Phone = row["Phone"].ToString();
                            }
                            if (row["Fax"] != DBNull.Value)
                            {
                                shipTo.FaxNumber = row["Fax"].ToString();
                            }

                            shipTo.PoRequired = row["PORequired"].ToString() == "Y";

                            shipTo.Active = row["Active"].ToString() == "Y";

                            // Set Default PONumber
                            if (row["PONumber"] != DBNull.Value)
                            {
                                shipTo.PoNumbers = new PurchaseOrderList();
                                var orderNumber = new PurchaseOrder
                                {
                                    PoNumber = row["PONumber"].ToString()
                                };
                                shipTo.PoNumbers.Add(orderNumber);
                            }

                            if (row["CreditHold"] != DBNull.Value && row["CreditHold"].Equals("Y"))
                            {
                                shipTo.IsCreditHold = true;
                            }
                            else
                            {
                                shipTo.IsCreditHold = false;
                            }

                            shipTo.DefaultOrderType = SalesOrderType.Tank;

                            shipToList.Add(shipTo);

                            if (shipToList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(shipToList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushCustomerShipTo(shipToList);

                                HandleSLXResponse(shipToId.ToString(), shipToList.First().ShipToId, MasterDataType.ShipToType, serialisedData,
                                    null, result);

                                foreach (ShipTo shipToItem in shipToList)
                                {
                                    Logger.WriteToSLXLog(shipToId.ToString(), shipToItem.ShipToId, MasterDataType.ShipToType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            shipToList = new ShipToList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all suppliers.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllSuppliers(int? pushTopN)
        {
            var supplierList = new SupplierPeeringList();

            // Get supplier data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific configuration info from the master database
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " SupplierID, Code, Descr FROM dbo.Supplier";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplierDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplierDataTableName];
                        // Build supplier list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplier = new SupplierPeering();
                            int supplierId = Convert.ToInt32(row["SupplierID"]);
                            if (row["Code"] != DBNull.Value)
                            {
                                supplier.Number = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                supplier.Name = row["Descr"].ToString();
                            }

                            supplierList.Add(supplier);

                            if (supplierList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(supplierList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });
                                
                                var result = PushSupplier(supplierList);

                                HandleSLXResponse(supplierId.ToString(), supplierList.First().Number, MasterDataType.SupplierType, serialisedData,
                                    null, result);

                                foreach (SupplierPeering insite in supplierList)
                                {
                                    Logger.WriteToSLXLog(supplierId.ToString(), insite.Number, MasterDataType.SupplierType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            supplierList = new SupplierPeeringList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all supply PTS.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllSupplyPts(int? pushTopN)
        {
            var supplyPtId = 0;
            var supplyPtList = new TerminalPeeringList();

            // Get supplypt data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific configuration info from the master database
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " SupplyPtID, Code, Descr FROM dbo.SupplyPt";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplyPtDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplyPtDataTableName];
                        // Build supplypt list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplyPt = new TerminalPeering();
                            supplyPtId = Convert.ToInt32(row["SupplyPtID"]);
                            if (row["Code"] != DBNull.Value)
                            {
                                supplyPt.Number = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                supplyPt.Name = row["Descr"].ToString();
                            }

                            supplyPtList.Add(supplyPt);

                            if (supplyPtList.Any())
                            {                               
                                string serialisedData = JsonConvert.SerializeObject(supplyPtList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushSupplyPt(supplyPtList);

                                HandleSLXResponse(supplyPtId.ToString(), supplyPtList.First().Number, MasterDataType.SupplyPtType, serialisedData,
                                    null, result);

                                foreach (TerminalPeering supplyPtItem in supplyPtList)
                                {
                                    Logger.WriteToSLXLog(supplyPtId.ToString(), supplyPtItem.Number, MasterDataType.SupplyPtType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            supplyPtList = new TerminalPeeringList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all supplier supply PTS.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllSupplierSupplyPts(int? pushTopN)
        {
            var supplierSupplyPtId = 0;
            var supplierSupplyPtList = new TerminalSupplierList();

            // Get supplypt data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " SupplierSupplyPt.SupplierSupplyPtID AS SupplierSupplyPtID, Supplier.Code AS Supplier, SupplyPt.Code AS SupplyPt FROM dbo.SupplierSupplyPt " +
                    " JOIN Supplier ON Supplier.SupplierId = SupplierSupplyPt.SupplierID" +
                    " JOIN SupplyPt ON SupplyPt.SupplyPtId = SupplierSupplyPt.SupplyPtID";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplierSupplyPtDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplierSupplyPtDataTableName];
                        // Build suppliersupplypt list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplierSupplyPt = new TerminalSupplier();
                            supplierSupplyPtId = Convert.ToInt32(row["SupplierSupplyPtID"]);
                            if (row["Supplier"] != DBNull.Value)
                            {
                                supplierSupplyPt.SupplierNumber = row["Supplier"].ToString();
                            }
                            if (row["SupplyPt"] != DBNull.Value)
                            {
                                supplierSupplyPt.TerminalNumber = row["SupplyPt"].ToString();
                            }

                            supplierSupplyPtList.Add(supplierSupplyPt);

                            if (supplierSupplyPtList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(supplierSupplyPtList,
                                    new JsonSerializerSettings
                                    {
                                        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                    });
                             
                                var result = PushSupplierSupplyPt(supplierSupplyPtList);

                                HandleSLXResponse(supplierSupplyPtId.ToString(),
                                    supplierSupplyPtList.First().SupplierNumber + "/" + supplierSupplyPtList.First().TerminalNumber,
                                    MasterDataType.SupplierSupplyPtType, serialisedData, null, result);

                                foreach (TerminalSupplier supplierSupplyPtItem in supplierSupplyPtList)
                                {
                                    Logger.WriteToSLXLog(supplierSupplyPtId.ToString(),
                                        supplierSupplyPtItem.SupplierNumber + "/" + supplierSupplyPtItem.TerminalNumber,
                                        MasterDataType.SupplierSupplyPtType, result.ResponseStatus, serialisedData, null, null,
                                        false);
                                }                                
                            }

                            supplierSupplyPtList = new TerminalSupplierList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all tanks.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllTanks(int? pushTopN)
        {
            var tankId = 0;
            var tankList = new CustomerTankList();

            // Get tank data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " ARShipTo_Tank.TankID AS TankID, ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.Code AS ShipToCode, ARShipTo_Tank.Code AS Code, ARShipTo_Tank.Descr AS Descr," +
                    " SalesAlias.Code AS ProductID, ARShipTo_Tank.FillCapacity AS FillCapacity FROM dbo." +
                    AscendTable.ARShipToTankDataTableName +
                    " JOIN " + AscendTable.ARShipToDataTableName + " ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID" +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " JOIN " + AscendTable.ARShipToTankProductDataTableName +
                    " ON ARShipTo_Tank.TankID = ARShipTo_TankProduct.TankID" +
                    " JOIN " + AscendTable.SalesAliasTableName +
                    " ON ARShipTo_TankProduct.SalesAliasID = SalesAlias.SalesAliasID " +
                    " WHERE (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];

                        foreach (DataRow row in dataTable.Rows)
                        {
                            tankId = Convert.ToInt32(row["TankID"]);
                            var tank = new CustomerTank
                            {
                                CustomerId = row["StandardAcctNo"].ToString(),
                                ShipToId = row["ShipToCode"].ToString(),
                                TankId = row["Code"].ToString()
                            };

                            if (row["Descr"] != DBNull.Value)
                            {
                                tank.Barcode = row["Descr"].ToString();
                            }
                            if (row["ProductID"] != DBNull.Value)
                            {
                                tank.ProductId = row["ProductID"].ToString();
                            }
                            if (row["FillCapacity"] != DBNull.Value)
                            {
                                tank.Capacity = (float) Convert.ToDecimal(row["FillCapacity"]);
                            }

                            tank.OrderType = SalesOrderType.Tank;

                            tankList.Add(tank);

                            if (tankList.Any())
                            {                                
                                string serialisedData = JsonConvert.SerializeObject(tankList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushTank(tankList);

                                HandleSLXResponse(tankId.ToString(), tankList.First().TankId, MasterDataType.TankType, serialisedData, null,
                                    result);

                                foreach (CustomerTank tankItem in tankList)
                                {
                                    Logger.WriteToSLXLog(tankId.ToString(), tankItem.TankId, MasterDataType.TankType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            tankList = new CustomerTankList();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Pushes all trucks.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllTrucks(int? pushTopN)
        {
            var truckList = new TruckList();

            // Get truck data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " VehicleID, Code, VINNumber, LicPlateNo FROM dbo." +
                    AscendTable.VehicleDataTableName + " WHERE CLType = 'M'";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.VehicleDataTableName);
                        DataTable truckDataTable = myDataSet.Tables[AscendTable.VehicleDataTableName];

                        foreach (DataRow row in truckDataTable.Rows)
                        {
                            var truck = new Truck();
                            int truckId = Convert.ToInt32(row["VehicleID"]);
                            if (row["Code"] != DBNull.Value)
                            {
                                truck.TruckId = row["Code"].ToString();
                            }

                            truck.VIN = row["VINNumber"] != DBNull.Value ? row["VINNumber"].ToString() : "";

                            truck.Tag = row["LicPlateNo"] != DBNull.Value ? row["LicPlateNo"].ToString() : "";

                            if (row["VehicleID"] != DBNull.Value)
                            {
                                truck.BackOfficeTruckId = row["VehicleID"].ToString();
                            }

                            if (!string.IsNullOrEmpty(truck.TruckId.Trim()))
                            {
                                truckList.Add(truck);
                            }

                            if (truckList.Any())
                            {                               
                                string serialisedData = JsonConvert.SerializeObject(truckList, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushTruck(truckList);

                                HandleSLXResponse(truckId.ToString(), truckList.First().TruckId, MasterDataType.TruckType, serialisedData, null,
                                    result);

                                foreach (Truck truckItem in truckList)
                                {
                                    Logger.WriteToSLXLog(truckId.ToString(), truckItem.TruckId, MasterDataType.TruckType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }                                
                            }

                            truckList = new TruckList();
                        }
                    }
                }
            }
        }
        /// <summary>
        ///     Pushes the carrier.
        /// </summary>
        /// <param name="carrierlist">The carrier list.</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">carrierlist cannot be null.  Contact Firestream to report this issue.</exception>
        public SmartLogixResponse PushCarrier(CarrierPeeringList carrierlist)
        {
            // This check is required because it is possible for a bug to happen when a null is passed for pushing the customer
            // The error then looks like it comes from SLX but really it does not.
            if (carrierlist != null)
            {
                var response = new SmartLogixResponse();

                SmartLogixConfiguration configuration =
                    SLXConfigurationManager.GetConfigurationValues(SmartLogixConstants.SmartLogixPushWebServiceName);

                DateTime currentDateTime = DateTime.Now;
                string messageType = null;
                var serviceUri = new Uri(configuration.Url + "BOLService.svc");
                var endpointAddress = new EndpointAddress(serviceUri);

                //Create the binding here
                WSHttpBinding binding = SLXConfigurationManager.SetBinding();

                // Need to truncate description to 30 characters due to SLX field lengths before doing the push
                foreach (CarrierPeering carrier in carrierlist)
                {
                    carrier.Name = carrier.Name.Substring(0, Math.Min(carrier.Name.Length, 30));
                }

                using (var client = new BOLServiceClient(binding, endpointAddress))
                {
                    response.ResponseStatus = 0;
                    response.ResponseMessage = "In Progress";

                    Logger.WriteToSLXLog(null, "carrierlist", MasterDataType.CarrierType, response.ResponseStatus,
                        JsonConvert.SerializeObject(carrierlist), null, null, false);

                    string[] responseMessages;
                    ResponseDetail[] responseDetailList = client.UpdateCarrierPeerings(ref currentDateTime, ref messageType, carrierlist, false,
                        null, "slx", 0, "slx", out responseMessages);

                    // Check for web service success
                    foreach (ResponseDetail detail in responseDetailList)
                    {
                        if (!detail.ResponseStatus.Equals(ResponseStatus.Success))
                        {
                            response.ResponseStatus = 2;
                            response.ResponseMessage = detail.Message;
                            response.SLXException = detail.SLXException;
                            return response;
                        }
                    }
                }

                response.ResponseStatus = 1;
                response.ResponseMessage = "Success";

                return response;
            }

            throw new ApplicationException("CarrierList cannot be null.  Contact Firestream to report this issue.");
        }


        /// <summary>
        ///     Pushes all Carrier.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        public void PushAllCarriers(int? pushTopN)
        {
            var carrierlist = new CarrierList();

            // Get carrier data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                var queryString =
                    "SELECT ";

                if (pushTopN != null && pushTopN != 0)
                {
                    queryString += " TOP " + pushTopN;
                }

                queryString +=
                    " CarrierID,Code, SCACCode from dbo." +
                    AscendTable.CarrierTablename;

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.CarrierTablename);
                        DataTable driversDataTable = myDataSet.Tables[AscendTable.CarrierTablename];
                        // Build carrier list
                        foreach (DataRow row in driversDataTable.Rows)
                        {
                            var carrier = new Carrier();
                            int carrierid = Convert.ToInt32(row["CarrierID"]);
                            if (row["CarrierID"] != DBNull.Value)
                            {
                                carrier.Number = Convert.ToInt32(row["CarrierID"]);
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                carrier.Name = row["Code"].ToString();
                            }
                            if (row["SCACCode"] != DBNull.Value)
                            {
                                carrier.SCAC = row["SCACCode"].ToString();
                            }
                            carrierlist.Add(carrier);

                            if (carrierlist.Any())
                            {
                                string serialisedData = JsonConvert.SerializeObject(carrierlist, new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                });

                                var result = PushCarrier(carrierlist);

                                HandleSLXResponse(carrierid.ToString(), Convert.ToString(carrierlist.First().Number), MasterDataType.CarrierType, serialisedData,
                                    null, result);

                                foreach (Carrier carrieritem in carrierlist)
                                {
                                    Logger.WriteToSLXLog(carrierid.ToString(), carrieritem.Number.ToString(), MasterDataType.CarrierType,
                                        result.ResponseStatus, serialisedData, null, null, false);
                                }
                            }

                            carrierlist = new CarrierList();
                        }
                    }
                }
            }
        }




        /// <summary>
        ///     Gets the configuration values.
        /// </summary>
        /// <returns></returns>
        private static SmartLogixConfiguration GetConfigurationValues()
        {
            var configuration = new SmartLogixConfiguration();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                // Get specific configuration info from the master database
                string configurationQueryString =
                    "SELECT ID, Code, UserID, Password, ClientID, URL, DatabaseName, WebServiceName from dbo." +
                    AscendTable.ThirdPartyWebService
                    + " WHERE WebServiceName = @Code";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(configurationQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value =
                            SmartLogixConstants.SmartLogixWebServiceName;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ThirdPartyWebService);
                        DataTable myConfigurationDataTable = myDataSet.Tables[AscendTable.ThirdPartyWebService];
                        if (myConfigurationDataTable.Rows.Count > 0)
                        {
                            configuration.ClientId = Convert.ToInt32(myConfigurationDataTable.Rows[0]["ClientID"]);
                            configuration.Code = myConfigurationDataTable.Rows[0]["Code"].ToString();
                            configuration.DatabaseName = myConfigurationDataTable.Rows[0]["DatabaseName"].ToString();
                            configuration.WebServiceName = myConfigurationDataTable.Rows[0]["WebServiceName"].ToString();
                            configuration.Id = Convert.ToInt32(myConfigurationDataTable.Rows[0]["Id"]);
                            configuration.Password = myConfigurationDataTable.Rows[0]["Password"].ToString();
                            configuration.UserId = myConfigurationDataTable.Rows[0]["UserId"].ToString();
                            configuration.Url = myConfigurationDataTable.Rows[0]["Url"].ToString();
                        }
                    }
                }
            }

            return configuration;
        }

        /// <summary>
        ///     Handles the SLX response.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="result">The result.</param>
        private static void HandleSLXResponse(string ascendId, string masterDataId, string masterDataType,
            string request, string requestRecieved, SmartLogixResponse result)
        {
            if (result == null || result.ResponseStatus != 1)
            {
                // This is to bypass a bug in slx where the status value does not actually change to 1
                if (result?.ResponseMessage == null || !result.ResponseMessage.Contains("Success"))
                {
                    if (result == null)
                    {
                        result = new SmartLogixResponse();
                    }

                    if (result.SLXException == null)
                    {
                        result.SLXException = new Exception(result.ResponseMessage);
                    }

                    WriteToSLXLog(ascendId, masterDataId, masterDataType, result.ResponseStatus, request, requestRecieved, result.SLXException, true);
                }
            }
        }


        /// <summary>
        ///     Writes to SLX log.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="status">The status.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="isError">if set to <c>true</c> [is error].</param>
        /// <returns></returns>
        private static Guid WriteToSLXLog(string ascendId, string masterDataId, string masterDataType, int? status,
            string request, string requestRecieved, Exception ex, bool isError)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // define INSERT query with parameters
            string query =
                "INSERT INTO " + AscendTable.SmartLogixLogDataTableName +
                " (LogID, LogDate, AscendId, MasterDataId, MasterDataType, Status, Details, Request, RequestRecieved, StackTrace, IsError) " +
                "VALUES (@LogID, @LogDate, @AscendId, @MasterDataId, @MasterDataType, @Status, @Details, @Request, @RequestRecieved, @StackTrace, @IsError)";

            Guid logId = Guid.NewGuid();

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                // create command
                using (var cmd = new SqlCommand(query, cn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@LogID", logId);
                        cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                        if (ascendId != null)
                        {
                            cmd.Parameters.AddWithValue("@AscendId", ascendId);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@AscendId", DBNull.Value);
                        }
                        cmd.Parameters.AddWithValue("@MasterDataId", masterDataId);
                        cmd.Parameters.AddWithValue("@MasterDataType", masterDataType);

                        if (ex == null)
                        {
                            cmd.Parameters.AddWithValue("@Status", status.ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Status", "2");
                        }

                        if (ex != null)
                        {
                            cmd.Parameters.AddWithValue("@Details", ex.Message);
                            if (ex.StackTrace != null)
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", ex.StackTrace);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Details", "Success");
                            cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(request))
                        {
                            cmd.Parameters.AddWithValue("@Request", request);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Request", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(requestRecieved))
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", requestRecieved);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", DBNull.Value);
                        }

                        if (isError)
                        {
                            cmd.Parameters.AddWithValue("@IsError", 1);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@IsError", 0);
                        }

                        // open connection, execute INSERT, close connection
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        // Do not want to throw an error if something goes wrong logging
                    }
                }
            }

            return logId;
        }

        /// <summary>
        ///     Checks the is purchase only.
        /// </summary>
        /// <param name="productCode">The product code.</param>
        /// <returns></returns>
        private static bool CheckIsPurchaseOnly(string productCode)
        {
            var isPurchaseOnly = true;
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            string sysTrxNoLookupQueryString =
                "SELECT SalesAliasID FROM dbo." +
                AscendTable.SalesAliasTableName +
                " WHERE Code = @Code";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(sysTrxNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 24).Value = productCode;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "SalesAlias");
                        DataTable myOrderNoDataTable = myDataSet.Tables["SalesAlias"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            isPurchaseOnly = false;
                        }
                    }
                }
            }

            return isPurchaseOnly;
        }
       




    }
}
