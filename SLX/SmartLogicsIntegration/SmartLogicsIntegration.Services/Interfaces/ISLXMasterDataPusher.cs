﻿using SmartLogixIntegration.AscendSLXInt.Models;
using CustomerList = SmartLogixIntegration.Services.CustomerShipToService.CustomerList;
using CustomerTankList = SmartLogixIntegration.Services.CustomerShipToService.CustomerTankList;
using DriverList = SmartLogixIntegration.Services.DriverDOTService.DriverList;
using FleetVehicleList = SmartLogixIntegration.Services.CustomerShipToService.FleetVehicleList;
using ProductList = SmartLogixIntegration.Services.ProductService.ProductList;
using ShipToList = SmartLogixIntegration.Services.CustomerShipToService.ShipToList;
using SupplierPeeringList = SmartLogixIntegration.Services.BOLService.SupplierPeeringList;
using TerminalPeeringList = SmartLogixIntegration.Services.BOLService.TerminalPeeringList;
using TerminalSupplierList = SmartLogixIntegration.Services.BOLService.TerminalSupplierList;
using TruckList = SmartLogixIntegration.Services.DriverDOTService.TruckList;
using WarehouseList = SmartLogixIntegration.Services.BOLService.WarehouseList;
using CarrierList = SmartLogixIntegration.Services.BOLService.CarrierPeeringList;


namespace SmartLogixIntegration.Services.Interfaces
{
    public interface ISLXMasterDataPusher
    {    
        SmartLogixResponse PushInsite(WarehouseList insiteList);
        SmartLogixResponse PushSupplier(SupplierPeeringList supplierList);
        SmartLogixResponse PushSupplyPt(TerminalPeeringList supplyPtList);
        SmartLogixResponse PushSupplierSupplyPt(TerminalSupplierList supplierSupplyPtList);
        SmartLogixResponse PushCustomerShipTo(ShipToList shipToList);
        SmartLogixResponse PushDriver(DriverList driverList);
        SmartLogixResponse PushCustomer(CustomerList customerList);
        SmartLogixResponse PushTank(CustomerTankList tankList);
        SmartLogixResponse PushTruck(TruckList truckList);
        SmartLogixResponse PushFleetVehicle(FleetVehicleList fleetVehicleList);
        SmartLogixResponse PushProduct(ProductList productList);
        SmartLogixResponse PushCarrier(CarrierList carrierList);
        void PushAllCustomers(int ? pushTopN);
        void PushAllDrivers(int? pushTopN);
        void PushAllFleetVehicles(int? pushTopN);
        void PushAllInsites(int? pushTopN);
        void PushAllProducts(int? pushTopN);
        void PushAllShipTos(int? pushTopN);
        void PushAllSuppliers(int? pushTopN);
        void PushAllSupplyPts(int? pushTopN);
        void PushAllSupplierSupplyPts(int? pushTopN);
        void PushAllTanks(int? pushTopN);
        void PushAllTrucks(int? pushTopN);
        void PushAllCarriers(int? pushTopN);
    }
}
