﻿using System.ServiceModel;
using SmartLogixIntegration.AscendSLXInt.Models;

namespace SmartLogixIntegration.Services.Interfaces
{
    public interface ISLXConfigurationManager
    {
        SmartLogixConfiguration GetConfigurationValues(string webServiceName);
        WSHttpBinding SetBinding();
    }
}
