﻿using System.Linq;
using SmartLogicsIntegration.Domain;

namespace SmartLogixIntegration.Services.Interfaces
{
    public interface ISLXLogManager
    {

        IQueryable<SmartLogixPushLog> GetLogs(int n, int skip);

    }
}
