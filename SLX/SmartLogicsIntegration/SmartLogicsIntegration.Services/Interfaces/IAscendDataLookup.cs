﻿using SmartLogixIntegration.AscendSLXInt.Models;

namespace SmartLogixIntegration.Services.Interfaces
{
    public interface IAscendDataLookup
    {
        /// <summary>
        /// Lookups the ship to identifier and standard acct identifier.
        /// </summary>
        /// <param name="currentShipToId">The current ship to identifier.</param>
        /// <param name="currentStandardAcctId">The current standard acct identifier.</param>
        /// <param name="standardAcctNo">The standard acct no.</param>
        /// <param name="shipToCode">The ship to code.</param>
        void LookupShipToIdAndStandardAcctId(ref string currentShipToId, ref string currentStandardAcctId,
                                             string standardAcctNo, string shipToCode);

        /// <summary>
        /// Lookups the inventory adjustment site.
        /// </summary>
        /// <param name="vehicleCode">The vehicle code.</param>
        /// <returns></returns>
        string LookupInventoryAdjustmentSite(string vehicleCode);

        /// <summary>
        /// Lookups the inventory site identifier.
        /// </summary>
        /// <param name="inventorySiteCode">The inventory site code.</param>
        /// <returns></returns>
        string LookupInventorySiteId(string inventorySiteCode);

        /// <summary>
        /// Lookups the supplier supply pt identifier.
        /// </summary>
        /// <param name="supplierCode">The supplier code.</param>
        /// <param name="supplyPtCode">The supply pt code.</param>
        /// <returns></returns>
        string LookupSupplierSupplyPtId(string supplierCode, string supplyPtCode);

        /// <summary>
        /// Lookups the vehicle identifier.
        /// </summary>
        /// <param name="vehicleCode">The vehicle code.</param>
        /// <returns></returns>
        string LookupVehicleId(string vehicleCode);

        /// <summary>
        /// Lookups the sales alias identifier.
        /// </summary>
        /// <param name="salesAliasCode">The sales alias code.</param>
        /// <returns></returns>
        string LookupSalesAliasId(string salesAliasCode);

        /// <summary>
        /// Lookups the purch alias identifier.
        /// </summary>
        /// <param name="purchAliasCode">The purch alias code.</param>
        /// <returns></returns>
        string LookupPurchAliasId(string purchAliasCode);

        /// <summary>
        /// Lookups the billing item identifier.
        /// </summary>
        /// <param name="billingItemCode">The billing item code.</param>
        /// <returns></returns>
        string LookupBillingItemId(string billingItemCode);

        /// <summary>
        /// Lookups the tank identifier.
        /// </summary>
        /// <param name="tankCode">The tank code.</param>
        /// <returns></returns>
        string LookupTankId(string tankCode);

        /// <summary>
        /// Lookups the state of the ship to.
        /// </summary>
        /// <param name="shipToId">The ship to identifier.</param>
        /// <returns></returns>
        string LookupShipToState(string shipToId);

        /// <summary>
        /// Lookups the driver Id in ascend to confirm it is real
        /// </summary>
        /// <param name="driverId">The driver identifier.</param>
        /// <returns>True if the driver ID is valid otherwise false</returns>
        bool IsValidDriverId(string driverId);

        bool IsValidProductAtSupplierSupplyPt(string productId, string supplierSupplyPtId);
    }
}
