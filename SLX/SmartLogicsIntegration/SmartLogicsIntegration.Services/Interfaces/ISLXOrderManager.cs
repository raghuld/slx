﻿using SmartLogixIntegration.AscendSLXInt.Models;

namespace SmartLogixIntegration.Services.Interfaces
{
    public interface ISLXOrderManager
    {

        SmartLogixResponse NewSalesOrder(SalesOrderModel model);
        SmartLogixResponse ModifiedSalesOrder(SalesOrderModel model);
        SmartLogixResponse PushNewSalesOrder(SalesOrderModel model);
        SmartLogixResponse PushModifiedSalesOrder(SalesOrderModel model);
        SmartLogixResponse CompletedSalesOrder(CompletedSalesOrderModel model);
        SmartLogixResponse CancelSalesOrder(CancelSalesOrder model);
        SmartLogixResponse UpdateOrderStatus(UpdateOrderStatusModel model);
        SmartLogixResponse ValidateSalesOrder(SalesOrderModel model);
        SmartLogixResponse ValidateCompletedSalesOrder(CompletedSalesOrderModel model);

    }
}
