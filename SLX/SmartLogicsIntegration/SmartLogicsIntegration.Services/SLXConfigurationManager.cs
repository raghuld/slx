﻿using System;
using System.Data;
using System.Data.SqlClient;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using System.Configuration;
using System.ServiceModel;
using System.Text;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Services
{
    public class SLXConfigurationManager : ISLXConfigurationManager
    {
        /// <summary>
        /// Gets the configuration values.
        /// </summary>
        /// <param name="webServiceName">Name of the web service.</param>
        /// <returns></returns>
        public SmartLogixConfiguration GetConfigurationValues(string webServiceName)
        {
            var configuration = new SmartLogixConfiguration();

            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            var connectionString = connection.ConnectionString;

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get specific configuration info from the master database
                var configurationQueryString =
                    "SELECT ID, Code, UserID, Password, ClientID, URL, DatabaseName, WebServiceName from dbo." +
                    AscendTable.ThirdPartyWebService
                    + " WHERE WebServiceName = @Code";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(configurationQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = webServiceName;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ThirdPartyWebService);
                        var myConfigurationDataTable = myDataSet.Tables[AscendTable.ThirdPartyWebService];
                        if (myConfigurationDataTable.Rows.Count > 0)
                        {
                            configuration.ClientId = Convert.ToInt32(myConfigurationDataTable.Rows[0]["ClientID"]);
                            configuration.Code = myConfigurationDataTable.Rows[0]["Code"].ToString();
                            configuration.DatabaseName = myConfigurationDataTable.Rows[0]["DatabaseName"].ToString();
                            configuration.WebServiceName = myConfigurationDataTable.Rows[0]["WebServiceName"].ToString();
                            configuration.Id = Convert.ToInt32(myConfigurationDataTable.Rows[0]["Id"]);
                            configuration.Password = myConfigurationDataTable.Rows[0]["Password"].ToString();
                            configuration.UserId = myConfigurationDataTable.Rows[0]["UserId"].ToString();
                            configuration.Url = myConfigurationDataTable.Rows[0]["Url"].ToString();
                        }
                    }
                }
            }

            return configuration;
        }

        /// <summary>
        /// Sets the binding.
        /// </summary>
        /// <returns></returns>
        public WSHttpBinding SetBinding()
        {
            var binding = new WSHttpBinding
            {
                CloseTimeout = new TimeSpan(0, 1, 0),
                OpenTimeout = new TimeSpan(0, 1, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                SendTimeout = new TimeSpan(0, 1, 0),
                BypassProxyOnLocal = false,
                TransactionFlow = false,
                HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                MaxBufferPoolSize = 524288,
                MaxReceivedMessageSize = 65536,
                MessageEncoding = WSMessageEncoding.Text,
                TextEncoding = Encoding.UTF8,
                UseDefaultWebProxy = true,
                AllowCookies = false,
                ReaderQuotas =
                {
                    MaxDepth = 32,
                    MaxStringContentLength = 8192,
                    MaxArrayLength = 16384,
                    MaxBytesPerRead = 4096,
                    MaxNameTableCharCount = 16384
                },
                ReliableSession =
                {
                    Ordered = true,
                    InactivityTimeout = new TimeSpan(0, 10, 0),
                    Enabled = false
                },
                Security =
                {
                    Mode = SecurityMode.None,
                    Transport =
                    {
                        ClientCredentialType = HttpClientCredentialType.Windows,
                        ProxyCredentialType = HttpProxyCredentialType.None,
                        Realm = ""
                    },
                    Message =
                    {
                        ClientCredentialType = MessageCredentialType.Windows,
                        NegotiateServiceCredential = true,
                        EstablishSecurityContext = true
                    }
                }
            };

            return binding;
        }

    }
}
