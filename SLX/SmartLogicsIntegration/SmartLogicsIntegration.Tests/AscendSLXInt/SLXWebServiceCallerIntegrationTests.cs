﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartLogicsIntegration.Domain;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Tests.AscendSLXInt
{
    [TestClass]
    public class SLXWebServiceCallerIntegrationTests
    {
        #region Fields

        private readonly SmartLogicsIntegrationEntities _entities = new SmartLogicsIntegrationEntities();

        private ISLXOrderManager SLXOrderManager { get; set; }

        #endregion

        [TestInitialize]
        public void SmartLogixWebServiceCallerTestSetup()
        {
           SLXOrderManager = new SLXOrderManager(new SLXConfigurationManager(), new AscendDataLookup(), new SLXMasterDataPusher(new SLXConfigurationManager()));
        }

        [TestMethod]
        public void PingRabbitMQTest()
        {
            int messageReceived = SmartLogixWebServiceCaller.PingRabbitMQ();

            Assert.AreEqual(messageReceived, 1);
        }

        [TestMethod]
        public void Ping2RabbitMQTest()
        {
            int messageReceived = SmartLogixWebServiceCaller.PingRabbitMQ2();

            Assert.AreEqual(messageReceived, 1);
        }



        #region Master Data Push Tests'

        [TestMethod]
        public void UpdateProductMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateProductsService(out returnValue, DateTime.Now, 1502);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateStandardAcctMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateCustomerService(out returnValue, DateTime.Now, 1000);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateShipToMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateShipToService(out returnValue, DateTime.Now, 4470);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateDriverMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateDriverService(out returnValue, DateTime.Now, 1001);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateFleetVehicleMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateFleetVehiclesService(out returnValue, DateTime.Now, 1005);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateTankMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateTanksService(out returnValue, DateTime.Now, 1006);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateTruckMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateTruckService(out returnValue, DateTime.Now, 1000);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateSupplierMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateSupplierService(out returnValue, DateTime.Now, 1036);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateSupplyPtMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateSupplyPtService(out returnValue, DateTime.Now, 1032);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateSupplierSupplyPtMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateSupplierSupplyPtService(out returnValue, DateTime.Now, 3934);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateInsiteMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateInsiteService(out returnValue, DateTime.Now, 1002);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateSalesOrderMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateSalesOrderService(out returnValue);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        //[TestMethod]
        //public void InsertProductMQTest()
        //{
        //    int returnValue = 0;
        //    Exception error = null;
        //    try
        //    {
        //        SmartLogixWebServiceCaller.CallInsertProductService(returnValue,);
        //    }
        //    catch (Exception ex)
        //    {
        //        error = ex;
        //    }

        //    Assert.IsNull(error);
        //}

        [TestMethod]
        public void UpdateDriverBasicTest()
        {
            // Arrange
            Exception exception = null;
            var firstDriver = _entities.Drivers.First(d => d.DriverID == 1001);
            firstDriver.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();

            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateDriverService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateSalesAliasProductBasicTest()
        {
            // Arrange
            Exception exception = null;
            var previousModifyDateTime = _entities.SalesAliases.First(d => d.SalesAliasID == 1441).LastModifiedDtTm;
            var firstAlias = _entities.SalesAliases.First(d => d.SalesAliasID == 1441);
            firstAlias.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            var returnValue = 0;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateProductsService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstAlias.LastModifiedDtTm = previousModifyDateTime;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdatePurchAliasProductBasicTest()
        {
            // Arrange
            Exception exception = null;
            var previousModifyDateTime = _entities.PurchAlias.First(d => d.PurchAliasID == 1300).LastModifiedDtTm;
            var firstAlias = _entities.PurchAlias.First(d => d.PurchAliasID == 1300);
            firstAlias.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            var returnValue = 0;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateProductsService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstAlias.LastModifiedDtTm = previousModifyDateTime;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateTruckBasicTest()
        {
            // Arrange
            Exception exception = null;
            var previous = _entities.Vehicles.First(v => v.VehicleID == 1001).LicPlateNo;
            var firstVehicle = _entities.Vehicles.First(v => v.VehicleID == 1001);
            firstVehicle.LicPlateNo = "Test";
            firstVehicle.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            var returnValue = 0;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateTruckService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstVehicle.LicPlateNo = previous;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateCustomerBasicTest()
        {
            // Arrange
            Exception exception = null;
            var previousModifyDateTime = _entities.ARStandardAccts.First(a => a.StandardAcctID == 1031).LastModifiedDtTm;
            var firstAccount = _entities.ARStandardAccts.First(a => a.StandardAcctID == 1031);
            firstAccount.LastModifiedDtTm = DateTime.Now;
            firstAccount.CustomerName = "Test";
            _entities.SaveChanges();
            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateCustomerService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstAccount.LastModifiedDtTm = previousModifyDateTime;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateShipToBasicTest()
        {
            // Arrange
            Exception exception = null;
            var descr = _entities.ARShiptoes.First(s => s.ShiptoID == 2197).LongDescr;
            _entities.ARShiptoes.First(s => s.ShiptoID == 2197).LongDescr = "Test";
            var firstShipTo = _entities.ARShiptoes.First(s => s.ShiptoID == 2197);
            firstShipTo.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            var returnValue = 0;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateShipToService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstShipTo.LongDescr = descr;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateTankBasicTest()
        {
            // Arrange
            Exception exception = null;
            var previousModifyDateTime = _entities.ARShipTo_Tank.First(v => v.TankID == 1005).LastModifiedDtTm;
            var firstVehicle = _entities.ARShipTo_Tank.First(v => v.TankID == 1005);
            firstVehicle.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateTanksService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstVehicle.LastModifiedDtTm = previousModifyDateTime;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateVehicleBasicTest()
        {
            // Arrange
            Exception exception = null;
            var descr = _entities.ARShipTo_Tank.First(v => v.TankID == 1011).Descr = "Test";
            var firstVehicle = _entities.ARShipTo_Tank.First(v => v.TankID == 1011);
            firstVehicle.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();
            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateFleetVehiclesService(out returnValue, DateTime.Now.AddMinutes(-10),
                    null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                firstVehicle.Descr = descr;
                _entities.SaveChanges();
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void UpdateSalesOrderBasicTest()
        {
            // Arrange
            Exception exception = null;

            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateSalesOrderService(out returnValue);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        #endregion

        #region NSO Tests

        [TestMethod]
        public void ReceiveNewSalesOrderBasicTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 2000;

            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = null;
            model.DriverId = null;
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = null;
            model.VehicleType = null;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {
                returnValue = SLXOrderManager.NewSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void ReceiveNewSalesOrderInventorySiteTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 200;
            product1.InventorySiteId = "PT";

            var product2 = new ProductModel();
            product2.Date = DateTime.Now;
            product2.ProductId = "003";
            product2.Quantity = 200;
            product2.InventorySiteId = "PT";

            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = null;
            model.DriverId = null;
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = null;
            model.VehicleType = null;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);
            model.ProductList.Add(product2);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {                
                returnValue = SLXOrderManager.NewSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void ReceiveNewSalesOrderSupplierSupplyPtTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 200;
            product1.SupplierId = "APX";
            product1.SupplyPtId = "44";

            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = null;
            model.DriverId = null;
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = null;
            model.VehicleType = null;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {                
                returnValue = SLXOrderManager.NewSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void ReceiveNewSalesOrderPassInPriceTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 200;
            product1.Price = 4.995;

            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = null;
            model.DriverId = null;
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = null;
            model.VehicleType = null;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {                
                returnValue = SLXOrderManager.NewSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        [TestMethod]
        public void ReceiveNewSalesOrderPassInEverythingTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 200;
            product1.SupplierId = "APX";
            product1.SupplyPtId = "44";
            product1.Price = 4.995;

            var product2 = new ProductModel();
            product2.Date = DateTime.Now;
            product2.ProductId = "003";
            product2.Quantity = 200;
            product2.InventorySiteId = "PT";

            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = null;
            model.DriverId = "1001";
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = "36918";
            model.VehicleType = VehicleType.Tank;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);
            model.ProductList.Add(product2);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {                
                returnValue = SLXOrderManager.NewSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        #endregion

        #region MSO Tests

        [TestMethod]
        public void ReceiveModifiedSalesOrderBasicTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 2000;
            product1.NetQuantity = 2000;
            product1.AssetId = "333";
            //product1.SupplyPtId = "34";
            //product1.SupplierId = "2160";
            product1.Date = DateTime.Now;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            boughtProduct.Quantity = 1995;
            product1.BoughtAsProducts.Add(boughtProduct);
            var boughtProduct2 = new BoughtAsProduct();
            boughtProduct2.ProductId = "101";
            boughtProduct2.Quantity = 5;
            product1.BoughtAsProducts.Add(boughtProduct2);
            var model = new SalesOrderModel();

            model.AscendSalesOrderNumber = "54460";
            model.DriverId = null;
            model.SLXSalesOrderNumber = "123";
            model.ShipToId = "001";
            model.StandardAcctId = "000001";
            model.VehicleId = "PGA001";
            model.VehicleType = VehicleType.Truck;
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(product1);

            var returnValue = new SmartLogixResponse();

            // Act
            try
            {                
                returnValue = SLXOrderManager.ModifiedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        #endregion

        #region CSO Tests

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderBasicTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 2000;
            product1.NetQuantity = 2000;
            product1.InventorySiteId = "PT";
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct1 = new BoughtAsProduct();
            boughtProduct1.ProductId = "002";
            product1.BoughtAsProducts.Add(boughtProduct1);
            boughtProduct1.Quantity = 1700;


            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst7";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);

            model.AscendSalesOrderNumber = "54465";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }


        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderInventorySiteTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 200;
            product1.NetQuantity = 203;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            product1.BoughtAsProducts.Add(boughtProduct);

            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);
            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "72985";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderPassInPriceTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 200;
            product1.NetQuantity = 203;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            product1.BoughtAsProducts.Add(boughtProduct);

            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);
            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "72985";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderSupplierSupplyPtTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 200;
            product1.NetQuantity = 203;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            product1.BoughtAsProducts.Add(boughtProduct);

            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);
            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "72985";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderMultipleBOLTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 200;
            product1.NetQuantity = 203;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            product1.BoughtAsProducts.Add(boughtProduct);

            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);
            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "72985";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderPassInEverythingTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 2000;
            product1.NetQuantity = 2000;
            product1.AssetId = "333";
            //product1.SupplyPtId = "34";
            //product1.SupplierId = "2160";
            product1.Date = DateTime.Now;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            boughtProduct.Quantity = 1995;
            product1.BoughtAsProducts.Add(boughtProduct);
            var boughtProduct2 = new BoughtAsProduct();
            boughtProduct2.ProductId = "101";
            boughtProduct2.Quantity = 5;
            product1.BoughtAsProducts.Add(boughtProduct2);

            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";
            bol1.VehicleId = "8";


            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);
            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "54460";
            model.UserId = "UnitTest";
            model.OrderDeliveryDate = DateTime.Now;
            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderThatNeedsReorganizationTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "003";
            product1.Quantity = 2000;
            product1.NetQuantity = 2000;
            product1.AssetId = "333";
            //product1.SupplyPtId = "34";
            //product1.SupplierId = "2160";
            product1.Date = DateTime.Now;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "003";
            boughtProduct.Quantity = 1995;
            product1.BoughtAsProducts.Add(boughtProduct);


            //ProductModel product2 = new ProductModel();
            //product2.Date = DateTime.Now;
            //product2.ProductId = "1400";
            //product2.Quantity = 200;
            //product2.NetQuantity = 200;

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";
            bol1.VehicleId = "8";


            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);

            var bol2 = new BOLModel();
            bol2.BOLDate = DateTime.Now;
            bol2.BOLNumber = "SLX-AscendTst7";
            bol2.ShipToId = "001";
            bol2.StandardAcctId = "000001";
            bol2.VehicleId = "8";

            var boughtProduct2 = new BoughtAsProduct();
            boughtProduct2.ProductId = "101";
            boughtProduct2.Quantity = 5;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            product1.BoughtAsProducts.Add(boughtProduct2);

            bol2.Products = new List<ProductModel>();
            bol2.Products.Add(product1);

            //bol1.Products.Add(product2);

            model.AscendSalesOrderNumber = "54460";
            model.UserId = "UnitTest";
            model.OrderDeliveryDate = DateTime.Now;
            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        /// <summary>
        ///     In order for this test to work you will need to populate a value AscendSalesOrderNumber that has not already been
        ///     completed
        /// </summary>
        [TestMethod]
        public void ReceieveCompletedSalesOrderBoughtAsSoldAsSameTest()
        {
            // Arrange
            Exception exception = null;

            var product1 = new ProductModel();
            product1.Date = DateTime.Now;
            product1.ProductId = "002";
            product1.Quantity = 200;
            product1.NetQuantity = 203;
            product1.BoughtAsProducts = new List<BoughtAsProduct>();
            var boughtProduct = new BoughtAsProduct();
            boughtProduct.ProductId = "002";
            product1.BoughtAsProducts.Add(boughtProduct);

            var model = new CompletedSalesOrderModel();

            var bol1 = new BOLModel();
            bol1.BOLDate = DateTime.Now;
            bol1.BOLNumber = "SLX-AscendTst6";
            bol1.ShipToId = "001";
            bol1.StandardAcctId = "000001";

            bol1.Products = new List<ProductModel>();
            bol1.Products.Add(product1);

            model.AscendSalesOrderNumber = "24405";

            model.BOLS = new List<BOLModel>();
            model.BOLS.Add(bol1);

            // Act
            try
            {
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }

        #endregion

        [TestMethod]
        public void UpdateCarrierMQTest()
        {
            int returnValue = 0;
            Exception error = null;
            try
            {
                SmartLogixWebServiceCaller.CallUpdateCarrierService(out returnValue, DateTime.Now, 1001);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.IsNull(error);
        }

        [TestMethod]
        public void UpdateCarrierBasicTest()
        {
            // Arrange
            Exception exception = null;
            var firstDriver = _entities.Drivers.First(d => d.DriverID == 1001);
            firstDriver.LastModifiedDtTm = DateTime.Now;
            _entities.SaveChanges();

            int returnValue;

            // Act
            try
            {
                SmartLogixWebServiceCaller.CallUpdateCarrierService(out returnValue, DateTime.Now.AddMinutes(-10), null);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            // Assert
            Assert.IsNull(exception);
        }
    }
}
