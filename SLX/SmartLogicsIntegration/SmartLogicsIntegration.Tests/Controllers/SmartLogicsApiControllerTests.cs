﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Controllers;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Tests.Controllers
{
    [TestClass]
    public class SmartLogixApiControllerTests
    {
        private SmartLogixApiController _apiController { get; set; }
        private Mock<ISLXOrderManager> SLXOrderManager { get; set; }
        private Mock<ISLXMasterDataPusher> SLXMasterDataPusher { get; set; }

        [TestInitialize]
        public void ApiControllerTestSetup()
        {
            SLXOrderManager = new Mock<ISLXOrderManager>();
            SLXMasterDataPusher = new Mock<ISLXMasterDataPusher>();
            _apiController = new SmartLogixApiController(SLXOrderManager.Object, SLXMasterDataPusher.Object);
        }

        [TestMethod]
        public void NewSalesOrderBasicTest()
        {
            var model = new SalesOrderModel();
            model.SLXSalesOrderNumber = "12345";
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(new ProductModel {ProductId = "Product1", Quantity = 23, Date = DateTime.Now});
            model.ProductList.Add(new ProductModel {ProductId = "Product2", Quantity = 2, Date = DateTime.Now});
            model.ProductList.Add(new ProductModel {ProductId = "Product3", Quantity = 10, Date = DateTime.Now});

            // Call smart logics web service
            using (var client = new WebClient())
            {
                //string uri = "http://SmartLogixintegration.azurewebsites.net/SmartLogixApi/NewSalesOrder";
                var uri = "http://localhost:53510/SmartLogixApi/NewSalesOrder";
                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(model);
                //string serialisedData =
                //   "{\"SLXSalesOrderNumber\":\"D0000000651\",\"AscendSalesOrderNumber\":null,\"ProductList\":[{\"ProductId\":\"301\",\"Quantity\":15,\"Date\":\"2015-11-24T10:15:21.6241212-05:00\",\"TotalPrice\":0.0,\"Tax\":0.0,\"CompoundTax\":0.0,\"Freight\":0.0}]},";

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                if (result != null && result.ResponseStatus != 1)
                {
                    // Something went wrong
                    throw new ApplicationException("No Response from Server");
                }
            }
        }

        [TestMethod]
        public void PingIntegrationBasicTest()
        {
            var model = new SalesOrderModel();
            model.SLXSalesOrderNumber = "12345";
            model.ProductList = new List<ProductModel>();
            model.ProductList.Add(new ProductModel {ProductId = "Product1", Quantity = 23, Date = DateTime.Now});
            model.ProductList.Add(new ProductModel {ProductId = "Product2", Quantity = 2, Date = DateTime.Now});
            model.ProductList.Add(new ProductModel {ProductId = "Product3", Quantity = 10, Date = DateTime.Now});

            // Call smart logics web service
            using (var client = new WebClient())
            {
                //string uri = "http://SmartLogixintegration.azurewebsites.net/SmartLogixApi/Ping";
                var uri = "http://localhost:53510/SmartLogixApi/Ping";
                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(model);

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                if (result != null && result.ResponseStatus != 1)
                {
                    // Something went wrong
                    throw new ApplicationException("No Response from Server");
                }
            }
        }

        [TestMethod]
        public void PingTest()
        {
            // Act
            string response = _apiController.Ping();

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(response, "Ping Successful!");
        }
    }
}
