﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartLogixIntegration.AscendSLXInt.Models;
using Moq;
using SmartLogixIntegration.Services;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Tests.Services
{
    /// <summary>
    /// All of these tests are written with the assumption that VALID lookups come from the Demo409 database.  However, the direct database connection is not actually required.
    /// </summary>
    [TestClass]
    public class SLXOrderManagerTests
    {

        #region Fields

        private SLXOrderManager SLXOrderManagerUnderTest { get; set; }

        private SalesOrderModel SalesOrderModelInTest { get; set; }

        private CompletedSalesOrderModel CompletedSalesOrderModelInTest { get; set; }

        private ProductModel ProductModelInTest { get; set; }

        private BoughtAsProduct BoughtAsProductInTest { get; set; }

        private SmartLogixResponse TestMethodResponse { get; set; }

        private BOLModel BOLModelInTest { get; set; }

        private string VehicleId = "1000";
        private string StandardAcctId = null;
        private string ShipToId = null;
        private string InventoryAdjustmentSiteId = "1001";
        private string InventorySiteId = "1001";
        private string SalesAliasId = "1399";
        private string PurchAliasId = "1301";
        private string SupplierSupplyPtId = "3934";

        #endregion

        #region Test Initialization

        [TestInitialize]
        public void SLXOrderManagerTestSetup()
        {
            BoughtAsProductInTest = new BoughtAsProduct
            {
                ProductId = "002",
                SupplierId = "20058",
                SupplyPtId = "64",
                InventorySiteId = "PT",
                BOLNumber = "TestBOL",
                BOLDate = DateTime.Now
            };

            ProductModelInTest = new ProductModel
            {
                ProductId = "002",
                SupplierId = "20058",
                SupplyPtId = "64",
                InventorySiteId = "PT",
                Quantity = 200,
                NetQuantity = 200
            };

            ProductModelInTest.BoughtAsProducts = new List<BoughtAsProduct>();

            SalesOrderModelInTest = new SalesOrderModel
            {
                AscendSalesOrderNumber = "12345",
                StandardAcctId = "000001",
                ShipToId = "Primary",
                DriverId = "1001",
                VehicleId = "0",
                UserId = "SystemInTest",
                IsInventoryAdjustment = false,
                VehicleType = VehicleType.Truck
            };

            SalesOrderModelInTest.ProductList = new List<ProductModel>();
            SalesOrderModelInTest.ProductList.Add(ProductModelInTest);

            BOLModelInTest = new BOLModel
            {

            };

            CompletedSalesOrderModelInTest = new CompletedSalesOrderModel
            {
                AscendSalesOrderNumber = "12345",
                UserId = "SystemInTest",
                IsNetBilling = false
            };

            CompletedSalesOrderModelInTest.BOLS = new List<BOLModel>();
            CompletedSalesOrderModelInTest.BOLS.Add(BOLModelInTest);
            TestMethodResponse = new SmartLogixResponse();

            Mock<ISLXConfigurationManager> mockSLXConfigurationManager = new Mock<ISLXConfigurationManager>();

            Mock<IAscendDataLookup> mockAscendDataLookup = new Mock<IAscendDataLookup>();
            mockAscendDataLookup.Setup(m => m.LookupVehicleId(SalesOrderModelInTest.VehicleId)).Returns(VehicleId);
            mockAscendDataLookup.Setup(
                m =>
                    m.LookupShipToIdAndStandardAcctId(ref ShipToId, ref StandardAcctId, SalesOrderModelInTest.StandardAcctId,
                        SalesOrderModelInTest.ShipToId)).Callback(() => {
                                                                            ShipToId = "1019";
                                                                            StandardAcctId = "1000";
                                                                        });

            mockAscendDataLookup.Setup(m => m.LookupInventoryAdjustmentSite(SalesOrderModelInTest.VehicleId)).Returns(InventoryAdjustmentSiteId);
            mockAscendDataLookup.Setup(m => m.IsValidDriverId(SalesOrderModelInTest.DriverId)).Returns(true);
            mockAscendDataLookup.Setup(m => m.LookupInventorySiteId(ProductModelInTest.InventorySiteId)).Returns(InventorySiteId);
            mockAscendDataLookup.Setup(m => m.LookupSalesAliasId(ProductModelInTest.ProductId)).Returns(SalesAliasId);
            mockAscendDataLookup.Setup(m => m.LookupPurchAliasId(BoughtAsProductInTest.ProductId)).Returns(PurchAliasId);
            mockAscendDataLookup.Setup(m => m.LookupSupplierSupplyPtId(BoughtAsProductInTest.SupplierId, BoughtAsProductInTest.SupplyPtId)).Returns(SupplierSupplyPtId);
            mockAscendDataLookup.Setup(m => m.IsValidProductAtSupplierSupplyPt(BoughtAsProductInTest.ProductId, SupplierSupplyPtId)).Returns(true);

            Mock<ISLXMasterDataPusher> mockSLXMasterDataPusher = new Mock<ISLXMasterDataPusher>();



            SLXOrderManagerUnderTest = new SLXOrderManager(mockSLXConfigurationManager.Object, mockAscendDataLookup.Object, mockSLXMasterDataPusher.Object);

        }

        #endregion

        #region ValidateProduct Method Tests

        [TestMethod]
        public void ValidateProductValidProductTest()
        {

        }

        #endregion

        #region ValidateBoughtAsProduct Method Tests

        [TestMethod]
        public void ValidateBoughtAsProductValidProductTest()
        {


        }

        #endregion

        #region ValidateSalesOrder Method Tests

        [TestMethod]
        public void ValidateSalesOrderBasicValidSalesOrderTest()
        {
            // Arrange

            
            // Act
            TestMethodResponse = SLXOrderManagerUnderTest.ValidateSalesOrder(SalesOrderModelInTest);

            // Assert
            Assert.AreEqual(1, TestMethodResponse.ResponseStatus);
            Assert.AreEqual("Success", TestMethodResponse.ResponseMessage);
        }

        [TestMethod]
        public void ValidateSalesOrderValidInventoryAdjustmentSalesOrderTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidVehicleIdTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidStandardAcctShipToAndVehicleIdTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidIsInventoryAdjustmentFlagTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidStandardAcctShipToComboTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidDriverIdTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderInvalidProductDataTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderEverythingInvalidTest()
        {

        }

        [TestMethod]
        public void ValidateSalesOrderMultipleThingsInvalidTest()
        {

        }

        #endregion

        #region ValidateCompletedSalesOrder Method Tests

        [TestMethod]
        public void ValidateCompletedSalesOrderBasicValidCSOTest()
        {
            CompletedSalesOrderModel model = new CompletedSalesOrderModel();
        }

        [TestMethod]
        public void ValidateCompletedSalesOrderValidSalesOrderStatus1003Test()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderValidSalesOrderStatus1005Test()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidVehicleIdTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidSalesOrderNumberTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidBOLListTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidSalesOrderStatusTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderNoVehicleIDTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidStandardAcctAndShipToAndNotInventoryAdjustmentTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidVehicleIdOnInventoryAdjustmentTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidStandardAcctAndShipToTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidDriverIdTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderInvalidProductDataTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderEverythingInvalidTest()
        {

        }

        [TestMethod]
        public void ValidateCompletedSalesOrderMultipleThingsInvalidTest()
        {

        }

        #endregion

    }
}
