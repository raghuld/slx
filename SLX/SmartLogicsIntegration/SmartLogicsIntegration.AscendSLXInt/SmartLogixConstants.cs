﻿namespace SmartLogixIntegration.AscendSLXInt
{
    /// <summary>
    /// This class includes all other constants necessary in the SLX Integration
    /// </summary>
    public static class SmartLogixConstants
    {
        public static readonly string FirestreamSecretToken = "F1reStream@()BigSecretToken";  
        public static readonly string AscendDatabaseName = "Ascend";
        public static readonly string SmartLogixWebServiceName = "SmartLogix";
        public static readonly string SmartLogixPushWebServiceName = "SmartLogixPush";
        public static readonly string RabbitMQServiceName = "SmartLogixRabbitMQ";
    }

    /// <summary>
    /// This class includes all the master data types
    /// </summary>
    public static class MasterDataType
    {
        public static readonly string DriverType = "Driver";
        public static readonly string VehicleType = "Vehicle";
        public static readonly string TankType = "Tank";
        public static readonly string TruckType = "Truck";
        public static readonly string CustomerType = "Customer";
        public static readonly string ShipToType = "ShipTo";
        public static readonly string ProductType = "Product";
        public static readonly string UpdatedSalesOrderType = "UpdatedSalesOrder";
        public static readonly string NewSalesOrderType = "NewSalesOrder";
        public static readonly string InventorySiteType = "InventorySite";
        public static readonly string SupplyPtType = "SupplyPt";
        public static readonly string SupplierType = "Supplier";
        public static readonly string SupplierSupplyPtType = "SupplierSupplyPt";
        public static readonly string InsiteType = "Insite";
        public static readonly string CarrierType = "Carrier";
    }

    /// <summary>
    /// This class includes all the ascend table definitions
    /// </summary>
    public static class AscendTable
    {
        public static readonly string PersonDataTableName = "Person";
        public static readonly string ARShipToDataTableName = "ARShipto";
        public static readonly string StateDataTableName = "StateProv";
        public static readonly string CityDataTableName = "City";
        public static readonly string PaymentStreamContactCreditCardDataTableName = "PaymentStreamContactCreditCard";
        public static readonly string AddressDataTableName = "Address";
        public static readonly string PaymentStreamCreditCardLogDataTableName = "PaymentStreamCreditCardLog";
        public static readonly string AccountDataTableName = "Account";
        public static readonly string ARStandardAcctDataTableName = "ARStandardAcct";
        public static readonly string ARShipToAddressDataTableName = "ARShipToAddress";
        public static readonly string PaymentStreamHdrTransactionDataTableName = "PaymentStreamHdrTransaction";
        public static readonly string ARInvoiceDataTableName = "ARInvoice";
        public static readonly string ARReceiptTrxDataTableName = "ARReceiptTrx";
        public static readonly string ARInvoiceCCDataTableName = "ARInvoiceCC";
        public static readonly string CreditCardNoticeDataTableName = "CreditCardNotice";
        public static readonly string ARReceiptDataTableName = "ARReceipt";
        public static readonly string ARReceiptSessionDataTableName = "ARReceiptSession";
        public static readonly string ThirdPartyWebService = "ThirdPartyWebService";
        public static readonly string PaymentStreamTransactionDataTableName = "PaymentStreamTransaction";
        public static readonly string DriversDataTableName = "Drivers";
        public static readonly string SalesAliasTableName = "SalesAlias";
        public static readonly string PurchAliasTableName = "PurchAlias";
        public static readonly string ProdContTableName = "ProdCont";
        public static readonly string DriversPhoneTableName = "DriversPhone";
        public static readonly string HzrdMaterialsInstructionTableName = "HzrdMaterialsInstruction";
        public static readonly string VehicleDataTableName = "Vehicle";
        public static readonly string ARShipToTankProductDataTableName = "ARShipTo_TankProduct";
        public static readonly string ARShipToTankDataTableName = "ARShipTo_Tank";
        public static readonly string SmartLogixLogDataTableName = "SmartLogixPushLog";
        public static readonly string SmartLogixPriceUpdateRequiredDataTableName = "SmartLogixPriceUpdateRequired";
        public static readonly string OrderHdrDataTableName = "OrderHdr";
        public static readonly string OrderItemDataTableName = "OrderItem";
        public static readonly string InSiteDataTableName = "INSite";
        public static readonly string SupplierSupplyPtDataTableName = "SupplierSupplyPt";
        public static readonly string BillingItemDataTableName = "BillingItem";
        public static readonly string UOMTableName = "UOM";
        public static readonly string ProdTypeTableName = "ProdType";
        public static readonly string SupplierDataTableName = "Supplier";
        public static readonly string SupplyPtDataTableName = "SupplyPt";
        public static readonly string PurchRackDataTableName = "PurchRack";
        public static readonly string SmartLogixSettings = "SmartLogixSettings";
        public static readonly string CarrierTablename = "carrier";
    }
}
