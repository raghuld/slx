﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLogixIntegration.AscendSLXInt
{
  static  class SQLHelper
    {

        public static DataTable ExecuteAndGetDataTable(String Connstr, string sql)
        {
            using (SqlConnection Conn = new SqlConnection(Connstr))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable Dt = new DataTable();
                Conn.Open();
                adapter.SelectCommand = new SqlCommand(sql, Conn);
                adapter.Fill(Dt);
                return Dt;
            }
        }

       


    }
}
