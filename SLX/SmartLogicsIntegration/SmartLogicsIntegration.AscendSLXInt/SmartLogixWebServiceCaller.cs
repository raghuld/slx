﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SmartLogixIntegration.AscendSLXInt.Models;



namespace SmartLogixIntegration.AscendSLXInt
{
    /// <summary>
    ///     This class contains all the CLR Methods that call web services when an entity changes in Ascend SQL
    /// </summary>
    public static class SmartLogixWebServiceCaller
    {
        #region Public Methods

        public static int PingRabbitMQ()
        {
            Debug.WriteLine("Start of Ping");
            var factory = new ConnectionFactory
            {
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare("masterdata",
                                            "topic", true);

                    var routingKey = "ping";
                    string message = "Ping Queue...";
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "masterdata",
                                         routingKey: routingKey,
                                         basicProperties: null,
                                         body: body);
                }
            }

            return 1;
        }

        public static int PingRabbitMQ2()
        {
            Debug.WriteLine("Start of Ping2");
            var factory = new ConnectionFactory
            {
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "masterdata",
                                            type: "topic", durable: true);

                    var routingKey = "ping2";
                    string message = "Ping 2 Queue...";
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "masterdata",
                                         routingKey: routingKey,
                                         basicProperties: null,
                                         body: body);
                }
            }

            return 1;
        }

        #region CustomerShipToServices

                /// <summary>
                ///     Calls the update sales order service.
                /// </summary>
                /// <param name="returnValue">The return value.</param>
        public static void CallUpdateSalesOrderService(out int returnValue)
        {
            var salesOrderList = new List<SalesOrderModel>();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            DataTable priceUpdateDataTable;
            using (var cn = new SqlConnection(connectionString))
            {
                // Lookup all the orders where a price update is required
                string queryString =
                    "SELECT distinct SysTrxNo from dbo." +
                    AscendTable.SmartLogixPriceUpdateRequiredDataTableName;

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (!cn.State.Equals(ConnectionState.Open))
                        {
                            cn.Open();
                        }

                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SmartLogixPriceUpdateRequiredDataTableName);
                        priceUpdateDataTable =
                            myDataSet.Tables[AscendTable.SmartLogixPriceUpdateRequiredDataTableName];
                    }
                }
            }

            foreach (DataRow row in priceUpdateDataTable.Rows)
            {
                long sysTrxNo = Convert.ToInt64(row["SysTrxNo"]);
                bool isValidSmartLogixOrder = IsValidSmartLogixOrder(sysTrxNo);

                // If it is a valid sales order that was created from SLX then push the price update
                if (isValidSmartLogixOrder)
                {
                    // Build up a sales order model that is needed to push to SLX
                    SalesOrderModel model = BuildSalesOrderModel(sysTrxNo);
                    if (model != null)
                    {
                        salesOrderList.Add(model);
                    }
                }

                using (var cn = new SqlConnection(connectionString))
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }

                    // Clear the row out of the smartlogixpriceupdaterequired table since it no longer needs a price update
                    var deleteFromPriceUpdateRequired =
                        "DELETE FROM SmartLogixPriceUpdateRequired WHERE SysTrxNo = @SysTrxNo";

                    using (var cmd = new SqlCommand(deleteFromPriceUpdateRequired, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        cmd.ExecuteNonQuery();
                    }
                }
            }

            foreach (SalesOrderModel salesOrder in salesOrderList)
            {                
                string serializedData = JsonConvert.SerializeObject(salesOrder, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("salesorder", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert ship to service to insert a new ship to in SLX
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="standardAcctNo">The standard acct no.</param>
        /// <param name="name">The name.</param>
        /// <param name="address1">The address1.</param>
        /// <param name="address2">The address2.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="fax">The fax.</param>
        /// <param name="poRequired">The po required.</param>
        /// <param name="poNumber">The po number.</param>
        /// <param name="active">The active.</param>
        /// <param name="onHold">The on hold.</param>
        public static void CallInsertShipToService(string id, string code, string standardAcctNo, string name,
            string address1, string address2, string city, string state, string zip,
            string phone, string fax, string poRequired, string poNumber, string active, string onHold)
        {
            var shipToList = new ShipToList();
            var shipTo = new ShipTo
            {
                ShipToId = code,
                CustomerId = standardAcctNo
            };
            if (id != null)
            {
                shipTo.PeerId = Convert.ToInt32(id);
            }
            if (!string.IsNullOrEmpty(name))
            {
                shipTo.Name = name;
            }
            if (!string.IsNullOrEmpty(address1))
            {
                shipTo.Address = address1;
            }
            if (!string.IsNullOrEmpty(address2))
            {
                shipTo.Address2 = address2;
            }
            if (!string.IsNullOrEmpty(city))
            {
                shipTo.City = city;
            }
            if (!string.IsNullOrEmpty(state))
            {
                shipTo.State = state;
            }
            if (!string.IsNullOrEmpty(zip))
            {
                shipTo.Zip = zip;
            }
            if (!string.IsNullOrEmpty(phone))
            {
                shipTo.Phone = phone;
            }
            if (!string.IsNullOrEmpty(fax))
            {
                shipTo.FaxNumber = fax;
            }

            shipTo.PoRequired = poRequired == "Y";

            shipTo.Active = active == "Y";

            if (onHold != null && onHold.Equals("Y"))
            {
                shipTo.IsCreditHold = true;
            }
            else
            {
                shipTo.IsCreditHold = false;
            }

            // Set Default PONumber, currently there is only one POnumber per ship to in Ascend but SLX model allows for multiple
            if (!string.IsNullOrEmpty(poNumber))
            {
                shipTo.PoNumbers = new PurchaseOrderList();
                var orderNumber = new PurchaseOrder
                {
                    PoNumber = poNumber
                };
                shipTo.PoNumbers.Add(orderNumber);
            }

            shipTo.DefaultOrderType = SalesOrderType.Tank;

            shipToList.Add(shipTo);

            if (shipToList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(shipToList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("arshipto", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update ship to service to update the shipto in SLX.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="shipToId">The ship to identifier.</param>
        public static void CallUpdateShipToService(out int returnValue, DateTime lastModifiedDate, int? shipToId)
        {
            var shipToList = new ShipToList();

            // Get driver data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get the ship to and all of its components out of ascend that needs to be updated
                string queryString =
                    "SELECT ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.CLCreditHold AS CreditHold, ARShipTo.Code AS ShipToCode, ARShipTo.LongDescr as ShipToName, " +
                    " ARShipToAddress.Addr1 AS Address1," +
                    " ARShipToAddress.Addr2 AS Address2, City.Code AS CityCode, StateProv.Code AS StateCode, ARShipToAddress.Zip AS Zip, ARShipTo.PrimaryPhone AS Phone, " +
                    " ARShipTo.PrimaryFax as Fax, ARShipTo.PORequired AS PORequired, Dest.PONumber AS PONumber, ARShipTo.Active AS Active FROM dbo." +
                    AscendTable.ARShipToDataTableName +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipto.StandardAcctID = ARStandardAcct.StandardAcctID " +
                    " LEFT OUTER JOIN " + AscendTable.ARShipToAddressDataTableName +
                    " ON ARShipto.ShiptoID = ARShipToAddress.ShipToID and ARShipToAddress.Type = 'P' " +
                    " LEFT OUTER JOIN " + AscendTable.StateDataTableName +
                    " ON ARShipToAddress.StateProvID = StateProv.ID" +
                    " LEFT OUTER JOIN " + AscendTable.CityDataTableName + " ON ARShipToAddress.CityID = City.ID" +
                    " LEFT OUTER JOIN MasterSite" +
                    " ON MasterSite.ShipToID = ARShipTo.ShiptoID" +
                    " LEFT OUTER JOIN Dest" +
                    " ON Dest.DestID = MasterSite.DestID";

                if (shipToId == null)
                {
                    queryString +=
                        " WHERE ARShipto.LastModifiedDtTm >= @LastModified and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }
                else
                {
                    queryString +=
                        " WHERE ARShipto.ShipToID = @ShipToId and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (shipToId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@ShipToId", SqlDbType.Int).Value = shipToId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToDataTableName];
                        // Build shipto list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var shipTo = new ShipTo();
                            if (shipToId != null)
                            {
                                shipTo.PeerId = Convert.ToInt32(shipToId);
                            }
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                shipTo.CustomerId = row["StandardAcctNo"].ToString();
                            }
                            if (row["ShipToCode"] != DBNull.Value)
                            {
                                shipTo.ShipToId = row["ShipToCode"].ToString();
                            }
                            if (row["ShipToName"] != DBNull.Value)
                            {
                                shipTo.Name = row["ShipToName"].ToString();
                            }
                            if (row["Address1"] != DBNull.Value)
                            {
                                shipTo.Address = row["Address1"].ToString();
                            }
                            if (row["Address2"] != DBNull.Value)
                            {
                                shipTo.Address2 = row["Address2"].ToString();
                            }
                            if (row["CityCode"] != DBNull.Value)
                            {
                                shipTo.City = row["CityCode"].ToString();
                            }
                            if (row["StateCode"] != DBNull.Value)
                            {
                                shipTo.State = row["StateCode"].ToString();
                            }
                            if (row["Zip"] != DBNull.Value && !row["Zip"].ToString().Trim().Equals(""))
                            {
                                shipTo.Zip = row["Zip"].ToString();
                            }
                            if (row["Phone"] != DBNull.Value)
                            {
                                shipTo.Phone = row["Phone"].ToString();
                            }
                            if (row["Fax"] != DBNull.Value)
                            {
                                shipTo.FaxNumber = row["Fax"].ToString();
                            }

                            shipTo.PoRequired = row["PORequired"].ToString() == "Y";

                            shipTo.Active = row["Active"].ToString() == "Y";

                            // Set Default PONumber
                            if (row["PONumber"] != DBNull.Value)
                            {
                                shipTo.PoNumbers = new PurchaseOrderList();
                                var orderNumber = new PurchaseOrder {PoNumber = row["PONumber"].ToString()};
                                shipTo.PoNumbers.Add(orderNumber);
                            }

                            if (row["CreditHold"] != DBNull.Value && row["CreditHold"].Equals("Y"))
                            {
                                shipTo.IsCreditHold = true;
                            }
                            else
                            {
                                shipTo.IsCreditHold = false;
                            }

                            shipTo.DefaultOrderType = SalesOrderType.Tank;

                            shipToList.Add(shipTo);
                        }
                    }
                }
            }

            if (shipToList.Any())
            {               
                string serializedData = JsonConvert.SerializeObject(shipToList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("arshipto", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert insite service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        public static void CallInsertInsiteService(string id, string code, string name)
        {
            var insiteList = new WarehouseList();
            var insite = new Warehouse {WarehouseID = code};

            if (!string.IsNullOrEmpty(name))
            {
                insite.Description = name;
            }

            insiteList.Add(insite);

            if (insiteList.Any())
            {                
                string serializedData = JsonConvert.SerializeObject(insiteList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("insite", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update insite service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="insiteId">The insite identifier.</param>
        public static void CallUpdateInsiteService(out int returnValue, DateTime lastModifiedDate, int? insiteId)
        {
            var insiteList = new WarehouseList();

            // Get insite data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get the inventory sites that need to be updated from Ascend
                var queryString =
                    "SELECT Code, LongDescr FROM dbo.Insite";

                if (insiteId == null)
                {
                    queryString += " WHERE LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE SiteID = @SiteId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (insiteId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@SiteID", SqlDbType.Int).Value = insiteId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.InSiteDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.InSiteDataTableName];
                        // Build insite list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var insite = new Warehouse();
                            if (row["Code"] != DBNull.Value)
                            {
                                insite.WarehouseID = row["Code"].ToString();
                            }
                            if (row["LongDescr"] != DBNull.Value)
                            {
                                insite.Description = row["LongDescr"].ToString();
                            }

                            insiteList.Add(insite);
                        }
                    }
                }
            }

            if (insiteList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(insiteList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("insite", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert supplier service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        public static void CallInsertSupplierService(string id, string code, string name)
        {
            var supplierList = new SupplierPeeringList();
            var supplier = new SupplierPeering
            {
                Number = code
            };
            if (id != null)
            {
                supplier.PeerId = Convert.ToInt32(id);
            }
            if (!string.IsNullOrEmpty(name))
            {
                supplier.Name = name;
            }

            supplierList.Add(supplier);

            if (supplierList.Any())
            {               
                string serializedData = JsonConvert.SerializeObject(supplierList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("supplier", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update supplier service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="supplierId">The supplier identifier.</param>
        public static void CallUpdateSupplierService(out int returnValue, DateTime lastModifiedDate, int? supplierId)
        {
            var supplierList = new SupplierPeeringList();

            // Get supplier data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get specific configuration info from the master database
                var queryString =
                    "SELECT Code, Descr FROM dbo.Supplier";

                if (supplierId == null)
                {
                    queryString += " WHERE LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE SupplierID = @SupplierID";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (supplierId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@SupplierID", SqlDbType.Int).Value = supplierId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplierDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplierDataTableName];
                        // Build supplier list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplier = new SupplierPeering();
                            if (supplierId != null)
                            {
                                supplier.PeerId = Convert.ToInt32(supplierId);
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                supplier.Number = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                supplier.Name = row["Descr"].ToString();
                            }

                            supplierList.Add(supplier);
                        }
                    }
                }
            }

            if (supplierList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(supplierList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("supplier", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert supply pt service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        public static void CallInsertSupplyPtService(string id, string code, string name)
        {
            var supplyPtList = new TerminalPeeringList();
            var supplyPt = new TerminalPeering
            {
                Number = code
            };

            if (id != null)
            {
                supplyPt.PeerId = Convert.ToInt32(id);
            }
            if (!string.IsNullOrEmpty(name))
            {
                supplyPt.Name = name;
            }

            supplyPtList.Add(supplyPt);

            if (supplyPtList.Any())
            {               
                string serializedData = JsonConvert.SerializeObject(supplyPtList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("supplypt", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update supply pt service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="supplyPtId">The supply pt identifier.</param>
        public static void CallUpdateSupplyPtService(out int returnValue, DateTime lastModifiedDate, int? supplyPtId)
        {
            var supplyPtList = new TerminalPeeringList();

            // Get supplypt data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get the supplypoints that need to be updated
                var queryString =
                    "SELECT Code, Descr FROM dbo.SupplyPt";

                if (supplyPtId == null)
                {
                    queryString += " WHERE LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE SupplyPtID = @SupplyPtId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (supplyPtId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@SupplyPtID", SqlDbType.Int).Value = supplyPtId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplyPtDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplyPtDataTableName];
                        // Build supplypt list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplyPt = new TerminalPeering();
                            if (supplyPtId != null)
                            {
                                supplyPt.PeerId = Convert.ToInt32(supplyPtId);
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                supplyPt.Number = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                supplyPt.Name = row["Descr"].ToString();
                            }

                            supplyPtList.Add(supplyPt);
                        }
                    }
                }
            }

            if (supplyPtList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(supplyPtList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("supplypt", serializedData);
            }

            returnValue = 1;
        }


        /// <summary>
        ///     Calls the insert supplier supply pt service.
        /// </summary>
        /// <param name="supplierId">The supplier identifier.</param>
        /// <param name="supplyPtId">The supply pt identifier.</param>
        public static void CallInsertSupplierSupplyPtService(string supplierId, string supplyPtId)
        {
            var supplierSupplyPtList = new TerminalSupplierList();
            var supplierSupplyPt = new TerminalSupplier
            {
                SupplierNumber = supplierId,
                TerminalNumber = supplyPtId
            };

            supplierSupplyPtList.Add(supplierSupplyPt);

            if (supplierSupplyPtList.Any())
            {                
                string serializedData = JsonConvert.SerializeObject(supplierSupplyPtList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("suppliersupplypt", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update supplier supply pt service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="supplierSupplyPtId">The supplier supply pt identifier.</param>
        public static void CallUpdateSupplierSupplyPtService(out int returnValue, DateTime lastModifiedDate,
            int? supplierSupplyPtId)
        {
            var supplierSupplyPtList = new TerminalSupplierList();

            // Get supplypt data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get all of the supplier supply point relationships that need to be updated in SLX
                string queryString =
                    "SELECT Supplier.Code AS Supplier, SupplyPt.Code AS SupplyPt FROM dbo.SupplierSupplyPt " +
                    " JOIN Supplier ON Supplier.SupplierId = SupplierSupplyPt.SupplierID" +
                    " JOIN SupplyPt ON SupplyPt.SupplyPtId = SupplierSupplyPt.SupplyPtID";

                if (supplierSupplyPtId == null)
                {
                    queryString += " WHERE LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE SupplierSupplyPtID = @SupplierSupplyPtId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (supplierSupplyPtId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@SupplierSupplyPtID", SqlDbType.Int).Value = supplierSupplyPtId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SupplierSupplyPtDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SupplierSupplyPtDataTableName];
                        // Build suppliersupplypt list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var supplierSupplyPt = new TerminalSupplier();
                            if (row["Supplier"] != DBNull.Value)
                            {
                                supplierSupplyPt.SupplierNumber = row["Supplier"].ToString();
                            }
                            if (row["SupplyPt"] != DBNull.Value)
                            {
                                supplierSupplyPt.TerminalNumber = row["SupplyPt"].ToString();
                            }

                            supplierSupplyPtList.Add(supplierSupplyPt);
                        }
                    }
                }
            }

            if (supplierSupplyPtList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(supplierSupplyPtList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("suppliersupplypt", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert customer service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        /// <param name="address">The address.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip.</param>
        /// <param name="onHold">The on hold.</param>
        public static void CallInsertCustomerService(string id, string code, string name, string address, string city,
            string state, string zip, string onHold)
        {
            var customerList = new CustomerList();
            var customer = new Customer
            {
                CustomerId = code
            };
            if (id != null)
            {
                customer.PeerId = Convert.ToInt32(id);
            }
            if (!string.IsNullOrEmpty(name))
            {
                customer.Name = name;
            }
            if (!string.IsNullOrEmpty(address))
            {
                customer.Address = address;
            }
            if (!string.IsNullOrEmpty(city))
            {
                customer.City = city;
            }
            if (!string.IsNullOrEmpty(state))
            {
                customer.State = state;
            }
            if (!string.IsNullOrEmpty(zip))
            {
                customer.Zip = zip;
            }
            if (onHold != null && onHold.Equals("Y"))
            {
                customer.IsCreditHold = true;
            }
            else
            {
                customer.IsCreditHold = false;
            }

            customerList.Add(customer);

            if (customerList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(customerList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("arstandardacct", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update customer service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="customerId">The customer identifier.</param>
        public static void CallUpdateCustomerService(out int returnValue, DateTime lastModifiedDate, int? customerId)
        {
            var customerList = new CustomerList();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get the customers that were updated and need to be passed to SLX
                string queryString =
                    "SELECT ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARStandardAcct.CreditHold AS CreditHold, ARStandardAcct.CustomerName AS CustomerName, ARShipToAddress.Addr1 AS Address1," +
                    " City.Code AS CityCode, StateProv.Code AS StateCode, ARShipToAddress.Zip AS Zip FROM dbo.ARStandardAcct " +
                    " LEFT OUTER JOIN(SELECT StandardAcctID, MIN(ShipToID) AS ShipToID FROM ARShipTo " +
                    " WHERE Active = 'Y' GROUP BY StandardAcctID) AS ARShipTo ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " LEFT OUTER JOIN ARShipToAddress" +
                    " ON ARShipto.ShiptoID = ARShipToAddress.ShipToID and ARShipToAddress.Type = 'P'" +
                    " LEFT OUTER JOIN StateProv" +
                    " ON ARShipToAddress.StateProvID = StateProv.ID" +
                    " LEFT OUTER JOIN City ON ARShipToAddress.CityID = City.ID";

                if (customerId == null)
                {
                    queryString +=
                        " WHERE ARStandardAcct.LastModifiedDtTm >= @LastModified and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C') and CustomerName is not null";
                }
                else
                {
                    queryString +=
                        " WHERE ARStandardAcct.StandardAcctId = @StandardAcctId and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C') and CustomerName is not null";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (customerId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@StandardAcctId", SqlDbType.Int).Value = customerId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARStandardAcctDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARStandardAcctDataTableName];
                        // Build customer list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var customer = new Customer();

                            if (customerId != null)
                            {
                                customer.PeerId = Convert.ToInt32(customerId);
                            }
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                customer.CustomerId = row["StandardAcctNo"].ToString();
                            }                                                                            
                            if (row["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = row["CustomerName"].ToString();
                            }
                            if (row["Address1"] != DBNull.Value)
                            {
                                customer.Address = row["Address1"].ToString();
                            }
                            if (row["CityCode"] != DBNull.Value)
                            {
                                customer.City = row["CityCode"].ToString();
                            }
                            if (row["StateCode"] != DBNull.Value)
                            {
                                customer.State = row["StateCode"].ToString();
                            }
                            if (row["Zip"] != DBNull.Value && !row["Zip"].ToString().Trim().Equals(""))
                            {
                                customer.Zip = row["Zip"].ToString();
                            }

                            if (row["CreditHold"] != DBNull.Value && row["CreditHold"].Equals("Y"))
                            {
                                customer.IsCreditHold = true;
                            }
                            else
                            {
                                customer.IsCreditHold = false;
                            }

                            customerList.Add(customer);
                        }
                    }
                }
            }

            if (customerList.Any())
            {               
                string serializedData = JsonConvert.SerializeObject(customerList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("arstandardacct", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert tank service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="standardAcctNo">The standard acct no.</param>
        /// <param name="shipToCode">The ship to code.</param>
        /// <param name="barcode">The barcode.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="capacity">The capacity.</param>
        public static void CallInsertTankService(string id, string code, string standardAcctNo, string shipToCode,
            string barcode, string productCode, float? capacity)
        {
            var tankList = new CustomerTankList();
            var tank = new CustomerTank
            {
                TankId = code,
                CustomerId = standardAcctNo,
                ShipToId = shipToCode
            };
            if (id != null)
            {
                tank.CrossReferenceId = id;
            }
            if (!string.IsNullOrEmpty(barcode))
            {
                tank.Barcode = barcode;
            }
            if (!string.IsNullOrEmpty(productCode))
            {
                tank.ProductId = productCode;
            }
            if (capacity != null)
            {
                tank.Capacity = capacity.Value;
            }

            tank.OrderType = SalesOrderType.Tank;

            tankList.Add(tank);

            if (tankList.Any())
            {                
                string serializedData = JsonConvert.SerializeObject(tankList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("tank", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update tanks service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="tankId">The tank identifier.</param>
        public static void CallUpdateTanksService(out int returnValue, DateTime lastModifiedDate, int? tankId)
        {
            var tankList = new CustomerTankList();

            // Get tank data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                string queryString =
                    "SELECT ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.Code AS ShipToCode, ARShipTo_Tank.Code AS TankID, ARShipTo_Tank.BarCode AS BarCode," +
                    " SalesAlias.Code AS ProductID, ARShipTo_Tank.FillCapacity AS FillCapacity FROM dbo." +
                    AscendTable.ARShipToTankDataTableName +
                    " JOIN " + AscendTable.ARShipToDataTableName + " ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID" +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " JOIN " + AscendTable.ARShipToTankProductDataTableName +
                    " ON ARShipTo_Tank.TankID = ARShipTo_TankProduct.TankID" +
                    " JOIN " + AscendTable.SalesAliasTableName +
                    " ON ARShipTo_TankProduct.SalesAliasID = SalesAlias.SalesAliasID ";

                if (tankId == null)
                {
                    queryString +=
                        " WHERE ARShipTo_Tank.LastModifiedDtTm >= @LastModified and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }
                else
                {
                    queryString +=
                        " WHERE ARShipTo_Tank.TankID = @TankId and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (tankId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@TankId", SqlDbType.Int).Value = tankId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                        // Build the tank list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var tank = new CustomerTank
                            {
                                CustomerId = row["StandardAcctNo"].ToString(),
                                ShipToId = row["ShipToCode"].ToString(),
                                TankId = row["TankID"].ToString()
                            };
                            if (tankId != null)
                            {
                                tank.CrossReferenceId = tankId.ToString();
                            }
                            if (row["BarCode"] != DBNull.Value)
                            {
                                tank.Barcode = row["BarCode"].ToString();
                            }
                            if (row["ProductID"] != DBNull.Value)
                            {
                                tank.ProductId = row["ProductID"].ToString();
                            }
                            if (row["FillCapacity"] != DBNull.Value)
                            {
                                tank.Capacity = (float) Convert.ToDecimal(row["FillCapacity"]);
                            }

                            tank.OrderType = SalesOrderType.Tank;

                            tankList.Add(tank);
                        }
                    }
                }
            }

            if (tankList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(tankList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("tank", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert fleet vehicle service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="standardAcctNo">The standard acct no.</param>
        /// <param name="shipToCode">The ship to code.</param>
        /// <param name="description">The description.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="fillCapacity">The fill capacity.</param>
        public static void CallInsertFleetVehicleService(string id, string code, string standardAcctNo,
            string shipToCode, string description, string productCode, float? fillCapacity)
        {
            var vehicleList = new FleetVehicleList();
            var vehicle = new FleetVehicle
            {
                VehicleNumber = code
            };
            if (id != null)
            {
                vehicle.CrossReferenceId = id;
            }
            if (!string.IsNullOrEmpty(standardAcctNo))
            {
                vehicle.CustomerId = standardAcctNo;
            }
            if (!string.IsNullOrEmpty(shipToCode))
            {
                vehicle.ShipToId = shipToCode;
            }
            if (!string.IsNullOrEmpty(description))
            {
                vehicle.Description = description;
            }
            if (!string.IsNullOrEmpty(productCode))
            {
                vehicle.ProductId = productCode;
            }
            if (fillCapacity != null)
            {
                vehicle.TankSize = fillCapacity.Value;
            }

            vehicleList.Add(vehicle);

            if (vehicleList.Any())
            {
                // Call the SLX Integration web service and push data to it from Ascend
                //SmartLogixConfiguration configuration = GetConfigurationValues();

                //using (var client = new WebClient())
                //{
                //    string uri = configuration.Url;


                //    uri += "SmartLogixApi/PushFleetVehicle";

                //    client.Headers.Clear();
                //    client.Headers.Add("content-type", "application/json");
                //    client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                //    string serialisedData = JsonConvert.SerializeObject(vehicleList, new JsonSerializerSettings
                //    {
                //        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                //    });

                //    // HTTP POST
                //    string response = client.UploadString(uri, serialisedData);

                //    var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                //    HandleSLXResponse(id, code, MasterDataType.VehicleType, serialisedData, null, result);

                //    // Log
                //    Logger.WriteToSLXLog(id, code, MasterDataType.VehicleType, result.ResponseStatus, serialisedData,
                //        null, null, false);
                //}

                string serializedData = JsonConvert.SerializeObject(vehicleList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("fleetvehicle", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update fleet vehicles service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="tankId">The tank identifier.</param>
        public static void CallUpdateFleetVehiclesService(out int returnValue, DateTime lastModifiedDate, int? tankId)
        {
            var vehicleList = new FleetVehicleList();

            // Get fleet vehicle data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get all the ARShipTo_Tank records that are considered fleet vehicles that need to be updated in SLX
                string queryString =
                    "SELECT ARStandardAcct.StandardAcctNo AS StandardAcctNo, ARShipTo.Code AS ShipToCode, ARShipTo_Tank.Code AS Code, ARShipTo_Tank.Descr AS Descr," +
                    " SalesAlias.Code AS ProductID, ARShipTo_Tank.FillCapacity AS FillCapacity FROM dbo." +
                    AscendTable.ARShipToTankDataTableName +
                    " JOIN " + AscendTable.ARShipToDataTableName + " ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID" +
                    " JOIN " + AscendTable.ARStandardAcctDataTableName +
                    " ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID" +
                    " JOIN " + AscendTable.ARShipToTankProductDataTableName +
                    " ON ARShipTo_Tank.TankID = ARShipTo_TankProduct.TankID" +
                    " JOIN SalesAlias ON SalesAlias.SalesAliasID = ARShipTo_TankProduct.SalesAliasID";

                if (tankId == null)
                {
                    queryString +=
                        " WHERE ARShipTo_Tank.LastModifiedDtTm >= @LastModified  and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }
                else
                {
                    queryString +=
                        " WHERE ARShipTo_Tank.TankID = @TankId  and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or ARStandardAcct.Type = 'C')";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (tankId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@TankId", SqlDbType.Int).Value = tankId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                        // Build fleet vehicle list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var vehicle = new FleetVehicle();
                            if (tankId != null)
                            {
                                vehicle.CrossReferenceId = tankId.ToString();
                            }
                            if (row["StandardAcctNo"] != DBNull.Value)
                            {
                                vehicle.CustomerId = row["StandardAcctNo"].ToString();
                            }
                            if (row["ShipToCode"] != DBNull.Value)
                            {
                                vehicle.ShipToId = row["ShipToCode"].ToString();
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                vehicle.VehicleNumber = row["Code"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                vehicle.Description = row["Descr"].ToString();
                            }
                            if (row["ProductID"] != DBNull.Value)
                            {
                                vehicle.ProductId = row["ProductID"].ToString();
                            }
                            if (row["FillCapacity"] != DBNull.Value)
                            {
                                vehicle.TankSize = (float) Convert.ToDecimal(row["FillCapacity"]);
                            }

                            vehicleList.Add(vehicle);
                        }
                    }
                }
            }

            if (vehicleList.Any())
            {                
                string serializedData = JsonConvert.SerializeObject(vehicleList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("fleetvehicle", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the update driver service and pushes data to it from Ascend
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        /// <param name="cellPhone">The cell phone.</param>
        public static void CallInsertDriverService(string id, string code, string name, string cellPhone)
        {
            var driverList = new DriverList();
            var driver = new Driver
            {
                DriverId = code
            };

            if (!string.IsNullOrEmpty(name))
            {
                driver.Name = name;
            }
            if (!string.IsNullOrEmpty(cellPhone))
            {
                driver.CellPhone = cellPhone;
            }
            driverList.Add(driver);


            if (driverList.Any())
            {                            
                string serializedData = JsonConvert.SerializeObject(driverList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("driver", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update driver service and pushes data to it from Ascend
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="driverId">The driver identifier.</param>
        public static void CallUpdateDriverService(out int returnValue, DateTime lastModifiedDate, int? driverId)
        {
            var driverList = new DriverList();

            // Get driver data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get all the drivers that need to be updated in SLX
                string driversQueryString =
                    "SELECT Drivers.DriverId, Drivers.FirstName + ' ' + Drivers.LastName AS Name, Drivers.Email, CellPhone.PhoneNo AS CellPhone from dbo." +
                    AscendTable.DriversDataTableName +
                    " AS Drivers " +
                    " LEFT OUTER JOIN " + AscendTable.DriversPhoneTableName +
                    " AS CellPhone ON CellPhone.DriverID = Drivers.DriverID and CellPhone.Type = (SELECT top 1 PhoneNoTypeID FROM PhoneNoType WHERE Code = 'Mobile' or Code = 'Cell')";

                if (driverId == null)
                {
                    driversQueryString += " WHERE Drivers.LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    driversQueryString += " WHERE Drivers.DriverId = @DriverId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(driversQueryString, cn))
                    {
                        if (driverId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@DriverId", SqlDbType.Int).Value = driverId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.DriversDataTableName);
                        DataTable driversDataTable = myDataSet.Tables[AscendTable.DriversDataTableName];
                        // Build driver list
                        foreach (DataRow row in driversDataTable.Rows)
                        {
                            var driver = new Driver();
                            if (row["DriverId"] != DBNull.Value)
                            {
                                driver.DriverId = row["DriverId"].ToString();
                            }
                            if (row["Name"] != DBNull.Value)
                            {
                                driver.Name = row["Name"].ToString();
                            }
                            if (row["CellPhone"] != DBNull.Value)
                            {
                                driver.CellPhone = row["CellPhone"].ToString();
                            }
                            driverList.Add(driver);
                        }
                    }
                }
            }

            if (driverList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(driverList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("driver", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert truck service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="vinNumber">The vin number.</param>
        /// <param name="licPlateNo">The lic plate no.</param>
        public static void CallInsertTruckService(string id, string code, string vinNumber, string licPlateNo)
        {
            var truckList = new TruckList();
            var truck = new Truck
            {
                TruckId = code,
                VIN = !string.IsNullOrEmpty(vinNumber) ? vinNumber : "",
                Tag = !string.IsNullOrEmpty(licPlateNo) ? licPlateNo : ""
            };
            if (id != null)
            {
                truck.AlternativeTruckID = id;
            }
            if (!string.IsNullOrEmpty(code))
            {
                truck.BackOfficeTruckId = code;
            }

            if (!string.IsNullOrEmpty(truck.TruckId.Trim()))
            {
                truckList.Add(truck);
            }

            if (truckList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(truckList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("truck", serializedData);
            }
        }

        /// <summary>
        ///     Calls the update truck service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="truckId">The truck identifier.</param>
        public static void CallUpdateTruckService(out int returnValue, DateTime lastModifiedDate, int? truckId)
        {
            var truckList = new TruckList();

            // Get truck data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get all the vehicles that need to be updated in SLX
                string queryString =
                    "SELECT VehicleID, Code, VINNumber, LicPlateNo FROM dbo." +
                    AscendTable.VehicleDataTableName;

                if (truckId == null)
                {
                    queryString += " WHERE LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE VehicleID = @VehicleID";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (truckId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@VehicleId", SqlDbType.Int).Value = truckId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.VehicleDataTableName);
                        DataTable truckDataTable = myDataSet.Tables[AscendTable.VehicleDataTableName];
                        // Build driver list
                        foreach (DataRow row in truckDataTable.Rows)
                        {
                            var truck = new Truck();
                            if (truckId != null)
                            {
                                truck.AlternativeTruckID = truckId.ToString();
                            }
                            if (row["Code"] != DBNull.Value)
                            {
                                truck.TruckId = row["Code"].ToString();
                            }

                            truck.VIN = row["VINNumber"] != DBNull.Value ? row["VINNumber"].ToString() : "";

                            truck.Tag = row["LicPlateNo"] != DBNull.Value ? row["LicPlateNo"].ToString() : "";

                            if (row["VehicleID"] != DBNull.Value)
                            {
                                truck.BackOfficeTruckId = row["VehicleID"].ToString();
                            }

                            truckList.Add(truck);
                        }
                    }
                }
            }

            if (truckList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(truckList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("truck", serializedData);
            }

            returnValue = 1;
        }

        /// <summary>
        ///     Calls the insert product service.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="description">The description.</param>
        /// <param name="isPurchaseOnly">if set to <c>true</c> [is purchase only].</param>
        /// <param name="epaDisclaimer">The epa disclaimer.</param>
        /// <param name="nonFuel">The non fuel.</param>
        /// <param name="uomId">The uom identifier.</param>
        /// <param name="uomCode">The uom code.</param>
        /// <param name="productCategory">The product category.</param>
        public static void CallInsertProductService(string id, string code, string description, bool isPurchaseOnly,
            string epaDisclaimer, string nonFuel, int? uomId,
            string uomCode, string productCategory)
        {
            var productList = new ProductList();

            var product = new Product
            {
                ProductID = code
            };
            // Using PeerId for a temporary storage location
            if (id != null)
            {
                product.PeerId = Convert.ToInt32(id);
            }
            if (!string.IsNullOrEmpty(description))
            {
                product.Description = description;
            }
            if (!string.IsNullOrEmpty(epaDisclaimer))
            {
                product.EPADisclaimer = epaDisclaimer;
            }

            if (!string.IsNullOrEmpty(nonFuel))
            {
                product.NonFuel = nonFuel.Equals("Y");
            }
            else
            {
                product.NonFuel = false;
            }

            if (!string.IsNullOrEmpty(uomCode))
            {
                product.UOM = uomCode;
            }

            if (!string.IsNullOrEmpty(productCategory))
            {
                product.ProductCategory = productCategory;
            }

            product.ProductPurchasedOnly = isPurchaseOnly;

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Need to look up the product sold as values
                string queryString =
                    "select SalesAlias.Code As Code, SalesAlias.SellAsDescr As Description from PurchAlias " +
                    " join SalesAlias ON SalesAlias.ProdContID = PurchAlias.ProdContID WHERE PurchAlias.Code = @PurchAliasId";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@PurchAliasId", SqlDbType.VarChar, 24).Value = product.ProductID;

                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.PurchAliasTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.PurchAliasTableName];

                        var soldAsList = new ProductSoldAsList();
                        foreach (DataRow row in dataTable.Rows)
                        {
                            if (!row["Code"].ToString().Equals(product.ProductID))
                            {
                                var soldAsProduct = new ProductSoldAs();

                                if (row["Description"] != DBNull.Value)
                                {
                                    soldAsProduct.SoldAsProductDescription = row["Description"].ToString();
                                }

                                soldAsProduct.SoldAsProductId = row["Code"].ToString();

                                soldAsList.Add(soldAsProduct);
                            }
                        }
                        product.ProductSoldAsList = soldAsList;
                    }
                }
            }

            productList.Add(product);

            // Call the SLX Integration web service and push data to it from Ascend
            //SmartLogixConfiguration configuration = GetConfigurationValues();

            //using (var client = new WebClient())
            //{
            //    string uri = configuration.Url;


            //    uri += "SmartLogixApi/PushProduct";

            //    client.Headers.Clear();
            //    client.Headers.Add("content-type", "application/json");
            //    client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

            //    string serialisedData = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
            //    {
            //        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            //    });

            //    // HTTP POST
            //    string response = client.UploadString(uri, serialisedData);

            //    var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

            //    HandleSLXResponse(id, product.ProductID, MasterDataType.ProductType, serialisedData, null, result);

            //    // Log
            //    Logger.WriteToSLXLog(id, product.ProductID, MasterDataType.ProductType, result.ResponseStatus,
            //        serialisedData, null, null, false);
            //}
            string serializedDate = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            });
            Enqueue("product", serializedDate);
        }

        /// <summary>
        ///     Calls the update products service.
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="productId">The product identifier.</param>
        public static void CallUpdateProductsService(out int returnValue, DateTime lastModifiedDate, int? productId)
        {
            var productList = new ProductList();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                // Get all the sales alias products that need to be updated in SLX
                string queryString =
                    "SELECT SalesAlias.Code AS ProductId, SalesAlias.SellAsDescr AS SellAsDescr, HzrdMaterialsInstruction.LabelsReq AS LabelsReq, UOM.UOMID AS UOMID, " +
                    " UOM.Code AS UOM, UOM.IsPackaged AS IsPackaged,ProdType.Code AS ProductCategory" +
                    " FROM dbo." + AscendTable.SalesAliasTableName +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdContTableName +
                    " ON SalesAlias.ProdContID = ProdCont.ProdContID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.HzrdMaterialsInstructionTableName +
                    " ON ProdCont.HzrdMaterialID = HzrdMaterialsInstruction.HzrdMaterialID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.UOMTableName +
                    " ON UOM.UOMID = SalesAlias.SellByUOMID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdTypeTableName +
                    " ON ProdType.ProdTypeID = ProdCont.ProdTypeID";

                if (productId == null)
                {
                    queryString += " WHERE SalesAlias.LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE SalesAlias.SalesAliasID = @SalesAliasId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (productId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@SalesAliasId", SqlDbType.Int).Value = productId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.SalesAliasTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.SalesAliasTableName];
                        // Build product list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            // Using PeerId for a temporary storage location
                            if (productId != null)
                            {
                                product.PeerId = Convert.ToInt32(productId);
                            }
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["SellAsDescr"] != DBNull.Value)
                            {
                                product.Description = row["SellAsDescr"].ToString();
                            }
                            if (row["LabelsReq"] != DBNull.Value)
                            {
                                product.EPADisclaimer = row["LabelsReq"].ToString();
                            }
                            if (row["UOM"] != DBNull.Value)
                            {
                                product.UOM = row["UOM"].ToString();
                            }
                            if (row["ProductCategory"] != DBNull.Value)
                            {
                                product.ProductCategory = row["ProductCategory"].ToString();
                            }

                            product.ProductPurchasedOnly = false;
                            product.NonFuel = row["IsPackaged"].Equals("Y");

                            productList.Add(product);
                        }
                    }
                }

                // Get all the purch alias products that need to be updated in SLX
                queryString =
                    "SELECT PurchAlias.Code AS ProductId, PurchAlias.Descr AS Descr, HzrdMaterialsInstruction.LabelsReq AS LabelsReq, UOM.UOMID AS UOMID, UOM.Code as UOM," +
                    " UOM.IsPackaged, ProdType.Code AS ProductCategory FROM dbo." + AscendTable.PurchAliasTableName +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdContTableName +
                    " ON PurchAlias.ProdContID = ProdCont.ProdContID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.HzrdMaterialsInstructionTableName +
                    " ON ProdCont.HzrdMaterialID = HzrdMaterialsInstruction.HzrdMaterialID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.UOMTableName +
                    " ON UOM.UOMID = PurchAlias.UOMID" +
                    " LEFT OUTER JOIN dbo." + AscendTable.ProdTypeTableName +
                    " ON ProdType.ProdTypeID = ProdCont.ProdTypeID";

                if (productId == null)
                {
                    queryString += " WHERE PurchAlias.LastModifiedDtTm >= @LastModified";
                }
                else
                {
                    queryString += " WHERE PurchAlias.PurchAliasID = @PurchAliasId";
                }

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        if (productId == null)
                        {
                            cmd.Parameters.Add("@LastModified", SqlDbType.DateTime).Value = lastModifiedDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@PurchAliasId", SqlDbType.Int).Value = productId;
                        }
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.PurchAliasTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.PurchAliasTableName];
                        // Build the full product list that needs to be pushed
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            // Using PeerId for a temporary storage location
                            if (productId != null)
                            {
                                product.PeerId = (int)productId;
                            }
                            
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                product.Description = row["Descr"].ToString();
                            }
                            if (row["LabelsReq"] != DBNull.Value)
                            {
                                product.EPADisclaimer = row["LabelsReq"].ToString();
                            }
                            if (row["UOM"] != DBNull.Value)
                            {
                                product.UOM = row["UOM"].ToString();
                            }
                            if (row["ProductCategory"] != DBNull.Value)
                            {
                                product.ProductCategory = row["ProductCategory"].ToString();
                            }

                            product.ProductPurchasedOnly = CheckIsPurchaseOnly(product.ProductID);

                            if (row["IsPackaged"] != DBNull.Value)
                            {
                                product.NonFuel = row["IsPackaged"].Equals("Y");
                            }
                            else
                            {
                                product.NonFuel = false;
                            }

                            productList.Add(product);
                        }
                    }
                }

                foreach (Product product in productList.Where(p => p.ProductPurchasedOnly))
                {
                    // Need to look up the product sold as relationship values
                    queryString =
                        "select SalesAlias.Code As Code, SalesAlias.SellAsDescr As Description from PurchAlias " +
                        " join SalesAlias ON SalesAlias.ProdContID = PurchAlias.ProdContID WHERE PurchAlias.Code = @PurchAliasId";

                    using (var mySqlDataAdapter = new SqlDataAdapter())
                    {
                        using (var cmd = new SqlCommand(queryString, cn))
                        {
                            cmd.Parameters.Add("@PurchAliasId", SqlDbType.VarChar, 24).Value = product.ProductID;

                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, AscendTable.PurchAliasTableName);
                            DataTable dataTable = myDataSet.Tables[AscendTable.PurchAliasTableName];

                            var soldAsList = new ProductSoldAsList();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                if (!row["Code"].ToString().Equals(product.ProductID))
                                {
                                    var soldAsProduct = new ProductSoldAs();

                                    if (row["Description"] != DBNull.Value)
                                    {
                                        soldAsProduct.SoldAsProductDescription = row["Description"].ToString();
                                    }

                                    soldAsProduct.SoldAsProductId = row["Code"].ToString();

                                    soldAsList.Add(soldAsProduct);
                                }
                            }
                            product.ProductSoldAsList = soldAsList;
                        }
                    }
                }

                // Get all the billing items out of ascend, these are treated as products in SLX
                queryString =
                    "SELECT BillingItem.Code AS ProductId, BillingItem.Descr AS Descr FROM dbo." +
                    AscendTable.BillingItemDataTableName +
                    " WHERE BillingItem.BillingItemID = @BillingItemId";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@BillingItemId", SqlDbType.Int).Value = productId;

                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.BillingItemDataTableName);
                        DataTable dataTable = myDataSet.Tables[AscendTable.BillingItemDataTableName];
                        // Build driver list
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var product = new Product();
                            // Using PeerId for a temporary storage location
                            if (productId != null)
                            {
                                product.PeerId = (int)productId;
                            }
                            if (row["ProductId"] != DBNull.Value)
                            {
                                product.ProductID = row["ProductId"].ToString();
                            }
                            if (row["Descr"] != DBNull.Value)
                            {
                                product.Description = row["Descr"].ToString();
                            }

                            product.ProductCategory = "BillingItem";
                            product.ProductPurchasedOnly = false;
                            product.UOM = "Each";
                            product.NonFuel = true;

                            productList.Add(product);
                        }
                    }
                }
            }

            if (productList.Any())
            {
                string serializedData = JsonConvert.SerializeObject(productList, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("product", serializedData);
            }

            returnValue = 1;
        }


        /// <summary>
        ///     Calls the update carrier service and pushes data to it from Ascend
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="number">The number.</param>
        /// <param name="scac">The scac.</param>
        public static void CallInsertCarrierService(string name, int number, string scac)
        {
            var carrierlist = new CarrierList();
            var carrier = new Carrier
            {
                Name = name,
                Number = number,
                SCAC = scac
            };

            carrierlist.Add(carrier);

            if (carrierlist.Any())
            {
                string serializedData = JsonConvert.SerializeObject(carrierlist, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("carrier", serializedData);
            }
        }
        /// <summary>
        ///     Calls the update Carrier service and pushes data to it from Ascend
        /// </summary>
        /// <param name="returnValue">The return value.</param>
        /// <param name="lastModifiedDate">The last modified date.</param>
        /// <param name="carrierID">The carrier identifier.</param>
        public static void CallUpdateCarrierService(out int returnValue, DateTime lastModifiedDate, int? carrierID)
        {
            var carrierlist = new CarrierList();

            // Get carrier data from ascend 
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }


            string carrierQueryString =
                    "select CarrierID , Code,SCACCode from  dbo." + AscendTable.CarrierTablename;

            if (carrierID == null)
            {
                carrierQueryString += " WHERE LastModifiedDtTm >= " + lastModifiedDate + "";
            }
            else
            {
                carrierQueryString += " WHERE CarrierID = " + carrierID + " ";
            }
            DataTable driversDataTable = SQLHelper.ExecuteAndGetDataTable(connectionString, carrierQueryString);

            foreach (DataRow row in driversDataTable.Rows)
            {
                var carrier = new Carrier();
                if (row["CarrierID"] != DBNull.Value)
                {
                    carrier.Number = Convert.ToInt32(row["CarrierID"]);
                }
                if (row["Code"] != DBNull.Value)
                {
                    carrier.Name = row["Code"].ToString();
                }
                if (row["SCACCode"] != DBNull.Value)
                {
                    carrier.SCAC = row["SCACCode"].ToString();
                }
                carrierlist.Add(carrier);
            }



            if (carrierlist.Any())
            {

                string serializedData = JsonConvert.SerializeObject(carrierlist, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                Enqueue("carrier", serializedData);
            }

            returnValue = 1;
        }

        #endregion

        #endregion

        #region Private Methods

        private static void Enqueue(string routingKey, string message)
        {
            SmartLogixConfiguration config = GetConfigurationValues(SmartLogixConstants.RabbitMQServiceName);

            var factory = new ConnectionFactory
            {
                HostName = config.Url,
                UserName = config.UserId,
                Password = config.Password,
                VirtualHost = "/",
                Port = AmqpTcpEndpoint.UseDefaultPort,
                AutomaticRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(15)
            };

            using (var rabbitMQConnection = factory.CreateConnection())
            {
                using (var channel = rabbitMQConnection.CreateModel())
                {
                    channel.ExchangeDeclare("masterdata", "topic", true);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish("masterdata", routingKey, null, body);
                }
            }
        }

        /// <summary>
        ///     Gets the configuration values.
        /// </summary>
        /// <returns></returns>
        private static SmartLogixConfiguration GetConfigurationValues(string webServiceName)
        {
            var configuration = new SmartLogixConfiguration();

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get specific configuration info from the master database
                string configurationQueryString =
                    "SELECT ID, Code, UserID, Password, ClientID, URL, DatabaseName, WebServiceName from dbo." +
                    AscendTable.ThirdPartyWebService
                    + " WHERE WebServiceName = @Code";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(configurationQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 50).Value = webServiceName;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ThirdPartyWebService);
                        DataTable myConfigurationDataTable = myDataSet.Tables[AscendTable.ThirdPartyWebService];
                        if (myConfigurationDataTable.Rows.Count > 0)
                        {
                            configuration.ClientId = Convert.ToInt32(myConfigurationDataTable.Rows[0]["ClientID"]);
                            configuration.Code = myConfigurationDataTable.Rows[0]["Code"].ToString();
                            configuration.DatabaseName = myConfigurationDataTable.Rows[0]["DatabaseName"].ToString();
                            configuration.WebServiceName = myConfigurationDataTable.Rows[0]["WebServiceName"].ToString();
                            configuration.Id = Convert.ToInt32(myConfigurationDataTable.Rows[0]["Id"]);
                            configuration.Password = myConfigurationDataTable.Rows[0]["Password"].ToString();
                            configuration.UserId = myConfigurationDataTable.Rows[0]["UserId"].ToString();
                            configuration.Url = myConfigurationDataTable.Rows[0]["Url"].ToString();
                        }
                    }
                }
            }

            return configuration;
        }

        /// <summary>
        ///     Builds the sales order model when an order update from Ascend to SLX needs to happen for something like credit hold
        ///     or pricing
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <returns></returns>
        private static SalesOrderModel BuildSalesOrderModel(long sysTrxNo)
        {
            var model = new SalesOrderModel {ProductList = new List<ProductModel>()};
            int? tempVehicleId = null;
            int? currentShipToId = null;

            // Lookup everything that SLX would pass for a New Sales Order

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get the OrderHdr Row in Ascend
                string queryString =
                    "SELECT * FROM dbo." +
                    AscendTable.OrderHdrDataTableName +
                    " WHERE SysTrxNo = @SysTrxNo";

                // Lookup Sales Order
                DataTable orderHdrTable;
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        orderHdrTable = myDataSet.Tables["OrderHdr"];
                        if (orderHdrTable.Rows.Count > 0)
                        {
                            // Set OrderNo
                            model.AscendSalesOrderNumber = orderHdrTable.Rows[0]["OrderNo"].ToString();

                            if (orderHdrTable.Rows[0]["UserID"] != DBNull.Value)
                            {
                                model.UserId = orderHdrTable.Rows[0]["UserID"].ToString();
                            }
                            if (orderHdrTable.Rows[0]["DefPromisedDtTm"] != DBNull.Value)
                            {
                                model.OrderDeliveryDate = Convert.ToDateTime(orderHdrTable.Rows[0]["DefPromisedDtTm"]);
                            }
                            // Set DriverID
                            if (orderHdrTable.Rows[0]["DefDriverID"] != DBNull.Value)
                            {
                                model.DriverId = orderHdrTable.Rows[0]["DefDriverID"].ToString();
                            }
                            // Save the backoffice vehicle ID to lookup the visible driver ID used by SLX later
                            if (orderHdrTable.Rows[0]["DefVehicleID"] != DBNull.Value)
                            {
                                tempVehicleId = Convert.ToInt32(orderHdrTable.Rows[0]["DefVehicleID"]);
                            }
                        }
                    }
                }

                // Lookup ShipTo
                queryString =
                    "SELECT ARShipTo.Code AS Code, ARStandardAcct.StandardAcctNo AS StandardAcctNo FROM dbo." +
                    AscendTable.ARShipToDataTableName +
                    " JOIN ARStandardAcct ON ARStandardAcct.StandardAcctID = ARShipTo.StandardAcctID WHERE ShiptoID = @ShipToID";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@ShipToID", SqlDbType.Int).Value =
                            Convert.ToInt32(orderHdrTable.Rows[0]["ToSiteID"]);
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        DataTable myDataTable = myDataSet.Tables[AscendTable.ARShipToDataTableName];
                        if (myDataTable.Rows.Count > 0)
                        {
                            // Set StandardAcctId
                            model.StandardAcctId = myDataTable.Rows[0]["StandardAcctNo"].ToString();
                            // set shiptoid
                            model.ShipToId = myDataTable.Rows[0]["Code"].ToString();
                            currentShipToId = Convert.ToInt32(orderHdrTable.Rows[0]["ToSiteID"]);
                        }
                    }
                }

                // Lookup VehicleID
                queryString =
                    "SELECT Code FROM dbo." +
                    AscendTable.VehicleDataTableName +
                    " WHERE VehicleID = @VehicleID";

                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@VehicleID", SqlDbType.Int).Value = tempVehicleId;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.VehicleDataTableName);
                        DataTable myDataTable = myDataSet.Tables[AscendTable.VehicleDataTableName];
                        if (myDataTable.Rows.Count > 0)
                        {
                            // Set VehicleID
                            model.VehicleId = myDataTable.Rows[0]["Code"].ToString();
                        }
                    }
                }

                // Lookup OrderItems and add them to model along with their taxes
                queryString =
                    "SELECT MasterProdID, Qty, UnitPrice, UnitPriceKeyed, DtTm, MasterSiteID, SysTrxNo, SysTrxLine FROM dbo." +
                    AscendTable.OrderItemDataTableName +
                    " WHERE SysTrxNo = @SysTrxNo";

                // Lookup Order Items
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    DataTable orderItemDataSet;
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, AscendTable.OrderItemDataTableName);
                        orderItemDataSet = myDataSet.Tables[AscendTable.OrderItemDataTableName];
                    }

                    // Foreach orderitem / product line we need to match up the price with the current product so it is ready to be sent back to SLX
                    foreach (DataRow orderItem in orderItemDataSet.Rows)
                    {
                        var product = new ProductModel();

                        // Lookup Product Code
                        queryString = "SELECT Code FROM dbo." +
                                      AscendTable.SalesAliasTableName +
                                      " WHERE SalesAliasID = @SalesAliasID";

                        using (var cmd = new SqlCommand(queryString, cn))
                        {
                            cmd.Parameters.Add("@SalesAliasID", SqlDbType.Int).Value =
                                Convert.ToInt32(orderItem["MasterProdID"]);
                            mySqlDataAdapter.SelectCommand = cmd;
                            var myDataSet = new DataSet();
                            mySqlDataAdapter.Fill(myDataSet, AscendTable.SalesAliasTableName);
                            DataTable productDataSet = myDataSet.Tables[AscendTable.SalesAliasTableName];
                            if (productDataSet.Rows.Count > 0)
                            {
                                product.ProductId = productDataSet.Rows[0]["Code"].ToString();
                            }
                        }

                        if (string.IsNullOrEmpty(product.ProductId))
                        {
                            // Lookup Billing Item Product Code
                            queryString = "SELECT Code FROM dbo." +
                                          AscendTable.BillingItemDataTableName +
                                          " WHERE BillingItemID = @BillingItemID";

                            using (var cmd = new SqlCommand(queryString, cn))
                            {
                                cmd.Parameters.Add("@BillingItemID", SqlDbType.Int).Value =
                                    Convert.ToInt32(orderItem["MasterProdID"]);
                                mySqlDataAdapter.SelectCommand = cmd;
                                var myDataSet = new DataSet();
                                mySqlDataAdapter.Fill(myDataSet, AscendTable.BillingItemDataTableName);
                                DataTable productDataSet = myDataSet.Tables[AscendTable.BillingItemDataTableName];
                                if (productDataSet.Rows.Count > 0)
                                {
                                    product.ProductId = productDataSet.Rows[0]["Code"].ToString();
                                }
                            }
                        }

                        if (orderItem["DtTm"] != DBNull.Value)
                        {
                            product.Date = Convert.ToDateTime(orderItem["DtTm"]);
                        }
                        if (orderItem["Qty"] != DBNull.Value)
                        {
                            product.Quantity = Convert.ToDouble(orderItem["Qty"].ToString());
                            product.NetQuantity = Convert.ToDouble(orderItem["Qty"].ToString());
                        }

                        product.Price =
                            Convert.ToDouble(orderItem["UnitPriceKeyed"] != DBNull.Value
                                ? orderItem["UnitPriceKeyed"].ToString() : orderItem["UnitPrice"].ToString());

                        // Lookup Terminal ID in inventory sites
                        string terminalQueryString = "SELECT Code from dbo." +
                                                     AscendTable.InSiteDataTableName +
                                                     " WHERE SiteID = @SiteID";

                        if (orderItem["MasterSiteID"] != DBNull.Value)
                        {
                            DataTable terminalDataSet;
                            using (var cmd = new SqlCommand(terminalQueryString, cn))
                            {
                                cmd.Parameters.Add("@SiteID", SqlDbType.Int).Value =
                                    Convert.ToInt32(orderItem["MasterSiteID"]);
                                mySqlDataAdapter.SelectCommand = cmd;
                                var myDataSet = new DataSet();
                                mySqlDataAdapter.Fill(myDataSet, AscendTable.InSiteDataTableName);
                                terminalDataSet = myDataSet.Tables[AscendTable.InSiteDataTableName];
                                if (terminalDataSet.Rows.Count > 0)
                                {
                                    product.InventorySiteId = terminalDataSet.Rows[0]["Code"].ToString();
                                }
                                else
                                {
                                    product.InventorySiteId = null;
                                }
                            }

                            if (product.InventorySiteId == null)
                            {
                                // Get the current supplier supply pt for the order
                                terminalQueryString =
                                    "SELECT SupplierSupplyPt.SupplierSupplyPtID AS SupplierSupplyPtID, SupplierSupplyPt.SupplierID AS SupplierID, SupplierSupplyPt.SupplyPtID AS SupplyPtID," +
                                    " Supplier.Code AS SupplierName, SupplyPt.Code AS SupplyPtName FROM dbo." +
                                    AscendTable.SupplierSupplyPtDataTableName +
                                    " JOIN Supplier ON SupplierSupplyPt.SupplierID = Supplier.SupplierID" +
                                    " JOIN SupplyPt ON SupplierSupplyPt.SupplyPtID = SupplyPt.SupplyPtID" +
                                    " WHERE SupplierSupplyPtID = @SiteID";

                                using (var cmd = new SqlCommand(terminalQueryString, cn))
                                {
                                    cmd.Parameters.Add("@SiteID", SqlDbType.Int).Value =
                                        Convert.ToInt32(orderItem["MasterSiteID"]);
                                    mySqlDataAdapter.SelectCommand = cmd;
                                    var myDataSet = new DataSet();
                                    mySqlDataAdapter.Fill(myDataSet, AscendTable.InSiteDataTableName);
                                    terminalDataSet = myDataSet.Tables[AscendTable.InSiteDataTableName];
                                    if (terminalDataSet.Rows.Count > 0)
                                    {
                                        product.SupplierId = terminalDataSet.Rows[0]["SupplierName"].ToString();
                                        product.SupplyPtId = terminalDataSet.Rows[0]["SupplyPtName"].ToString();
                                    }
                                    else
                                    {
                                        product.SupplierId = null;
                                        product.SupplyPtId = null;
                                    }
                                }
                            }
                        }

                        // Lookup all the taxes for the order
                        string taxQueryString =
                            "SELECT Tax.ID AS TaxID, OrderItemTax.SysTrxNo as SysTrxNo, Tax.Descr as Descr, OrderItemTax.AmtDue as AmtDue, " +
                            " OrderItemTax.TaxPerUnit as TaxPerUnit, OrderItemTax.TaxAmt as TaxAmt, Tax.CalcMethod as CalcMethod, TaxRate.Rate AS Rate, OrderItemTax.OrgTaxRate AS OrgTaxRate " +
                            " FROM OrderItemTax " +
                            " JOIN Tax ON Tax.ID = OrderItemTax.TaxID " +
                            " CROSS APPLY (SELECT top 1 * FROM TaxRate WHERE TaxID = OrderItemTax.TaxID ORDER BY EffDt Desc) TaxRate " +
                            " WHERE OrderItemTax.SysTrxNo = @SysTrxNo and SysTrxLine = @SysTrxLine and Tax.TaxFeeAdj = 'T' and OrderItemTax.GLRuleNo = 2 and OrderItemTax.TaxType != 'F' and Exempt = 'N'";

                        DataSet taxDataSet;
                        using (var cmd = new SqlCommand(taxQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                                Convert.ToInt32(orderItem["SysTrxLine"]);
                            mySqlDataAdapter.SelectCommand = cmd;
                            taxDataSet = new DataSet();
                            mySqlDataAdapter.Fill(taxDataSet, "OrderItemTax");
                        }

                        if (taxDataSet.Tables["OrderItemTax"] != null)
                        {
                            product.Tax = new List<TaxModel>();
                            // Compile tax and pricing info as appropriate to get it ready to return to SLX
                            // Foreach tax line we need to build a tax item that can be sent back to SLX
                            foreach (DataRow taxLineItem in taxDataSet.Tables["OrderItemTax"].Rows)
                            {
                                var tax = new TaxModel();

                                if (taxLineItem["TaxPerUnit"] != DBNull.Value)
                                {
                                    tax.Amount = Convert.ToDouble(taxLineItem["TaxPerUnit"].ToString());
                                }
                                if (taxLineItem["Descr"] != DBNull.Value)
                                {
                                    tax.Description = taxLineItem["Descr"].ToString();
                                }
                                // We have to multiply percentage based taxes by 100 because thats required by SLX
                                if (taxLineItem["CalcMethod"].ToString().Equals("P"))
                                {
                                    tax.TaxFeeTypeId = 1;
                                    if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                    {
                                        tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString())*100;
                                    }
                                    else
                                    {
                                        tax.Amount = Convert.ToDouble(taxLineItem["Rate"].ToString())*100;
                                    }
                                }
                                else if (taxLineItem["CalcMethod"].ToString().Equals("U"))
                                {
                                    tax.TaxFeeTypeId = 2;
                                    if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                    {
                                        tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString());
                                    }
                                }
                                else if (taxLineItem["CalcMethod"].Equals("F"))
                                {
                                    tax.TaxFeeTypeId = 3;
                                    if (taxLineItem["OrgTaxRate"] != DBNull.Value)
                                    {
                                        tax.Amount = Convert.ToDouble(taxLineItem["OrgTaxRate"].ToString());
                                    }
                                }

                                tax.SLSODTI_Number = model.AscendSalesOrderNumber;

                                // Lookup Compound Tax Information for the order
                                string compoundTaxQueryString =
                                    "select TaxOnTax.TaxID AS TaxOnTaxTaxID, TaxOnTax.SubjectToTaxID AS SubjectToTaxID, Tax.Code As SubjectToTaxCode, " +
                                    " Tax.Descr AS SubjectToTaxDescr, Tax.CalcMethod AS SubjectToTaxCalcMethod, " +
                                    " TaxRate.Rate AS Rate, " +
                                    " OrderItemTax.TaxPerUnit AS TaxPerUnit, " +
                                    " OrderItemTax.OrgTaxRate AS OrgTaxRate " +
                                    " from TaxOnTax " +
                                    " JOIN ORderItemTax on OrderItemTax.TaxID = TaxOnTax.TaxID " +
                                    " join Tax ON Tax.ID = TaxOnTax.TaxID " +
                                    " CROSS APPLY (SELECT top 1 * FROM TaxRate WHERE TaxRate.TaxID = OrderItemTax.TaxID ORDER BY EffDt Desc) TaxRate" +
                                    " where SysTrxNo = @SysTrxNo and SysTrxLine = @SysTrxLine and TaxOnTax.SubjectToTaxID = @TaxID and TaxOnTax.TaxID in (select TaxID from OrderItemTax where SysTrxNo = @SysTrxNo2)" +
                                    "  and Tax.TaxFeeAdj = 'T' and OrderItemTax.GLRuleNo = 2 and OrderItemTax.TaxType != 'F' and OrderItemTax.Exempt = 'N'";

                                DataSet compoundTaxDataSet;
                                using (var cmd = new SqlCommand(compoundTaxQueryString, cn))
                                {
                                    cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                                    cmd.Parameters.Add("@SysTrxNo2", SqlDbType.BigInt).Value = sysTrxNo;
                                    cmd.Parameters.Add("@TaxID", SqlDbType.Int).Value = Convert.ToInt32(taxLineItem["TaxID"]);
                                    cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                                        Convert.ToInt32(orderItem["SysTrxLine"]);
                                    mySqlDataAdapter.SelectCommand = cmd;
                                    compoundTaxDataSet = new DataSet();
                                    mySqlDataAdapter.Fill(compoundTaxDataSet, "CompoundTax");
                                }

                                tax.CompoundTaxFeeItemsList = new List<CompoundTaxModel>();
                                foreach (DataRow compoundTaxLine in compoundTaxDataSet.Tables["CompoundTax"].Rows)
                                {
                                    var compoundTax = new CompoundTaxModel();

                                    if (compoundTaxLine["TaxPerUnit"] != DBNull.Value)
                                    {
                                        compoundTax.Amount = Convert.ToDouble(compoundTaxLine["TaxPerUnit"].ToString());
                                    }
                                    if (compoundTaxLine["SubjectToTaxDescr"] != DBNull.Value)
                                    {
                                        compoundTax.Description = compoundTaxLine["SubjectToTaxDescr"].ToString();
                                    }
                                    // We have to multiply percentage based taxes by 100 because thats required by SLX
                                    if (compoundTaxLine["SubjectToTaxCalcMethod"].ToString().Equals("P"))
                                    {
                                        compoundTax.STaxFeeTypeId = 1;
                                        if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                        {
                                            compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString())*100;
                                        }
                                        else
                                        {
                                            compoundTax.Amount = Convert.ToDouble(compoundTaxLine["Rate"].ToString())*100;
                                        }
                                    }
                                    else if (compoundTaxLine["SubjectToTaxCalcMethod"].ToString().Equals("U"))
                                    {
                                        compoundTax.STaxFeeTypeId = 2;
                                        if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                        {
                                            compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString());
                                        }
                                    }
                                    else if (compoundTaxLine["SubjectToTaxCalcMethod"].Equals("F"))
                                    {
                                        compoundTax.STaxFeeTypeId = 3;
                                        if (compoundTaxLine["OrgTaxRate"] != DBNull.Value)
                                        {
                                            compoundTax.Amount = Convert.ToDouble(compoundTaxLine["OrgTaxRate"].ToString());
                                        }
                                    }

                                    tax.CompoundTaxFeeItemsList.Add(compoundTax);
                                }

                                product.Tax.Add(tax);
                            }
                        }

                        // Lookup the freight from ascend for this order
                        string freightQueryString =
                            "SELECT FrtAmt FROM OrderItemComponent" +
                            " WHERE OrderItemComponent.SysTrxNo = @SysTrxNo and OrderItemComponent.SysTrxLine = @SysTrxLine";

                        DataSet orderItemFreightDataSet;
                        using (var cmd = new SqlCommand(freightQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                                Convert.ToInt32(orderItem["SysTrxLine"]);
                            mySqlDataAdapter.SelectCommand = cmd;
                            orderItemFreightDataSet = new DataSet();
                            mySqlDataAdapter.Fill(orderItemFreightDataSet, "OrderItemComponent");
                        }

                        decimal freightTotal = 0;

                        foreach (DataRow component in orderItemFreightDataSet.Tables["OrderItemComponent"].Rows)
                        {
                            if (component["FrtAmt"] != DBNull.Value)
                            {
                                freightTotal += (decimal) component["FrtAmt"];
                            }
                        }

                        // Lookup the tax on the freight from ascend for this order
                        freightQueryString =
                            "SELECT AmtDue FROM OrderItemTax" +
                            " WHERE OrderItemTax.SysTrxNo = @SysTrxNo and OrderItemTax.SysTrxLine = @SysTrxLine and OrderItemTax.TaxType = 'F' and Exempt = 'N'";

                        using (var cmd = new SqlCommand(freightQueryString, cn))
                        {
                            cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                            cmd.Parameters.Add("@SysTrxLine", SqlDbType.Int).Value =
                                Convert.ToInt32(orderItem["SysTrxLine"]);
                            mySqlDataAdapter.SelectCommand = cmd;
                            orderItemFreightDataSet = new DataSet();
                            mySqlDataAdapter.Fill(orderItemFreightDataSet, "OrderItemTax");
                        }

                        foreach (DataRow surcharge in orderItemFreightDataSet.Tables["OrderItemTax"].Rows)
                        {
                            if (surcharge["AmtDue"] != DBNull.Value)
                            {
                                freightTotal += (decimal) surcharge["AmtDue"];
                            }
                        }

                        product.Freight = freightTotal;

                        model.ProductList.Add(product);
                    }
                }
            }

            // Get the additional order header record information needed
            GetOrderHdrInfo(sysTrxNo, model);

            if (currentShipToId != null)
            {
                LookupPaymentTerms(currentShipToId.Value, model);
            }

            return model;
        }

        /// <summary>
        ///     Determines whether [is valid smart logix order] [the specified system TRX no].
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <returns>
        ///     <c>true</c> if [is valid smart logix order] [the specified system TRX no]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsValidSmartLogixOrder(long sysTrxNo)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            var isValidSmartLogixOrder = false;

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Make sure the order has been added to ascend from SLX
                var smartLogixOrdersQueryString =
                    "SELECT SysTrxNo from SmartLogixOrders where SysTrxNo = @SysTrxNo";

                // Lookup Vehicle MasterDataID
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(smartLogixOrdersQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var smartLogixOrdersDataSet = new DataSet();
                        mySqlDataAdapter.Fill(smartLogixOrdersDataSet, "SmartLogixOrders");
                        if (smartLogixOrdersDataSet.Tables["SmartLogixOrders"].Rows.Count > 0)
                        {
                            isValidSmartLogixOrder = true;
                        }
                    }
                }
            }

            return isValidSmartLogixOrder;
        }

        /// <summary>
        ///     Lookups the sales order no.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <returns></returns>
        private static string LookupSalesOrderNo(long sysTrxNo)
        {
            string salesOrderNo = null;

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            string orderNoLookupQueryString =
                "SELECT SysTrxNo, OrderNo FROM dbo." +
                AscendTable.OrderHdrDataTableName +
                " WHERE SysTrxNo = @SysTrxNo";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(orderNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.BigInt).Value = sysTrxNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        DataTable myOrderNoDataTable = myDataSet.Tables["OrderHdr"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            salesOrderNo = myOrderNoDataTable.Rows[0]["OrderNo"].ToString();
                        }
                    }
                }
            }

            return salesOrderNo;
        }

        /// <summary>
        ///     Lookups the system TRX no.
        /// </summary>
        /// <param name="orderNo">The order no.</param>
        /// <returns></returns>
        private static string LookupSysTrxNo(string orderNo)
        {
            string sysTrxNo = null;

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            string sysTrxNoLookupQueryString =
                "SELECT SysTrxNo, OrderNo FROM dbo." +
                AscendTable.OrderHdrDataTableName +
                " WHERE OrderNo = @OrderNo";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(sysTrxNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 24).Value = orderNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        DataTable myOrderNoDataTable = myDataSet.Tables["OrderHdr"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            sysTrxNo = myOrderNoDataTable.Rows[0]["SysTrxNo"].ToString();
                        }
                    }
                }
            }

            return sysTrxNo;
        }

        /// <summary>
        ///     Checks the is purchase only flag based on if the product is only a purch alias or if the same name is used for both
        ///     the purchalias and sales alias
        /// </summary>
        /// <param name="productCode">The product code.</param>
        /// <returns></returns>
        private static bool CheckIsPurchaseOnly(string productCode)
        {
            var isPurchaseOnly = true;
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the sales alias with a code of the passed in purch alias
            string sysTrxNoLookupQueryString =
                "SELECT SalesAliasID FROM dbo." +
                AscendTable.SalesAliasTableName +
                " WHERE Code = @Code";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(sysTrxNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@Code", SqlDbType.VarChar, 24).Value = productCode;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "SalesAlias");
                        DataTable myOrderNoDataTable = myDataSet.Tables["SalesAlias"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            isPurchaseOnly = false;
                        }
                    }
                }
            }

            return isPurchaseOnly;
        }

        /// <summary>
        ///     Writes to SLX log.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="status">The status.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="isError">if set to <c>true</c> [is error].</param>
        /// <returns></returns>
        private static Guid WriteToSLXLog(string ascendId, string masterDataId, string masterDataType, int? status,
            string request, string requestRecieved, Exception ex, bool isError)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // define INSERT query with parameters to the smartlogixpushlog table
            string query =
                "INSERT INTO " + AscendTable.SmartLogixLogDataTableName +
                " (LogID, LogDate, AscendId, MasterDataId, MasterDataType, Status, Details, Request, RequestRecieved, StackTrace, IsError) " +
                "VALUES (@LogID, @LogDate, @AscendId, @MasterDataId, @MasterDataType, @Status, @Details, @Request, @RequestRecieved, @StackTrace, @IsError)";

            Guid logId = Guid.NewGuid();

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                // create command
                using (var cmd = new SqlCommand(query, cn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@LogID", logId);
                        cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                        if (ascendId != null)
                        {
                            cmd.Parameters.AddWithValue("@AscendId", ascendId);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@AscendId", DBNull.Value);
                        }
                        cmd.Parameters.AddWithValue("@MasterDataId", masterDataId);
                        cmd.Parameters.AddWithValue("@MasterDataType", masterDataType);

                        if (ex == null)
                        {
                            cmd.Parameters.AddWithValue("@Status", status.ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Status", "2");
                        }

                        if (ex != null)
                        {
                            cmd.Parameters.AddWithValue("@Details", ex.Message);
                            if (ex.StackTrace != null)
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", ex.StackTrace);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Details", "Success");
                            cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(request))
                        {
                            cmd.Parameters.AddWithValue("@Request", request);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Request", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(requestRecieved))
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", requestRecieved);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", DBNull.Value);
                        }

                        if (isError)
                        {
                            cmd.Parameters.AddWithValue("@IsError", 1);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@IsError", 0);
                        }

                        // open connection, execute INSERT, close connection
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        // Do not want to throw an error if something goes wrong logging
                    }
                }
            }

            return logId;
        }

        /// <summary>
        ///     Handles the SLX response.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="result">The result.</param>
        /// <exception cref="ApplicationException">
        ///     Error in SLX" + result.SLXException
        ///     or
        ///     Error in SLX" + result.ResponseMessage
        ///     or
        ///     Error in SLX
        /// </exception>
        private static void HandleSLXResponse(string ascendId, string masterDataId, string masterDataType,
            string request, string requestRecieved, SmartLogixResponse result)
        {
            if (result == null || result.ResponseStatus != 1)
            {
                // This is to bypass a bug in slx where the status value does not actually change to 1
                if (result?.ResponseMessage == null || !result.ResponseMessage.Contains("Success"))
                {
                    if (result == null)
                    {
                        result = new SmartLogixResponse();
                    }

                    if (result.SLXException == null)
                    {
                        result.SLXException = new Exception(result.ResponseMessage);
                    }

                    WriteToSLXLog(ascendId, masterDataId, masterDataType, result.ResponseStatus, request, requestRecieved, result.SLXException, true);

                    if (result.SLXException != null)
                    {
                        throw new ApplicationException("Error in SLX" + result.SLXException);
                    }

                    if (result.ResponseMessage != null)
                    {
                        throw new ApplicationException("Error in SLX" + result.ResponseMessage);
                    }

                    throw new ApplicationException("Error in SLX");
                }
            }
        }

        /// <summary>
        ///     Gets the order HDR information.
        /// </summary>
        /// <param name="sysTrxNo">The system TRX no.</param>
        /// <param name="model">The model.</param>
        private static void GetOrderHdrInfo(long sysTrxNo, SalesOrderModel model)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get the hold status for the current order as well as the PO Number
                var orderHdrQueryString = "SELECT PONo, Hold, OnHold FROM OrderHdr WHERE SysTrxNo = @SysTrxNo";

                using (var orderHdrSqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(orderHdrQueryString, cn))
                    {
                        cmd.Parameters.Add("@SysTrxNo", SqlDbType.VarChar, 24).Value = sysTrxNo;
                        orderHdrSqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        orderHdrSqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToTankDataTableName);
                        DataTable orderHdrDataSet = myDataSet.Tables[AscendTable.ARShipToTankDataTableName];
                        if (orderHdrDataSet.Rows.Count > 0)
                        {
                            model.PONumber = orderHdrDataSet.Rows[0]["PONo"].ToString();
                            // If Hold = 'Y' then IsOnHold is true otherwise false
                            model.IsOnHold = orderHdrDataSet.Rows[0]["Hold"].ToString().Equals("Y");
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Lookups the payment terms.
        /// </summary>
        /// <param name="shipToId">The ship to identifier.</param>
        /// <param name="model">The model.</param>
        private static void LookupPaymentTerms(long shipToId, SalesOrderModel model)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }

                // Get the PaymentTerms for the ship to including the CODNotes and CODFlag
                string queryString =
                    "SELECT ARShipTo.PrepayCODCustomerFlag AS CODFlag, ARTerms.Descr AS CODNotes FROM ARShipTo " +
                    "JOIN ARTerms ON ARTerms.TermsID = ARShipTo.TermsID WHERE ARShipTo.ShipToID = @ShipToId";

                using (var sqlDataAdapter = new SqlDataAdapter())
                {
                    using (var cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.Parameters.Add("@ShipToId", SqlDbType.BigInt).Value = shipToId;
                        sqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        sqlDataAdapter.Fill(myDataSet, AscendTable.ARShipToDataTableName);
                        DataTable dataSet = myDataSet.Tables[AscendTable.ARShipToDataTableName];
                        if (dataSet.Rows.Count > 0)
                        {
                            // If CODFlag equals 'N' then CODFlag is true otherwise false
                            model.CODFlag = !dataSet.Rows[0]["CODFlag"].ToString().Equals("N");

                            if (dataSet.Rows[0]["CODNotes"] != DBNull.Value)
                            {
                                model.NoteCOD = dataSet.Rows[0]["CODNotes"].ToString();
                                if (model.NoteCOD.ToLower().Equals("cash on delivery") || model.NoteCOD.ToLower().Equals("cod"))
                                {
                                    model.CODFlag = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        

      



        #endregion
    }
}