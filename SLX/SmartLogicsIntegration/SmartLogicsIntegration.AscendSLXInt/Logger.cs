﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;

namespace SmartLogixIntegration.AscendSLXInt
{
    /// <summary>
    /// 
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Writes to SLX log.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="status">The status.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="isError">if set to <c>true</c> [is error].</param>
        /// <returns></returns>
        public static Guid WriteToSLXLog(string ascendId, string masterDataId, string masterDataType, int? status,
            string request, string requestRecieved, Exception ex, bool isError)
        {
            var connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // define INSERT query with parameters
            var query =
                "INSERT INTO " + AscendTable.SmartLogixLogDataTableName +
                " (LogID, LogDate, AscendId, MasterDataId, MasterDataType, Status, Details, Request, RequestRecieved, StackTrace, IsError) " +
                "VALUES (@LogID, @LogDate, @AscendId, @MasterDataId, @MasterDataType, @Status, @Details, @Request, @RequestRecieved, @StackTrace, @IsError)";

            var logId = Guid.NewGuid();

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                using (TransactionScope suppress = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    // create command
                    using (var cmd = new SqlCommand(query, cn))
                    {
                        try
                        {
                            // define parameters and their values
                            cmd.Parameters.Add("@LogID", SqlDbType.UniqueIdentifier).Value = logId;
                            cmd.Parameters.Add("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                            if (ascendId != null)
                            {
                                cmd.Parameters.Add("@AscendId", SqlDbType.VarChar, 24).Value = ascendId;
                            }
                            else
                            {
                                cmd.Parameters.Add("@AscendId", SqlDbType.VarChar, 24).Value = DBNull.Value;
                            }
                            cmd.Parameters.Add("@MasterDataId", SqlDbType.VarChar, 50).Value = masterDataId;
                            cmd.Parameters.Add("@MasterDataType", SqlDbType.VarChar, 50).Value = masterDataType;

                            if (ex == null)
                            {
                                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50).Value = status.ToString();
                            }
                            else
                            {
                                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50).Value = "2";
                            }

                            if (ex != null)
                            {
                                cmd.Parameters.Add("@Details", SqlDbType.VarChar, -1).Value = ex.Message;
                                if (ex.StackTrace != null)
                                {
                                    cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar, -1).Value =
                                        ex.StackTrace;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar, -1).Value = DBNull.Value;
                                }
                            }
                            else
                            {
                                cmd.Parameters.Add("@Details", SqlDbType.VarChar, -1).Value = "Success";
                                cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar, -1).Value = DBNull.Value;
                            }

                            if (!string.IsNullOrEmpty(request))
                            {
                                cmd.Parameters.Add("@Request", SqlDbType.VarChar, -1).Value = request;
                            }
                            else
                            {
                                cmd.Parameters.Add("@Request", SqlDbType.VarChar, -1).Value = DBNull.Value;
                            }

                            if (!string.IsNullOrEmpty(requestRecieved))
                            {
                                cmd.Parameters.Add("@RequestRecieved", SqlDbType.VarChar, -1).Value = requestRecieved;
                            }
                            else
                            {
                                cmd.Parameters.Add("@RequestRecieved", SqlDbType.VarChar, -1).Value = DBNull.Value;
                            }

                            if (isError)
                            {
                                cmd.Parameters.Add("@IsError", SqlDbType.Bit).Value = 1;
                            }
                            else
                            {
                                cmd.Parameters.Add("@IsError", SqlDbType.Bit).Value = 0;
                            }

                            // open connection, execute INSERT, close connection
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            // Do not want to throw an error if something goes wrong logging
                        }
                    }

                }
            }

            return logId;
        }
    }
}
