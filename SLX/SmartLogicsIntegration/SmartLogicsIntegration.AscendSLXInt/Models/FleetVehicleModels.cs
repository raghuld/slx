﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    public class FleetVehicleList : List<FleetVehicle>
    {
    }

    /// <summary>
    /// This is the FleetVehicle model that is tied to the ARShipTo_Tank table in ascend and used to pass data to SLX
    /// </summary>
    public class FleetVehicle
    {

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        public string Barcode { get; set; }
        /// <summary>
        /// Gets or sets the barcode cross reference identifier.
        /// </summary>
        /// <value>
        /// The barcode cross reference identifier.
        /// </value>
        public string BarcodeCrossReferenceId { get; set; }
        /// <summary>
        /// Gets or sets the cross reference identifier.
        /// </summary>
        /// <value>
        /// The cross reference identifier.
        /// </value>
        public string CrossReferenceId { get; set; }
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is transient.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is transient; otherwise, <c>false</c>.
        /// </value>
        public bool IsTransient { get; set; }
        /// <summary>
        /// Gets or sets the manufacturer.
        /// </summary>
        /// <value>
        /// The manufacturer.
        /// </value>
        public string Manufacturer { get; set; }
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public string Model { get; set; }
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductId { get; set; }
        /// <summary>
        /// Gets or sets the ship to identifier.
        /// </summary>
        /// <value>
        /// The ship to identifier.
        /// </value>
        public string ShipToId { get; set; }
        /// <summary>
        /// Gets or sets the smartap mac address.
        /// </summary>
        /// <value>
        /// The smartap mac address.
        /// </value>
        public string SmartapMacAddress { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }
        /// <summary>
        /// Gets or sets the size of the tank.
        /// </summary>
        /// <value>
        /// The size of the tank.
        /// </value>
        public float TankSize { get; set; }
        /// <summary>
        /// Gets or sets the vehicle number.
        /// </summary>
        /// <value>
        /// The vehicle number.
        /// </value>
        public string VehicleNumber { get; set; }
        /// <summary>
        /// Gets or sets the vin.
        /// </summary>
        /// <value>
        /// The vin.
        /// </value>
        public string Vin { get; set; }
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public string Year { get; set; }

    }
}
