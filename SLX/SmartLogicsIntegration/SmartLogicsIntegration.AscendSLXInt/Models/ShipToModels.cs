﻿using System;
using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{

    public class ShipToList : List<ShipTo>
    {
    }

    /// <summary>
    /// This class represents an ARShipto in ascend and passes that data to SLX through their web services
    /// </summary>
    public class ShipTo
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ShipTo"/> is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        public bool Active { get; set; }


        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the company identifier.
        /// </summary>
        /// <value>
        /// The company identifier.
        /// </value>
        public string CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        /// <value>
        /// The contact email.
        /// </value>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the name of the contact.
        /// </summary>
        /// <value>
        /// The name of the contact.
        /// </value>
        public string ContactName { get; set; }
        /// <summary>
        /// Gets or sets the contact phone.
        /// </summary>
        /// <value>
        /// The contact phone.
        /// </value>
        public string ContactPhone { get; set; }
        /// <summary>
        /// Gets or sets the contact phone extension.
        /// </summary>
        /// <value>
        /// The contact phone extension.
        /// </value>
        public string ContactPhoneExtension { get; set; }
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the customer tanks.
        /// </summary>
        /// <value>
        /// The customer tanks.
        /// </value>
        public CustomerTankList CustomerTanks { get; set; }
        /// <summary>
        /// Gets or sets the default type of the order.
        /// </summary>
        /// <value>
        /// The default type of the order.
        /// </value>
        public SalesOrderType DefaultOrderType { get; set; }
        /// <summary>
        /// Gets or sets the default product identifier list.
        /// </summary>
        /// <value>
        /// The default product identifier list.
        /// </value>
        public string[] DefaultProductIdList { get; set; }
        /// <summary>
        /// Gets or sets the name of the fax.
        /// </summary>
        /// <value>
        /// The name of the fax.
        /// </value>
        public string FaxName { get; set; }
        /// <summary>
        /// Gets or sets the fax number.
        /// </summary>
        /// <value>
        /// The fax number.
        /// </value>
        public string FaxNumber { get; set; }
        /// <summary>
        /// Gets or sets the fleet vehicles.
        /// </summary>
        /// <value>
        /// The fleet vehicles.
        /// </value>
        public FleetVehicleList FleetVehicles { get; set; }
        /// <summary>
        /// Gets or sets the fueling radius in feet.
        /// </summary>
        /// <value>
        /// The fueling radius in feet.
        /// </value>
        public int FuelingRadiusInFeet { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [locked fleet vehicles].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [locked fleet vehicles]; otherwise, <c>false</c>.
        /// </value>
        public bool LockedFleetVehicles { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [locked tanks].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [locked tanks]; otherwise, <c>false</c>.
        /// </value>
        public bool LockedTanks { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the note customer.
        /// </summary>
        /// <value>
        /// The note customer.
        /// </value>
        public string NoteCustomer { get; set; }
        /// <summary>
        /// Gets or sets the note dispatcher.
        /// </summary>
        /// <value>
        /// The note dispatcher.
        /// </value>
        public string NoteDispatcher { get; set; }
        /// <summary>
        /// Gets or sets the note driver.
        /// </summary>
        /// <value>
        /// The note driver.
        /// </value>
        public string NoteDriver { get; set; }
        /// <summary>
        /// Gets or sets the order hold identifier.
        /// </summary>
        /// <value>
        /// The order hold identifier.
        /// </value>
        public int? OrderHoldId { get; set; }
        /// <summary>
        /// Gets or sets the peer identifier.
        /// </summary>
        /// <value>
        /// The peer identifier.
        /// </value>
        public int PeerId { get; set; }
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone { get; set; }
        /// <summary>
        /// Gets or sets the po numbers.
        /// </summary>
        /// <value>
        /// The po numbers.
        /// </value>
        public PurchaseOrderList PoNumbers { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [po required].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [po required]; otherwise, <c>false</c>.
        /// </value>
        public bool PoRequired { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [print price on ticket].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [print price on ticket]; otherwise, <c>false</c>.
        /// </value>
        public bool PrintPriceOnTicket { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [pump required].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [pump required]; otherwise, <c>false</c>.
        /// </value>
        public bool PumpRequired { get; set; }

        /// <summary>
        /// Gets or sets the receiving hours.
        /// </summary>
        /// <value>
        /// The receiving hours.
        /// </value>
        public ReceivingHoursList ReceivingHours { get; set; }

        /// <summary>
        /// Gets or sets the name of the region.
        /// </summary>
        /// <value>
        /// The name of the region.
        /// </value>
        public string RegionName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [require fleet odometer].
        /// </summary>
        /// <value>
        /// <c>true</c> if [require fleet odometer]; otherwise, <c>false</c>.
        /// </value>
        public bool RequireFleetOdometer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [require stick readings].
        /// </summary>
        /// <value>
        /// <c>true</c> if [require stick readings]; otherwise, <c>false</c>.
        /// </value>
        public bool RequireStickReadings { get; set; }

        /// <summary>
        /// Gets or sets the service charges.
        /// </summary>
        /// <value>
        /// The service charges.
        /// </value>
        public DynamicServiceChargeList ServiceCharges { get; set; }

        /// <summary>
        /// Gets or sets the ship to identifier.
        /// </summary>
        /// <value>
        /// The ship to identifier.
        /// </value>
        public string ShipToId { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the name of the territory.
        /// </summary>
        /// <value>
        /// The name of the territory.
        /// </value>
        public string TerritoryName { get; set; }

        /// <summary>
        /// Gets or sets the ticket copies.
        /// </summary>
        /// <value>
        /// The ticket copies.
        /// </value>
        public int TicketCopies { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        /// <value>
        /// The zip.
        /// </value>
        public string Zip { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is credit hold.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is credit hold; otherwise, <c>false</c>.
        /// </value>
        public bool IsCreditHold { get; set; }

    }

    public class DynamicServiceChargeList : List<DynamicServiceCharge>
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DynamicServiceCharge
    {
        /// <summary>
        /// Gets or sets the charge amount.
        /// </summary>
        /// <value>
        /// The charge amount.
        /// </value>
        public decimal ChargeAmount { get; set; }
        /// <summary>
        /// Gets or sets the charge quantity.
        /// </summary>
        /// <value>
        /// The charge quantity.
        /// </value>
        public int ChargeQuantity { get; set; }
        /// <summary>
        /// Gets or sets the day of week.
        /// </summary>
        /// <value>
        /// The day of week.
        /// </value>
        public System.Nullable<System.DayOfWeek> DayOfWeek { get; set; }
        /// <summary>
        /// Gets or sets the effective date.
        /// </summary>
        /// <value>
        /// The effective date.
        /// </value>
        public System.DateTime EffectiveDate { get; set; }
        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        public DateTime? ExpirationDate { get; set; }
        /// <summary>
        /// Gets or sets the minimum volume.
        /// </summary>
        /// <value>
        /// The minimum volume.
        /// </value>
        public int MinVolume { get; set; }
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductID { get; set; }
    }

    public class ReceivingHoursList : List<ReceivingHours>
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReceivingHours
    {

        /// <summary>
        /// Gets or sets from day of week.
        /// </summary>
        /// <value>
        /// From day of week.
        /// </value>
        public System.DayOfWeek FromDayOfWeek { get; set; }
        /// <summary>
        /// Gets or sets from time.
        /// </summary>
        /// <value>
        /// From time.
        /// </value>
        public string FromTime { get; set; }
        /// <summary>
        /// Gets or sets to day of week.
        /// </summary>
        /// <value>
        /// To day of week.
        /// </value>
        public System.DayOfWeek ToDayOfWeek { get; set; }
        /// <summary>
        /// Gets or sets to time.
        /// </summary>
        /// <value>
        /// To time.
        /// </value>
        public string ToTime { get; set; }

    }

    public class PurchaseOrderList : List<PurchaseOrder>
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class PurchaseOrder
    {

        /// <summary>
        /// Gets or sets the cross reference identifier.
        /// </summary>
        /// <value>
        /// The cross reference identifier.
        /// </value>
        public string CrossReferenceId { get; set; }
        /// <summary>
        /// Gets or sets the date from.
        /// </summary>
        /// <value>
        /// The date from.
        /// </value>
        public System.DateTime DateFrom { get; set; }
        /// <summary>
        /// Gets or sets the date to.
        /// </summary>
        /// <value>
        /// The date to.
        /// </value>
        public System.Nullable<System.DateTime> DateTo { get; set; }
        /// <summary>
        /// Gets or sets the po number.
        /// </summary>
        /// <value>
        /// The po number.
        /// </value>
        public string PoNumber { get; set; }

    }

}
