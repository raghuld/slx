﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This model represents an insite in ascend (inventory site) and passes that entity to SLX
    /// </summary>
    public class Warehouse
   {
        /// <summary>
        /// Gets or sets the warehouse identifier.
        /// </summary>
        /// <value>
        /// The warehouse identifier.
        /// </value>
        public string WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

   }
   public class WarehouseList : List<Warehouse>
   {
   }
}
