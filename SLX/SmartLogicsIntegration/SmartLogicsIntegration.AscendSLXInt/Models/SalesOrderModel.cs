﻿using System;
using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This model represents the sales order for NSO or MSO.  It contains everything needed to create a new order through the
    /// web services SLX calls.
    /// </summary>
    public class SalesOrderModel
    {
        /// <summary>
        /// Gets or sets the SLX sales order number.
        /// </summary>
        /// <value>
        /// The SLX sales order number.
        /// </value>
        public string SLXSalesOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the ascend sales order number.
        /// </summary>
        /// <value>
        /// The ascend sales order number.
        /// </value>
        public string AscendSalesOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the standard acct identifier.
        /// </summary>
        /// <value>
        /// The standard acct identifier.
        /// </value>
        public string StandardAcctId { get; set; }

        /// <summary>
        /// Gets or sets the ship to identifier.
        /// </summary>
        /// <value>
        /// The ship to identifier.
        /// </value>
        public string ShipToId { get; set; }

        /// <summary>
        /// Gets or sets the driver identifier.
        /// </summary>
        /// <value>
        /// The driver identifier.
        /// </value>
        public string DriverId { get; set; }

        /// <summary>
        /// Gets or sets the vehicle identifier.
        /// </summary>
        /// <value>
        /// The vehicle identifier.
        /// </value>
        public string VehicleId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the po number.
        /// </summary>
        /// <value>
        /// The po number.
        /// </value>
        public string PONumber { get; set; }

        /// <summary>
        /// Gets or sets the order delivery date.
        /// </summary>
        /// <value>
        /// The order delivery date.
        /// </value>
        public DateTime? OrderDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is on hold.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is on hold; otherwise, <c>false</c>.
        /// </value>
        public bool IsOnHold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [cod flag].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cod flag]; otherwise, <c>false</c>.
        /// </value>
        public bool CODFlag { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the note cod.
        /// </summary>
        /// <value>
        /// The note cod.
        /// </value>
        public string NoteCOD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is inventory adjustment.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is inventory adjustment; otherwise, <c>false</c>.
        /// </value>
        public bool IsInventoryAdjustment { get; set; }

        /// <summary>
        /// Gets or sets the type of the vehicle.
        /// </summary>
        /// <value>
        /// The type of the vehicle.
        /// </value>
        public VehicleType? VehicleType { get; set; }

        /// <summary>
        /// Gets or sets the product list.
        /// </summary>
        /// <value>
        /// The product list.  This is the list of SalesAlias associated to an order.  This list can also include billing items.
        /// </value>
        public List<ProductModel> ProductList { get; set; } 

    }
}
