﻿using System;
using System.Collections.Generic;


namespace SmartLogixIntegration.AscendSLXInt.Models
{
    public class CarrierList : List<Carrier>
    {
    }
    /// <summary>
    /// This is the model representing a Carrier associated with the Carrier table in ascend passed to SLX web services
    /// </summary>
    public class Carrier
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Number.
        /// </summary>
        /// <value>
        /// The Number.
        /// </value>
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets the SCAC.
        /// </summary>
        /// <value>
        /// The SCAC.
        /// </value>
        public string SCAC { get; set; }

       
    }
}
