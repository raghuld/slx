﻿using System;
using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the primary class utilized for the CSO services. 
    /// </summary>
    public class CompletedSalesOrderModel
    {
        /// <summary>
        /// Gets or sets the ascend sales order number.
        /// </summary>
        /// <value>
        /// The ascend sales order number.
        /// </value>
        public string AscendSalesOrderNumber { get; set; }
        /// <summary>
        /// Gets or sets the user identifier.  This is the user who is completing the sales order.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }
        /// <summary>
        /// Gets or sets the order delivery date.
        /// </summary>
        /// <value>
        /// The order delivery date.
        /// </value>
        public DateTime? OrderDeliveryDate { get; set; }
        /// <summary>
        /// Gets or sets the is net billing.
        /// </summary>
        /// <value>
        /// The is net billing.
        /// </value>
        public bool? IsNetBilling { get; set; }
        /// <summary>
        /// Gets or sets the bols.
        /// </summary>
        /// <value>
        /// The bols.  This is a list of BOLs each must be different in order for the underlying code below the model to work.
        /// </value>
        public List<BOLModel> BOLS { get; set; }
        public string PONumber { get; set; }
    }
}
