﻿using System;
using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the model utilized for NSO / MSO / CSO as the ascend SalesAlias associated to an order
    /// </summary>
    public class ProductModel
   {
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductId { get; set; }
        /// <summary>
        /// Gets or sets the bought as products.
        /// </summary>
        /// <value>
        /// The bought as products.  This is a list of all the purchalias ascend table associations / order components related to this sales alias
        /// </value>
        public List<BoughtAsProduct> BoughtAsProducts { get; set; }
        /// <summary>
        /// Gets or sets the supplier identifier.
        /// </summary>
        /// <value>
        /// The supplier identifier.
        /// </value>
        public string SupplierId { get; set; }
        /// <summary>
        /// Gets or sets the supply pt identifier.
        /// </summary>
        /// <value>
        /// The supply pt identifier.
        /// </value>
        public string SupplyPtId { get; set; }
        /// <summary>
        /// Gets or sets the inventory site identifier.
        /// </summary>
        /// <value>
        /// The inventory site identifier.
        /// </value>
        public string InventorySiteId { get; set; }
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public double Quantity { get; set; }
        /// <summary>
        /// Gets or sets the net quantity.
        /// </summary>
        /// <value>
        /// The net quantity.
        /// </value>
        public double ? NetQuantity { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double? Price { get; set; }
        /// <summary>
        /// Gets or sets the freight.
        /// </summary>
        /// <value>
        /// The freight.
        /// </value>
        public decimal Freight { get; set; }
        /// <summary>
        /// Gets or sets the asset identifier.
        /// </summary>
        /// <value>
        /// The asset identifier.
        /// </value>
        public string AssetId { get; set; }
        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        public DateTime Date { get; set; }
        /// <summary>
        /// Gets or sets the tax.
        /// </summary>
        /// <value>
        /// The tax.  This is a list of all the taxes for this product
        /// </value>
        public List<TaxModel> Tax { get; set; }
   }
}


