﻿namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This class is used in the CancelSalesOrder web service call and tells Ascend what SalesOrderNumber to cancel.
    /// </summary>
    public class CancelSalesOrder
   {
        /// <summary>
        /// Gets or sets the sales order number to cancel
        /// </summary>
        /// <value>
        /// The sales order number to cancel.
        /// </value>
        public string SalesOrderNumber { get; set; }
   }
}
