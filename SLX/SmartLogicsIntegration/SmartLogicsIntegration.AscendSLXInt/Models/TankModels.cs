﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
   public class CustomerTankList : List<CustomerTank>
   {
   }

    /// <summary>
    /// This is the model representing ARShipTo_Tank in Ascend for passing tanks to SLX
    /// </summary>
    public class CustomerTank
    {
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        public string Barcode { get; set; }
        /// <summary>
        /// Gets or sets the barcode cross reference identifier.
        /// </summary>
        /// <value>
        /// The barcode cross reference identifier.
        /// </value>
        public string BarcodeCrossReferenceId { get; set; }
        /// <summary>
        /// Gets or sets the capacity.
        /// </summary>
        /// <value>
        /// The capacity.
        /// </value>
        public float Capacity { get; set; }
        /// <summary>
        /// Gets or sets the cross reference identifier.
        /// </summary>
        /// <value>
        /// The cross reference identifier.
        /// </value>
        public string CrossReferenceId { get; set; }
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the f factor.
        /// </summary>
        /// <value>
        /// The f factor.
        /// </value>
        public decimal FFactor { get; set; }
        /// <summary>
        /// Gets or sets the k factor.
        /// </summary>
        /// <value>
        /// The k factor.
        /// </value>
        public decimal KFactor { get; set; }
        /// <summary>
        /// Gets or sets the type of the order.
        /// </summary>
        /// <value>
        /// The type of the order.
        /// </value>
        public SalesOrderType OrderType { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerTank"/> is owned.
        /// </summary>
        /// <value>
        ///   <c>true</c> if owned; otherwise, <c>false</c>.
        /// </value>
        public bool Owned { get; set; }
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [rainy day delivery].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [rainy day delivery]; otherwise, <c>false</c>.
        /// </value>
        public bool RainyDayDelivery { get; set; }
        /// <summary>
        /// Gets or sets the ship to identifier.
        /// </summary>
        /// <value>
        /// The ship to identifier.
        /// </value>
        public string ShipToId { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public CustomerTankStatus Status { get; set; }
        /// <summary>
        /// Gets or sets the tank identifier.
        /// </summary>
        /// <value>
        /// The tank identifier.
        /// </value>
        public string TankId { get; set; }
        /// <summary>
        /// Gets or sets the type of the tank.
        /// </summary>
        /// <value>
        /// The type of the tank.
        /// </value>
        public CustomerTankType TankType { get; set; }

    }

    public enum SalesOrderType
   {

      [System.Runtime.Serialization.EnumMemberAttribute()]
      None = -1,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Tank = 10,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Pumpout = 11,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Gravity = 12,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Flush = 13,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Fleet = 15,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Item = 21,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Railroad = 23,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      PumpoutGravity = 24,
   }

   public enum CustomerTankType : int
   {

      [System.Runtime.Serialization.EnumMemberAttribute()]
      NonForecasted = 0,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Monitored = 1,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Forecasted = 2,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      DegreeDay = 3,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Unknown = 4,
   }

   public enum CustomerTankStatus : int
   {

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Inactive = 0,

      [System.Runtime.Serialization.EnumMemberAttribute()]
      Active = 1,
   }
}
