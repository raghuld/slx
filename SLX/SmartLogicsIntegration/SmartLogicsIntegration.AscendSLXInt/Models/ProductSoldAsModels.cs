﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    public class ProductSoldAsList : List<ProductSoldAs>
    {
    }

    /// <summary>
    /// This is the model representing the sold as relationship that a SalesAlias has to a PurchAlias when passing
    /// product data to SLX.
    /// </summary>
    public class ProductSoldAs
    {

        /// <summary>
        /// Gets or sets the sold as product description.
        /// </summary>
        /// <value>
        /// The sold as product description.
        /// </value>
        public string SoldAsProductDescription { get; set; }

        /// <summary>
        /// Gets or sets the sold as product identifier.
        /// </summary>
        /// <value>
        /// The sold as product identifier.
        /// </value>
        public string SoldAsProductId { get; set; }

    }
}
