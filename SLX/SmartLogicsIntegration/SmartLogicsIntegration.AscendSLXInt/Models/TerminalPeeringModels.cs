﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This model represents a terminal which can be a supplypt or an insite in ascend.  It is used for passing these items to SLX.
    /// </summary>
    public class TerminalPeering
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public string Number { get; set; }
        /// <summary>
        /// Gets or sets the peer identifier.
        /// </summary>
        /// <value>
        /// The peer identifier.
        /// </value>
        public int PeerId { get; set; }

    }
    public class TerminalPeeringList : List<TerminalPeering>
    {
    }
}
