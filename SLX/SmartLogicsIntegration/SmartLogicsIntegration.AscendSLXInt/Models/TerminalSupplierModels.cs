﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This model represents a relationship between a supplier and terminal in ascend it relates to the suppliersupplypt table and pushes those
    /// relationships to SLX
    /// </summary>
    public class TerminalSupplier
   {
        /// <summary>
        /// Gets or sets the supplier number.
        /// </summary>
        /// <value>
        /// The supplier number.
        /// </value>
        public string SupplierNumber { get; set; }

        /// <summary>
        /// Gets or sets the terminal number.
        /// </summary>
        /// <value>
        /// The terminal number.
        /// </value>
        public string TerminalNumber { get; set; }
   }

   public class TerminalSupplierList : List<TerminalSupplier>
   {
   }
}
