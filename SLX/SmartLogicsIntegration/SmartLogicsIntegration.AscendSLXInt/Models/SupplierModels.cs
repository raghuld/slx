﻿namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the Supplier model associated to the supplier table in ascend used for passing supplier items to SLX
    /// </summary>
    public class SupplierPeering
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public string Number { get; set; }
        /// <summary>
        /// Gets or sets the peer identifier.
        /// </summary>
        /// <value>
        /// The peer identifier.
        /// </value>
        public int PeerId { get; set; }
    }

    public class SupplierPeeringList : System.Collections.Generic.List<SupplierPeering>
   {
   }

}
