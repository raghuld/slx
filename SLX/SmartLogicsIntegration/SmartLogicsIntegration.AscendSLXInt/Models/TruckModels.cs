﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
   public class TruckList : List<Truck>
   {
   }

    /// <summary>
    /// This model is associated to the Vechicle table in ascend and is used to pass that data to SLX
    /// </summary>
    public class Truck
   {
        /// <summary>
        /// Gets or sets the alternative truck identifier.
        /// </summary>
        /// <value>
        /// The alternative truck identifier.
        /// </value>
        public string AlternativeTruckID { get; set; }

        /// <summary>
        /// Gets or sets the average confirmation time.
        /// </summary>
        /// <value>
        /// The average confirmation time.
        /// </value>
        public int AvgConfirmationTime { get; set; }
        /// <summary>
        /// Gets or sets the average transfer time.
        /// </summary>
        /// <value>
        /// The average transfer time.
        /// </value>
        public int AvgTransferTime { get; set; }
        /// <summary>
        /// Gets or sets the back office truck identifier.
        /// </summary>
        /// <value>
        /// The back office truck identifier.
        /// </value>
        public string BackOfficeTruckId { get; set; }
        /// <summary>
        /// Gets or sets the compartment list.
        /// </summary>
        /// <value>
        /// The compartment list.
        /// </value>
        public CompartmentList CompartmentList { get; set; }
        /// <summary>
        /// Gets or sets the cost per hour.
        /// </summary>
        /// <value>
        /// The cost per hour.
        /// </value>
        public float CostPerHour { get; set; }
        /// <summary>
        /// Gets or sets the cost per mile.
        /// </summary>
        /// <value>
        /// The cost per mile.
        /// </value>
        public float CostPerMile { get; set; }
        /// <summary>
        /// Gets or sets the dedicated customer identifier.
        /// </summary>
        /// <value>
        /// The dedicated customer identifier.
        /// </value>
        public string DedicatedCustId { get; set; }
        /// <summary>
        /// Gets or sets the default trailer identifier.
        /// </summary>
        /// <value>
        /// The default trailer identifier.
        /// </value>
        public string DefaultTrailerID { get; set; }
        /// <summary>
        /// Gets or sets the gross net.
        /// </summary>
        /// <value>
        /// The gross net.
        /// </value>
        public string GrossNet { get; set; }
        /// <summary>
        /// Gets or sets the in active.
        /// </summary>
        /// <value>
        /// The in active.
        /// </value>
        public string InActive { get; set; }
        /// <summary>
        /// Gets or sets the type of the inventory.
        /// </summary>
        /// <value>
        /// The type of the inventory.
        /// </value>
        public string InventoryType { get; set; }


        /// <summary>
        /// Gets or sets the jbus odometer offset.
        /// </summary>
        /// <value>
        /// The jbus odometer offset.
        /// </value>
        public float JbusOdometerOffset { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [manual entry].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [manual entry]; otherwise, <c>false</c>.
        /// </value>
        public bool ManualEntry { get; set; }
        /// <summary>
        /// Gets or sets the number of compartments.
        /// </summary>
        /// <value>
        /// The number of compartments.
        /// </value>
        public int NumberOfCompartments { get; set; }
        /// <summary>
        /// Gets or sets the number of meters.
        /// </summary>
        /// <value>
        /// The number of meters.
        /// </value>
        public int NumberOfMeters { get; set; }
        /// <summary>
        /// Gets or sets the number of reels.
        /// </summary>
        /// <value>
        /// The number of reels.
        /// </value>
        public int NumberOfReels { get; set; }
        /// <summary>
        /// Gets or sets the pump attached.
        /// </summary>
        /// <value>
        /// The pump attached.
        /// </value>
        public string PumpAttached { get; set; }
        /// <summary>
        /// Gets or sets the region identifier.
        /// </summary>
        /// <value>
        /// The region identifier.
        /// </value>
        public int RegionId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [require inspection].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [require inspection]; otherwise, <c>false</c>.
        /// </value>
        public bool RequireInspection { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [require loading].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [require loading]; otherwise, <c>false</c>.
        /// </value>
        public bool RequireLoading { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public byte Status { get; set; }
        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }
        /// <summary>
        /// Gets or sets the ticket copies.
        /// </summary>
        /// <value>
        /// The ticket copies.
        /// </value>
        public int TicketCopies { get; set; }
        /// <summary>
        /// Gets or sets the total capacity.
        /// </summary>
        /// <value>
        /// The total capacity.
        /// </value>
        public float TotalCapacity { get; set; }
        /// <summary>
        /// Gets or sets the trailer identifier.
        /// </summary>
        /// <value>
        /// The trailer identifier.
        /// </value>
        public string TrailerId { get; set; }
        /// <summary>
        /// Gets or sets the truck identifier.
        /// </summary>
        /// <value>
        /// The truck identifier.
        /// </value>
        public string TruckId { get; set; }
        /// <summary>
        /// Gets or sets the type of the truck.
        /// </summary>
        /// <value>
        /// The type of the truck.
        /// </value>
        public string TruckType { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets the unit of measure.
        /// </summary>
        /// <value>
        /// The unit of measure.
        /// </value>
        public string UnitOfMeasure { get; set; }
        /// <summary>
        /// Gets or sets the vin.
        /// </summary>
        /// <value>
        /// The vin.
        /// </value>
        public string VIN { get; set; }
        /// <summary>
        /// Gets or sets the warehouse identifier.
        /// </summary>
        /// <value>
        /// The warehouse identifier.
        /// </value>
        public string WarehouseID { get; set; } 

   }

    /// <summary>
    /// 
    /// </summary>
    public class Compartment
   {

        /// <summary>
        /// Gets or sets the capacity.
        /// </summary>
        /// <value>
        /// The capacity.
        /// </value>
        public float Capacity { get; set; }
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public int Number { get; set; }

   }

   public class CompartmentList : List<Compartment>
   {
   }
}
