﻿namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This represents an order status change request
    /// </summary>
    public class UpdateOrderStatusModel
    {
        /// <summary>
        /// Gets or sets the order no.
        /// </summary>
        /// <value>
        /// The order no.
        /// </value>
        public string OrderNo { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }

    }
}
