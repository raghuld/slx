﻿using System;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This class represents a response from SLX web service calls
    /// </summary>
    public class SmartLogixResponse
   {

        #region Properties

        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        /// <value>
        /// The response status.
        /// </value>
        public int ResponseStatus { get; set; }

        /// <summary>
        /// Gets or sets the response message.
        /// </summary>
        /// <value>
        /// The response message.
        /// </value>
        public string ResponseMessage { get; set; }

        /// <summary>
        /// Gets or sets the SLX exception.
        /// </summary>
        /// <value>
        /// The SLX exception.
        /// </value>
        public Exception SLXException { get; set; }
      
      #endregion

   }
}