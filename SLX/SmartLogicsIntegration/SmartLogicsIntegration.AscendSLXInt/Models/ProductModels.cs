﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    public class ProductList : List<Product>
    {
    }

    /// <summary>
    /// This model represents a product sent to SLX and represents changes in the SalesAlias, PurchAlias, or BillingItem tables in Ascend
    /// </summary>
    public class Product
    {

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the epa disclaimer.
        /// </summary>
        /// <value>
        /// The epa disclaimer.
        /// </value>
        public string EPADisclaimer { get; set; }
        /// <summary>
        /// Gets or sets the peer identifier.
        /// </summary>
        /// <value>
        /// The peer identifier.
        /// </value>
        public int PeerId { get; set; }
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductID { get; set; }
        /// <summary>
        /// Gets or sets the product category.
        /// </summary>
        /// <value>
        /// The product category.
        /// </value>
        public string ProductCategory { get; set; }
        /// <summary>
        /// Gets or sets the type of the product.
        /// </summary>
        /// <value>
        /// The type of the product.
        /// </value>
        public string ProductType { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [non fuel].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [non fuel]; otherwise, <c>false</c>.
        /// </value>
        public bool NonFuel { get; set; }
        /// <summary>
        /// Gets or sets the uom.
        /// </summary>
        /// <value>
        /// The uom.
        /// </value>
        public string UOM { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [product purchased only].
        /// </summary>
        /// <value>
        /// <c>true</c> if [product purchased only]; otherwise, <c>false</c>.
        /// </value>
        public bool ProductPurchasedOnly { get; set; }
        /// <summary>
        /// Gets or sets the product sold as list.
        /// </summary>
        /// <value>
        /// The product sold as list.  This list is only used if the product is a PurchAlias and passes all the salesaliases that could
        /// be built from this PurchAlias in an order.
        /// </value>
        public List<ProductSoldAs> ProductSoldAsList { get; set; }
    }

}
