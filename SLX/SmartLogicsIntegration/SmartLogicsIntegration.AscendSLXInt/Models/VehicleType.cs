﻿namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// VehicleType is mostly used to determine what SLX web service to call FleetVehicle or Tank
    /// </summary>
    public enum VehicleType
   {
        /// <summary>
        /// Represents a tank
        /// </summary>
        Tank = 1,
        /// <summary>
        /// Represents a truck
        /// </summary>
        Truck = 2
   }
}
