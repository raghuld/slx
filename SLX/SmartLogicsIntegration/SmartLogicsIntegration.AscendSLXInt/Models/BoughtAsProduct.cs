﻿using System;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// Bought as products are the components that make up a sales alias these are purch aliases in ascend
    /// </summary>
    public class BoughtAsProduct
    {
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public string ProductId { get; set; }
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public double Quantity { get; set; }
        /// <summary>
        /// Gets or sets the net quantity.
        /// </summary>
        /// <value>
        /// The net quantity.
        /// </value>
        public double ? NetQuantity { get; set; }
        /// <summary>
        /// Gets or sets the supplier identifier.
        /// </summary>
        /// <value>
        /// The supplier identifier.
        /// </value>
        public string SupplierId { get; set; }
        /// <summary>
        /// Gets or sets the supply pt identifier.
        /// </summary>
        /// <value>
        /// The supply pt identifier.
        /// </value>
        public string SupplyPtId { get; set; }
        /// <summary>
        /// Gets or sets the inventory site identifier.
        /// </summary>
        /// <value>
        /// The inventory site identifier.
        /// </value>
        public string InventorySiteId { get; set; }
        /// <summary>
        /// Gets or sets the bol number.
        /// </summary>
        /// <value>
        /// The bol number.
        /// </value>
        public string BOLNumber { get; set; }
        /// <summary>
        /// Gets or sets the bol date.
        /// </summary>
        /// <value>
        /// The bol date.
        /// </value>
        public DateTime? BOLDate { get; set; }

        /// <summary>
        /// Gets or sets the state of the diversion dest.  SLX will always use this to pass the current state that the delivery is being
        /// made to.
        /// </summary>
        /// <value>
        /// The state of the diversion dest.
        /// </value>
        public string DiversionDestState { get; set; }
    }
}
