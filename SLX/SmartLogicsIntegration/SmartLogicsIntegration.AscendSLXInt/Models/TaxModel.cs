﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the model representing an individual tax on a product for an ascend order
    /// </summary>
    public class TaxModel
    {
        /// <summary>
        /// Gets or sets the slsodti number.
        /// </summary>
        /// <value>
        /// The slsodti number.
        /// </value>
        public string SLSODTI_Number { get; set; }
        /// <summary>
        /// Gets or sets the tax fee type identifier.
        /// </summary>
        /// <value>
        /// The tax fee type identifier.
        /// </value>
        public int TaxFeeTypeId { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public double Amount { get; set; }
        /// <summary>
        /// Gets or sets the compound tax fee items list.
        /// </summary>
        /// <value>
        /// The compound tax fee items list.  Each tax can have other compound taxs on top of it.
        /// </value>
        public List<CompoundTaxModel> CompoundTaxFeeItemsList { get; set; }
    }
}
