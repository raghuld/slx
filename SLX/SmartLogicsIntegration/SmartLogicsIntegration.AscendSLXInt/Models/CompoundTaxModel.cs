﻿namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the model used for Compound Taxes in Ascend
    /// </summary>
    public class CompoundTaxModel
   {
        /// <summary>
        /// Gets or sets the slsodcti number.
        /// </summary>
        /// <value>
        /// The slsodcti number.
        /// </value>
        public string SLSODCTI_Number { get; set; }
        /// <summary>
        /// Gets or sets the s tax fee type identifier.
        /// </summary>
        /// <value>
        /// The s tax fee type identifier.
        /// </value>
        public int STaxFeeTypeId { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public double Amount { get; set; }
   }
}
