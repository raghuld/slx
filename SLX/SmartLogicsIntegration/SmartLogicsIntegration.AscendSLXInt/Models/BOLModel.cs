﻿using System;
using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    /// <summary>
    /// This is the BOLModel used in CSO a CSO will have a list of these based on the number of BOLs
    /// </summary>
    public class BOLModel
    {

        /// <summary>
        /// Gets or sets the bol number.
        /// </summary>
        /// <value>
        /// The bol number.
        /// </value>
        public string BOLNumber { get; set; }

        /// <summary>
        /// Gets or sets the bol date.
        /// </summary>
        /// <value>
        /// The bol date.
        /// </value>
        public DateTime ? BOLDate { get; set; }

        /// <summary>
        /// Gets or sets the standard acct identifier.
        /// </summary>
        /// <value>
        /// The standard acct identifier.
        /// </value>
        public string StandardAcctId { get; set; }

        /// <summary>
        /// Gets or sets the ship to identifier.
        /// </summary>
        /// <value>
        /// The ship to identifier.
        /// </value>
        public string ShipToId { get; set; }

        /// <summary>
        /// Gets or sets the vehicle identifier.
        /// </summary>
        /// <value>
        /// The vehicle identifier.
        /// </value>
        public string VehicleId { get; set; }

        /// <summary>
        /// Gets or sets the driver identifier.
        /// </summary>
        /// <value>
        /// The driver identifier.
        /// </value>
        public string DriverId { get; set; }

        /// <summary>
        /// Gets or sets the po number.
        /// </summary>
        /// <value>
        /// The po number.
        /// </value>
        public string PONumber { get; set; }

        /// <summary>
        /// Gets or sets the state of the diversion dest.  SLX will always use this to pass the current state that the delivery is being
        /// made to.
        /// </summary>
        /// <value>
        /// The state of the diversion dest.
        /// </value>
        public string DiversionDestState { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is inventory adjustment.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is inventory adjustment; otherwise, <c>false</c>.
        /// </value>
        public bool IsInventoryAdjustment { get; set; }

        /// <summary>
        /// Gets or sets the products.  This list of products represents the Sales Aliases that are on an order
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        public List<ProductModel> Products { get; set; }

    }
}
