﻿using System.Collections.Generic;

namespace SmartLogixIntegration.AscendSLXInt.Models
{
    public class CustomerList : List<Customer>
    {
    }

    /// <summary>
    /// This model represents a customer passed across to the SLX web services
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }


        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        /// <value>
        /// The contact email.
        /// </value>
        public string ContactEmail { get; set; }


        /// <summary>
        /// Gets or sets the name of the contact.
        /// </summary>
        /// <value>
        /// The name of the contact.
        /// </value>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the contact phone.
        /// </summary>
        /// <value>
        /// The contact phone.
        /// </value>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }


        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the order hold identifier.
        /// </summary>
        /// <value>
        /// The order hold identifier.
        /// </value>
        public int? OrderHoldId { get; set; }

        /// <summary>
        /// Gets or sets the peer identifier.
        /// </summary>
        /// <value>
        /// The peer identifier.
        /// </value>
        public int PeerId { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [print price on ticket].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [print price on ticket]; otherwise, <c>false</c>.
        /// </value>
        public bool PrintPriceOnTicket { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [require stick readings].
        /// </summary>
        /// <value>
        /// <c>true</c> if [require stick readings]; otherwise, <c>false</c>.
        /// </value>
        public bool RequireStickReadings { get; set; }


        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }


        /// <summary>
        /// Gets or sets the ticket copies.
        /// </summary>
        /// <value>
        /// The ticket copies.
        /// </value>
        public int TicketCopies { get; set; }


        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        /// <value>
        /// The zip.
        /// </value>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is credit hold.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is credit hold; otherwise, <c>false</c>.
        /// </value>
        public bool IsCreditHold { get; set; }

    }
}
