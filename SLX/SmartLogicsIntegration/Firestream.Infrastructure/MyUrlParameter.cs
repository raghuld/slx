﻿namespace Firestream.Infrastructure
{
   public sealed class MyUrlParameter
   {
      public static readonly MyUrlParameter Optional = new MyUrlParameter();

      private MyUrlParameter()
      {
      }
   }
}
