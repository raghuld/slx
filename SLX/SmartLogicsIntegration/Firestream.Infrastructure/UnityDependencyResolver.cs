﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;

namespace Firestream
{
    public class UnityDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// Gets the unity DI container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        public IUnityContainer Container { get; private set; }


        /// <summary>
        /// Gets or sets the bootstrapper.
        /// </summary>
        /// <value>
        /// The bootstrapper.
        /// </value>
        private UnityBootstrapper Bootstrapper { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityDependencyResolver"/> class.
        /// </summary>
        /// <param name="bootstrapper">The bootstrapper is required to setup unity.</param>
        public UnityDependencyResolver(UnityBootstrapper bootstrapper)
        {
            InitializeDependencyInjectionSystem(bootstrapper);
        }

        #region IDependencyResolver Implementation

        /// <summary>
        /// Gets the object type and resolves it
        /// </summary>
        /// <param name="serviceType">Type of the object.</param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            object service = Container.TryResolve(serviceType);

            return service;
        }

        /// <summary>
        /// Gets the the object type of multiple objects and resolves them.
        /// </summary>
        /// <param name="serviceType">Type of the object.</param>
        /// <returns></returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            IEnumerable<object> services = Container.ResolveAll(serviceType);

            return services;
        }

        #endregion

        /// <summary>
        /// Initializes the dependency injection system.
        /// </summary>
        /// <param name="bootstrapper">The bootstrapper.</param>
        private void InitializeDependencyInjectionSystem(UnityBootstrapper bootstrapper)
        {
            Bootstrapper = bootstrapper;

            //initialize our DI system by running the bootstrapper
            //basically, we're using this as a convenient way to load up
            //our assemblies and have them register contained services.
            Bootstrapper.Run();

            //With that complete, capture the container as this class is responsible
            //for delegating requests from the MVC system to the DI container.
            Container = Bootstrapper.Container;
        }
    }
}