
(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard',
                config: {
                    abstract: true,
                    url: '',
                    views: {
                        '@': {
                            templateUrl: 'app/dashboard/partials/dashboard.html',
                            controller: 'dashboard as vm'
                        },
                        'dashboardLeftNav@dashboard': {
                            templateUrl: 'app/dashboard/partials/dashboardLeftNav.html',
                            controller: 'dashboardLeftNav as vm'
                        },
                        'dashboardTopNav@dashboard': {
                            templateUrl: 'app/dashboard/partials/dashboardTopNav.html',
                            controller: 'dashboardTopNav as vm'
                        }
                    },
                    resolve: {
                        //allCounties: allCounties
                    },
                    requireADLogin: true     // Designates that the user must be authenticated to view this page
                }
            }
        ];
    }

    //allCounties.$inject = ['countyService'];

    //function allCounties(countyService) {

    //    return countyService.getCounties().then(function (counties) {

    //        return counties;
    //    });
    //}
})();