(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('dashboardLeftNav', dashboardLeftNav);

    dashboardLeftNav.$inject = ['$state', 'common']; 

    function dashboardLeftNav($state, common) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'dashboardLeftNav';

        activate();

        function activate() {

            common.activateController([], vm.title)
                .then(function () {
                    common.logger.success('Activated View', null, vm.title);
                });
        }
    }
})();
