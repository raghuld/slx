(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('dashboardTopNav', dashboardTopNav);

    dashboardTopNav.$inject = ['common'];

    function dashboardTopNav(common) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'dashboardTopNav';

        activate();

        function activate() {

            common.activateController([], vm.title)
                .then(function () {
                    common.logger.success('Activated View', null, vm.title);
                });
        }
    }
})();
