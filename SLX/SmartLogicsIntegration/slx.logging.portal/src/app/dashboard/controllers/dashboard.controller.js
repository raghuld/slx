
(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('dashboard', dashboard);

    dashboard.$inject = ['common'];
    function dashboard(common) {
        var vm = this;
        vm.title = 'Dashboard';

        activate();

        function activate() {
            common.activateController([], vm.title)
                .then(function () {
                    common.logger.success('Activated View', null, vm.title);
                });
        }
    }
})();