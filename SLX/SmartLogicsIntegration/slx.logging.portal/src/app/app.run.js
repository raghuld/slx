(function () {
    'use strict';

    angular.module('app')
        .run(stateChangeEvents);

    stateChangeEvents.$inject = ['$rootScope']

    function stateChangeEvents($rootScope) {
        // This event gets triggered on refresh or URL change
        $rootScope.$on('$stateChangeStart', function (event, to, toParams, from, fromParams) {

        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

            //console.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
        });
    }

})();