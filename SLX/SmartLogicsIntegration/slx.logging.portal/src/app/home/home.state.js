
(function () {
    'use strict';

    angular
        .module('app.home')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), '/');
    }

    function getStates() {
        return [
            {
                state: 'home',
                config: {
                    url: '/',
                    parent: 'dashboard',
                    templateUrl: 'app/home/partials/home.html',
                    controller: 'home as vm'
                }
            }
        ];
    }
})();