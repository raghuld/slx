(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('home', home);

    home.$inject = ['common']; 

    function home(common) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'home';

        // Members
        vm.query = {
            order: '-Id',
            limit: 25,
            page: 1,
            search: {
            }
        };

        vm.limitOptions = [25, 50, 100];
        vm.totalCount = 0;

        activate();

        function activate() {

            common.activateController([], vm.title)
                .then(function () {
                    common.logger.success('Activated View', null, vm.title);
                });
        }

        function getSLXLogsPaginated() {

        }
    }
})();
