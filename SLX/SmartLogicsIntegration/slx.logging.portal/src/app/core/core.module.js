
(function () {
    'use strict';

    angular.module('app.core', [
        /* Angular Modules */
        'ngAnimate',
        'ngSanitize',
        'ngMaterial',
        'ngAria',
        'ngMessages',
        /* Cross-app modules */
        'blocks.exception',
        'blocks.logger',
        'blocks.router',
        'blocks.common',
        /* 3rd-party modules */
        'ngMaterialSidemenu',
        'md.data.table',
        'ui.router'
    ]);
})();