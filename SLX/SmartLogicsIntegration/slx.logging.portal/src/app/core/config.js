
(function() {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }
    
    var events = {
        controllerActivateSuccess: 'controller.activateSuccess'
    };

    var config = {
        appErrorPrefix: '[' + "slx.logging.portal" + ' Error] ', //Configure the exceptionHandler decorator
        appTitle: "slx.logging.portal",
        events: events,
        version: '0.1.0',
        imageBasePath: './images/'
    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$compileProvider', 
                         '$logProvider', 
                         'routerHelperProvider', 
                         'exceptionHandlerProvider',
                         'loggerProvider',
                         'Settings'];
    /* @ngInject */
    function configure ($compileProvider, 
                        $logProvider,
                        routerHelperProvider, 
                        exceptionHandlerProvider,
                        loggerProvider,
                        Settings) {
        $compileProvider.debugInfoEnabled(false);
        
        
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        
        loggerProvider.logsEnabled(Settings.logsEnabled);
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        configureStateHelper();

        ////////////////

        function configureStateHelper() {
            var resolveAlways = {
                ready: ready
            };

            //ready.$inject = ['dataservice'];
            //function ready(dataservice) {
            //    //return dataservice.ready();
            //}

            function ready() {

            }

            routerHelperProvider.configure({
                docTitle: config.appTitle,
                resolveAlways: resolveAlways
            });
        }
    }
    
    /**
     * Configure the common services via commonConfig
     */
    core.config(['commonConfigProvider', function(cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
    }]);
})();
