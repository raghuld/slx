
(function () {
    'use strict';

    angular.module('app', [
        // Angular modules 

        // Custom modules 
        'app.core',
        'app.dashboard',
        'app.home',
        'app.widgets',
        
        // 3rd Party Modules
        'ui.router'
    ]);
})();