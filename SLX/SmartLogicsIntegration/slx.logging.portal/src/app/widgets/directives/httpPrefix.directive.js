(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('httpPrefix', httpPrefix);

    httpPrefix.$inject = [];

    function httpPrefix() {
        // Usage:
        //     <input type="url" name="field1" ng-model="vm.field1" http-prefix></input>
        // Creates:
        // 
        var directive = {
            link: link,
            require: 'ngModel',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs, controller) {

            function ensureHttpPrefix(value) {
                // Need to add prefix if we don't have http:// prefix already AND we don't have part of it
                if (value && !/^(https?):\/\//i.test(value)
                    && 'http://'.indexOf(value) !== 0 && 'https://'.indexOf(value) !== 0) {
                    controller.$setViewValue('http://' + value);
                    controller.$render();
                    return 'http://' + value;
                }
                else {
                    return value;
                }
            }

            controller.$formatters.push(ensureHttpPrefix);
            controller.$parsers.splice(0, 0, ensureHttpPrefix);
        }
    }

})();