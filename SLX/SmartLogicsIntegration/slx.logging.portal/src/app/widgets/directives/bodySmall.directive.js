(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('bodySmall', bodySmall);

    bodySmall.$inject = ['$window'];

    function bodySmall($window) {

        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {

            var w = angular.element($window);

            if (w[0].innerWidth < 769) {
                angular.element('body').addClass('body-small');
            }

            angular.element($window).bind('load resize', function () {

                var body = angular.element('body');

                if (this.innerWidth < 769) {
                    body.addClass('body-small');
                } else {
                    body.removeClass('body-small');
                }

                scope.$apply();
            });
        }
    }

})();