(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('minimalizeSidebar', minimalizeSidebar);

    minimalizeSidebar.$inject = ['$timeout'];

    function minimalizeSidebar($timeout) {

        var directive = {
            link: link,
            restrict: 'A',
            template: '<a href="" ng-click="minimalize()"><i class="material-icons">dehaze</i></a>'
        };

        return directive;

        function link(scope, element, attrs) {

            var body = angular.element('body');
            var sidemenu = angular.element('#side-menu');

            scope.minimalize = minimalize;

            function minimalize() {
                body.toggleClass('mini-navbar');

                if (body.hasClass('mini-navbar') || body.hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    sidemenu.hide();
                    // For smoothly turn on menu
                    $timeout(function () {
                        sidemenu.fadeIn(400);
                    }, 200);
                } else if (body.hasClass('fixed-sidebar')) {
                    sidemenu.hide();
                    $timeout(function () {
                        sidemenu.fadeIn(400);
                    }, 100);
                } else {
                    // Remove all inline style from jqeury fadeIn function to reset menu state
                    sidemenu.removeAttr('style');
                    sidemenu.hide();

                    $timeout(function () {
                        sidemenu.fadeIn(400);
                    }, 100);
                }
            }
        }
    }

})();