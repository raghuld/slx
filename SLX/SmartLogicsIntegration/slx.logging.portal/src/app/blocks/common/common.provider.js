


(function() {
    'use strict';

    angular
        .module('blocks.common')
        .provider('commonConfig', commonConfigProvider);

    // Must configure the common service and set its 
    // events via the commonConfigProvider
    function commonConfigProvider() {
        /* jshint validthis:true */
        this.config = {
            // These are the properties we need to set
            //controllerActivateSuccessEvent: '',
            //spinnerToggleEvent: ''
        };

        this.$get = function() {
            return {
                config: this.config
            };
        };
    }
})();
