(function () {
    'use strict';

    angular
        .module('blocks.logger')
        .provider('logger', loggerProvider);

    function loggerProvider() {
        var showToasts = true;
        var showLogs = true;

        this.logsEnabled = function (value) {
            showToasts = value;
            showLogs = value;
        };

        this.$get = loggerFactory;

        loggerFactory.$inject = ['$log', 'toastr'];
        function loggerFactory($log, toastr) {
            var service = {
                showToasts: showToasts,
                showLogs: showLogs,

                error: error,
                info: info,
                success: success,
                warning: warning,

                // straight to console; bypass toastr
                log: $log.log
            };

            return service;
            /////////////////////

            function error(message, data, title) {

                if (service.showToasts) {
                    toastr.error(message, title);
                }

                if (service.showLogs) {
                    $log.error('Error: ' + message, data);
                }
            }

            function info(message, data, title) {

                if (service.showToasts) {
                    toastr.info(message, title);
                }

                if (service.showLogs) {
                    $log.info('Info: ' + message, data);
                }
            }

            function success(message, data, title) {

                if (service.showToasts) {
                    toastr.success(message, title);
                }

                if (service.showLogs) {
                    $log.info('Success: ' + message, data);
                }
            }

            function warning(message, data, title) {
                if (service.showToasts) {
                    toastr.warning(message, title);
                }

                if (service.showLogs) {
                    $log.warn('Warning: ' + message, data);
                }
            }
        }
    }
})();