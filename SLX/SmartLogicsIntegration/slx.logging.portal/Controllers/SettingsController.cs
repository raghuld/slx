using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using slx.logging.portal.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace slx.logging.portal.Controllers
{
    public class SettingsController : Controller
    {
        public SettingsController(IOptions<AppSettings> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        AppSettings Options { get; }

        // GET: /Settings
        public IActionResult Index()
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var settingsJson = JsonConvert.SerializeObject(Options, Formatting.Indented, serializerSettings);

            var settingsVm = new SettingsViewModel
            {
                SettingsJson = settingsJson  
            };
            return View(settingsVm);
        }
    }
}
