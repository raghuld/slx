/// <binding BeforeBuild='optimize' />
/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

var gulp = require('gulp');

gulp.task('default', ['optimize'], function () {
    // place code for your default task here
});

require('require-dir')('./gulp');