'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Wire up bower css/js and app js into index.html
 */
gulp.task('inject', ['copy-vendor', 'copy-scripts', 'copy-partials', 'copy-fonts'], function () {
    config.log('Wire up the bower css js and our app js into the html');
    var options = config.getWiredepDefaultOptions();
    var injectOptions = {
        ignorePath: '/.tmp'
    };

    var wiredep = require('wiredep').stream;

    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.injectScripts, { read: false }), injectOptions))
        .pipe(gulp.dest(config.src))
        .pipe(gulp.dest(config.temp));
});

/**
 * Wire up the app css into the html 
 */
gulp.task('inject-styles', ['styles', 'inject', 'templatecache'], function () {
    config.log('Wire up the app css into the html, and call wiredep');
    var injectOptions = {
        ignorePath: '/.tmp'
    };

    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.css, { read: false }), injectOptions))
        .pipe(gulp.dest(config.src))
        .pipe(gulp.dest(config.temp));
});