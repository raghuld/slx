'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var del = require('del');

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Clean 
 */
gulp.task('clean', function () {
    var delconfig = [
        config.webroot,
        config.temp
    ];
    config.log('Cleaning: ' + $.util.colors.blue(delconfig));
    return del(delconfig);
});

/**
 * Clean CSS Styles
 */
gulp.task('clean-styles', function () {
    var files = [].concat(config.webroot + 'styles', config.temp + 'css');
    return clean(files);
});

/**
 * Clean Images
 */
gulp.task('clean-images', function () {
    var delconfig = [].concat(config.webroot + 'images', config.temp + 'images');
    return clean(delconfig);
});

/**
 * Clean Fonts
 */
gulp.task('clean-fonts', function () {
    var delconfig = [].concat(config.webroot + 'fonts', config.temp + 'fonts');
    return clean(delconfig);
});


/**
 * Clean JS Files
 */
gulp.task('clean-scripts', function () {
    var files = [].concat(config.temp + 'js', config.webroot + 'js');
    return clean(files);
});

/**
 * Clean Vendor Scripts and CSS
 */
gulp.task('clean-vendor', function () {
    var files = config.lib;
    return clean(files);
});

/**
 * Clean Partials
 */
gulp.task('clean-partials', function () {
    var files = [].concat(
            config.temp + 'app'
        );
    return clean(files);
});

/**
 * Clean Templatecache code
 */
gulp.task('clean-code', function () {
    var files = [].concat(
            config.temp + 'templates.js'
        );
    return clean(files);
});

///////////////////////////

function clean(path) {
    config.log('Cleaning: ' + path);
    return del(path);
}