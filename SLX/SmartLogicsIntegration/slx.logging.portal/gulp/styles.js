'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Compile Less to CSS
 */
gulp.task('styles', ['clean-styles'], function () {
    config.log('Compiling Less --> CSS');
    var lessOptions = {
        options: [
            config.webroot + 'less'
        ]
    };

    var injectOptions = {
        transform: function (filePath) {
            filePath = filePath.replace('/src/less/', '');
            return '@import "' + filePath + '";'
        },
        starttag: '// injector',
        endtag: '// endinjector'
    };
    return gulp
        .src(config.less)
        .pipe($.inject(gulp.src(config.injectLess, { read: false }), injectOptions))
        .pipe($.plumber())
        .pipe($.less(lessOptions))
        .pipe($.autoprefixer({ browsers: ['last 2 version', '> 5%'] }))
        .pipe(gulp.dest(config.temp + 'css'));
});