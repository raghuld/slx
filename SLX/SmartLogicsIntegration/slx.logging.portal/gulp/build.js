'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Create AngularJS Template Cache
 */
gulp.task('templatecache', ['clean-code'], function () {
    config.log('Creating AngularJS $templateCache');

    return gulp
        .src(config.htmlTemplates)
        .pipe($.minifyHtml({ empty: true }))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
            ))
        .pipe(gulp.dest(config.temp));
});

/**
 * Optimized build pipeline
 */
gulp.task('optimize', ['inject-styles', 'copy-images'], function () {
    config.log('Optimizing the javascript, css, html');

    var templateCache = config.temp + config.templateCache.file;
    var cssFilter = $.filter('**/*.css', { restore: true });
    var jsLibFilter = $.filter('**/lib.js', { restore: true });
    var jsAppFilter = $.filter('**/app.js', { restore: true });
    var indexHtmlFilter = $.filter(['**/*', '!**/index.html'], { restore: true });

    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, { read: false }), {
            starttag: '<!-- inject:templates:js -->',
            ignorePath: '.tmp'
        }))
        .pipe($.useref({ searchPath: './.tmp' }))
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore)
        .pipe(jsLibFilter)
        .pipe($.uglify())
        .pipe(jsLibFilter.restore)
        .pipe(jsAppFilter)
        .pipe($.ngAnnotate())
        .pipe($.uglify())
        .pipe(jsAppFilter.restore)
        .pipe(indexHtmlFilter)
        .pipe($.rev()) // app.js --> app-1j889jr.js
        .pipe(indexHtmlFilter.restore)
        .pipe($.revReplace())
        .pipe(gulp.dest(config.webroot));
});
