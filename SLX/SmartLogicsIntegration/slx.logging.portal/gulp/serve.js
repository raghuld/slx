
'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var browserSync = require('browser-sync');

/**
 * Serve from wwwroot folder with optimized files
 */
gulp.task('serve-wwwroot', ['optimize'], function() {
    serve(false /* isDev */);
});

/**
 * Serve from temp folder with plain pre-processed files
 */
gulp.task('serve-tmp', ['inject'], function() {
    serve(true /* isDev */);
});

//////////////////////////////

function serve(isDev) {

    if (browserSync.active) {
        return;
    }
    
    config.log('Starting browser-sync');

    if (isDev) {
        gulp.watch([config.src + 'less/**/*.less'], ['inject-styles', browserSync.reload])
            .on('change', changeEvent);

        gulp.watch([config.srcScripts, config.sourceApp + '**/*.html'], ['inject', browserSync.reload])
            .on('change', changeEvent);
    } else {
        gulp.watch([config.less, config.srcScripts, config.html], ['optimize', browserSync.reload])
            .on('change', changeEvent);
    }

    browserSync({
        proxy: 'localhost:61146',
        files: [
        ],
        port: 3000,
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        notify: true,
        reloadDelay: 0
    });
}

function changeEvent(event) {
    var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
    config.log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}