'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Analyze source with JSHINT and JSCS
 */
gulp.task('script', function () {
    config.log('Analyzing source with JSHint and JSCS');

    return gulp
        .src(config.alljs)
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', { verbos: true }))
        .pipe($.jshint.reporter('fail'));
});