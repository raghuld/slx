'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var mainBowerFiles = require('main-bower-files');

var $ = require('gulp-load-plugins')({ lazy: true });

/**
 * Copy Vendor Script Files
 */
gulp.task('copy-vendor', ['clean-vendor'], function () {
    config.log('Copying main bower files');

    return gulp
        .src(mainBowerFiles(), { base: config.bower.directory })
        .pipe(gulp.dest(config.lib));
});

/**
 * Copy JS Files
 */
gulp.task('copy-scripts', ['clean-scripts'], function () {
    config.log('Copying JS files');

    return gulp
        .src(config.srcScripts)
        .pipe(gulp.dest(config.temp + 'js'));
});

/**
 * Copy and compress images to ./wwwroot/
 */
gulp.task('copy-images', ['clean-images'], function () {
    config.log('Copying and compressing the images');

    return gulp
        .src(config.images)
        .pipe($.imagemin({ optimizationLevel: 4 }))
        .pipe(gulp.dest(config.temp + 'images'))
        .pipe(gulp.dest(config.webroot + 'images'));
});

/**
 * Copy partials
 */
gulp.task('copy-partials', ['clean-partials'], function () {
    config.log('Copying Partials');

    return gulp
        .src(config.htmlTemplates)
        .pipe(gulp.dest(config.temp + 'app'));
});

/**
 * Copy Fonts
 */
gulp.task('copy-fonts', ['clean-fonts'], function () {

    config.log('Copying Fonts');

    return gulp
        .src(config.fonts) //TODO
        .pipe(gulp.dest(config.temp + 'fonts'))
        .pipe(gulp.dest(config.webroot + 'fonts'));
});

