
'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var args = require('yargs').argv;

var $ = require('gulp-load-plugins')({lazy: true});

/**
 * Bump the version
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and ignore other flags
 */
gulp.task('bump', ['bump-packages', 'bump-core'], function () {

});

gulp.task('bump-packages', function () {
    var msg = 'Bumping versions';
    var type = args.type;
    var version = args.version;
    var options = {};

    if (version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }
    config.log(msg);
    return gulp
        .src(config.packages)
        .pipe($.bump(options))
        .pipe(gulp.dest(config.root));
});

gulp.task('bump-core', function () {
    var msg = 'Bumping Core Config versions';
    var type = args.type;
    var version = args.version;
    var options = {};

    if (version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }
    config.log(msg);
    return gulp
        .src(config.coreConfig)
        .pipe($.bump(options))
        .pipe(gulp.dest(config.sourceApp + 'core'));
});