/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var $ = require('gulp-load-plugins')({ lazy: true });
var gutil = require('gulp-util');

module.exports = function () {
    var root = './';
    var src = './src/';
    var sourceApp = src + 'app/';
    var temp = './.tmp/';
    var webroot = './wwwroot/';

    var config = {

        /**
         * File paths
         */
        alljs: [
            './src/**/*.js',
            './*.js'
        ],
        css: temp + 'css/**/*.css',
        fonts: './bower_components/font-awesome/fonts/**/*.*', 
        html: src + '**/*.html',
        htmlTemplates: sourceApp + '**/*.html',
        images: src + 'assets/images/**/*.*',
        index: src + 'index.html',
        
        injectLess: [
            src + 'less/**/*.less',
            '!' + src + 'less/**/_*.less',
            '!' + src + 'less/index.less'
        ],
        injectScripts: [
            temp + 'js/**/*.module.js',
            temp + 'js/**/*.js',
            '!' + temp + 'js/**/*.spec.js',
            '!' + temp + 'js/**/*.mock.js'
        ],
        less: src + 'less/index.less',
        lib: temp + 'lib',
        packages : [
            './package.json',
            './bower.json',
            './project.json',
            sourceApp + 'core/config.js'
        ],
        src: src,
        sourceApp: sourceApp,
        sourceLib: src + 'lib',
        srcScripts: [
            sourceApp + '**/*.module.js',
            sourceApp + '**/*.js',
            '!' + sourceApp + '**/*.spec.js',
            '!' + sourceApp + '**/*.mock.js'
        ],
        temp: temp,
        webroot: webroot,

        /**
         * template cache
         */
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'app.core',
                standAlone: false,
                root: 'app/'
            }
        },

        /**
         * Bower and NPM locations
         */
        bower: {
            json: require('../bower.json'),
            directory: 'bower_components',
            ignorePath: '../bower_components',
            fileTypes: {
                html: {
                    replace: {
                        js: '<script src="/lib{{filePath}}"></script>',
                        css: '<link rel="stylesheet" href="/lib{{filePath}}" />'
                    }
                }
            }
        }

    };

    /**
     * Common Logger Function
     */
    config.log = function (msg) {

        if (typeof (msg) === 'object') {

            for (var item in msg) {
                if (msg.hasOwnProperty(item)) {
                    console.log(msg[item])
                    $.util.log($.util.colors.yellow(msg[item]));
                }
            }
        } else {
            console.log(msg);
            $.util.log($.util.colors.yellow(msg));
        }
    };

    /**
     * Default Wiredep Options
     */
    config.getWiredepDefaultOptions = function () {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath,
            fileTypes: config.bower.fileTypes
        };
        return options;
    };

    return config;
};