using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace slx.logging.portal.Model
{
    public class AppSettings
    {
        public string clientID { get; set; }
        public string cmgapi { get; set; }
        public bool logsEnabled { get; set; }
        public string WorkspaceCollection { get; set; }
        public string WorkspaceId { get; set; }
        public string ReportID { get; set; }
    }
}
