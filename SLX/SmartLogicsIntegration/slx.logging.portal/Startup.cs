using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using slx.logging.portal.Model;

namespace slx.logging.portal
{
    public class Startup
    {
        private IHostingEnvironment env;

        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            this.env = env;
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup options with DI
            //services.AddOptions();

            // Configure AppSettings using Config
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // Add Framework Services
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (env.IsDevelopment())
            {
                var webSrcPath = env.WebRootPath + @"\..\.tmp";
                app.UseDefaultFiles(new DefaultFilesOptions()
                {
                    FileProvider = new PhysicalFileProvider(webSrcPath),
                    RequestPath = new PathString("")
                });
                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(webSrcPath),
                    RequestPath = new PathString("")
                });
            }
            else
            {
                app.UseHttpsRedirect();
                app.UseDefaultFiles();
                app.UseStaticFiles();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Settings}/{action=Index}/{id?}");
            });
        }
    }
}
