﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RabbitMQ.Client;

namespace SmartLogixIntegration
{
    public interface IMessageListener
    {
        void Start();
        void Stop();
    }

    public class MessageListener : IMessageListener
    {
        
        private IMasterDataQueue _masterDataQueue { get; set; }

       
        public MessageListener(IMasterDataQueue masterDataQueue)
        {
            _masterDataQueue = masterDataQueue;
        }

        public void Start()
        {
            _masterDataQueue.PingQueue();
            _masterDataQueue.PingQueue2();
            _masterDataQueue.ProductQueue();
            _masterDataQueue.ARShipToQueue();
            _masterDataQueue.ARStandardAcctQueue();
            _masterDataQueue.DriverQueue();
            _masterDataQueue.FleetVehicleQueue();
            _masterDataQueue.InsiteQueue();
            _masterDataQueue.SalesOrderQueue();
            _masterDataQueue.SupplierQueue();
            _masterDataQueue.SupplierSupplyPtQueue();
            _masterDataQueue.SupplyPtQueue();
            _masterDataQueue.TankQueue();
            _masterDataQueue.TruckQueue();
            _masterDataQueue.CarrierQueue();

        }

        public void Stop()
        {
            _masterDataQueue.KillConnection();
        }


    }
}