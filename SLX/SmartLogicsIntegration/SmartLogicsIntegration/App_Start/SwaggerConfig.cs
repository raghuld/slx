using System.Web.Http;
using WebActivatorEx;
using SmartLogixIntegration;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace SmartLogixIntegration
{
    /// <summary>
    /// This class is used to configure the Swagger API viewer
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// Registers this instance of swagger
        /// </summary>
        public static void Register()
        {
            GlobalConfiguration.Configuration 
                .EnableSwagger(c => c.SingleApiVersion("v1", "SLX Integration"))
                .EnableSwaggerUi();
        }
    }
}
