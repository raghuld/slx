﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration
{
    public interface IMasterDataConsumerHandlers
    {
        void PingConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void Ping2ConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void ProductConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void ARStandardAcctConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void ARShipToConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void DriverConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void FleetVehicleConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void InsiteConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void SupplierConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void SupplyPtConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void SupplierSupplyPtConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void TankConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void TruckConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void SalesOrderConsumerHandler(object sender, BasicDeliverEventArgs ea);
        void CarrierConsumerHandler(object sender, BasicDeliverEventArgs ea);
    }

    public class MasterDataConsumerHandlers : IMasterDataConsumerHandlers
    {

        private ISLXConfigurationManager _slxConfigurationManager { get; set; }

        public MasterDataConsumerHandlers(ISLXConfigurationManager slxConfigurationManager)
        {
            _slxConfigurationManager = slxConfigurationManager;
        }

        public void PingConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var message = Encoding.UTF8.GetString(body);
            Debug.WriteLine("Ping: {0}", message);
        }

        public void Ping2ConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var message = Encoding.UTF8.GetString(body);
            Debug.WriteLine("Ping2: {0}", message);
        }

        public void ProductConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<ProductList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushProduct";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                // HTTP POST
                string response = client.UploadString(uri, serializedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                HandleSLXResponse(tempList.First().PeerId.ToString(), tempList.First().ProductID, MasterDataType.ProductType, serializedData, null, result);

                foreach (Product product in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(product.PeerId.ToString(), product.ProductID, MasterDataType.ProductType,
                        result.ResponseStatus, serializedData, null, null, false);
                }
            }
        }

        public void ARStandardAcctConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<CustomerList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushCustomer";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.PeerId.ToString(), first.CustomerId, MasterDataType.CustomerType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(item.PeerId.ToString(), item.CustomerId, MasterDataType.CustomerType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }               
            }
        }

        public void ARShipToConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<ShipToList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushCustomerShipTo";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                bool slxException = result.SLXException != null;

                var first = tempList.First();

                HandleSLXResponse(first.PeerId.ToString(), first.ShipToId, MasterDataType.ShipToType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    Logger.WriteToSLXLog(item.PeerId.ToString(), item.ShipToId, MasterDataType.ShipToType, result.ResponseStatus, serialisedData,
                        null, result.SLXException, slxException);
                }
            }
        }

        public void DriverConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<DriverList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushDriver";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.DriverId, first.DriverId, MasterDataType.DriverType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(item.DriverId, item.DriverId, MasterDataType.DriverType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }
            }
        }

        public void FleetVehicleConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<FleetVehicleList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;


                uri += "SmartLogixApi/PushFleetVehicle";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.CrossReferenceId, first.VehicleNumber, MasterDataType.VehicleType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(item.ToString(), item.VehicleNumber, MasterDataType.VehicleType,
                        result.ResponseStatus, serialisedData, null, null, false);
                }
            }
        }

        public void InsiteConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<WarehouseList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushInsite";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.WarehouseID, first.WarehouseID, MasterDataType.InventorySiteType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    Logger.WriteToSLXLog(item.WarehouseID, item.WarehouseID, MasterDataType.InventorySiteType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }
            }
        }

        public void SupplierConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<SupplierPeeringList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;


                uri += "SmartLogixApi/PushSupplier";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var first = tempList.First();

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                HandleSLXResponse(first.PeerId.ToString(), first.Number, MasterDataType.SupplierType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    Logger.WriteToSLXLog(item.PeerId.ToString(), item.Number, MasterDataType.SupplierType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }
            }
        }

        public void SupplyPtConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<TerminalPeeringList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;


                uri += "SmartLogixApi/PushSupplyPt";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = list.First();

                HandleSLXResponse(first.PeerId.ToString(), first.Number, MasterDataType.SupplyPtType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    Logger.WriteToSLXLog(item.PeerId.ToString(), item.Number, MasterDataType.SupplyPtType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }
            }
        }

        public void SupplierSupplyPtConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<TerminalSupplierList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushSupplierSupplyPt";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                    });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(null,
                    first.SupplierNumber + "/" + first.TerminalNumber,
                    MasterDataType.SupplierSupplyPtType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(null,
                        item.SupplierNumber + "/" + item.TerminalNumber,
                        MasterDataType.SupplierSupplyPtType, result.ResponseStatus, serialisedData, null, null,
                        false);
                }
            }
        }

        public void TankConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<CustomerTankList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            // Call the SLX Integration web service and push data to it from Ascend
            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;


                uri += "SmartLogixApi/PushTank";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.CrossReferenceId, first.TankId, MasterDataType.TankType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    Logger.WriteToSLXLog(item.CrossReferenceId, item.TankId, MasterDataType.TankType, result.ResponseStatus, serialisedData,
                        null, null, false);
                }
            }
        }

        public void TruckConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<TruckList>(serializedData);

            // Need a temp product list for when we get rid of the alternatetruckids later
            var tempList = list;

            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            // Call SLX web service
            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushTruck";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(first.AlternativeTruckID, first.TruckId, MasterDataType.TruckType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(item.AlternativeTruckID, item.TruckId, MasterDataType.TruckType, result.ResponseStatus, serialisedData,
                        null, null, false);
                }
            }
        }

        public void SalesOrderConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var salesOrder = JsonConvert.DeserializeObject<SalesOrderModel>(serializedData);

            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushModifiedSalesOrder";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(salesOrder, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                string sysTrxNo = LookupSysTrxNo(salesOrder.AscendSalesOrderNumber);

                HandleSLXResponse(sysTrxNo, salesOrder.AscendSalesOrderNumber, MasterDataType.UpdatedSalesOrderType, serialisedData, null, result);

                Logger.WriteToSLXLog(sysTrxNo,
                    salesOrder.AscendSalesOrderNumber, MasterDataType.UpdatedSalesOrderType,
                    result.ResponseStatus, serialisedData, null, null, false);
            }
        }

        public void CarrierConsumerHandler(object sender, BasicDeliverEventArgs ea)
        {
            string serializedData = Encoding.UTF8.GetString(ea.Body);

            var list = JsonConvert.DeserializeObject<CarrierList>(serializedData);

            // Need a temp product list for when we get rid of the peerids later
            var tempList = list;

            SmartLogixConfiguration configuration = _slxConfigurationManager.GetConfigurationValues("SmartLogix");

            using (var client = new WebClient())
            {
                string uri = configuration.Url;

                uri += "SmartLogixApi/PushCarrier";

                client.Headers.Clear();
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("FirestreamSecretToken", SmartLogixConstants.FirestreamSecretToken);

                string serialisedData = JsonConvert.SerializeObject(list, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                // HTTP POST
                string response = client.UploadString(uri, serialisedData);

                var result = JsonConvert.DeserializeObject<SmartLogixResponse>(response);

                var first = tempList.First();

                HandleSLXResponse(Convert.ToString(first.Number), Convert.ToString(first.Number), MasterDataType.CarrierType, serialisedData, null, result);

                foreach (var item in tempList)
                {
                    // Log
                    Logger.WriteToSLXLog(Convert.ToString(item.Number), Convert.ToString(item.Number), MasterDataType.CarrierType, result.ResponseStatus,
                        serialisedData, null, null, false);
                }
            }
        }

        /// <summary>
        ///     Lookups the system TRX no.
        /// </summary>
        /// <param name="orderNo">The order no.</param>
        /// <returns></returns>
        private static string LookupSysTrxNo(string orderNo)
        {
            string sysTrxNo = null;

            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // Lookup the OrderNo for the order based on the systrxno
            string sysTrxNoLookupQueryString =
                "SELECT SysTrxNo, OrderNo FROM dbo." +
                AscendTable.OrderHdrDataTableName +
                " WHERE OrderNo = @OrderNo";

            using (var cn = new SqlConnection(connectionString))
            {
                using (var mySqlDataAdapter = new SqlDataAdapter())
                {
                    if (!cn.State.Equals(ConnectionState.Open))
                    {
                        cn.Open();
                    }
                    using (var cmd = new SqlCommand(sysTrxNoLookupQueryString, cn))
                    {
                        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 24).Value = orderNo;
                        mySqlDataAdapter.SelectCommand = cmd;
                        var myDataSet = new DataSet();
                        mySqlDataAdapter.Fill(myDataSet, "OrderHdr");
                        DataTable myOrderNoDataTable = myDataSet.Tables["OrderHdr"];

                        if (myOrderNoDataTable.Rows.Count > 0)
                        {
                            sysTrxNo = myOrderNoDataTable.Rows[0]["SysTrxNo"].ToString();
                        }
                    }
                }
            }

            return sysTrxNo;
        }

        /// <summary>
        ///     Handles the SLX response.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="result">The result.</param>
        /// <exception cref="ApplicationException">
        ///     Error in SLX" + result.SLXException
        ///     or
        ///     Error in SLX" + result.ResponseMessage
        ///     or
        ///     Error in SLX
        /// </exception>
        private static void HandleSLXResponse(string ascendId, string masterDataId, string masterDataType,
            string request, string requestRecieved, SmartLogixResponse result)
        {
            if (result == null || result.ResponseStatus != 1)
            {
                // This is to bypass a bug in slx where the status value does not actually change to 1
                if (result?.ResponseMessage == null || !result.ResponseMessage.Contains("Success"))
                {
                    if (result == null)
                    {
                        result = new SmartLogixResponse();
                    }

                    if (result.SLXException == null)
                    {
                        result.SLXException = new Exception(result.ResponseMessage);
                    }

                    WriteToSLXLog(ascendId, masterDataId, masterDataType, result.ResponseStatus, request, requestRecieved, result.SLXException, true);

                    if (result.SLXException != null)
                    {
                        throw new ApplicationException("Error in SLX" + result.SLXException);
                    }

                    if (result.ResponseMessage != null)
                    {
                        throw new ApplicationException("Error in SLX" + result.ResponseMessage);
                    }

                    throw new ApplicationException("Error in SLX");
                }
            }
        }

        /// <summary>
        ///     Writes to SLX log.
        /// </summary>
        /// <param name="ascendId">The ascend identifier.</param>
        /// <param name="masterDataId">The master data identifier.</param>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <param name="status">The status.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestRecieved">The request recieved.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="isError">if set to <c>true</c> [is error].</param>
        /// <returns></returns>
        private static Guid WriteToSLXLog(string ascendId, string masterDataId, string masterDataType, int? status,
            string request, string requestRecieved, Exception ex, bool isError)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["AscendConnectionString"];

            string connectionString;
            if (connection != null)
            {
                connectionString = connection.ConnectionString;
            }
            else
            {
                connectionString = "context connection=true";
            }

            // define INSERT query with parameters to the smartlogixpushlog table
            string query =
                "INSERT INTO " + AscendTable.SmartLogixLogDataTableName +
                " (LogID, LogDate, AscendId, MasterDataId, MasterDataType, Status, Details, Request, RequestRecieved, StackTrace, IsError) " +
                "VALUES (@LogID, @LogDate, @AscendId, @MasterDataId, @MasterDataType, @Status, @Details, @Request, @RequestRecieved, @StackTrace, @IsError)";

            Guid logId = Guid.NewGuid();

            using (var cn = new SqlConnection(connectionString))
            {
                if (!cn.State.Equals(ConnectionState.Open))
                {
                    cn.Open();
                }
                // create command
                using (var cmd = new SqlCommand(query, cn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@LogID", logId);
                        cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                        if (ascendId != null)
                        {
                            cmd.Parameters.AddWithValue("@AscendId", ascendId);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@AscendId", DBNull.Value);
                        }
                        cmd.Parameters.AddWithValue("@MasterDataId", masterDataId);
                        cmd.Parameters.AddWithValue("@MasterDataType", masterDataType);

                        if (ex == null)
                        {
                            cmd.Parameters.AddWithValue("@Status", status.ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Status", "2");
                        }

                        if (ex != null)
                        {
                            cmd.Parameters.AddWithValue("@Details", ex.Message);
                            if (ex.StackTrace != null)
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", ex.StackTrace);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Details", "Success");
                            cmd.Parameters.AddWithValue("@StackTrace", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(request))
                        {
                            cmd.Parameters.AddWithValue("@Request", request);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Request", DBNull.Value);
                        }

                        if (!string.IsNullOrEmpty(requestRecieved))
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", requestRecieved);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@RequestRecieved", DBNull.Value);
                        }

                        if (isError)
                        {
                            cmd.Parameters.AddWithValue("@IsError", 1);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@IsError", 0);
                        }

                        // open connection, execute INSERT, close connection
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        // Do not want to throw an error if something goes wrong logging
                    }
                }
            }

            return logId;
        }
        
    }
}