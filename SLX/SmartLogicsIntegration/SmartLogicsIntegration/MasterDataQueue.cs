﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration
{
    public interface IMasterDataQueue
    {
        void SetupConnection(string hostName, string userName, string password, string exchangeName, string exchangeType);
        void KillConnection();
        void PingQueue();
        void PingQueue2();
        void ProductQueue();
        void ARStandardAcctQueue();
        void ARShipToQueue();
        void DriverQueue();
        void FleetVehicleQueue();
        void InsiteQueue();
        void SupplierQueue();
        void SupplyPtQueue();
        void SupplierSupplyPtQueue();
        void TankQueue();
        void TruckQueue();
        void SalesOrderQueue();
        void CarrierQueue();
    }

    public class MasterDataQueue : IMasterDataQueue
    {
        private IConnection _connection { get; set; }
        private IModel _channel { get; set; }
        private IMasterDataConsumerHandlers _masterDataConsumerHandler { get; }
        private bool _connectionSetup = false;

        public MasterDataQueue(IMasterDataConsumerHandlers masterDataConsumerHandler, ISLXConfigurationManager slxConfigurationManager)
        {
            _masterDataConsumerHandler = masterDataConsumerHandler;

            // If connection is not setup setup a default connection and channel
            if (!_connectionSetup)
            {
                SmartLogixConfiguration config = slxConfigurationManager.GetConfigurationValues(SmartLogixConstants.RabbitMQServiceName);

                SetupConnection(config.Url, config.UserId, config.Password, "masterdata", "topic");
            }
        }

        public void SetupConnection(string hostName, string userName, string password, string exchangeName, string exchangeType)
        {
            var factory = new ConnectionFactory
            {
                HostName = hostName,
                UserName = userName,
                Password = password,
                AutomaticRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(15),
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchangeName, exchangeType, true);
        }

        public void KillConnection()
        {
            _channel.Close(200, "Goodbye");
            _connection.Close();
        }

        public void PingQueue()
        {            
            var queueName = _channel.QueueDeclare("ping", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "ping");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.PingConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void PingQueue2()
        {           
            var queueName = _channel.QueueDeclare("ping2", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "ping2");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.Ping2ConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void ProductQueue()
        {
            var queueName = _channel.QueueDeclare("product", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "product");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.ProductConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void ARStandardAcctQueue()
        {
            var queueName = _channel.QueueDeclare("arstandardacct", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "arstandardacct");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.ARStandardAcctConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void ARShipToQueue()
        {
            var queueName = _channel.QueueDeclare("arshipto", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "arshipto");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.ARShipToConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void DriverQueue()
        {
            var queueName = _channel.QueueDeclare("driver", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "driver");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.DriverConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void FleetVehicleQueue()
        {
            var queueName = _channel.QueueDeclare("fleetvehicle", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "fleetvehicle");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.FleetVehicleConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void InsiteQueue()
        {
            var queueName = _channel.QueueDeclare("insite", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "insite");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.InsiteConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void SupplierQueue()
        {
            var queueName = _channel.QueueDeclare("supplier", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "supplier");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.SupplierConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void SupplyPtQueue()
        {
            var queueName = _channel.QueueDeclare("supplypt", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "supplypt");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.SupplyPtConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void SupplierSupplyPtQueue()
        {
            var queueName = _channel.QueueDeclare("suppliersupplypt", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "suppliersupplypt");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.SupplierSupplyPtConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void TankQueue()
        {
            var queueName = _channel.QueueDeclare("tank", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "tank");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.TankConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void TruckQueue()
        {
            var queueName = _channel.QueueDeclare("truck", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "truck");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.TruckConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void SalesOrderQueue()
        {
            var queueName = _channel.QueueDeclare("salesorder", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "salesorder");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.SalesOrderConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }

        public void CarrierQueue()
        {
            var queueName = _channel.QueueDeclare("carrier", true, false, false).QueueName;

            _channel.QueueBind(queueName, "masterdata", "carrier");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += _masterDataConsumerHandler.CarrierConsumerHandler;

            _channel.BasicConsume(queueName, true, consumer);
        }
    }
}