﻿using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using SmartLogixIntegration.Controllers;
using SmartLogixIntegration.Services;
using SmartLogixIntegration.Services.Interfaces;


namespace SmartLogixIntegration
{
    public class Bootstrapper
    {

        public static void Configure(HttpConfiguration config)
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterInstance(config);
            containerBuilder.RegisterType<MessageListener>().As<IMessageListener>();
            containerBuilder.RegisterType<MasterDataQueue>().As<IMasterDataQueue>();
            containerBuilder.RegisterType<MasterDataConsumerHandlers>().As<IMasterDataConsumerHandlers>();
            containerBuilder.RegisterType<SLXOrderManager>().As<ISLXOrderManager>();
            containerBuilder.RegisterType<SLXMasterDataPusher>().As<ISLXMasterDataPusher>();
            containerBuilder.RegisterType<SLXLogManager>().As<ISLXLogManager>();
            containerBuilder.RegisterType<AscendDataLookup>().As<IAscendDataLookup>();
            containerBuilder.RegisterType<SLXConfigurationManager>().As<ISLXConfigurationManager>();
            containerBuilder.RegisterType<SLXLogController>();
            containerBuilder.RegisterType<SmartLogixApiController>();
            containerBuilder.RegisterType<SLXMasterDataPushController>();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(containerBuilder.Build());
        }        
    }
}