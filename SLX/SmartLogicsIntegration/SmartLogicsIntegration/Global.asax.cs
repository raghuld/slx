﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using SmartLogixIntegration.Services;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private IMessageListener _listener { get; set; }

        protected void Application_Start()
        {
            // Disable Aspx view engine we wont need it in this site
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            // Initialize HttpConfiguration
            var config = GlobalConfiguration.Configuration;
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //initialize injection system 
            //UnityConfig.RegisterComponents();   
            // Autofac IoC for dependency injection
            Bootstrapper.Configure(config);

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<MessageListener>().As<IMessageListener>();
            containerBuilder.RegisterType<MasterDataQueue>().As<IMasterDataQueue>();
            containerBuilder.RegisterType<MasterDataConsumerHandlers>().As<IMasterDataConsumerHandlers>();
            containerBuilder.RegisterType<SLXConfigurationManager>().As<ISLXConfigurationManager>();
            IContainer container = containerBuilder.Build();
            _listener = container.Resolve<IMessageListener>();
            // Start Rabbit MQ Listener
            _listener.Start();
        }

        protected void Application_End()
        {
            _listener?.Stop();
        }
    }
}
