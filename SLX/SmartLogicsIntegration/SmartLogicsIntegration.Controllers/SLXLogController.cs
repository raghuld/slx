﻿using System.Linq;
using System.Web.Http;
using SmartLogicsIntegration.Domain;
using SmartLogixIntegration.Services.Interfaces;


namespace SmartLogixIntegration.Controllers
{
    /// <summary>
    /// This class is the controller for the logger in the SLX Integration, it is primarily used to get log entries.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SLXLogController : ApiController
    {

        /// <summary>
        /// Gets or sets the SLX log manager.
        /// </summary>
        /// <value>
        /// The SLX log manager.
        /// </value>
        public ISLXLogManager SLXLogManager { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SLXLogController"/> class.
        /// </summary>
        /// <param name="slxLogManager">The SLX log manager.</param>
        public SLXLogController(ISLXLogManager slxLogManager)
        {
            SLXLogManager = slxLogManager;
        }

        /// <summary>
        /// Gets the specified n.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <param name="skip">The skip.</param>
        /// <returns></returns>
        public IQueryable<SmartLogixPushLog> Get(int n, int? skip)
        {
            if (skip == null)
            {
                skip = 0;
            }

            return SLXLogManager.GetLogs(n, (int)skip);
        }

    }
}
