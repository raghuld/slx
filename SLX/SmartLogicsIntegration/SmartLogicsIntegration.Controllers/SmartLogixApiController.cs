﻿using System;
using System.Web.Http;
using Firestream;
using Newtonsoft.Json;
using SmartLogixIntegration.AscendSLXInt;
using SmartLogixIntegration.AscendSLXInt.Models;
using SmartLogixIntegration.Services.Interfaces;
using CustomerList = SmartLogixIntegration.Services.CustomerShipToService.CustomerList;
using CustomerTankList = SmartLogixIntegration.Services.CustomerShipToService.CustomerTankList;
using DriverList = SmartLogixIntegration.Services.DriverDOTService.DriverList;
using FleetVehicleList = SmartLogixIntegration.Services.CustomerShipToService.FleetVehicleList;
using ProductList = SmartLogixIntegration.Services.ProductService.ProductList;
using ShipToList = SmartLogixIntegration.Services.CustomerShipToService.ShipToList;
using SupplierPeeringList = SmartLogixIntegration.Services.BOLService.SupplierPeeringList;
using TerminalPeeringList = SmartLogixIntegration.Services.BOLService.TerminalPeeringList;
using TerminalSupplierList = SmartLogixIntegration.Services.BOLService.TerminalSupplierList;
using TruckList = SmartLogixIntegration.Services.DriverDOTService.TruckList;
using WarehouseList = SmartLogixIntegration.Services.BOLService.WarehouseList;
using CarrierList = SmartLogixIntegration.Services.BOLService.CarrierPeeringList;

namespace SmartLogixIntegration.Controllers
{
    /// <summary>
    /// This is the controller class for all the SLX Integration web service methods
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SmartLogixApiController : ApiController
    {
        #region Fields

        /// <summary>
        /// Gets the smart logix manager.
        /// </summary>
        /// <value>
        /// The smart logix manager.
        /// </value>
        private ISLXOrderManager SLXOrderManager { get; set; }
        private ISLXMasterDataPusher SLXMasterDataPusher { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartLogixApiController" /> class.
        /// </summary>
        /// <param name="slxOrderManager">The SLX order manager.</param>
        /// <param name="slxMasterDataPusher">The SLX master data pusher.</param>
        public SmartLogixApiController(ISLXOrderManager slxOrderManager, ISLXMasterDataPusher slxMasterDataPusher)
        {
            SLXOrderManager = slxOrderManager;
            SLXMasterDataPusher = slxMasterDataPusher;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Verifies the specified API key.  This key confirms that the client accessing the api is valid.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <exception cref="ApplicationException">Invalid Api Key</exception>
        [HttpGet]
        public void Verified(string apiKey)
        {
            if (apiKey == SmartLogixConstants.FirestreamSecretToken)
            {
                return;
            }

            throw new ApplicationException("Invalid Api Key");
        }

        /// <summary>
        /// Pings this instance.  Ensures that the web services are actually up and running and endpoints are accessible.
        /// </summary>
        /// <returns>A Ping Successful string</returns>
        [HttpGet]
        public string Ping()
        {
            return "Ping Successful!";
        }

        #region Push Calls

        /// <summary>
        /// Pushes the supplier list to SLX
        /// </summary>
        /// <param name="supplierList">The supplier list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushSupplier(SupplierPeeringList supplierList)
        {
            return SLXMasterDataPusher.PushSupplier(supplierList);
        }


        /// <summary>
        /// Pushes the inventory site list to SLX
        /// </summary>
        /// <param name="insiteList">The inventory site list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushInsite(WarehouseList insiteList)
        {
            return SLXMasterDataPusher.PushInsite(insiteList);
        }


        /// <summary>
        /// Pushes the supply pt list to SLX
        /// </summary>
        /// <param name="supplyPtList">The supply pt list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushSupplyPt(TerminalPeeringList supplyPtList)
        {
            return SLXMasterDataPusher.PushSupplyPt(supplyPtList);
        }


        /// <summary>
        /// Pushes the supplier supply pt list to SLX
        /// </summary>
        /// <param name="supplierSupplyPtList">The supplier supply pt list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushSupplierSupplyPt(TerminalSupplierList supplierSupplyPtList)
        {
            return SLXMasterDataPusher.PushSupplierSupplyPt(supplierSupplyPtList);
        }


        /// <summary>
        /// Pushes the customer ship to list to SLX
        /// </summary>
        /// <param name="shipToList">The ship to list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushCustomerShipTo(ShipToList shipToList)
        {
            return SLXMasterDataPusher.PushCustomerShipTo(shipToList);
        }

        /// <summary>
        /// Pushes the driver list to SLX
        /// </summary>
        /// <param name="driverList">The driver list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushDriver(DriverList driverList)
        {
            return SLXMasterDataPusher.PushDriver(driverList);
        }

        /// <summary>
        /// Pushes the tank list to SLX
        /// </summary>
        /// <param name="tankList">The tank list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushTank(CustomerTankList tankList)
        {
            return SLXMasterDataPusher.PushTank(tankList);
        }

        /// <summary>
        /// Pushes the customer list to SLX
        /// </summary>
        /// <param name="customerList">The customer list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushCustomer(CustomerList customerList)
        {
            return SLXMasterDataPusher.PushCustomer(customerList);
        }

        /// <summary>
        /// Pushes the fleet vehicle list to SLX
        /// </summary>
        /// <param name="fleetVehicleList">The fleet vehicle list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushFleetVehicle(FleetVehicleList fleetVehicleList)
        {
            return SLXMasterDataPusher.PushFleetVehicle(fleetVehicleList);
        }

        /// <summary>
        /// Pushes the truck list to SLX
        /// </summary>
        /// <param name="truckList">The truck list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushTruck(TruckList truckList)
        {
            return SLXMasterDataPusher.PushTruck(truckList);
        }

        /// <summary>
        /// Pushes the product list to SLX
        /// </summary>
        /// <param name="productList">The product list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushProduct(ProductList productList)
        {
            return SLXMasterDataPusher.PushProduct(productList);
        }

        /// <summary>
        /// Pushes the new sales order with updated pricing, tax, credit hold, and PO Number information back to SLX after they send an NSO
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushNewSalesOrder(SalesOrderModel model)
        {
            return SLXOrderManager.PushNewSalesOrder(model);
        }

        /// <summary>
        /// Pushes the modified sales order with updated pricing, tax, credit hold, and po number infomation back to SLX after they send an MSO
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushModifiedSalesOrder(SalesOrderModel model)
        {
            return SLXOrderManager.PushModifiedSalesOrder(model);
        }

        #endregion

        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse ValidateSalesOrder(SalesOrderModel model)
        {
            return SLXOrderManager.ValidateSalesOrder(model);
        }

        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse ValidateCompletedSalesOrder(CompletedSalesOrderModel model)
        {
            return SLXOrderManager.ValidateCompletedSalesOrder(model);
        }

        /// <summary>
        /// Updates the order status in ascend
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public string UpdateOrderStatus(UpdateOrderStatusModel model)
        {
            return JsonConvert.SerializeObject(SLXOrderManager.UpdateOrderStatus(model));
        }

        /// <summary>
        /// Creates a new sales order in Ascend.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public string NewSalesOrder(SalesOrderModel model)
        {
            return JsonConvert.SerializeObject(SLXOrderManager.NewSalesOrder(model));
        }

        /// <summary>
        /// Modifies a sales order that is already completed in Ascend
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public string ModifiedSalesOrder(SalesOrderModel model)
        {
            return JsonConvert.SerializeObject(SLXOrderManager.ModifiedSalesOrder(model));
        }

        /// <summary>
        /// Completes the sales order and changes the status to billing review
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public string CompletedSalesOrder(CompletedSalesOrderModel model)
        {
            var response = new SmartLogixResponse();

            try
            {
                response.ResponseMessage = "Success";
                response.ResponseStatus = 1;
                SLXOrderManager.CompletedSalesOrder(model);
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ex.Message;
                response.ResponseStatus = 2;
                response.SLXException = ex;
            }

            return JsonConvert.SerializeObject(response);
        }

        /// <summary>
        /// Cancels the sales order in Ascend
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public string CancelSalesOrder(CancelSalesOrder model)
        {
            return JsonConvert.SerializeObject(SLXOrderManager.CancelSalesOrder(model));
        }

        /// <summary>
        /// Pushes the carrier list to SLX
        /// </summary>
        /// <param name="carrierList">The carrier list.</param>
        /// <returns>A SmartLogixResponse that includes success or failure</returns>
        [RequireApiKey]
        [HttpPost]
        public SmartLogixResponse PushCarrier(CarrierList carrierList)
        {
            return SLXMasterDataPusher.PushCarrier(carrierList);
        }


        #endregion
    }
}
