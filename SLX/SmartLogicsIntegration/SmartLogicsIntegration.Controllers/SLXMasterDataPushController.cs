﻿using System.Web.Http;
using SmartLogixIntegration.Services.Interfaces;

namespace SmartLogixIntegration.Controllers
{
    /// <summary>
    /// This class is used as the controller for all the API methods involved in doing master data pushes.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SLXMasterDataPushController : ApiController
    {
        /// <summary>
        /// Gets or sets the SLX master data pusher.
        /// </summary>
        /// <value>
        /// The SLX master data pusher.
        /// </value>
        private ISLXMasterDataPusher SLXMasterDataPusher { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SLXMasterDataPushController"/> class.
        /// </summary>
        /// <param name="slxMasterDataPusher">The SLX master data pusher.</param>
        public SLXMasterDataPushController(ISLXMasterDataPusher slxMasterDataPusher)
        {
            SLXMasterDataPusher = slxMasterDataPusher;
        }

        /// <summary>
        /// Pushes all master data. Including CUstomers, ShipTos, Products, Trucks, Drivers, Fleet Vehicles, Inventory Sites, Suppliers,
        /// Supply Points, Supplier supply point relationships, and tanks to SLX
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllMasterData(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllCustomers(pushTopN);
            SLXMasterDataPusher.PushAllShipTos(pushTopN);
            SLXMasterDataPusher.PushAllProducts(pushTopN);
            SLXMasterDataPusher.PushAllTrucks(pushTopN);
            SLXMasterDataPusher.PushAllDrivers(pushTopN);
            SLXMasterDataPusher.PushAllFleetVehicles(pushTopN);
            SLXMasterDataPusher.PushAllInsites(pushTopN);       
            SLXMasterDataPusher.PushAllSuppliers(pushTopN);
            SLXMasterDataPusher.PushAllSupplyPts(pushTopN);
            SLXMasterDataPusher.PushAllSupplierSupplyPts(pushTopN);
            SLXMasterDataPusher.PushAllTanks(pushTopN);
            SLXMasterDataPusher.PushAllCarriers(pushTopN);
        }

        /// <summary>
        /// Pushes all customers to SLX.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllCustomers(int ? pushTopN)
        {
            SLXMasterDataPusher.PushAllCustomers(pushTopN);
        }

        /// <summary>
        /// Pushes all ship tos.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllShipTos(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllShipTos(pushTopN);
        }

        /// <summary>
        /// Pushes all tanks.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllTanks(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllTanks(pushTopN);
        }

        /// <summary>
        /// Pushes all suppliers.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllSuppliers(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllSuppliers(pushTopN);
        }

        /// <summary>
        /// Pushes all supply PTS.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllSupplyPts(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllSupplyPts(pushTopN);
        }

        /// <summary>
        /// Pushes all insites.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllInsites(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllInsites(pushTopN);
        }

        /// <summary>
        /// Pushes all supplier supply PTS.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllSupplierSupplyPts(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllSupplierSupplyPts(pushTopN);
        }

        /// <summary>
        /// Pushes all drivers.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllDrivers(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllDrivers(pushTopN);
        }

        /// <summary>
        /// Pushes all trucks.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllTrucks(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllTrucks(pushTopN);
        }

        /// <summary>
        /// Pushes all products.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllProducts(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllProducts(pushTopN);
        }

        /// <summary>
        /// Pushes all fleet vehicles.
        /// </summary>
        /// <param name="pushTopN">The push top n.</param>
        [HttpPost]
        public void PushAllFleetVehicles(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllFleetVehicles(pushTopN);
        }

        [HttpPost]
        public void PushAllCarriers(int? pushTopN)
        {
            SLXMasterDataPusher.PushAllCarriers(pushTopN);
        }

    }
}
