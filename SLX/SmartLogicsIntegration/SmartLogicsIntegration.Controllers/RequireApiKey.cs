﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using SmartLogixIntegration.AscendSLXInt;

namespace Firestream
{
    /// <summary>
    /// This class creates an attribute to authorize if a web request it looks for a FirestreamSecretToken in the request
    /// header.  If that token does not exist then the request will be denied.
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.AuthorizationFilterAttribute" />
    public class RequireApiKey : AuthorizationFilterAttribute
    {
        /// <summary>
        /// Calls when a process requests authorization through an attribute in web api.
        /// </summary>
        /// <param name="actionContext">The action context, which encapsulates information for using <see cref="T:System.Web.Http.Filters.AuthorizationFilterAttribute" />.</param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var re = actionContext.Request;
            var headers = re.Headers;

            string token = null;
            IEnumerable<string> values;

            // Look for the FirestreamSecretToken header value
            if (headers.TryGetValues("FirestreamSecretToken", out values))
            {
                token = values.First();
            }

            if (String.IsNullOrEmpty(token) || token != SmartLogixConstants.FirestreamSecretToken)
            {
                actionContext.Response = new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden };
            }
        }
    }
}