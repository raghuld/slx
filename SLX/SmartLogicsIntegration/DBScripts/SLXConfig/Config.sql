if not exists(select * from ThirdPartyWebService where Code = 'SmartLogix')
BEGIN
INSERT INTO dbo.ThirdPartyWebService (ID, Code, UserID, Password, ClientID, URL, WebServiceName, DatabaseName)  VALUES ((SELECT MAX(ID)+1 from ThirdPartyWebService), 'SmartLogix', 'fire1stream', 
                                                    'fire2stream', 4111, '<insert slx web service location here example: http://208.104.29.80:8081/>', 'SmartLogixPush', 'Ascend')
INSERT INTO dbo.ThirdPartyWebService (ID, Code, UserID, Password, ClientID, URL, WebServiceName, DatabaseName) VALUES ((SELECT MAX(ID)+1 from ThirdPartyWebService), 'SmartLogix', 'fire1stream', 
                                                    'fire2stream', 4111, '<insert ascend slx web service location here example:http://192.168.128.27/>', 'SmartLogix', 'Ascend')
END

if not exists(select * from ThirdPartyWebService where Code = 'SmartLogix' and WebServiceName = 'SmartLogixRabbitMQ')
BEGIN
	INSERT INTO dbo.ThirdPartyWebService (ID, Code, UserID, Password, ClientID, URL, WebServiceName, DatabaseName) VALUES ((SELECT MAX(ID)+1 from ThirdPartyWebService), 'SmartLogix', 'fire1stream', 
                                                    'fire2stream', 4111, '<insert rabbitmq server name here example: FSSLTest>', 'SmartLogixRabbitMQ', 'Ascend')
END