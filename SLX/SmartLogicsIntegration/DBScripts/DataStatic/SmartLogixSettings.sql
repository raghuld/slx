IF NOT EXISTS (SELECT * FROM SmartLogixSettings WHERE Name = 'DiversionStandardAccount')
BEGIN
    INSERT INTO SmartLogixSettings VALUES ('DiversionStandardAccount', null)
END

IF NOT EXISTS (SELECT * FROM SmartLogixSettings WHERE Name = 'RabbitMQServer')
BEGIN
	INSERT into SmartLogixsettings VALUES ('RabbitMQServer', 'localhost')
END

