IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.SmartLogixPushLog') AND NAME ='ix_SmartLogixPushLog_MasterDataType')
    DROP INDEX [ix_SmartLogixPushLog_MasterDataType] ON dbo.SmartLogixPushLog
GO
CREATE NONCLUSTERED INDEX [ix_SmartLogixPushLog_MasterDataType] ON [dbo].[SmartLogixPushLog]
(
 [MasterDataType] ASC
)
INCLUDE (  [LogDate],
 [MasterDataId],
 [Status]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO