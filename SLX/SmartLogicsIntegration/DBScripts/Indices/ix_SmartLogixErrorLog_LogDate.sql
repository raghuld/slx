IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.SmartLogixErrorLog') AND NAME ='ix_SmartLogixErrorLog_LogDate')
    DROP INDEX [ix_SmartLogixErrorLog_LogDate] ON dbo.SmartLogixErrorLog
GO
CREATE NONCLUSTERED INDEX [ix_SmartLogixErrorLog_LogDate] ON
[dbo].[SmartLogixErrorLog]
(
    [LogDate] ASC
)
INCLUDE ( 	[LogID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =
ON, ALLOW_PAGE_LOCKS = ON)
GO

