EXEC sp_changedbowner 'sa'

--IF EXISTS ( SELECT * 
--			FROM sys.objects
--			where object_id = OBJECT_ID(N'SLXIntegration'))
--BEGIN  
--	DROP ASSEMBLY [SLXIntegration]
--END
--GO

-- Add Assemblies to the database
if not exists(select * from sys.assemblies where name = 'System.Runtime.Serialization')
BEGIN
declare @DB varchar(100), @S varchar(max)

SELECT @DB = DB_NAME() 

exec('ALTER DATABASE ' + @DB + ' SET TRUSTWORTHY ON')
CREATE ASSEMBLY [System.Runtime.Serialization]
AUTHORIZATION [dbo]
FROM 'C:\Windows\Microsoft.NET\Framework\v4.0.30319\System.Runtime.Serialization.dll'
WITH PERMISSION_SET = UNSAFE
END
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'UpdateSLXAssembly')
                                AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[UpdateSLXAssembly]
END
GO
CREATE PROCEDURE [dbo].[UpdateSLXAssembly](@ProgDir varchar(200))
AS	
BEGIN

declare @DB varchar(100), @S varchar(max)

SELECT @DB = DB_NAME() 

exec('ALTER DATABASE ' + @DB + ' SET TRUSTWORTHY ON')

exec('EXECUTE sp_changedbowner ''sa''')
if not exists (select * from sys.assemblies where name = 'SLXIntegration')
begin
set @S = '
CREATE ASSEMBLY [SLXIntegration]
AUTHORIZATION [dbo]
FROM ''' + @ProgDir + '\SmartLogixIntegration.AscendSLXInt.dll''
WITH PERMISSION_SET = UNSAFE
'
end
else
begin
set @S = '
ALTER ASSEMBLY [SLXIntegration]
FROM ''' + @ProgDir + '\SmartLogixIntegration.AscendSLXInt.dll''
WITH PERMISSION_SET = UNSAFE
'
end 

exec(@S)
end
GO

execute [UpdateSLXAssembly] 'C:\Program Files (x86)\Ascend\SecAdmin'