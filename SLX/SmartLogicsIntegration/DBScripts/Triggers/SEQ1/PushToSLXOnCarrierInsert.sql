-- Carrier Insert Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnCarrierInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnCarrierInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnCarrierInsert]
   ON  [dbo].[Carrier] AFTER INSERT
AS 
BEGIN
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = null
    DECLARE @currentId bigint = null
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @CarrierID int = null
    DECLARE @Code varchar(254)
    DECLARE @SCACCode varchar(40)

    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select CarrierID from INSERTED 

    SELECT @maxCounter = Count(*) FROM INSERTED GROUP BY CarrierID

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

      select @CarrierID=INSERTED.CarrierID,@Code=INSERTED.Code,@SCACCode=INSERTED.SCACCode from INSERTED
	     WHERE INSERTED.CarrierID = @currentId

		    if (@currentId is not null)
            BEGIN

		

			       EXEC dbo.SLX_CallInsertCarrierService
                    @name = @Code,
                    @number = @CarrierID,
                    @scac = @SCACCode
                 
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
           
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO