-- Driver Trigger 
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnDriversChange]
   ON  [dbo].[Drivers] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(Name) or Update(FirstName) or Update(LastName)
    BEGIN
        DECLARE @returnValue int = 0
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
        DECLARE @currentId bigint = null
        DECLARE @count bigint = 0
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        declare @tmp table
        (
          id int not null
          primary key(id)
        )

        insert @tmp
        SELECT INSERTED.DriverID
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.DriverID = INSERTED.DriverID
        WHERE (ISNULL(INSERTED.Name, 'null') <> ISNULL(DELETED.Name, 'null'))  and INSERTED.DriverType='FS'
        GROUP BY INSERTED.DriverID

        SELECT @maxCounter = Count(*)
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.DriverID = INSERTED.DriverID
        WHERE (ISNULL(INSERTED.Name, 'null') <> ISNULL(DELETED.Name, 'null')) and INSERTED.DriverType='FS'
        GROUP BY INSERTED.DriverID

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentId = id from @tmp

                if (@newLastModifiedDate is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateDriverService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @driverId = @currentId
                END

                delete from @tmp where id = @currentId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'Driver', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO