-- Insert new supplier
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplyPtInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplyPtInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnSupplyPtInsert]
   ON  [dbo].[SupplyPt] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int;
    DECLARE @newLastModifiedDate datetime = null;
    DECLARE @currentId bigint = null;
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null;
    DECLARE @currentName varchar(30)

    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select SupplyPtID from INSERTED

    SELECT @maxCounter = Count(*) FROM INSERTED

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            SELECT @currentCode = INSERTED.Code, @currentName = INSERTED.Descr FROM INSERTED
            WHERE INSERTED.SupplyPtID = @currentId

            if (@currentId is not null)
            BEGIN
            EXEC dbo.SLX_CallInsertSupplyPtService
                @id = @currentId,
                @code = @currentCode,
                @name = @currentName
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
        END
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
        END
        set implicit_transactions off
        DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',code=' + CAST(@currentCode as varchar) + ',name=' + CAST(@currentName as varchar);
        INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), @currentCode, 'SupplyPt', 2, @ErrorMessage, @Request, null, null, 1)
        set implicit_transactions on
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO
