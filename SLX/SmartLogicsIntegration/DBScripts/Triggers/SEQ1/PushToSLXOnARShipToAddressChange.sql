-- ARShipToAddress Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToAddressChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToAddressChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARShipToAddressChange]
   ON  [dbo].[ARShipToAddress] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(StateProvID) or UPDATE(CityID) or UPDATE(Zip) or UPDATE(Addr1) or UPDATE(Addr2)
    BEGIN
        DECLARE @returnValue int
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
        DECLARE @currentCustomerId int = null
        DECLARE @currentShipToId int = null
        DECLARE @currentId bigint = null;
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        DECLARE @isStandardAcctError bit = 1
        declare @tmp table
        (
          id int not null,
          customerId int not null,
          primary key(id)
        )

        insert @tmp
        SELECT	INSERTED.ShipToID AS id, ARShipTo.StandardAcctID AS customerId
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.ShiptoID = INSERTED.ShiptoID
        JOIN ARShipTo on ARShipTo.ShiptoID = INSERTED.ShipToID
        WHERE (INSERTED.StateProvID <> DELETED.StateProvID or 
        INSERTED.CityID <> DELETED.CityID or
        ISNULL(INSERTED.Zip, 'null') <> ISNULL(DELETED.Zip, 'null') or
        ISNULL(INSERTED.Addr1, 'null') <> ISNULL(DELETED.Addr1, 'null') or
        ISNULL(INSERTED.Addr2, 'null') <> ISNULL(DELETED.Addr2, 'null'))
        GROUP BY INSERTED.ShipToID, ARShipTo.StandardAcctID

        SELECT	@maxCounter = Count(*)
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.ShiptoID = INSERTED.ShiptoID
        JOIN ARShipTo on ARShipTo.ShiptoID = INSERTED.ShipToID
        WHERE (INSERTED.StateProvID <> DELETED.StateProvID or 
        INSERTED.CityID <> DELETED.CityID or
        ISNULL(INSERTED.Zip, 'null') <> ISNULL(DELETED.Zip, 'null') or
        ISNULL(INSERTED.Addr1, 'null') <> ISNULL(DELETED.Addr1, 'null') or
        ISNULL(INSERTED.Addr2, 'null') <> ISNULL(DELETED.Addr2, 'null'))
        GROUP BY INSERTED.ShipToID, ARShipTo.StandardAcctID

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentShipToId = id, @currentCustomerId = customerId from @tmp

                if (@currentCustomerId is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateCustomerService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @customerId = @currentCustomerId;
                END

                SET @isStandardAcctError = 0;

                if (@currentShipToId is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateShipToService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @shipToId = @currentShipToId
                END

                delete from @tmp where id = @currentShipToId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentCustomerId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            IF (@isStandardAcctError = 1)
            BEGIN
                INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentCustomerId AS varchar(24)), '', 'Customer', 2, @ErrorMessage, @Request, null, null, 1)
            END
            ELSE
            BEGIN
                SET @Request = 'id=' + CAST(@currentShipToId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
                INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentShipToId AS varchar(24)), '', 'ShipTo', 2, @ErrorMessage, @Request, null, null, 1)
            END
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO