-- Insert new suppliersupplypt
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierSupplyPtInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplierSupplyPtInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnSupplierSupplyPtInsert]
   ON  [dbo].[SupplierSupplyPt] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int;
    DECLARE @newLastModifiedDate datetime = null;
    DECLARE @currentId bigint = null;
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentSupplierId varchar(24) = null;
    DECLARE @currentSupplyPtId varchar(24) = null;

    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select SupplierSupplyPtID from INSERTED

    SELECT @maxCounter = Count(*) FROM INSERTED

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            SELECT @currentSupplierId = Supplier.Code, @currentSupplyPtId = SupplyPt.Code FROM INSERTED 
            JOIN Supplier ON Supplier.SupplierId = INSERTED.SupplierID
            JOIN SupplyPt ON SupplyPt.SupplyPtId = INSERTED.SupplyPtID
            WHERE INSERTED.SupplierSupplyPtID = @currentId

            if (@currentId is not null)
            BEGIN
            EXEC dbo.SLX_CallInsertSupplierSupplyPtService
                @supplierId = @currentSupplierId,
                @supplyPtId = @currentSupplyPtId
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
        END
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
        END
        set implicit_transactions off
        DECLARE @Request varchar(max) = 'supplierId=' + CAST(@currentSupplierId as varchar) + ',supplyPtId=' + CAST(@currentSupplyPtId as varchar);
        INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, @currentSupplierId + '/' + @currentSupplyPtId, @currentSupplierId + '/' + @currentSupplyPtId, 'SupplierSupplyPt', 2, @ErrorMessage, @Request, null, null, 1)
        set implicit_transactions on
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO

