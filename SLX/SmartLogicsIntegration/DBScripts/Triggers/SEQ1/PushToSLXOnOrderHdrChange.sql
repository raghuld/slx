-- Sales Order Hold Change Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnOrderHdrChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnOrderHdrChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnOrderHdrChange]
   ON  [dbo].[OrderHdr] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF (UPDATE(Hold) or UPDATE(OnHold))
    BEGIN
        DECLARE @returnValue int
        DECLARE @count int
        DECLARE @exists bit = 0
        DECLARE @sysTrxNo int
        DECLARE @sysTrxLine int

        SELECT @sysTrxNo = INSERTED.SysTrxNo, @sysTrxLine = 1 FROM INSERTED

        SELECT @exists = 1 FROM dbo.SmartLogixPriceUpdateRequired			  
        WHERE SmartLogixPriceUpdateRequired.SysTrxNo = @sysTrxNo
        AND SmartLogixPriceUpdateRequired.SysTrxLine = 1

        INSERT dbo.SmartLogixPriceUpdateRequired(SysTrxNo, SysTrxLine)
              SELECT INSERTED.SysTrxNo, 1 FROM INSERTED 
              JOIN DELETED ON DELETED.SysTrxNo = INSERTED.SysTrxNo 
              WHERE ((INSERTED.Status = 'O') and (INSERTED.OrderStatusID = 1001 or INSERTED.OrderStatusID = 1003 or INSERTED.OrderStatusID = 1005) and INSERTED.DtTm < DATEADD(SECOND, -2, CURRENT_TIMESTAMP) and
              (ISNULL(INSERTED.Hold, 'N') <> ISNULL(DELETED.Hold, 'N') or ISNULL(INSERTED.OnHold, 'N') <> ISNULL(DELETED.OnHold, 'N'))
              AND @exists <> 1)

        SELECT @count = Count(*) FROM SmartLogixPriceUpdateRequired GROUP BY SysTrxNo

        if (@count > 0 and @exists <> 1)
        BEGIN
            EXEC dbo.SLX_CallUpdateSalesOrderService
                @returnValue OUT
        END
    END
END
GO