-- DriverPhone Trigger 
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversPhoneInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversPhoneInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnDriversPhoneInsert]
   ON  [dbo].[DriversPhone] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
    DECLARE @currentId int = null
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null
    DECLARE @currentName varchar(254)
    DECLARE @currentCellPhone varchar(40)
    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select DriverID from INSERTED GROUP BY DriverID

    SELECT @maxCounter = Count(*) FROM INSERTED GROUP BY DriverID

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN	
            select top 1 @currentId = id from @tmp
            
            SELECT @currentCode = Drivers.DriverId, @currentName = Drivers.FirstName + ' ' + Drivers.LastName, @currentCellPhone = INSERTED.PhoneNo from Drivers
                      LEFT OUTER JOIN INSERTED ON INSERTED.DriverID = Drivers.DriverID and INSERTED.Type = (SELECT top 1 PhoneNoTypeID FROM PhoneNoType WHERE Code = 'Mobile' or Code = 'Cell')
                      WHERE INSERTED.DriverID = @currentId

            if (@currentId is not null)
            BEGIN
                EXEC dbo.SLX_CallInsertDriverService
                    @id = @currentId,
                    @code = @currentCode,
                    @name = @currentName,
                    @cellPhone = @currentCellPhone
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO