-- Product Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSalesAliasInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSalesAliasInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnSalesAliasInsert]
   ON  [dbo].[SalesAlias] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = NULL
    DECLARE @currentId bigint = null
    DECLARE @count bigint = 0
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null
    DECLARE @currentEpaDisclaimer varchar(254)
    DECLARE @currentDescription varchar(40)
    DECLARE @currentUOM varchar(24)
    DECLARE @currentUOMId int
    DECLARE @currentProductCategory varchar(24)
    DECLARE @currentIsPackaged varchar(1)
    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select SalesAliasID from INSERTED

    SELECT @maxCounter = Count(*), @currentId = INSERTED.SalesAliasID FROM INSERTED GROUP BY SalesAliasID

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN
            select top 1 @currentId=id from @tmp

            SELECT  @currentCode = INSERTED.Code, @currentDescription = INSERTED.SellAsDescr, @currentEpaDisclaimer = HzrdMaterialsInstruction.LabelsReq, @currentUOMId = UOM.UOMID, @currentUOM = UOM.Code,
            @currentProductCategory = ProdType.Code, @currentIsPackaged = UOM.IsPackaged
            FROM    INSERTED
            LEFT OUTER JOIN dbo.ProdCont
            ON INSERTED.ProdContID = ProdCont.ProdContID
            LEFT OUTER JOIN dbo.HzrdMaterialsInstruction
            ON ProdCont.HzrdMaterialID = HzrdMaterialsInstruction.HzrdMaterialID
            LEFT OUTER JOIN dbo.UOM
            ON UOM.UOMID = INSERTED.SellByUOMID
            LEFT OUTER JOIN dbo.ProdType
            ON ProdType.ProdTypeID = ProdCont.ProdTypeID
            WHERE INSERTED.SalesAliasID = @currentId  

            -- 0 represents SalesAlias table
            if (@currentId is not null)
            BEGIN
                EXEC dbo.SLX_CallInsertProductService
                    @id = @currentId,
                    @code = @currentCode,
                    @description = @currentDescription,
                    @isPurchaseOnly = 0,
                    @epaDisclaimer = @currentEpaDisclaimer,
                    @nonFuel = @currentIsPackaged,
                    @uomId = @currentUOMId,
                    @uomCode = @currentUOM,
                    @productCategory = @currentProductCategory
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO
