-- Truck Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnVehicleChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnVehicleChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnVehicleChange]
   ON  [dbo].[Vehicle] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    IF UPDATE(VINNumber) or UPDATE(LicPlateNo)
    BEGIN
        DECLARE @returnValue int
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
        DECLARE @currentId bigint = null
        DECLARE @count bigint = 0
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        declare @tmp table
        (
            id int not null
            primary key(id)
        )

        insert @tmp
        SELECT INSERTED.VehicleID
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.VehicleID = INSERTED.VehicleID
        WHERE ((ISNULL(INSERTED.VINNumber, 'null') <> ISNULL(DELETED.VINNumber, 'null')) or
        (ISNULL(INSERTED.LicPlateNo, 'null') <> ISNULL(DELETED.LicPlateNo, 'null'))) and
        INSERTED.Code = DELETED.Code and INSERTED.CLType = 'M'

        SELECT @maxCounter = COUNT(*)
        FROM    INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.VehicleID = INSERTED.VehicleID
        WHERE ((ISNULL(INSERTED.VINNumber, 'null') <> ISNULL(DELETED.VINNumber, 'null')) or
        (ISNULL(INSERTED.LicPlateNo, 'null') <> ISNULL(DELETED.LicPlateNo, 'null'))) and
        INSERTED.Code = DELETED.Code AND INSERTED.CLType = 'M'

        BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            if (@newLastModifiedDate is not null)
            BEGIN
                EXEC dbo.SLX_CallUpdateTruckService
                    @returnValue OUT,
                    @lastModifiedDate = @newLastModifiedDate,
                    @truckId = @currentId
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'Truck', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO