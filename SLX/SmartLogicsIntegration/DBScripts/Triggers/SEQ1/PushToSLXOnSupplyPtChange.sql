-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplyPtChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplyPtChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnSupplyPtChange]
   ON  [dbo].[SupplyPt] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(Descr)
    BEGIN
        DECLARE @returnValue int;
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP;
        DECLARE @currentId bigint = null;
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        declare @tmp table
        (
          id int not null
          primary key(id)
        )
        insert @tmp select INSERTED.SupplyPtID from INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.SupplyPtID = INSERTED.SupplyPtID 
        WHERE (INSERTED.Descr <> DELETED.Descr) and
        INSERTED.Code = DELETED.Code

        SELECT @maxCounter = Count(*) FROM INSERTED 
        LEFT OUTER JOIN DELETED ON DELETED.SupplyPtID = INSERTED.SupplyPtID 
        WHERE (INSERTED.Descr <> DELETED.Descr) and
        INSERTED.Code = DELETED.Code

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentId = id from @tmp

                if (@newLastModifiedDate is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateSupplyPtService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @supplyPtId = @currentId;
                END
                delete from @tmp where id = @currentId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'SupplyPt', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO