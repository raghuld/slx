-- Sales Order Price Change Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSalesOrderPriceChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSalesOrderPriceChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnSalesOrderPriceChange]
   ON  [dbo].[OrderItem] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF (UPDATE(UnitPrice) or UPDATE(UnitPriceKeyed))
    BEGIN
        DECLARE @returnValue int
        DECLARE @count int
        DECLARE @exists bit = 0
        DECLARE @sysTrxNo int
        DECLARE @sysTrxLine int

        SELECT @sysTrxNo = INSERTED.SysTrxNo, @sysTrxLine = INSERTED.SysTrxLine FROM INSERTED

        SELECT @exists = 1 FROM dbo.SmartLogixPriceUpdateRequired			  
        WHERE SmartLogixPriceUpdateRequired.SysTrxNo = @sysTrxNo
        AND SmartLogixPriceUpdateRequired.SysTrxLine = @sysTrxLine

        INSERT dbo.SmartLogixPriceUpdateRequired(SysTrxNo, SysTrxLine)
              SELECT INSERTED.SysTrxNo, INSERTED.SysTrxLine FROM INSERTED 
              JOIN DELETED ON DELETED.SysTrxNo = INSERTED.SysTrxNo and DELETED.SysTrxLine = INSERTED.SysTrxLine
              JOIN OrderHdr ON OrderHdr.SysTrxNo = INSERTED.SysTrxNo 		  
              WHERE ((INSERTED.Status = 'O') and (OrderHdr.OrderStatusID = 1001 or OrderHdr.OrderStatusID = 1003 or OrderHdr.OrderStatusID = 1005) and INSERTED.DtTm < DATEADD(SECOND, -2, CURRENT_TIMESTAMP) and
              (INSERTED.UnitPrice <> DELETED.UnitPrice or INSERTED.UnitPriceKeyed <> DELETED.UnitPriceKeyed))
              AND @exists <> 1

        SELECT @count = Count(*) FROM SmartLogixPriceUpdateRequired GROUP BY SysTrxNo, SysTrxLine
        if (@count > 0 and @exists <> 1)
        BEGIN
        EXEC dbo.SLX_CallUpdateSalesOrderService
            @returnValue OUT
        END
    END
END
GO