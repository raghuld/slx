-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARStandardAcctChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARStandardAcctChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARStandardAcctChange]
   ON  [dbo].[ARStandardAcct] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(CustomerName) or UPDATE(CreditHold)
    BEGIN
        DECLARE @returnValue int;
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP;
        DECLARE @currentId bigint = null;
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        declare @tmp table
        (
          id int not null
          primary key(id)
        )
        insert @tmp select INSERTED.StandardAcctID from INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.StandardAcctID = INSERTED.StandardAcctID 
        WHERE (INSERTED.CustomerName <> DELETED.CustomerName or
        ISNULL(INSERTED.CreditHold, 'N') <> ISNULL(DELETED.CreditHold, 'N')) and
        INSERTED.StandardAcctID = DELETED.StandardAcctID

        SELECT @maxCounter = Count(*) FROM INSERTED 
        LEFT OUTER JOIN DELETED ON DELETED.StandardAcctID = INSERTED.StandardAcctID 
        WHERE ((INSERTED.Type = 'S' or INSERTED.Type = 'D'  or INSERTED.Type = 'C') and INSERTED.CustomerName <> DELETED.CustomerName or
        ISNULL(INSERTED.CreditHold, 'N') <> ISNULL(DELETED.CreditHold, 'N')) and
        INSERTED.StandardAcctID = DELETED.StandardAcctID

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentId = id from @tmp

                if (@newLastModifiedDate is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateCustomerService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @customerId = @currentId;
                END
                delete from @tmp where id = @currentId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'Customer', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO