IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankProductInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankProductInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARShipToTankProductInsert]
   ON  [dbo].[ARShipTo_TankProduct] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = null
    DECLARE @currentId bigint = null
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null
    DECLARE @currentStandardAcctNo varchar(24)
    DECLARE @currentShipToCode varchar(24)
    DECLARE @currentDescription varchar(40)
    DECLARE @currentProductCode varchar(24)
    DECLARE @currentFillCapacity float(24)
    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select TankID from INSERTED

    SELECT @maxCounter = Count(*) FROM INSERTED GROUP BY TankID

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            SELECT @currentStandardAcctNo = ARStandardAcct.StandardAcctNo, @currentShipToCode = ARShipTo.Code, @currentCode = ARShipTo_Tank.Code, @currentDescription = ARShipTo_Tank.Descr,
            @currentProductCode = SalesAlias.Code, @currentFillCapacity = ARShipTo_Tank.FillCapacity FROM ARShipTo_Tank
            JOIN ARShipTo ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID
            JOIN ARStandardAcct
            ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID
            LEFT OUTER JOIN INSERTED
            ON ARShipTo_Tank.TankID = INSERTED.TankID
            LEFT OUTER JOIN SalesAlias ON SalesAlias.SalesAliasID = INSERTED.SalesAliasID
            WHERE ARShipTo_Tank.TankID = @currentId

            if (@currentId is not null)
            BEGIN
            EXEC dbo.SLX_CallInsertFleetVehicleService
                @id = @currentId,
                @code = @currentCode,
                @standardAcctNo = @currentStandardAcctNo,
                @shipToCode = @currentShipToCode,
                @description = @currentDescription,
                @productCode = @currentProductCode,
                @fillCapacity = @currentFillCapacity
            END

            SELECT @currentStandardAcctNo = ARStandardAcct.StandardAcctNo, @currentShipToCode = ARShipTo.Code, @currentCode = ARShipTo_Tank.Code, @currentDescription = ARShipTo_Tank.Descr,
            @currentProductCode = SalesAlias.Code, @currentFillCapacity = ARShipTo_Tank.FillCapacity FROM ARShipTo_Tank
            JOIN ARShipTo ON ARShipTo_Tank.ShipToID = ARShipto.ShiptoID
            JOIN ARStandardAcct
            ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID
            LEFT OUTER JOIN INSERTED 
            ON ARShipTo_Tank.TankID = INSERTED.TankID
            LEFT OUTER JOIN SalesAlias 
            ON INSERTED.SalesAliasID = SalesAlias.SalesAliasID 
            WHERE ARShipTo_Tank.TankID = @currentId

            if (@currentId is not null)
            BEGIN
                EXEC dbo.SLX_CallInsertTankService
                    @id = @currentId,
                    @code = @currentCode,
                    @standardAcctNo = @currentStandardAcctNo,
                    @shipToCode = @currentShipToCode,
                    @barcode = @currentDescription,
                    @productCode = @currentProductCode,
                    @capacity = @currentFillCapacity
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO