-- Ship To Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnShipToInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnShipToInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnShipToInsert]
   ON  [dbo].[ARShipto] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
    DECLARE @currentId bigint = null;
    DECLARE @count bigint = 0;
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null
    DECLARE @currentStandardAcctNo varchar(24)
    DECLARE @currentName varchar(40)
    DECLARE @currentAddress1 varchar(100)
    DECLARE @currentAddress2 varchar(100)
    DECLARE @currentCity varchar(24)
    DECLARE @currentState varchar(24)
    DECLARE @currentZip varchar(15)
    DECLARE @currentPhone varchar(24)
    DECLARE @currentFax varchar(24)
    DECLARE @currentPoRequired char(1)
    DECLARE @currentPoNumber varchar(50)
    DECLARE @currentIsActive char(1)
    DECLARE @currentOnHold char(1)

    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
        select ShiptoID from INSERTED
        JOIN ARStandardAcct
        ON INSERTED.StandardAcctID = ARStandardAcct.StandardAcctID
        WHERE (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D'  or INSERTED.Type = 'C')

    SELECT @maxCounter = Count(*) FROM INSERTED
    JOIN ARStandardAcct
    ON INSERTED.StandardAcctID = ARStandardAcct.StandardAcctID
    WHERE (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D'  or INSERTED.Type = 'C')

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp
            
            SELECT @currentStandardAcctNo = ARStandardAcct.StandardAcctNo, @currentCode = INSERTED.Code, @currentName = INSERTED.LongDescr, @currentAddress1 = ARShipToAddress.Addr1,
                          @currentAddress2 = ARShipToAddress.Addr2, @currentCity = City.Code, @currentState = StateProv.Code, @currentZip = ARShipToAddress.Zip, @currentPhone = INSERTED.PrimaryPhone,
                          @currentFax = INSERTED.PrimaryFax, @currentPoRequired = INSERTED.PORequired, @currentPoNumber = Dest.PONumber, @currentIsActive = INSERTED.Active, @currentOnHold = INSERTED.CLCreditHold FROM INSERTED
                          JOIN ARStandardAcct
                          ON INSERTED.StandardAcctID = ARStandardAcct.StandardAcctID
                          LEFT OUTER JOIN ARShipToAddress
                          ON INSERTED.ShiptoID = ARShipToAddress.ShipToID and ARShipToAddress.Type = 'P'
                          LEFT OUTER JOIN StateProv
                          ON ARShipToAddress.StateProvID = StateProv.ID
                          LEFT OUTER JOIN City ON ARShipToAddress.CityID = City.ID
                          LEFT OUTER JOIN MasterSite
                          ON MasterSite.ShipToID = INSERTED.ShiptoID
                          LEFT OUTER JOIN Dest
                          ON Dest.DestID = MasterSite.DestID
                          WHERE INSERTED.ShiptoID = @currentId and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D'  or INSERTED.Type = 'C')

            if (@currentId is not null)
            BEGIN
                EXEC dbo.SLX_CallInsertShipToService
                    @id = @currentId,
                    @code = @currentCode,
                    @standardAcctno = @currentStandardAcctNo,
                    @name = @currentName,
                    @address1 = @currentAddress1,
                    @address2 = @currentAddress2,
                    @city = @currentCity,
                    @state = @currentState,
                    @zip = @currentZip,
                    @phone = @currentPhone,
                    @fax = @currentFax,
                    @poRequired = @currentPoRequired,
                    @poNumber = @currentPoNumber,
                    @active = @currentIsActive,
                    @onHold = @currentOnHold

                EXEC dbo.SLX_CallUpdateCustomerService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @customerId = @currentId;
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO
