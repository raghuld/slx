-- Billing Item Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnBillingItemChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnBillingItemChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnBillingItemChange]
   ON  [dbo].[BillingItem] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(Descr)
    BEGIN
        DECLARE @returnValue int
        DECLARE @newLastModifiedDate datetime = NULL
        DECLARE @currentId bigint = null
        DECLARE @counter int = 0
        DECLARE @maxCounter int = 0
        DECLARE @newLastModifiedDtTm datetime = CURRENT_TIMESTAMP
        declare @tmp table
        (
            id int not null
            primary key(id)
        )

        insert @tmp
        select INSERTED.BillingItemID from INSERTED LEFT OUTER JOIN DELETED ON DELETED.BillingItemID = INSERTED.BillingItemID
            WHERE (ISNULL(INSERTED.Descr, 'null') <> ISNULL(DELETED.Descr, 'null')) and
            INSERTED.Code = DELETED.Code
            GROUP BY INSERTED.BillingItemID

        SELECT @maxCounter = Count(*) FROM INSERTED LEFT OUTER JOIN DELETED ON DELETED.BillingItemID = INSERTED.BillingItemID
            WHERE (ISNULL(INSERTED.Descr, 'null') <> ISNULL(DELETED.Descr, 'null'))  and
            INSERTED.Code = DELETED.Code
            GROUP BY INSERTED.BillingItemID

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentId = id from @tmp

                if (@currentId is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateProductsService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDtTm,
                        @productId = @currentId
                END

                delete from @tmp where id = @currentId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'Product', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO