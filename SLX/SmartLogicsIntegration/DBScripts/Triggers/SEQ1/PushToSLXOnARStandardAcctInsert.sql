-- Customer Triggers
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARStandardAcctInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARStandardAcctInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARStandardAcctInsert]
   ON  [dbo].[ARStandardAcct] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int;
    DECLARE @newLastModifiedDate datetime = null;
    DECLARE @currentId bigint = 0;
    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    DECLARE @currentCode varchar(24) = null;
    DECLARE @currentName varchar(30)
    DECLARE @currentAddress varchar(100)
    DECLARE @currentCity varchar(24)
    DECLARE @currentState varchar(24)
    DECLARE @currentZip varchar(15)
    DECLARE @currentOnHold varchar(1)
    declare @tmp table
    (
      id int not null
      primary key(id)
    )

    insert @tmp
    select StandardAcctID from INSERTED
    WHERE (INSERTED.Type = 'S' or INSERTED.Type = 'D' or INSERTED.Type = 'C')

    SELECT @maxCounter = Count(*) FROM INSERTED
    WHERE (INSERTED.Type = 'S' or INSERTED.Type = 'D' or INSERTED.Type = 'C')

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            SELECT @currentCode = INSERTED.StandardAcctNo, @currentName = INSERTED.CustomerName, @currentOnHold = INSERTED.CreditHold FROM INSERTED
            WHERE INSERTED.StandardAcctID = @currentId and (INSERTED.Type = 'S' or INSERTED.Type = 'D' or INSERTED.Type = 'C')

            if (@currentId is not null and @currentId <> 0)
            BEGIN
            EXEC dbo.SLX_CallInsertCustomerService
                @id = @currentId,
                @code = @currentCode,
                @name = @currentName,
                @address = @currentAddress,
                @city = @currentCity,
                @state = @currentState,
                @zip = @currentZip,
                @onHold = @currentOnHold
            END

            delete from @tmp where id = @currentId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
        END
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
        END
        set implicit_transactions off
        DECLARE @Request varchar(max) = 'id=' + CAST(@currentId as varchar) + ',code=' + CAST(@currentCode as varchar) + ',name=' + CAST(@currentName as varchar) + ',address=' + CAST(@currentAddress as varchar) + ',city=' + CAST(@currentCity as varchar) + ',state=' + CAST(@currentState as varchar) + ',zip=' + CAST(@currentZip as varchar) + ',onhold=' + CAST(@currentOnHold as varchar);
        INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, @currentId, @currentCode, 'Customer', 2, @ErrorMessage, @Request, null, null, 1)
        set implicit_transactions on
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO