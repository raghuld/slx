-- ARShipToAddress Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToAddressInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToAddressInsert] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARShipToAddressInsert]
   ON  [dbo].[ARShipToAddress] AFTER INSERT
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    DECLARE @returnValue int
    DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
    DECLARE @currentCustomerId int = null
    DECLARE @currentShipToId int = null
    DECLARE @currentId bigint = null;
    DECLARE @currentStandardAcctCode varchar(24) = null;

    DECLARE @counter int = 0;
    DECLARE @maxCounter int
    declare @tmp table
    (
        id int not null,
        primary key(id)
    )

    insert @tmp
        select ARShipTo.ShipToID from INSERTED
        JOIN ARShipTo ON ARShipTo.ShipToID = INSERTED.ShipToID
        JOIN ARStandardAcct ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID
        WHERE INSERTED.Type = 'P' and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or INSERTED.Type = 'C')

    SELECT @maxCounter = Count(*) FROM INSERTED 
    JOIN ARShipTo ON ARShipTo.ShipToID = INSERTED.ShipToID
    JOIN ARStandardAcct ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID
    WHERE INSERTED.Type = 'P' and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or INSERTED.Type = 'C')

    BEGIN TRY
        WHILE (@counter < @maxCounter)
        BEGIN		
            select top 1 @currentId = id from @tmp

            SELECT @currentCustomerId = ARStandardAcct.StandardAcctID, @currentShipToId = ARShipTo.ShiptoID FROM INSERTED
                        JOIN ARShipTo ON ARShipTo.ShipToID = INSERTED.ShipToID
                        JOIN ARStandardAcct ON ARShipTo.StandardAcctID = ARStandardAcct.StandardAcctID
                        WHERE INSERTED.ShipToID = @currentId and INSERTED.Type = 'P' and (ARStandardAcct.Type = 'S' or ARStandardAcct.Type = 'D' or INSERTED.Type = 'C')

            if (@currentId is not null)
            BEGIN
                EXEC dbo.SLX_CallUpdateCustomerService
                    @returnValue OUT,
                    @lastModifiedDate = @newLastModifiedDate,
                    @customerId = @currentCustomerId;

                EXEC dbo.SLX_CallUpdateShipToService
                    @returnValue OUT,
                    @lastModifiedDate = @newLastModifiedDate,
                    @shipToId = @currentShipToId;
            END

            delete from @tmp where id = @currentShipToId

            set @counter = @counter + 1;
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT 
            @ErrorMessage = ERROR_MESSAGE(),
            @ErrorSeverity = ERROR_SEVERITY(),
            @ErrorState = ERROR_STATE();
        IF (@ErrorMessage like '%Error in SLX%')
        BEGIN
            SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.'
        END
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
    END CATCH;
END
GO