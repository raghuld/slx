-- Vehicle and Tanks Trigger
-- Add
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankChange] 
END
GO

CREATE TRIGGER [dbo].[PushToSLXOnARShipToTankChange]
   ON  [dbo].[ARShipTo_Tank] AFTER UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF UPDATE(Descr) or UPDATE(FillCapacity)
    BEGIN
        DECLARE @returnValue int
        DECLARE @newLastModifiedDate datetime = CURRENT_TIMESTAMP
        DECLARE @currentId bigint = null
        DECLARE @count bigint = 0
        DECLARE @counter int = 0;
        DECLARE @maxCounter int
        declare @tmp table
        (
          id int not null
          primary key(id)
        )

        insert @tmp
        SELECT INSERTED.TankID
        FROM INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.TankID = INSERTED.TankID
        WHERE (ISNULL(INSERTED.Descr, 'null') <> ISNULL(DELETED.Descr, 'null') or 
        INSERTED.FillCapacity <> DELETED.FillCapacity) and
        INSERTED.Code = DELETED.Code
        GROUP BY INSERTED.TankID

        SELECT @maxCounter = Count(*)
        FROM INSERTED
        LEFT OUTER JOIN DELETED ON DELETED.TankID = INSERTED.TankID
        WHERE (ISNULL(INSERTED.Descr, 'null') <> ISNULL(DELETED.Descr, 'null') or 
        INSERTED.FillCapacity <> DELETED.FillCapacity) and
        INSERTED.Code = DELETED.Code
        GROUP BY INSERTED.TankID

        BEGIN TRY
            WHILE (@counter < @maxCounter)
            BEGIN		
                select top 1 @currentId = id from @tmp

                if (@newLastModifiedDate is not null)
                BEGIN
                    EXEC dbo.SLX_CallUpdateFleetVehiclesService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @tankId = @currentId

                    EXEC dbo.SLX_CallUpdateTanksService
                        @returnValue OUT,
                        @lastModifiedDate = @newLastModifiedDate,
                        @tankId = @currentId
                END
                delete from @tmp where id = @currentId

                set @counter = @counter + 1;
            END
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT 
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();
            IF (@ErrorMessage like '%Error in SLX%')
            BEGIN
                SET @ErrorMessage = 'Error in SLX.  Please contact SLX for more information.' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Details: ' + ERROR_MESSAGE()
            END
            IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION;
            END
            set implicit_transactions off
            DECLARE @Request varchar(max) = 'id=' + CAST(@currentId AS varchar) + ',lastModifiedDate=' + CAST(@newLastModifiedDate AS varchar);
            INSERT INTO SmartLogixPushLog VALUES (NEWID(), CURRENT_TIMESTAMP, CAST(@currentId AS varchar(24)), '', 'Tank', 2, @ErrorMessage, @Request, null, null, 1)
            set implicit_transactions on
            RAISERROR (@ErrorMessage, -- Message text.
                       @ErrorSeverity, -- Severity.
                       @ErrorState -- State.
                       );
        END CATCH;
    END
END
GO