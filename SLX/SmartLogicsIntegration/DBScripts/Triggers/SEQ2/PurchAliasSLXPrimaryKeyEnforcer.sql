EXECUTE SafeCreateTriggerStub 'dbo', 'PurchAliasSLXPrimaryKeyEnforcer', 'PurchAlias'
GO

ALTER TRIGGER PurchAliasSLXPrimaryKeyEnforcer  
ON PurchAlias  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.PurchAliasID = I.PurchAliasID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')
)  
BEGIN  
RAISERROR('Changes to the Code in table PurchAlias are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  