EXECUTE SafeCreateTriggerStub 'dbo', 'VehicleSLXPrimaryKeyEnforcer', 'Vehicle'
GO

ALTER TRIGGER VehicleSLXPrimaryKeyEnforcer  
ON Vehicle  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.VehicleID = I.VehicleID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')
)  
BEGIN  
RAISERROR('Changes to the Code in table Vehicle are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  