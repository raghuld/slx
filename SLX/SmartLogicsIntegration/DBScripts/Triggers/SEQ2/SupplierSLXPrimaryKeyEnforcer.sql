EXECUTE SafeCreateTriggerStub 'dbo', 'SupplierSLXPrimaryKeyEnforcer', 'Supplier'
GO

ALTER TRIGGER SupplierSLXPrimaryKeyEnforcer  
ON Supplier  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *  
   FROM INSERTED I  
   JOIN DELETED D ON D.SupplierID = I.SupplierID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')  
)  
BEGIN  
RAISERROR('Changes to the Code in table Supplier are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  