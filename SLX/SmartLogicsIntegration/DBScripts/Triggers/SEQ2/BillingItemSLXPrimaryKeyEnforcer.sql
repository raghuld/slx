EXECUTE SafeCreateTriggerStub 'dbo', 'BillingItemSLXPrimaryKeyEnforcer', 'BillingItem'
GO

ALTER TRIGGER BillingItemSLXPrimaryKeyEnforcer  
ON BillingItem  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.BillingItemID = I.BillingItemID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')
)  
BEGIN  
RAISERROR('Changes to the Code in table BillingItem are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  