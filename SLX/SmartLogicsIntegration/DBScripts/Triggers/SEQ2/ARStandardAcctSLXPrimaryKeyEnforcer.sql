EXECUTE SafeCreateTriggerStub 'dbo', 'ARStandardAcctSLXPrimaryKeyEnforcer', 'ARStandardAcct'
GO

ALTER TRIGGER ARStandardAcctSLXPrimaryKeyEnforcer  
ON ARStandardAcct
FOR UPDATE
AS  
IF UPDATE(StandardAcctNo) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.StandardAcctID = I.StandardAcctID AND ISNULL(D.StandardAcctNo,I.StandardAcctNo+'.') <> ISNULL(I.StandardAcctNo,D.StandardAcctNo+'.')
)  
BEGIN  
RAISERROR('Changes to the StandardAcctNo in table ARStandardAcct are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  