EXECUTE SafeCreateTriggerStub 'dbo', 'SupplyPtSLXPrimaryKeyEnforcer', 'SupplyPt'
GO

ALTER TRIGGER SupplyPtSLXPrimaryKeyEnforcer  
ON SupplyPt  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *  
   FROM INSERTED I  
   JOIN DELETED D ON D.SupplyPtID = I.SupplyPtID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')  
)  
BEGIN  
RAISERROR('Changes to the Code in table SupplyPt are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  