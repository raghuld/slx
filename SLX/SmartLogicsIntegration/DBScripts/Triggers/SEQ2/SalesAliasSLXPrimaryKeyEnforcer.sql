EXECUTE SafeCreateTriggerStub 'dbo', 'SalesAliasSLXPrimaryKeyEnforcer', 'SalesAlias'
GO

ALTER TRIGGER SalesAliasSLXPrimaryKeyEnforcer  
ON SalesAlias  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.SalesAliasID = I.SalesAliasID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')
)  
BEGIN  
RAISERROR('Changes to the Code in table SalesAlias are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  