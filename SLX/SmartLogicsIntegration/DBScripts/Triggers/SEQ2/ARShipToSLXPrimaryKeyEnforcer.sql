EXECUTE SafeCreateTriggerStub 'dbo', 'ARShipToSLXPrimaryKeyEnforcer', 'ARShipTo'
GO

ALTER TRIGGER ARShipToSLXPrimaryKeyEnforcer  
ON ARShipTo  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *  
   FROM INSERTED I  
   JOIN DELETED D ON D.ShiptoID = I.ShiptoID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')  
)  
BEGIN  
RAISERROR('Changes to the Code in table ARShipTo are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  