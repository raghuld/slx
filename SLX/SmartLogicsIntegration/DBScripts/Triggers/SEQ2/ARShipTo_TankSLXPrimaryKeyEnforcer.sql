EXECUTE SafeCreateTriggerStub 'dbo', 'ARShipTo_TankSLXPrimaryKeyEnforcer', 'ARShipTo_Tank'
GO

ALTER TRIGGER ARShipTo_TankSLXPrimaryKeyEnforcer  
ON ARShipTo_Tank  
FOR UPDATE
AS  
IF UPDATE(Code) AND EXISTS (  
   SELECT *
   FROM INSERTED I
   JOIN DELETED D ON D.TankID = I.TankID AND ISNULL(D.Code,I.Code+'.') <> ISNULL(I.Code,D.Code+'.')
)  
BEGIN  
RAISERROR('Changes to the Code in table ARShipTo_Tank are NOT allowed due to SmartLogix Integration', 16,1);  
END
GO  