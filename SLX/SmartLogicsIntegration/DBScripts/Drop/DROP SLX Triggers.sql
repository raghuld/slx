IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateShipToService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateShipToService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateInsiteService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateInsiteService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertInsiteService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertInsiteService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplierService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateSupplierService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertSupplierService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertSupplierService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplyPtService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateSupplyPtService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertSupplyPtService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertSupplyPtService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplierSupplyPtService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateSupplierSupplyPtService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertSupplierSupplyPtService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertSupplierSupplyPtService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateCustomerService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateCustomerService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateTanksService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateTanksService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateFleetVehiclesService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateFleetVehiclesService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateDriverService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateDriverService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateTruckService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateTruckService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateProductsService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateProductsService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSalesOrderService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateSalesOrderService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertProductService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertProductService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertDriverService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertDriverService
END
GO	

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertCustomerService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertCustomerService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertFleetVehicleService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertFleetVehicleService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertShipToService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertShipToService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertTankService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertTankService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertTruckService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertTruckService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallInsertCarrierService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallInsertCarrierService
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateCarrierService')AND type IN ( N'P', N'PC' )) 
BEGIN
  DROP Procedure [dbo].SLX_CallUpdateCarrierService
END
GO
  

	IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnInsiteInsert]
END
GO   

 
	IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplierInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplyPtInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplyPtInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierSupplyPtInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplierSupplyPtInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARStandardAcctInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARStandardAcctInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToAddressInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToAddressInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnShipToInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnShipToInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSalesAliasInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSalesAliasInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnBillingItemInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnBillingItemInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnPurchAliasInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnPurchAliasInsert]
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankProductInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankProductInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversPhoneInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversPhoneInsert] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnVehicleInsert'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnVehicleInsert]
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnInsiteChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnInsiteChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplierChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplyPtChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplyPtChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSupplierSupplyPtChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSupplierSupplyPtChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARStandardAcctChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARStandardAcctChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnShipToChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnShipToChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToAddressChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToAddressChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSalesAliasChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSalesAliasChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnBillingItemChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnBillingItemChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnPurchAliasChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnPurchAliasChange]
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnProdContChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnProdContChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnARShipToTankProductChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnARShipToTankProductChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnDriversPhoneChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnDriversPhoneChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnVehicleChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnVehicleChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnSalesOrderPriceChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnSalesOrderPriceChange] 
END
GO
IF OBJECTPROPERTY(OBJECT_ID('PushToSLXOnOrderHdrChange'), 'IsTrigger') = 1
BEGIN
    DROP Trigger [dbo].[PushToSLXOnOrderHdrChange] 
END
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_CreateSalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_CreateSalesOrder]
END
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_CompletedSalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_CompletedSalesOrder]
END
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_ModifySalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_ModifySalesOrder]
END
GO

IF OBJECTPROPERTY(OBJECT_ID('ARStandardAcctSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
	DROP TRIGGER ARStandardAcctSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('VehicleSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER VehicleSLXPrimaryKeyEnforcer 
END
Go


IF OBJECTPROPERTY(OBJECT_ID('ARShipTo_TankSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER ARShipTo_TankSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('SalesAliasSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER SalesAliasSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('PurchAliasSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER PurchAliasSLXPrimaryKeyEnforcer 
END
GO


IF OBJECTPROPERTY(OBJECT_ID('BillingItemSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER BillingItemSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('ARShipToSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER ARShipToSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('SupplierSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER SupplierSLXPrimaryKeyEnforcer 
END
GO


IF OBJECTPROPERTY(OBJECT_ID('SupplyPtSLXPrimaryKeyEnforcer'), 'IsTrigger') = 1
BEGIN
DROP TRIGGER SupplyPtSLXPrimaryKeyEnforcer  
END
GO


IF OBJECTPROPERTY(OBJECT_ID('SLXIntegration'), 'IsTrigger') = 1
BEGIN
DROP ASSEMBLY SLXIntegration
END
GO




