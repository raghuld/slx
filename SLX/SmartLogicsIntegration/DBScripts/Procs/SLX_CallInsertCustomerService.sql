
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertCustomerService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertCustomerService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertCustomerService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@name [nvarchar](30),
	@address [nvarchar](100),
	@city [nvarchar](24),
	@state [nvarchar](24),
	@zip [nvarchar](15),
	@onHold [nvarchar](1)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertCustomerService]
GO
