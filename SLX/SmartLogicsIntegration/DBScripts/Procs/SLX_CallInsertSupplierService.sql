
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertSupplierService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallInsertSupplierService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertSupplierService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@name [nvarchar](40)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertSupplierService]
GO
