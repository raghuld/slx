
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertProductService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertProductService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertProductService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@description [nvarchar](40),
	@isPurchaseOnly [bit],
	@epaDisclaimer [nvarchar](254),
	@nonFuel [nvarchar](1),
	@uomId [int],
	@uomCode [nvarchar](24),
	@productCategory [nvarchar](24)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertProductService]
GO
