IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertCarrierService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallInsertCarrierService
END
GO

exec('CREATE PROCEDURE [dbo].[SLX_CallInsertCarrierService]
	@name [nvarchar](24),
	@number [int],
	@scac [nvarchar](82)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertCarrierService]')
GO