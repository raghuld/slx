IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_CreateSalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_CreateSalesOrder]
END
GO

CREATE PROCEDURE [dbo].[SmartLogix_CreateSalesOrder] 
(
  @sysTrxNo                NUMERIC(20,0),
  @companyId               varchar(8),
  @standardAcctId          int = null,
  @driverId                int = null,
  @toSiteId                int = null,
  @vehicleId               int = null,
  @vehicleType             int =  null,
  @orderDtTm               DATETIME,
  @inUserId                VARCHAR(32),
  @orderDeliveryDate       DATETIME = null, 
  @Status			       VARCHAR(24) = null,
  @poNumber                VARCHAR(24) = null,
  @outMessage              VARCHAR(1000) OUTPUT -- to return messages to UI
)
AS
BEGIN
    DECLARE @OrderType char(1);
    DECLARE @SalesPersonId int;
    DECLARE @FrtRateID int;
    DECLARE @PriceExpiresDtTm datetime;
    DECLARE @DefPromisedDtTm datetime;
    DECLARE @DefRequestedDtTm datetime;
    DECLARE @OrderNo           VARCHAR(24);
    DECLARE @CurrentItemNumber INTEGER
    DECLARE @MaxItemNumber     INTEGER
    DECLARE @TerminalID        INTEGER
    DECLARE @TankID            INTEGER
    DECLARE @ProductID         INTEGER
    DECLARE @Quantity          NUMERIC(17, 5)
    DECLARE @OrderDate         DATETIME
    DECLARE @Price             DECIMAL(17,7)
    DECLARE @CarrierID         INTEGER = NULL
    DECLARE @OrderStatusID     INTEGER = 1001
	DECLARE @ErrorOccurred bit = 0      
	DECLARE @ErrorMessage NVARCHAR(4000);      
	DECLARE @ErrorSeverity INT;      
	DECLARE @ErrorState INT; 

Begin Transaction SmartLogix_CreateSalesOrder
	Begin Try
	Select top 1 @companyId = Company_Id from company order by Company_Id
    EXECUTE dbo.ARInvoiceNextInvNo @companyId, '', @OrderNo OUTPUT

		WHILE EXISTS(SELECT SysTrxNo FROM dbo.OrderHdr (NOLOCK)
					 WHERE CompanyID = @CompanyID AND OrderNo = @OrderNo AND SysTrxNo <> @SysTrxNo    
					 UNION
					 SELECT SysTrxNo FROM dbo.OrderHdrHistory (NOLOCK)
					 WHERE CompanyID = @CompanyID AND OrderNo = @OrderNo AND SysTrxNo <> @SysTrxNo)
		BEGIN
		  EXECUTE dbo.ARInvoiceNextInvNo @companyId, '', @OrderNo OUTPUT
		END
    
		EXECUTE dbo.GetOrderHdrValues @ToSiteID, null, null, @orderDtTm, 
		  @outOrderType = @OrderType OUTPUT, 
		  @outStandardAcctID = @standardAcctID OUTPUT, 
		  @outSalesPersonId = @SalesPersonID OUTPUT, 
		  @outFrtRateID = @FrtRateID OUTPUT, 
		  @outPriceExpiresDtTm = @PriceExpiresDtTm OUTPUT, 
		  @outPromisedDtTm = @DefPromisedDtTm OUTPUT,
		  @outRequestedDtTm = @DefRequestedDtTm OUTPUT

		IF @vehicleId is not null
		BEGIN
		select @CarrierID = CarrierID FROM Vehicle where VehicleID = @vehicleID
		END

		IF @Status is not null
		BEGIN
			select top 1 @OrderStatusID = OrderStatusID from OrderOEStatus where Code = @Status
		END
		ELSE
		BEGIN
			select top 1 @OrderStatusID = OrderStatusID from OrderOEStatus where Code = 'Open'
		END
    
		INSERT INTO dbo.OrderHdr (SysTrxNo, CompanyID, OrderNo, PONo, DefCarrierID, OrderDtTm, DtTm, UserID, OnHold, Hold, 
			  OrderedBy, ReceivingContact, DefSalesPersonID, FrtRateID, Status, DefKeepPrice, PriceExpiresDtTm, 
			  SalesDollarDiscAmt, DefRequestedDtTm, DefPromisedDtTm, InvoiceCompleteOrder, PrintedDtTm, 
			  DefVehicleID, DefDriverID, ToSiteID, OrderType, OrderStatusID, FreightCalcDate, 
			  SONO, AspenID, sync_demandstream, SchedEarlyDtTm, SchedLatestDtTm, DispatchedDate, LastStatusDate,
			  LastModifiedUser,LastModifiedDtTm, InternalTransferOrder)

		VALUES (@SysTrxNo, 
				@companyId, 
				@OrderNo, 
				@poNumber,
				@CarrierID, 
				@OrderDtTm, 
				GetDate(), 
				@inUserID, 
				'N', 
				'N', 
				NULL, 
				NULL, 
				@SalesPersonID, 
				@FrtRateID, 
				'O', 
				'N', 
				@PriceExpiresDtTm, 
				NULL, 
				NULL, 
				@orderDeliveryDate, 
				'N',
				NULL, 
				@vehicleID, 
				@driverID,  
				@ToSiteID, 
				@OrderType,
				@OrderStatusID,
				null, 
				3, -- SO
				null, 
				0,
				null,
				null,
				null, 
				GetDate(),
				@inUserID,
				GetDate(),
				'N')

		SELECT @poNumber = OrderHdr.PONo FROM  OrderHdr WHERE OrderHdr.SysTrxNo = @SysTrxNo AND OrderHdr.PONo is null 

		IF (@poNumber is null) 
		BEGIN
			SELECT @StandardAcctID = ST.StandardAcctID
			FROM dbo.OrderHdr as OHD
			INNER JOIN dbo.MasterSite ms ON MS.MasterSiteID = OHD.ToSiteID
			INNER JOIN dbo.ARShipTo ST ON ST.ShiptoID = ms.ShipToID
			WHERE OHD.SysTrxNo = @SysTrxNo

			SELECT @poNumber = D.PONumber FROM dbo.Dest D
			JOIN dbo.ARShipTo S ON S.DestinationID = D.DestID 
			WHERE S.StandardAcctID = @StandardAcctID and S.ShipToID = @ToSiteID
        
			IF @PONumber is null
			BEGIN
				SELECT @poNumber = D.PONumber FROM dbo.Dest D
				JOIN dbo.ARShipTo S ON S.DestinationID = D.DestID 
				WHERE S.StandardAcctID = @StandardAcctID and S.PrimaryAcct = 'Y'
			END

			UPDATE OrderHdr SET PONo = @poNumber WHERE SysTrxNo = @SysTrxNo	
		END 

		SELECT
		  @CurrentItemNumber = 1,
		  @MaxItemNumber = COUNT(*)
		FROM dbo.SmartLogixProduct
		WHERE SysTrxNo = @sysTrxNo and ComponentNumber = 0                     
   
		WHILE @CurrentItemNumber <= @MaxItemNumber
		BEGIN
			-- Grab the next product
			SELECT
				@TerminalID = SmartLogixProduct.TerminalID,
				@ProductID = SmartLogixProduct.ProductID,
				@Quantity = SmartLogixProduct.Quantity,
				@OrderDate = SmartLogixProduct.OrderDate, 
				@Price = SmartLogixProduct.Price,
				@TankID = SmartLogixProduct.TankID
			FROM SmartLogixProduct
			WHERE SysTrxNo = @sysTrxNo and ItemNumber = @CurrentItemNumber and ComponentNumber = 0

			IF @Price = 0
			BEGIN
			SET @Price = NULL;
			END

			if (@TankID is not NULL)
			BEGIN
				EXECUTE dbo.OrderAddItem @SysTrxNo, @TerminalID, @ProductID, @Quantity, @inUserID, NULL, @CurrentItemNumber, NULL, NULL, NULL, @Price, NULL, NULL, 'N' , DEFAULT, DEFAULT, DEFAULT, 'Y', DEFAULT, DEFAULT, DEFAULT, @TankID, DEFAULT
			END
			ELSE
			BEGIN
				EXECUTE dbo.OrderAddItem @SysTrxNo, @TerminalID, @ProductID, @Quantity, @inUserID, NULL, @CurrentItemNumber, NULL, NULL, NULL, @Price, NULL, NULL, 'N' , DEFAULT, DEFAULT, DEFAULT, 'Y', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT
			END

			-- If it's a Purchase Site need to grab the Purchase Alias
			IF @TerminalID is not null and ((SELECT Type FROM dbo.MasterSite WHERE MasterSiteID = @TerminalID) = 'P')  
			BEGIN
			  SELECT TOP 1 @ProductID = PurchAlias.PurchAliasID
			  FROM dbo.PurchAlias
			  JOIN dbo.ProdCont ON ProdCont.ProdContID = PurchAlias.ProdContID
			  WHERE (dbo.GetProdContID(@ProductID) = ProdCont.ProdContID)
			  AND EXISTS (SELECT 1 FROM dbo.PurchRack (NOLOCK)
						  JOIN dbo.MasterSite ON MasterSite.SupplierSupplyPtID = PurchRack.SupplierSupplyPtID
						  WHERE MasterSite.MasterSiteID = @TerminalID)
			END    
			ELSE
			BEGIN
				SET @ProductID = (select top 1 PurchAliasID FROM PurchAlias where ProdContID = dbo.GetProdContID(@ProductID))
			END  	          
    

			IF @ProductID is not null
			BEGIN
				EXECUTE dbo.OrderAddComp @SysTrxNo, @CurrentItemNumber, 1, @ProductID, @TerminalID, @Quantity, NULL, @CarrierID, @vehicleId, 100, DEFAULT, DEFAULT, DEFAULT, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT 		 
			END
        
			EXECUTE dbo.OrderCalcPrice @SysTrxNo, @CurrentItemNumber 	

			EXECUTE dbo.OrderCalcTaxes @SysTrxNo, @CurrentItemNumber, DEFAULT

			SET @CurrentItemNumber = @CurrentItemNumber + 1;		
		END 
    
		EXECUTE dbo.UpdateFreightDetails @SysTrxNo 
		EXECUTE dbo.OrderCalc @SysTrxNo

		-- Get the credit release value because if Credit Release is Y we do not want to check the credit on the order
		DECLARE @CreditRelease char(1)
		select @CreditRelease = CreditRelease FROM OrderHdr WHERE SysTrxNo = @SysTrxNo

		IF @CreditRelease = 'N'
		BEGIN
			EXECUTE dbo.DXImport_OEOpenOrder_CreditCheck @SysTrxNo, @companyId  
		END
		Commit Transaction SmartLogix_CreateSalesOrder      
	End Try		        	
	Begin Catch
        Set @Status = ERROR_MESSAGE()       
            SELECT       
                @ErrorMessage = ERROR_MESSAGE(),      
                @ErrorSeverity = ERROR_SEVERITY(),      
                @ErrorState = ERROR_STATE();      
        Rollback Transaction SmartLogix_CreateSalesOrder      
        SET @ErrorOccurred = 1      
    End Catch
	
	IF @ErrorOccurred = 1      
    BEGIN      
        RAISERROR (@ErrorMessage, -- Message text.      
                   @ErrorSeverity, -- Severity.      
                   @ErrorState -- State.      
                   );      
	END
END

GO