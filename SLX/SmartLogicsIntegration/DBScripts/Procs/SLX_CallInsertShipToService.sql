
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertShipToService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertShipToService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertShipToService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@standardAcctNo [nvarchar](24),
	@name [nvarchar](40),
	@address1 [nvarchar](100),
	@address2 [nvarchar](100),
	@city [nvarchar](24),
	@state [nvarchar](24),
	@zip [nvarchar](15),
	@phone [nvarchar](24),
	@fax [nvarchar](24),
	@poRequired [nvarchar](1),
	@poNumber [nvarchar](50),
	@active [nvarchar](1),
	@onHold [nvarchar](1)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertShipToService]
GO
