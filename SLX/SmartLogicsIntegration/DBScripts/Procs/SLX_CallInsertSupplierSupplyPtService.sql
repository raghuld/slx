
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertSupplierSupplyPtService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallInsertSupplierSupplyPtService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertSupplierSupplyPtService]
	@supplierId [nvarchar](24),
	@supplyPtId [nvarchar](24)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertSupplierSupplyPtService]
GO
