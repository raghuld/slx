
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertInsiteService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertInsiteService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertInsiteService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@name [nvarchar](40)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertInsiteService]
GO
