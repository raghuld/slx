
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateProductsService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateProductsService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateProductsService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@productId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateProductsService]
GO
