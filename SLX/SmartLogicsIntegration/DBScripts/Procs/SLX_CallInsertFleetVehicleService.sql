
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertFleetVehicleService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallInsertFleetVehicleService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertFleetVehicleService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@standardAcctNo [nvarchar](24),
	@shipToCode [nvarchar](24),
	@description [nvarchar](40),
	@productCode [nvarchar](24),
	@fillCapacity [real]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertFleetVehicleService]
GO
