
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertTruckService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertTruckService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertTruckService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@vinNumber [nvarchar](24),
	@licPlateNo [nvarchar](30)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertTruckService]
GO
