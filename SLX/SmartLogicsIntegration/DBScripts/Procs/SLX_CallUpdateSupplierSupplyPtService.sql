
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplierSupplyPtService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallUpdateSupplierSupplyPtService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateSupplierSupplyPtService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@supplierSupplyPt [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateSupplierSupplyPtService]
GO
