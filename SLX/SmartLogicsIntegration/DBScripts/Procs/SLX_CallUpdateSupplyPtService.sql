
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplyPtService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateSupplyPtService
END
GO


CREATE PROCEDURE [dbo].[SLX_CallUpdateSupplyPtService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@supplyPtId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateSupplyPtService]
GO
