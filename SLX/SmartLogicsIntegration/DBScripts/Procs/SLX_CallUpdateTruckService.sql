
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateTruckService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateTruckService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateTruckService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@truckId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateTruckService]
GO
