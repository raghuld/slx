
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateSupplierService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallUpdateSupplierService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateSupplierService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@supplierId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateSupplierService]
GO
