
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateCustomerService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateCustomerService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateCustomerService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@customerId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateCustomerService]
GO
