IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateCarrierService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateCarrierService
END
GO

exec('CREATE PROCEDURE [dbo].[SLX_CallUpdateCarrierService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@carrierid [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateCarrierService]')
GO