IF EXISTS ( Select  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_CompletedSalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_CompletedSalesOrder]
END
GO
      
CREATE PROCEDURE [SmartLogix_CompletedSalesOrder]      
(      
    @SysTrxNo                NUMERIC(20,0),      
    @SysTrxLine     int,      
    @CompanyId               varchar(8),      
    @BolNo                   varchar(24) = null,      
    @GrossQty                numeric(17,2) = null,       
    @NetQty                  numeric(17,2) = null,           
    @TerminalId              int,      
    @Price                   numeric(17,7) = null,      
    @VehicleId               int = null,      
    @DriverId                int = null,      
    @SaleProdId              int,      
    @TankID                  int = null,      
    @UserID                  varchar(32) = null,      
    @OrderDeliveryDate       datetime = null,      
    @OrderNo                 varchar(24),      
    @DiversionDestState      varchar(2),      
    @IsNetBilling            bit = null,      
    @PONumber                varchar(24),      
    @Status varchar(max) OUTPUT      
)      
AS      
BEGIN      
      
DECLARE @ComponentNo int      
DECLARE @Source varchar(50)      
DECLARE @ShipDocSysTrxNo NUMERIC(20)      
DECLARE @ShipDocSysTrxLine int      
DECLARE @CurrentItemNumber int;      
DECLARE @MaxItemNumber int      
DECLARE @BoughtAsTerminalID int      
DECLARE @BoughtAsProductID int       
DECLARE @BoughtAsQuantity numeric(17,7)      
DECLARE @BoughtAsNetQuantity numeric(17,7) = null     
DECLARE @BoughtAsOrderDate datetime      
DECLARE @BoughtAsPrice numeric(17,7)      
DECLARE @CarrierID INTEGER = null      
DECLARE @KeyedPrice numeric(17,7)      
DECLARE @BoughtAsBOLNo varchar(24)      
DECLARE @ProductCode VARCHAR(24)      
DECLARE @BlendRecipeId INTEGER = NULL      
DECLARE @CurrentBOLItemCount INTEGER = 1      
DECLARE @blendPrct numeric(17,7) = 0      
DECLARE @ComponentBlendPrct numeric(17,7) = 0      
DECLARE @TotalComponentQuantity numeric(17,7) = 0      
DECLARE @ExistingBolItemId int = null      
DECLARE @CountComponents int = 0      
DECLARE @OrderStatusID int = NULL      
DECLARE @ProductType varchar(24) = NULL      
DECLARE @ErrorOccurred bit = 0      
DECLARE @ErrorMessage NVARCHAR(4000);      
DECLARE @ErrorSeverity INT;      
DECLARE @ErrorState INT;    
DECLARE @ComponentDiversionDestState varchar(2);  
DECLARE @CompProductID       INTEGER 
DECLARE @BasicOverrideFlag CHAR(1)
    
DECLARE @Invoice char(1);
DECLARE @DeliverDtTm datetime=NULL;
      

Select top 1 @companyId = Company_Id from company order by Company_Id      
if exists(Select top 1 1 from [SmartLogix_ProcessOrder_Processing] where SysTrxNo = @SysTrxNo and SysTrxLine = @SysTrxLine)      
    return;      
      
INSERT INTO SmartLogix_ProcessOrder_Processing (LogDate, SysTrxNo, SysTrxLine) VALUES ((Select getdate()), @SysTrxNo, @SysTrxLine)      
      
BEGIN TRANSACTION SmartLogix_CompletedSalesOrder      
          
    BEGIN TRY       
        Set @Source = 'SmartLogix_CompletedSalesOrder'      
		
		-- Delete all previous order item components
        DELETE FROM OrderItemComponent WHERE SysTrxNo = @SysTrxNo And SysTrxLine = @SysTrxLine

        -- Lookup the product type for the current sales alias      
        Select @ProductType = ProdType.Code FROM dbo.SalesAlias       
        JOIN dbo.ProdCont ON ProdCont.ProdContID = SalesAlias.ProdContID       
        JOIN dbo.ProdType ON ProdType.ProdTypeID = ProdCont.ProdTypeID      
        WHERE SalesAlias.SalesAliasID = @SaleProdId      
      
        IF (@vehicleId is not null)      
        BEGIN      
            Select @CarrierID = CarrierID FROM Vehicle where VehicleID = @vehicleID      
        END      
      
        Select top 1 @OrderStatusID = OrderStatusID from OrderOEStatus where Code = 'Billing Review'      
      
        -- Update the orderhdr status, delivery date, carrier, companyid, and vehicle      
		UPDATE OrderHdr      
			Set   CompanyID = @companyId,       
			DefCarrierID = @CarrierID,       
			UserID = @UserID,       
			DefVehicleID = @vehicleID,       
			DefDriverID = @driverID,        
			LastStatusDate = GetDate(),      
			LastModifiedUser = @UserID,      
			LastModifiedDtTm = GetDate(),      
			DefPromisedDtTm = @orderDeliveryDate,      
			DefRequestedDtTm= @orderDeliveryDate,      
			OrderStatusID = @OrderStatusID,      
			PONo = @PONumber        
		WHERE SysTrxNo = @sysTrxNo      
      
        IF @PONumber is not NULL      
        BEGIN      
            UPDATE OrderHdr Set PONo = @PONumber where SysTrxNo = @SysTrxNo    
        END      
      
        IF @Price = 0      
        BEGIN      
            Set @Price = NULL;      
        END       
      
        Select TOP 1 @BlendRecipeId = BlendRecipeID FROM BlendRecipe WHERE Code =       
        (Select TOP 1 SalesAlias.Code FROM OrderItem JOIN SalesAlias ON SalesAlias.SalesAliasID = OrderItem.MasterProdID       
         WHERE OrderItem.SysTrxNo = @SysTrxNo and OrderItem.SysTrxLine = @SysTrxLine)       
      
        EXECUTE dbo.OrderUpdItem @SysTrxNo, @SysTrxLine, @SaleProdId, @TerminalID, @GrossQty, @Price, 'N', NULL, NULL, NULL, NULL, @BlendRecipeId, NULL, NULL, DEFAULT,       
                                 @CarrierID, @vehicleID, @BolNo, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, @TankID, DEFAULT      
              
        Select      
          @CurrentItemNumber = 1,      
          @MaxItemNumber = COUNT(*)      
        FROM dbo.SmartLogixProduct      
        WHERE SysTrxNo = @sysTrxNo and ItemNumber = @SysTrxLine                          
         
        -- Check if there is a shipdoc already in the system for this item      
      
        WHILE @CurrentItemNumber <= @MaxItemNumber      
        BEGIN      
            -- Grab the next product      
            Select      
                @BoughtAsTerminalID = SmartLogixProduct.TerminalID,      
                @BoughtAsProductID = SmartLogixProduct.ProductID,      
                @BoughtAsQuantity = SmartLogixProduct.Quantity,      
                @BoughtAsNetQuantity = SmartLogixProduct.NetQuantity,      
                @BoughtAsOrderDate = SmartLogixProduct.OrderDate,       
                @BoughtAsPrice = SmartLogixProduct.Price,      
                @BoughtAsBOLNo = SmartLogixProduct.BOLNumber,
				@ComponentDiversionDestState = SmartLogixProduct.DiversionDestState      
            FROM SmartLogixProduct      
            WHERE SysTrxNo = @sysTrxNo and ItemNumber = @SysTrxLine and ComponentNumber = @CurrentItemNumber                
      
            Select @TotalComponentQuantity = SUM(Quantity) FROM SmartLogixProduct where SysTrxNo = @sysTrxNo and ItemNumber = @SysTrxLine and ComponentNumber is not null      
      
            IF (@TotalComponentQuantity > @GrossQty)      
            BEGIN      
                Set @ComponentBlendPrct = @BoughtAsQuantity / @TotalComponentQuantity * 1.0000000      
            END      
            -- The .0000000 is required here in order to keep things in the correct number of sig figs      
            Set @blendPrct = @BoughtAsQuantity / @GrossQty * 1.0000000      
      
            -- if this is not a blend then the blendprct needs to be 100.00   
			IF @MaxItemNumber = 1
            BEGIN      
                Set @blendPrct = 100.00      
            END      
			           
            EXECUTE dbo.OrderAddComp @SysTrxNo, @SysTrxLine, @CurrentItemNumber, @BoughtAsProductID, @BoughtAsTerminalID, @BoughtAsQuantity, NULL, @CarrierID, @VehicleId, @blendPrct, DEFAULT, DEFAULT, DEFAULT, NULL          
          
            EXECUTE dbo.OrderCalcPrice @SysTrxNo, @SysTrxLine       
            EXECUTE dbo.OrderCalcTaxes @SysTrxNo, @SysTrxLine, DEFAULT      
            EXECUTE dbo.RecalculateFreight @SysTrxNo, @SysTrxLine      
      
            -- If the UOM for the sales alias does not match the UOM for the purchalias we need to do a reset, this should only be the case for cylinders       
            DECLARE @SalesAliasUOM int      
            DECLARE @PurchAliasUOM int      
      
            -- Get the UOM for the SalesAlias      
            Select @SalesAliasUOM = SellByUOMID from SalesAlias WHERE SalesAliasID = @SaleProdID      
      
            -- Get the UOM for the PurchAlias      
            Select @PurchAliasUOM = UOMID from PurchAlias WHERE PurchAliasID = @BoughtAsProductID      
      
            IF (@SalesAliasUOM != @PurchAliasUOM)      
            BEGIN      
                EXECUTE OrderResetComp @SysTrxNo      
            END      
      
            IF (@BoughtAsBOLNo is not null)      
            BEGIN      
                Set @ShipDocSysTrxNo = null         
                Select @ShipDocSysTrxNo = SysTrxNo FROM ShipDoc where DocNo = @OrderNo      
                Select @ShipDocSysTrxLine = SysTrxLine FROM ShipDocItem where OrderSysTrxNo = @SysTrxNo and OrderSysTrxLine = @SysTrxLine      
      
                IF (@ShipDocSysTrxNo is null)      
                BEGIN      
                    EXECUTE dbo.GetNextInventoryCtr 'SysTrxNo', @ShipDocSysTrxNo OUTPUT      
                    INSERT INTO ShipDoc (SysTrxNo, DocNo, ShipDtTm, SO, CompanyID, UserID, DocType, [Status], BOLNo, CarrierID, VehicleID, DriverID)      
                            VALUES(@ShipDocSysTrxNo, @OrderNo, @BoughtAsOrderDate, 3, @CompanyId, @UserID, 'D', 'O', @BoughtAsBOLNo, @CarrierID, @VehicleID, @DriverID)        
                END                                               
                     
                IF (@ShipDocSysTrxLine is null)      
                BEGIN      
                    execute OrderShipItem @ShipDocSysTrxNo, @SysTrxNo, @SysTrxLine, @GrossQty, @NetQty, 'N', @ShipDocSysTrxLine output      
					EXEC dbo.GetProcessDate @SysTrxNo, 'D', @DeliverDtTm OUT 
					Update ShipDocItem Set DelivDtTm=@DeliverDtTm where  SysTrxNO = @ShipDocSysTrxNo AND SysTrxLine = @SysTrxLine
                END      
                      
                DECLARE @StateCode AS VARCHAR(5)    
                 
                Select @StateCode = isnull(StateProv.Code, '') from OrderHdr
                JOIN ARShipTo ON OrderHdr.ToSiteID = ARShipTo.ShipToID
                JOIN ARShipToAddress ON ARShipToAddress.ShipToId = ARShipTo.ShipToID
                Join StateProv ON StateProv.ID = ARShipToAddress.StateProvID
                where systrxno = @SysTrxNo and ARShipToAddress.Type = 'P'         
                    
                IF (@StateCode <> '')     
                BEGIN    
                    IF (@StateCode = @DiversionDestState  )  
                    BEGIN    
                        Set @DiversionDestState = null
                    END 
					
					IF (@StateCode = @ComponentDiversionDestState  )  
                    BEGIN    
                        Set @ComponentDiversionDestState = null
                    END          
                END         

                DECLARE @DiversionShipTo int = null      
				
				-- ComponentDiversionDestState always overrides a top level diversionstate provided at the bol level
				IF @ComponentDiversionDestState is not null
				BEGIN
					Select @DiversionShipTo = ShipToID FROM ARShipTo   
                    Join ARStandardAcct ON ARStandardAcct.StandardAcctID = ARShipTo.StandardAcctID    
                    WHERE StandardAcctNo = (Select Value from SmartLogixSettings where Name = 'DiversionStandardAccount') and LongDescr like @ComponentDiversionDestState + ' %'   
				END    
      
                -- If this is order has multiple different BOLs we need to update the BOLNo for the ShipDoc table to the orderNo      
                DECLARE @CountDistinctBolNo int      
                Select @CountDistinctBolNo = Count(distinct OvrBOLNo)       
                FROM ShipDocItemComp JOIN ShipDocItem ON ShipDocItem.SysTrxNo = ShipDocItemComp.SysTrxNo       
                WHERE ShipDocItem.OrderSysTrxNo = @SysTrxNo      
      
                IF (@CountDistinctBolNo > 1)      
                BEGIN      
                    UPDATE ShipDoc Set BolNo = @OrderNo WHERE SysTrxNo = @ShipDocSysTrxNo      
                END 

                EXECUTE OrderShipComp @ShipDocSysTrxNo, @ShipDocSysTrxLine, @CurrentItemNumber, 1, @BoughtAsProductID, @BoughtAsTerminalID, @blendPrct, null,       
                                      @BoughtAsQuantity, @BoughtAsNetQuantity, DEFAULT, DEFAULT, 'Y', @BoughtAsBOLNo,       
                                      @BoughtAsOrderDate, null, @VehicleID, @DriverID, DEFAULT, 0, DEFAULT, DEFAULT, @BoughtAsQuantity, DEFAULT, @ComponentDiversionDestState, @DiversionShipTo,       
                                      DEFAULT, DEFAULT, @UserID, DEFAULT, DEFAULT      
            END      
                  
            Set @CurrentItemNumber = @CurrentItemNumber + 1;        
        END     
		

		 SET @BasicOverrideFlag = 'N'
      
      SELECT 
        @BasicOverrideFlag = BasisOverride
      FROM Dbo.ShipDoc(NOLOCK) WHERE SystrxNo = @ShipDocSysTrxNo

		SET @CompProductID = NULL
      SELECT @CompProductID = MasterProdID 
      FROM dbo.ShipDocItemComp(NOLOCK)
      WHERE SysTrxNo = @ShipDocSysTrxNo AND SysTrxLine = @ShipDocSysTrxLine 
      
        DECLARE @DeliveredQty numeric(17,2) = null      
        IF (@IsNetBilling = 1)    
            Set @DeliveredQty = @NetQty       
        ELSE IF (@IsNetBilling = 0)         
            Set @DeliveredQty = @GrossQty      
        ELSE      
        BEGIN      
            DECLARE @BlendType char(1) = dbo.IsBlend(@SaleProdId)      
            DECLARE @ToSiteID int      
            Select @ToSiteID = ToSiteID FROM OrderHdr where SysTrxNo = @SysTrxNo      
            DECLARE @SellAt char(1) = dbo.GetGrossNet(@SaleProdId, @BoughtAsTerminalID, @ToSiteID, @CompanyId,@CompProductID, 'P', @BasicOverrideFlag,DEFAULT)      
            Set @DeliveredQty = dbo.GrossOrNet(@SellAt, 'N', @GrossQty, @NetQty)        
        END      
                 
        EXECUTE dbo.OrderDeliverItem @SysTrxNo, @SysTrxLine, @GrossQty, @NetQty, @DeliveredQty,       
                                     @OrderDeliveryDate, @UserID, DEFAULT, @SysTrxNo,       
                                     @SysTrxLine, DEFAULT, @outShipDocLineNo = @ShipDocSysTrxLine OUTPUT      
      
        EXECUTE dbo.UpdateFreightDetails @SysTrxNo       
        EXECUTE dbo.OrderCalc @SysTrxNo      
        -- Get the credit release value because if Credit Release is Y we do not want to check the credit on the order      
        DECLARE @CreditRelease char(1)      
        Select @CreditRelease = CreditRelease FROM OrderHdr WHERE SysTrxNo = @SysTrxNo      
      
        IF @CreditRelease = 'N'      
        BEGIN      
            EXECUTE dbo.DXImport_OEOpenOrder_CreditCheck @SysTrxNo, @companyId        
        END        
		
		--TWFF Delivery Details Populate
		Execute dbo.TWFFCreateDeliveryProcess @companyId,@UserID,@SysTrxNo,@Invoice Output
		            
        Set @Status = 'Delivered - Accepted'      
      
        COMMIT TRANSACTION SmartLogix_CompletedSalesOrder      
      
    END TRY
    BEGIN CATCH
        Set @Status = ERROR_MESSAGE()       
      
            Select       
                @ErrorMessage = ERROR_MESSAGE(),      
                @ErrorSeverity = ERROR_SEVERITY(),      
                @ErrorState = ERROR_STATE();      
      
        ROLLBACK TRANSACTION SmartLogix_CompletedSalesOrder      
      
        Set @ErrorOccurred = 1      
    END CATCH
      
    INSERT INTO SmartLogix_ProcessOrder_Logs (LogDate, SysTrxNo, Source, Status, SysTrxLine, CompanyId, BolNo, GrossQty, NetQty, BolCreatedDateTime, TerminalId)       
    VALUES ((Select getdate()), @SysTrxNo, @Source, @Status, @sysTrxLine, @CompanyId, @BoughtAsBolNo, @BoughtAsQuantity, @BoughtAsNetQuantity, @BoughtAsOrderDate, @TerminalId)      
                                                                                                                                                          
    DELETE FROM SmartLogix_ProcessOrder_Processing where SysTrxNo = @SysTrxNo and SysTrxLine = @SysTrxLine        
          
    IF @ErrorOccurred = 1      
    BEGIN      
        RAISERROR (@ErrorMessage, -- Message text.      
                       @ErrorSeverity, -- Severity.      
                       @ErrorState -- State.      
                       );      
    END                                                                            
END 																																	            																															  
GO