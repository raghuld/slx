
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateInsiteService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateInsiteService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateInsiteService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@insiteId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateInsiteService]
GO
