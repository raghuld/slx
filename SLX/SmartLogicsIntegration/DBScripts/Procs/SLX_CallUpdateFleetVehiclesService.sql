
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateFleetVehiclesService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallUpdateFleetVehiclesService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateFleetVehiclesService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@tankId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateFleetVehiclesService]
GO
