
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateShipToService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateShipToService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateShipToService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@shipToId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateShipToService]
GO
