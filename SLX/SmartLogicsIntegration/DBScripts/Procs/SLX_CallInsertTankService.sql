
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertTankService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
DROP Procedure [dbo].SLX_CallInsertTankService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertTankService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@standardAcctNo [nvarchar](24),
	@shipToCode [nvarchar](24),
	@barcode [nvarchar](40),
	@productCode [nvarchar](24),
	@capacity [real]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertTankService]
GO
