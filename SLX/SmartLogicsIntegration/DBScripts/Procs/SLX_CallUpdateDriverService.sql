
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallUpdateDriverService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallUpdateDriverService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallUpdateDriverService]
	@returnValue [int] OUTPUT,
	@lastModifiedDate [datetime],
	@driverId [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallUpdateDriverService]
GO
