
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SLX_CallInsertDriverService')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].SLX_CallInsertDriverService
END
GO

CREATE PROCEDURE [dbo].[SLX_CallInsertDriverService]
	@id [nvarchar](24),
	@code [nvarchar](24),
	@name [nvarchar](82),
	@cellPhone [nvarchar](24)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SLXIntegration].[SmartLogixIntegration.AscendSLXInt.SmartLogixWebServiceCaller].[CallInsertDriverService]
GO
