IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SmartLogix_ModifySalesOrder')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN
    DROP Procedure [dbo].[SmartLogix_ModifySalesOrder]
END
GO

CREATE PROCEDURE [dbo].[SmartLogix_ModifySalesOrder] 
(
  @sysTrxNo                NUMERIC(20,0),
  @companyId               varchar(8),
  @standardAcctId          int = null,
  @driverId                int = null,
  @toSiteId                int = null,
  @vehicleId               int = null,
  @vehicleType             int =  null,
  @orderDtTm               DATETIME,
  @inUserId                VARCHAR(32),
  @orderDeliveryDate       DATETIME,
  @PONumber		           VARCHAR(24), 
  @Status			       VARCHAR(24) = null,
  @outMessage              VARCHAR(1000) OUTPUT -- to return messages to UI
)
AS
BEGIN
    DECLARE @PriceExpiresDtTm datetime;
    DECLARE @DefPromisedDtTm datetime;
    DECLARE @OrderNo           VARCHAR(24);
    DECLARE @CurrentItemNumber INTEGER
    DECLARE @CurrentComponentNumber INTEGER
    DECLARE @MaxItemNumber     INTEGER
    DECLARE @MaxComponentNumber INTEGER
    DECLARE @TerminalID        INTEGER
    DECLARE @ProductID         INTEGER
    DECLARE @Quantity          NUMERIC(17, 5)
    DECLARE @OrderDate         DATETIME
    DECLARE @Price             DECIMAL(17,7)
       
    DECLARE @TankID            INTEGER
    DECLARE @CarrierID         INTEGER
    declare @BoughtAsTerminalID int
    declare @BoughtAsProductID int 
    declare @BoughtAsQuantity numeric(17,5)
    declare @BoughtAsOrderDate datetime
    declare @BoughtAsPrice numeric(17,7)
    DECLARE @BoughtAsComponentNumber INTEGER
    DECLARE @ProductCode VARCHAR(24)
    DECLARE @BlendRecipeId INTEGER = NULL
    DECLARE @OrderStatusID INTEGER = 1001
	
	DECLARE @ErrorOccurred bit = 0      
	DECLARE @ErrorMessage NVARCHAR(4000);      
	DECLARE @ErrorSeverity INT;      
	DECLARE @ErrorState INT;    

Begin Transaction SmartLogix_ModifySalesOrder
	Begin Try
		--Delete all previous order items
		 DELETE FROM OrderItem WHERE SysTrxNo = @SysTrxNo 

		Select top 1 @companyId = Company_Id from company order by Company_Id
		SELECT
		  @CurrentItemNumber = 1,
		  @MaxItemNumber = COUNT(*)
		FROM dbo.OrderItem
		WHERE SysTrxNo = @sysTrxNo

		WHILE @CurrentItemNumber <= @MaxItemNumber
		BEGIN
			Execute dbo.OrderDeleteItem @sysTrxNo, @CurrentItemNumber, 'SmartLogix'
		END

		IF @vehicleId is not null
		BEGIN
		select @CarrierID = CarrierID FROM Vehicle where VehicleID = @vehicleID
		END

		IF @Status is not null
		BEGIN
			select top 1 @OrderStatusID = OrderStatusID from OrderOEStatus where Code = @Status
		END
		ELSE
		BEGIN
			select top 1 @OrderStatusID = OrderStatusID from OrderOEStatus where Code = 'Open'
		END

		-- Update OrderHdr values
		UPDATE OrderHdr
		SET   CompanyID = @companyId, 
			  DefCarrierID = @CarrierID, 
			  UserID = @inUserID, 
			  DefVehicleID = @vehicleID, 
			  DefDriverID = @driverID,  
			  ToSiteID = @toSiteID, 
			  LastStatusDate = GetDate(),
			  LastModifiedUser = @inUserID,
			  LastModifiedDtTm = GetDate(),
			  DefPromisedDtTm = @orderDeliveryDate,
			  OrderStatusID = @OrderStatusID,
			  PONo = @PONumber  
		WHERE SysTrxNo = @sysTrxNo

		SELECT @PONumber = OrderHdr.PONo FROM  OrderHdr WHERE OrderHdr.SysTrxNo = @SysTrxNo AND OrderHdr.PONo is null 

		IF (@PONumber is null) 
		BEGIN
			SELECT @StandardAcctID = ST.StandardAcctID
			FROM dbo.OrderHdr as OHD
			INNER JOIN dbo.MasterSite ms ON MS.MasterSiteID = OHD.ToSiteID
			INNER JOIN dbo.ARShipTo ST ON ST.ShiptoID = ms.ShipToID
			WHERE OHD.SysTrxNo = @SysTrxNo

			SELECT @PONumber = D.PONumber FROM dbo.Dest D
			JOIN dbo.ARShipTo S ON S.DestinationID = D.DestID 
			WHERE S.StandardAcctID = @StandardAcctID and S.ShipToID = @ToSiteID
        
			IF @PONumber is null
			BEGIN
				SELECT @PONumber = D.PONumber FROM dbo.Dest D
				JOIN dbo.ARShipTo S ON S.DestinationID = D.DestID 
				WHERE S.StandardAcctID = @StandardAcctID and S.PrimaryAcct = 'Y'
			END

			UPDATE OrderHdr SET PONo = @PONumber WHERE SysTrxNo = @SysTrxNo	
		END 

		SELECT
		  @CurrentItemNumber = 1,
		  @MaxItemNumber = COUNT(*)
		FROM dbo.SmartLogixProduct
		WHERE SysTrxNo = @sysTrxNo and ComponentNumber = 0                
   
		WHILE @CurrentItemNumber <= @MaxItemNumber
		BEGIN
			-- Grab the next product
			SELECT
				@TerminalID = SmartLogixProduct.TerminalID,
				@ProductID = SmartLogixProduct.ProductID,
				@Quantity = SmartLogixProduct.Quantity,
				@OrderDate = SmartLogixProduct.OrderDate, 
				@Price = SmartLogixProduct.Price,
				@TankID = SmartLogixProduct.TankID,
				@ProductCode = SmartLogixProduct.ProductCode
			FROM SmartLogixProduct
			WHERE SysTrxNo = @sysTrxNo and ItemNumber = @CurrentItemNumber and ComponentNumber = 0

			IF @Price = 0
			BEGIN
			SET @Price = NULL;
			END

			if (@TankID is not NULL)
			BEGIN
				EXECUTE dbo.OrderAddItem @SysTrxNo, @TerminalID, @ProductID, @Quantity, @inUserID, NULL, @CurrentItemNumber, DEFAULT, NULL, NULL, @Price, NULL, NULL, 'N' , DEFAULT, DEFAULT, DEFAULT, 'Y', DEFAULT, DEFAULT, DEFAULT, @TankID, DEFAULT
			END
			ELSE
			BEGIN
				EXECUTE dbo.OrderAddItem @SysTrxNo, @TerminalID, @ProductID, @Quantity, @inUserID, NULL, @CurrentItemNumber, DEFAULT, NULL, NULL, @Price, NULL, NULL, 'N' , DEFAULT, DEFAULT, DEFAULT, 'Y', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT
			END
             
			SELECT
			  @CurrentComponentNumber = 1,
			  @MaxComponentNumber = COUNT(*)
			FROM dbo.SmartLogixProduct
			WHERE SysTrxNo = @sysTrxNo and ItemNumber = @CurrentItemNumber and ComponentNumber != 0                     
   
			IF (@MaxComponentNumber = 0)
			BEGIN
				-- If it's a Purchase Site need to grab the Purchase Alias
				IF (SELECT Type FROM dbo.MasterSite WHERE MasterSiteID = @TerminalID) = 'P'  
				BEGIN
				  SELECT TOP 1 @ProductID = PurchAlias.PurchAliasID
				  FROM dbo.PurchAlias
				  JOIN dbo.ProdCont ON ProdCont.ProdContID = PurchAlias.ProdContID
				  WHERE (dbo.GetProdContID(@ProductID) = ProdCont.ProdContID)
				  AND EXISTS (SELECT 1 FROM dbo.PurchRack (NOLOCK)
							  JOIN dbo.MasterSite ON MasterSite.SupplierSupplyPtID = PurchRack.SupplierSupplyPtID
							  WHERE MasterSite.MasterSiteID = @TerminalID)
				END
				ELSE
				BEGIN
					SET @ProductID = (select top 1 PurchAliasID FROM PurchAlias where ProdContID = dbo.GetProdContID(@ProductID))
				END 

				IF @ProductID is not null
				BEGIN
					EXECUTE dbo.OrderAddComp @SysTrxNo, @CurrentItemNumber, 1, @ProductID, @BoughtAsTerminalID, @Quantity, NULL, @CarrierID, @vehicleId, 1, DEFAULT, DEFAULT, DEFAULT, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT
				END
			END
			ELSE
			BEGIN
				WHILE @CurrentComponentNumber <= @MaxComponentNumber
				BEGIN
					-- Grab the next product
					SELECT
						@BoughtAsTerminalID = SmartLogixProduct.TerminalID,
						@BoughtAsProductID = SmartLogixProduct.ProductID,
						@BoughtAsQuantity = SmartLogixProduct.Quantity,
						@BoughtAsOrderDate = SmartLogixProduct.OrderDate, 
						@BoughtAsPrice = SmartLogixProduct.Price,
						@BoughtAsComponentNumber = SmartLogixProduct.ComponentNumber
					FROM SmartLogixProduct
					WHERE SysTrxNo = @sysTrxNo and ItemNumber = @CurrentItemNumber and ComponentNumber = @CurrentComponentNumber
            
					DECLARE @blendPrct decimal;
					SET @blendPrct = @BoughtAsQuantity / @Quantity
                  
					EXECUTE dbo.OrderAddComp @SysTrxNo, @CurrentItemNumber, @BoughtAsComponentNumber, @BoughtAsProductID, @BoughtAsTerminalID, @BoughtAsQuantity, NULL, @CarrierID, @vehicleId, @blendPrct, DEFAULT, DEFAULT, DEFAULT, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT 		 
            
					SET @CurrentComponentNumber = @CurrentComponentNumber + 1;
				END
			END

			EXECUTE dbo.OrderCalcPrice @SysTrxNo, @CurrentItemNumber 	
			EXECUTE dbo.OrderCalcTaxes @SysTrxNo, @CurrentItemNumber, DEFAULT
			EXECUTE dbo.RecalculateFreight @SysTrxNo, @CurrentItemNumber

			SET @CurrentItemNumber = @CurrentItemNumber + 1;		
		END 
    
		EXECUTE dbo.UpdateFreightDetails @SysTrxNo 
		EXECUTE dbo.OrderCalc @SysTrxNo
		-- Get the credit release value because if Credit Release is Y we do not want to check the credit on the order
		DECLARE @CreditRelease char(1)
		select @CreditRelease = CreditRelease FROM OrderHdr WHERE SysTrxNo = @SysTrxNo

		IF @CreditRelease = 'N'
		BEGIN
			EXECUTE dbo.DXImport_OEOpenOrder_CreditCheck @SysTrxNo, @companyId  
		END    
		commit transaction SmartLogix_ModifySalesOrder  	
	End Try
	Begin Catch
        set @Status = ERROR_MESSAGE()       
            SELECT       
	            @ErrorMessage = ERROR_MESSAGE(),      
                @ErrorSeverity = ERROR_SEVERITY(),      
                @ErrorState = ERROR_STATE();      
        rollback transaction SmartLogix_ModifySalesOrder      
        SET @ErrorOccurred = 1      
    End Catch		 	

	IF @ErrorOccurred = 1      
    BEGIN      
        RAISERROR (@ErrorMessage, -- Message text.      
                   @ErrorSeverity, -- Severity.      
                   @ErrorState -- State.      
                   );      
	END

END

GO