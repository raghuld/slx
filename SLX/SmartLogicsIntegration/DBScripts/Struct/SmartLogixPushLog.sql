IF dbo.TableExists('dbo', 'SmartLogixPushLog', DEFAULT) = 'N'
CREATE TABLE [dbo].SmartLogixPushLog(
    [LogID] uniqueidentifier NOT NULL,
    [LogDate] [datetime] NOT NULL,
    [AscendId] [varchar](24) NULL,
    [MasterDataId] [varchar](50) NOT NULL,
    [MasterDataType] [varchar](50) NOT NULL,
    [Status] [varchar](50) NOT NULL,
    [Details] varchar(max) NULL,
    [Request] varchar(max) NULL,
    [RequestRecieved] varchar(max) NULL,
    [StackTrace] [varchar](max) NULL,
    [IsError] bit NULL,
 CONSTRAINT [PK_SmartLogixPushLog] PRIMARY KEY CLUSTERED 
(
    [LogID] desc
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] 
GO

ALTER TABLE SmartLogixPushLog ALTER COLUMN MasterDataId varchar(50) null
GO