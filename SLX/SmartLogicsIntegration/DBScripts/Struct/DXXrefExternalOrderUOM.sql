IF dbo.TableExists('dbo', 'DXXrefExternalOrderUOM', DEFAULT) = 'N' 
BEGIN
  CREATE TABLE dbo.DXXrefExternalOrderUOM
  ( 
    DXXrefFormatID			INTEGER NOT NULL,
    DXCode					VARCHAR(50) NOT NULL,
    MasterProdID			INTEGER NOT NULL,
    Conversion              NUMERIC(14,7),
    ClientID				INTEGER NOT NULL CONSTRAINT DF_ClientID_DXXrefExternalOrderUOM DEFAULT dbo.GetClientID(),
    CONSTRAINT PK_DXXrefExternalOrderUOM PRIMARY KEY (DXXrefFormatID, DXCode, MasterProdID)
  )
END  