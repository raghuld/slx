IF dbo.TableExists('dbo', 'SmartLogixPriceUpdateRequired', DEFAULT) = 'N'
CREATE TABLE SmartLogixPriceUpdateRequired
(
    SysTrxNo numeric(20,0) not null,
    SysTrxLine int not null,
    CONSTRAINT PK_SmartLogixPriceUpdateRequired PRIMARY KEY (SysTrxNo, SysTrxLine)
)
GO