IF dbo.TableExists('dbo', 'SmartLogixSettings', DEFAULT) = 'N' 
BEGIN
  CREATE TABLE dbo.SmartLogixSettings
  ( 
    Name			Varchar(50) NOT NULL,
    Value					VARCHAR(50) NULL,
    CONSTRAINT PK_SmartLogixSettings PRIMARY KEY (Name)
  )
END 