    
IF dbo.TableExists('dbo', 'SmartLogixOrders', DEFAULT) = 'N'						
CREATE TABLE [dbo].[SmartLogixOrders] (
    SysTrxNo numeric(20,0) not null,
    CONSTRAINT PK_SmartLogixOrders PRIMARY KEY (SysTrxNo),
    CONSTRAINT FK_SmartLogixOrders_OrderHdr FOREIGN KEY (SysTrxNo) REFERENCES OrderHdr(SysTrxNo)
)	
                                                          
