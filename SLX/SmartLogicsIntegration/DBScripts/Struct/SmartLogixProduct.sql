IF dbo.TableExists('dbo', 'SmartLogixProduct', DEFAULT) = 'Y'
BEGIN
	DROP TABLE SmartLogixProduct
END
GO

CREATE TABLE SmartLogixProduct
(
    SysTrxNo numeric(20,0) not null,
    ItemNumber int not null default 0,
    ComponentNumber int not null default 0,
    BOLNumber varchar(50) not null default '',
    TerminalID int null,
    ProductID int not null,
    ProductCode varchar(50) not null,
    Quantity  numeric(17,5) not null default 0,
    NetQuantity numeric(17,5) not null default 0,
    OrderDate datetime not null default current_timestamp,
    Price decimal(17,7) null,
    TankID int null,	
	DiversionDestState varchar(2) null,
    CONSTRAINT PK_SmartLogixProduct PRIMARY KEY (SysTrxNo, ItemNumber, ComponentNumber, BOLNumber)
)
GO