IF dbo.TableExists('dbo', 'SmartLogix_ProcessOrder_Processing', DEFAULT) = 'N'
CREATE TABLE [dbo].[SmartLogix_ProcessOrder_Processing](
    [LogID] [bigint] IDENTITY(1,1) NOT NULL,
    [LogDate] [datetime] NOT NULL,
    [SysTrxNo] [varchar](50) NOT NULL,
    [SysTrxLine] INTEGER NOT NULL,
 CONSTRAINT [PK_SmartLogix_ProcessOrders_Processing] PRIMARY KEY CLUSTERED 
(
    [LogID] desc,
    [LogDate] desc,
    [SysTrxNo] ASC,
    [SysTrxLine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO