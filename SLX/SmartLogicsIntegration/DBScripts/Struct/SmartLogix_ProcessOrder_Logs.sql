IF dbo.TableExists('dbo', 'SmartLogix_ProcessOrder_Logs', DEFAULT) = 'N'
CREATE TABLE [dbo].[SmartLogix_ProcessOrder_Logs](
    [LogID] [bigint] IDENTITY(1,1) NOT NULL,
    [LogDate] [datetime] NOT NULL,
    SysTrxNo NUMERIC(20,0) NOT NULL,
    [Source] [varchar](50) NOT NULL,
    [Status] [varchar](50) NOT NULL,
    SysTrxLine				 int NULL,
    CompanyId               varchar(8) NULL,
    BolNo                   varchar(50) NULL,
    GrossQty                numeric(17,5) NULL, 
    NetQty                  numeric(17,5) NULL,
    BolCreatedDateTime      datetime NULL,
    TerminalId              int NULL
 CONSTRAINT [PK_SmartLogix_ProcessOrders_Logs] PRIMARY KEY CLUSTERED 
(
    [LogID] desc,
    [LogDate] desc,
    [SysTrxNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] 
GO