IF dbo.TableExists('dbo', 'SmartLogixErrorLog', DEFAULT) = 'N'
CREATE TABLE [dbo].SmartLogixErrorLog(
    [LogID] uniqueidentifier NOT NULL,
    [LogDate] [datetime] NOT NULL,
    [Message] [varchar](max) NOT NULL,
    [Details] varchar(max) NULL,
    [StackTrace] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SmartLogixErrorLog] PRIMARY KEY CLUSTERED 
(
    [LogID] desc
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] 

GO